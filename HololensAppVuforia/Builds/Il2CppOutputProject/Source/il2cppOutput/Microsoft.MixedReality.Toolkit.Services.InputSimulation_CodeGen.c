﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Examples.InputSimulationHelpGuide::Start()
extern void InputSimulationHelpGuide_Start_m475D23A76644A5672FF24FB4EDB04FAC31992C75 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Examples.InputSimulationHelpGuide::Update()
extern void InputSimulationHelpGuide_Update_m4827FBD5BA077C8598B20A9A8E0787418DAA949D (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Examples.InputSimulationHelpGuide::.ctor()
extern void InputSimulationHelpGuide__ctor_m7A8BB53944392590E7D584F8000E7C40CC6A709D (void);
// 0x00000004 Microsoft.MixedReality.Toolkit.Input.IMixedRealityController[] Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetActiveControllers()
extern void BaseInputSimulationService_GetActiveControllers_mF2C747162AA7163685B3FAA4ACF31A905F47F271 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void BaseInputSimulationService__ctor_m85F8191D8511AF069ADA87DBE8D78C9BFFE9B764 (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void BaseInputSimulationService__ctor_m064170922D0E2DFDE990B5C79B3BFDFDE6B0EC14 (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateControllerDevice(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Object)
extern void BaseInputSimulationService_UpdateControllerDevice_mFFB0DC32268DF3ACD93A6ACB48952FB158D3E223 (void);
// 0x00000008 Microsoft.MixedReality.Toolkit.Input.BaseController Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_GetControllerDevice_mB7259909FFC45776FD94AEC20D2FEE4D9FE9FFBF (void);
// 0x00000009 Microsoft.MixedReality.Toolkit.Input.BaseController Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetOrAddControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void BaseInputSimulationService_GetOrAddControllerDevice_m5DB8C6ED98633335D0326B034372000D1D540E9F (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_RemoveControllerDevice_m13E89A8C09037EF57187F2CE6F8EE851FC5C04D6 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveAllControllerDevices()
extern void BaseInputSimulationService_RemoveAllControllerDevices_mF9A6C46723EA6668367C422CB905DE90934BC72D (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateActiveControllers()
extern void BaseInputSimulationService_UpdateActiveControllers_m3A7CD05A5E317FEE8779541575C0BA0633AE7A76 (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateHandDevice(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void BaseInputSimulationService_UpdateHandDevice_mA9B830A183C7808F0514325287FF4E07E456ACB9 (void);
// 0x0000000E Microsoft.MixedReality.Toolkit.Input.SimulatedHand Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_GetHandDevice_m19FAEDB37CF5335D58F0CED05E63BDFF5541CB63 (void);
// 0x0000000F Microsoft.MixedReality.Toolkit.Input.SimulatedHand Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetOrAddHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void BaseInputSimulationService_GetOrAddHandDevice_m5BC2E9478A6C57B3E1A642F3E70A638623BE6FE8 (void);
// 0x00000010 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_RemoveHandDevice_m456384E54E1CE46C9FE79E62BD521ACE2B902A18 (void);
// 0x00000011 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveAllHandDevices()
extern void BaseInputSimulationService_RemoveAllHandDevices_m59A8A8B8DA7C43183728D418893DC6EC9613A291 (void);
// 0x00000012 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_InputSimulationProfile()
// 0x00000013 Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_EyeGazeSimulationMode()
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_EyeGazeSimulationMode(Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode)
// 0x00000015 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerSimulationMode()
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerSimulationMode(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
// 0x00000017 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandDataLeft()
// 0x00000018 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandDataRight()
// 0x00000019 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_MotionControllerDataLeft()
// 0x0000001A Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_MotionControllerDataRight()
// 0x0000001B System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_UserInputEnabled()
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_UserInputEnabled(System.Boolean)
// 0x0000001D System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingControllerLeft()
// 0x0000001E System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingControllerRight()
// 0x0000001F System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleControllerLeft()
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleControllerLeft(System.Boolean)
// 0x00000021 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleControllerRight()
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleControllerRight(System.Boolean)
// 0x00000023 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerPositionLeft()
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerPositionLeft(UnityEngine.Vector3)
// 0x00000025 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerPositionRight()
// 0x00000026 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerPositionRight(UnityEngine.Vector3)
// 0x00000027 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerRotationLeft()
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerRotationLeft(UnityEngine.Vector3)
// 0x00000029 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerRotationRight()
// 0x0000002A System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerRotationRight(UnityEngine.Vector3)
// 0x0000002B System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetControllerLeft()
// 0x0000002C System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetControllerRight()
// 0x0000002D Microsoft.MixedReality.Toolkit.Input.HandSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandSimulationMode()
// 0x0000002E System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandSimulationMode(Microsoft.MixedReality.Toolkit.Input.HandSimulationMode)
// 0x0000002F System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingHandLeft()
// 0x00000030 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingHandRight()
// 0x00000031 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleHandLeft()
// 0x00000032 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleHandLeft(System.Boolean)
// 0x00000033 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleHandRight()
// 0x00000034 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleHandRight(System.Boolean)
// 0x00000035 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandPositionLeft()
// 0x00000036 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandPositionLeft(UnityEngine.Vector3)
// 0x00000037 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandPositionRight()
// 0x00000038 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandPositionRight(UnityEngine.Vector3)
// 0x00000039 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandRotationLeft()
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandRotationLeft(UnityEngine.Vector3)
// 0x0000003B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandRotationRight()
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandRotationRight(UnityEngine.Vector3)
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetHandLeft()
// 0x0000003E System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetHandRight()
// 0x0000003F Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_Animation()
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::set_Animation(Microsoft.MixedReality.Toolkit.Input.InputAnimation)
// 0x00000041 System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_IsPlaying()
// 0x00000042 System.Single Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_LocalTime()
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::set_LocalTime(System.Single)
// 0x00000044 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Play()
// 0x00000045 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Stop()
// 0x00000046 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Pause()
// 0x00000047 System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::LoadInputAnimation(System.String)
// 0x00000048 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_HandDataLeft()
extern void InputPlaybackService_get_HandDataLeft_m84B178B9A58B7210C43A7C7236F32B5A315A5711 (void);
// 0x00000049 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_HandDataRight()
extern void InputPlaybackService_get_HandDataRight_mDD7220C2A41B148F35D3507D343182ED25487C02 (void);
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputPlaybackService__ctor_m12F6326FCAAF459F37C614A6B5D04AB45C836451 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputPlaybackService__ctor_m197A16D87DB4C61DED00B0FAD1417B05493B51AF (void);
// 0x0000004C System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void InputPlaybackService_CheckCapability_mE6BC4ACBF32E7F3B780EDB26C3AB3A3FB327A768 (void);
// 0x0000004D Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_Animation()
extern void InputPlaybackService_get_Animation_m4FEEABABE429ECE8023A2407F1F811F691FF4DF9 (void);
// 0x0000004E System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::set_Animation(Microsoft.MixedReality.Toolkit.Input.InputAnimation)
extern void InputPlaybackService_set_Animation_m06C3761D40C27537DE5225AC425EC1189BFBC3F2 (void);
// 0x0000004F System.Single Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_Duration()
extern void InputPlaybackService_get_Duration_mA495B0C2583F9A743770E55069C07010F95F4AE6 (void);
// 0x00000050 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_IsPlaying()
extern void InputPlaybackService_get_IsPlaying_m7E5D50CF258830BF3A9483ADA521D225A6E9BDFA (void);
// 0x00000051 System.Single Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_LocalTime()
extern void InputPlaybackService_get_LocalTime_m6738B6E1060DBFD07C01965DB911B19B828075F2 (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::set_LocalTime(System.Single)
extern void InputPlaybackService_set_LocalTime_mE3CB0B7AF06AC479B0B935D8054D9EE5DBCEB61E (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Play()
extern void InputPlaybackService_Play_m254C2F7E3DA591DFBBCD08EE99844E3137EF66B2 (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Stop()
extern void InputPlaybackService_Stop_m6BE0A0CB07CE43F891CECD64D4E241CFB4E893A3 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Pause()
extern void InputPlaybackService_Pause_m73982C1B88BA07066EC6A9970ADC5BC07E002891 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Update()
extern void InputPlaybackService_Update_m68ACD2512F93D170C48DA7F983980E2CB9582F95 (void);
// 0x00000057 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::SetPlaying(System.Boolean)
extern void InputPlaybackService_SetPlaying_m197E71420EC3D24BED0CF4B222271147AE79C770 (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Evaluate()
extern void InputPlaybackService_Evaluate_m3DA25CD6A4C5B15ABA60A5DC0B7AF6A94C600AEA (void);
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::EvaluateHandData(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void InputPlaybackService_EvaluateHandData_m4237F623E0B98F0883751BE03995305179A4903A (void);
// 0x0000005A System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::LoadInputAnimation(System.String)
extern void InputPlaybackService_LoadInputAnimation_m222F6A84CF1D8C2DC9ADBD5C70ED1AC7512E7704 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::.cctor()
extern void InputPlaybackService__cctor_mB21D1F0ECB88CDC6CEDE2466014117F4F383A2C7 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m781FEF0197866BE561E9FA465EDCB66899023664 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<>c__DisplayClass29_0::<EvaluateHandData>b__0(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void U3CU3Ec__DisplayClass29_0_U3CEvaluateHandDataU3Eb__0_mB2BCB396F2FACD240A54DD2F48A53835CC070792 (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Input.MouseDelta::Reset()
extern void MouseDelta_Reset_m0AEA879CA11D8CCE6D6421CCC6FD94045026162A (void);
// 0x0000005F System.Void Microsoft.MixedReality.Toolkit.Input.MouseDelta::.ctor()
extern void MouseDelta__ctor_m28C03A62F23F401F2C5297C644D878F379F3536D (void);
// 0x00000060 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerSimulationMode()
extern void InputSimulationService_get_ControllerSimulationMode_m5C2BB99A87CF427718C93AFADD5014C77E50650C (void);
// 0x00000061 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerSimulationMode(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void InputSimulationService_set_ControllerSimulationMode_m9A00A586B8B5DA4C9DECC85C46D033FD96A723E4 (void);
// 0x00000062 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataLeft()
extern void InputSimulationService_get_HandDataLeft_m98B0BD5D16C143A60CE897776A3BD80421CA4F8A (void);
// 0x00000063 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataRight()
extern void InputSimulationService_get_HandDataRight_m8DBFB8C070C84935B56366331CE068E3DF1F04B0 (void);
// 0x00000064 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataGaze()
extern void InputSimulationService_get_HandDataGaze_mF09C4A235A5954C7BD9D40F4CF24230534242861 (void);
// 0x00000065 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_MotionControllerDataLeft()
extern void InputSimulationService_get_MotionControllerDataLeft_m9ED5154B1B8879A8B389139AFE614354F85EFEFB (void);
// 0x00000066 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_MotionControllerDataRight()
extern void InputSimulationService_get_MotionControllerDataRight_m9079E5F1EC6D4562D556E6478863DD981B89538E (void);
// 0x00000067 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingControllerLeft()
extern void InputSimulationService_get_IsSimulatingControllerLeft_m140240A942EFE9D9E953B13AB78DC2E1EA869B99 (void);
// 0x00000068 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingControllerRight()
extern void InputSimulationService_get_IsSimulatingControllerRight_m7DC2E6087E5DDAEF49B14C1D1215A1DDA530CAA6 (void);
// 0x00000069 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleControllerLeft()
extern void InputSimulationService_get_IsAlwaysVisibleControllerLeft_mA1EC5E8D5E6D7C27D4367F4B7403B3A4A3D67265 (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleControllerLeft(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleControllerLeft_mE008C1FB8CB2466C82357AC5B3ABA6912C22FB20 (void);
// 0x0000006B System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleControllerRight()
extern void InputSimulationService_get_IsAlwaysVisibleControllerRight_m29AA450588031AED3CA89E88B199AC2753F7D4B8 (void);
// 0x0000006C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleControllerRight(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleControllerRight_m53069AE80CE41A3167A4D02AA313493BB00C77B2 (void);
// 0x0000006D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerPositionLeft()
extern void InputSimulationService_get_ControllerPositionLeft_m3A88C8308D00998CE5014A30D3272B829ECD9E1C (void);
// 0x0000006E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerPositionLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerPositionLeft_mD63860DDB9C5B67260933CFC89935B449100F748 (void);
// 0x0000006F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerPositionRight()
extern void InputSimulationService_get_ControllerPositionRight_m549BBFDB881C36C1B2C75064045B6FF44267DD52 (void);
// 0x00000070 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerPositionRight(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerPositionRight_m97D54623C77669DDC3BC30647B94756B1C4670E4 (void);
// 0x00000071 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerRotationLeft()
extern void InputSimulationService_get_ControllerRotationLeft_mA22D1BA8514159CD44C9D5B5BC004B69B46516A1 (void);
// 0x00000072 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerRotationLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerRotationLeft_m3C68959F080B755DA57D343C1C4C0FCB36ED2462 (void);
// 0x00000073 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerRotationRight()
extern void InputSimulationService_get_ControllerRotationRight_m7A2DD07B5A831AC7610DC3DAC60CEC27ADEF97FE (void);
// 0x00000074 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerRotationRight(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerRotationRight_mCEF83C015F5DEEA7A93144FD6D4DC291E9048927 (void);
// 0x00000075 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetControllerLeft()
extern void InputSimulationService_ResetControllerLeft_m51C82FFF6F2818A1D428946D650A8459666DDE3F (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetControllerRight()
extern void InputSimulationService_ResetControllerRight_m2B80BF8F9DBDF17A629A360E372F806B37D0647D (void);
// 0x00000077 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_SimulateEyePosition()
extern void InputSimulationService_get_SimulateEyePosition_m40BD0C6709D6228F1237DA4E3C85FD78978C662B (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_SimulateEyePosition(System.Boolean)
extern void InputSimulationService_set_SimulateEyePosition_m63DCFD8AA884675583BEA3AE0C2CB455E3888C0A (void);
// 0x00000079 Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_EyeGazeSimulationMode()
extern void InputSimulationService_get_EyeGazeSimulationMode_m5BE2C0C42FD96604390D433B86E8CC7A13D29F30 (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_EyeGazeSimulationMode(Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode)
extern void InputSimulationService_set_EyeGazeSimulationMode_m2892153046A23A7B9A27A9E8688BDAF3BE3631DE (void);
// 0x0000007B System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_UserInputEnabled()
extern void InputSimulationService_get_UserInputEnabled_m6D63571F3821DB7485C628876E3E844BFE71EC5C (void);
// 0x0000007C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_UserInputEnabled(System.Boolean)
extern void InputSimulationService_set_UserInputEnabled_mE857BC356F694BE2A5E2F11D7D1883CBB800D89D (void);
// 0x0000007D System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputSimulationService__ctor_mEB564D75663C0501B3AC27B8BD5B7EE55F9768ED (void);
// 0x0000007E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputSimulationService__ctor_m5A3710F1EB46D0040F0F222DAD33A98DA9152584 (void);
// 0x0000007F System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void InputSimulationService_CheckCapability_m96473FE081D388EA62FAE70A281E8ACCA7E6C268 (void);
// 0x00000080 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Initialize()
extern void InputSimulationService_Initialize_mD50874D0B717748BCA596E1B054D50AF599A391F (void);
// 0x00000081 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Destroy()
extern void InputSimulationService_Destroy_mE346FF8815BBF076E30E294873FD6B6C0C65A349 (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Enable()
extern void InputSimulationService_Enable_mAD1952BC2B055623356DF6475386007F802BF85C (void);
// 0x00000083 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Disable()
extern void InputSimulationService_Disable_mD92589DC3819D3462F1F06A9FEB864C602F9F84F (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Update()
extern void InputSimulationService_Update_m7229E7271E0B770B520411287231EE9DC7035C2E (void);
// 0x00000085 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::LateUpdate()
extern void InputSimulationService_LateUpdate_m6C95257157FA234363CDD509B6CF1F776068D3EF (void);
// 0x00000086 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_InputSimulationProfile()
extern void InputSimulationService_get_InputSimulationProfile_m3BA9686028141B76C8EC24BB81575A9B8079EA46 (void);
// 0x00000087 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_InputSimulationProfile(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void InputSimulationService_set_InputSimulationProfile_m1EB8A508743BC2D2C61B8FC4F15966D438709CC9 (void);
// 0x00000088 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.get_SaccadeProvider()
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SaccadeProvider_m5242992A09AF642B57754EBA46D2BAF74663B9B3 (void);
// 0x00000089 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.get_SmoothEyeTracking()
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SmoothEyeTracking_m85838FAF50D3898C7EF4CF565881E1C613DE8433 (void);
// 0x0000008A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.set_SmoothEyeTracking(System.Boolean)
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_set_SmoothEyeTracking_mD05F38EA97F499C4C55493A20ABAA27914D05110 (void);
// 0x0000008B System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableCameraControl()
extern void InputSimulationService_EnableCameraControl_mB3008B5BC7DC66F0A287C6BDC1161D0DCCBD7D6D (void);
// 0x0000008C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::DisableCameraControl()
extern void InputSimulationService_DisableCameraControl_m72D2FE758D4BBDDCAF070C7DFF8DB8830A2B2634 (void);
// 0x0000008D System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableHandSimulation()
extern void InputSimulationService_EnableHandSimulation_m0C764704AD1B9D1F11B0390369B0204C610435D2 (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableMotionControllerSimulation()
extern void InputSimulationService_EnableMotionControllerSimulation_mAA79A753F0F5DB1911C90844131BB4F64FB56D8F (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::DisableControllerSimulation()
extern void InputSimulationService_DisableControllerSimulation_m81D1CBCE97E029319CB68353F9D5C525A33BCD59 (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetMouseDelta()
extern void InputSimulationService_ResetMouseDelta_mC5931363ACD613134D328073437059A0E10EDB66 (void);
// 0x00000091 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::UpdateMouseDelta()
extern void InputSimulationService_UpdateMouseDelta_m28D4BF91691EF0F1A1A2076692653ED3F921EE94 (void);
// 0x00000092 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ScreenToWorld(UnityEngine.Vector3)
extern void InputSimulationService_ScreenToWorld_m03047E7359DDFA55700C57BBC07168529A161208 (void);
// 0x00000093 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::WorldToScreen(UnityEngine.Vector2)
extern void InputSimulationService_WorldToScreen_m8130A54068206AB76FECD02D7696BFE74A77D907 (void);
// 0x00000094 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::WorldToViewport(UnityEngine.Vector2)
extern void InputSimulationService_WorldToViewport_m251E12624E64A31AB829974FC4FA6354C4003E1C (void);
// 0x00000095 Microsoft.MixedReality.Toolkit.Input.HandSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandSimulationMode()
extern void InputSimulationService_get_HandSimulationMode_mD980C86DBF185DDD04F3E4317E41398C62FF3BFF (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandSimulationMode(Microsoft.MixedReality.Toolkit.Input.HandSimulationMode)
extern void InputSimulationService_set_HandSimulationMode_mEFBCF4330009D2E3D93A16AA1DFEFCD044187BFF (void);
// 0x00000097 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingHandLeft()
extern void InputSimulationService_get_IsSimulatingHandLeft_m2EF6AF08EDC24EE783E853087A45B8857179BD52 (void);
// 0x00000098 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingHandRight()
extern void InputSimulationService_get_IsSimulatingHandRight_mC7F71CC417973EE053118F762128B00BC3F054E3 (void);
// 0x00000099 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleHandLeft()
extern void InputSimulationService_get_IsAlwaysVisibleHandLeft_mAA29EB526F329E0CAB047A3D64BF9DC11C7ED3F9 (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleHandLeft(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleHandLeft_m3A3DD8B53DC009A6FEA981E59612EC0AAB7D95AE (void);
// 0x0000009B System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleHandRight()
extern void InputSimulationService_get_IsAlwaysVisibleHandRight_m799817502E4A06157EC9935C7B6A2DA6C09BB9E4 (void);
// 0x0000009C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleHandRight(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleHandRight_mA9FFB5CB0848C4E3791D0FEA87185636AA768EB5 (void);
// 0x0000009D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandPositionLeft()
extern void InputSimulationService_get_HandPositionLeft_m199BB1F741E460485467E811D983167A860C38EE (void);
// 0x0000009E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandPositionLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_HandPositionLeft_mF541BC38C4CA9FEEE3BDAA4B46C12871EEE86DBA (void);
// 0x0000009F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandPositionRight()
extern void InputSimulationService_get_HandPositionRight_m53704AA4BEAAFEE0EBC2AE7D2F4A31305F961A79 (void);
// 0x000000A0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandPositionRight(UnityEngine.Vector3)
extern void InputSimulationService_set_HandPositionRight_m19FF605A0DF226BFDB561AB77C61CDB7043215B4 (void);
// 0x000000A1 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandRotationLeft()
extern void InputSimulationService_get_HandRotationLeft_m001A59786A486D00C97F0E633D76B88F39D0DED8 (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandRotationLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_HandRotationLeft_mE7C0C362D7DA0C94CBCFB3E8B74C44BB585C4E32 (void);
// 0x000000A3 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandRotationRight()
extern void InputSimulationService_get_HandRotationRight_m2E4C13B6DA382C98EDC93C0C84EB7ED1A641B64E (void);
// 0x000000A4 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandRotationRight(UnityEngine.Vector3)
extern void InputSimulationService_set_HandRotationRight_mAF2B18F0AD9EC629C539019F3C89C65F58939CD3 (void);
// 0x000000A5 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetHandLeft()
extern void InputSimulationService_ResetHandLeft_mAEEEF1FF233623898722AB90A8A78B8503D03F2F (void);
// 0x000000A6 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetHandRight()
extern void InputSimulationService_ResetHandRight_m1479E339384C357A7406167E0FDC1678BF296A36 (void);
// 0x000000A7 System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding::.cctor()
extern void KeyBinding__cctor_m0C3F262A778D96E16F8FC6EB1BDEF96C8D640315 (void);
// 0x000000A8 Microsoft.MixedReality.Toolkit.Input.KeyBinding/KeyType Microsoft.MixedReality.Toolkit.Input.KeyBinding::get_BindingType()
extern void KeyBinding_get_BindingType_m4865FD00DE59B37BAE922C5DFF37C39BB100CF5E (void);
// 0x000000A9 System.String Microsoft.MixedReality.Toolkit.Input.KeyBinding::ToString()
extern void KeyBinding_ToString_m19E5162244657C0CAF1CE542F4C695EA15F6C296 (void);
// 0x000000AA System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetKeyCode(UnityEngine.KeyCode&)
extern void KeyBinding_TryGetKeyCode_mC94C514E2FE9CFC8372A6BD671DFA68DF8336085 (void);
// 0x000000AB System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetMouseButton(System.Int32&)
extern void KeyBinding_TryGetMouseButton_m0726DFBCCF20C5726E68B5766CAC8F991ECA91D5 (void);
// 0x000000AC System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetMouseButton(Microsoft.MixedReality.Toolkit.Input.KeyBinding/MouseButton&)
extern void KeyBinding_TryGetMouseButton_m83C8B35064098A2CBFA7D634C7C4211C89935B99 (void);
// 0x000000AD Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::Unbound()
extern void KeyBinding_Unbound_m6A91AB82DA79FE7B6B045A7EC3384FF75798D1E6 (void);
// 0x000000AE Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromKey(UnityEngine.KeyCode)
extern void KeyBinding_FromKey_m6585166FD1FE2AF96428212F8EB6C119902E87A3 (void);
// 0x000000AF Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromMouseButton(System.Int32)
extern void KeyBinding_FromMouseButton_mFF74F0829CA9BD73D1FE8CCA59C3BF4F704435D2 (void);
// 0x000000B0 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromMouseButton(Microsoft.MixedReality.Toolkit.Input.KeyBinding/MouseButton)
extern void KeyBinding_FromMouseButton_m95365EEB21C9242EC716CDC2B7F785B60E210876 (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m3B4B239601AEA7ABB3B9B2C679813D68E5C01D6B (void);
// 0x000000B2 System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding/<>c__DisplayClass5_0::<.cctor>b__0(Microsoft.MixedReality.Toolkit.Input.KeyBinding/KeyType,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_cctorU3Eb__0_m988961A53043F6B6572DC32C2F713662FC5443A3 (void);
// 0x000000B3 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::get_SimulatingInput()
extern void KeyInputSystem_get_SimulatingInput_mF3CB8811D973827E2C83ECAA82F5F6E46F56EC95 (void);
// 0x000000B4 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::StartKeyInputStimulation()
extern void KeyInputSystem_StartKeyInputStimulation_m16EC395569C1DFF113C2E5F7328BF32BBCF1BDFA (void);
// 0x000000B5 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::StopKeyInputSimulation()
extern void KeyInputSystem_StopKeyInputSimulation_mCA54EA6107A8901AE525DE5C25DEA32331B389A5 (void);
// 0x000000B6 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::ResetKeyInputSimulation()
extern void KeyInputSystem_ResetKeyInputSimulation_mAA38947B5A4DDCD416BD7175A433A1F7659FA51F (void);
// 0x000000B7 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::PressKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_PressKey_mF5901FC758ABBEEB118CB05100DEEB144D33B047 (void);
// 0x000000B8 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::ReleaseKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_ReleaseKey_m3265592C4F467DCEB5A181EE17AC607B5BE3FFC2 (void);
// 0x000000B9 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::AdvanceSimulation()
extern void KeyInputSystem_AdvanceSimulation_mFA3DC653C807E268AE916981BAB816FC7E0B47DA (void);
// 0x000000BA System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKey_m6CBF61B319ECB14A403BE0CEB2B43D86C0080F3F (void);
// 0x000000BB System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKeyDown(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKeyDown_m6EECCE6FA9BEC21565289E40EFC7BCF91E5E2FA2 (void);
// 0x000000BC System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKeyUp(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKeyUp_m7E47BC465852FAB6253B990E0905CCF89A039EFB (void);
// 0x000000BD System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void ManualCameraControl__ctor_m3514F7F7A420CCD77AD286A805FB19CDA94F313B (void);
// 0x000000BE System.Single Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::InputCurve(System.Single)
extern void ManualCameraControl_InputCurve_mCE2F23BF1974155B5FC22C087BB17BED87112A62 (void);
// 0x000000BF System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::SetInitialTransform(UnityEngine.Transform)
extern void ManualCameraControl_SetInitialTransform_m19950240F3173F1FF9ADD9A6C328EC4BA0427251 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::UpdateTransform(UnityEngine.Transform,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void ManualCameraControl_UpdateTransform_m0FE2241819406CAA729621C5AD10B873190F1A2F (void);
// 0x000000C1 System.Single Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetKeyDir(System.String,System.String)
extern void ManualCameraControl_GetKeyDir_mABC2C9AAAEE2695100B4E08C421C4573202F2A69 (void);
// 0x000000C2 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetCameraControlTranslation(UnityEngine.Transform)
extern void ManualCameraControl_GetCameraControlTranslation_m35450DDF7810FD1A41E704CE8E5C3FBB3439465A (void);
// 0x000000C3 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetCameraControlRotation(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void ManualCameraControl_GetCameraControlRotation_mD52B1A6A77508F50D874E917BCF87875B822C7F3 (void);
// 0x000000C4 System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::.cctor()
extern void ManualCameraControl__cctor_m29898A871CA03C44B849640D8FA07517FDB0B18E (void);
// 0x000000C5 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IndicatorsPrefab()
extern void MixedRealityInputSimulationProfile_get_IndicatorsPrefab_m0D34BBBA318A5773011EA6254B3BEC1A44DBB0EF (void);
// 0x000000C6 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseRotationSensitivity()
extern void MixedRealityInputSimulationProfile_get_MouseRotationSensitivity_m2EE0E80F81ECB00732D8366B50CC28B28D518E9F (void);
// 0x000000C7 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseX()
extern void MixedRealityInputSimulationProfile_get_MouseX_m48F663A219D12611B54B03CA43CDF1C32BEDD2BE (void);
// 0x000000C8 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseY()
extern void MixedRealityInputSimulationProfile_get_MouseY_m4E9685C10A13F2A03C08DE4E4A24741BBD531C89 (void);
// 0x000000C9 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseScroll()
extern void MixedRealityInputSimulationProfile_get_MouseScroll_m98A4BDE515DA26AD083F79F26E91EFF7EF614AFC (void);
// 0x000000CA Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_InteractionButton()
extern void MixedRealityInputSimulationProfile_get_InteractionButton_m41A86704A8559A467B46029A3E28931C8CCDCC98 (void);
// 0x000000CB System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DoublePressTime()
extern void MixedRealityInputSimulationProfile_get_DoublePressTime_m1E73696EA0E5DDEB0558ABFB8C40B5F38230C134 (void);
// 0x000000CC System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsHandsFreeInputEnabled()
extern void MixedRealityInputSimulationProfile_get_IsHandsFreeInputEnabled_m2E81793895EFE29DFD19DDBEE38966D5BF0D0B22 (void);
// 0x000000CD System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsCameraControlEnabled()
extern void MixedRealityInputSimulationProfile_get_IsCameraControlEnabled_mAB0618F98A37CC4315881F11F416519FDC3B12BE (void);
// 0x000000CE System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseLookSpeed_mD6823B4E6143841B971F82D65C31F93076CCE6F7 (void);
// 0x000000CF Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookButton()
extern void MixedRealityInputSimulationProfile_get_MouseLookButton_mFE38DE138E579ADD658C8BBC26D97ECC7C3D9E0A (void);
// 0x000000D0 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookToggle()
extern void MixedRealityInputSimulationProfile_get_MouseLookToggle_mD962C2FDD3B8AE5C5B8E5D694CD28D38AE76039C (void);
// 0x000000D1 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsControllerLookInverted()
extern void MixedRealityInputSimulationProfile_get_IsControllerLookInverted_m790B6026A8750F5E63B95906636F27AA414AF65F (void);
// 0x000000D2 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_CameraOriginOffset()
extern void MixedRealityInputSimulationProfile_get_CameraOriginOffset_m0577C5510AB8CD9A3B2CB624C6A2DEAE2CDE2058 (void);
// 0x000000D3 Microsoft.MixedReality.Toolkit.Input.InputSimulationControlMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_CurrentControlMode()
extern void MixedRealityInputSimulationProfile_get_CurrentControlMode_m6837CB72AC67A99846DAEDBD9B1B27AE8BEC5B58 (void);
// 0x000000D4 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_FastControlKey()
extern void MixedRealityInputSimulationProfile_get_FastControlKey_mA19B636CBA3D5E96DF17A30FF97C6FF185C9CEF1 (void);
// 0x000000D5 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControlSlowSpeed()
extern void MixedRealityInputSimulationProfile_get_ControlSlowSpeed_mA9A52A02B3279B004A42770ECC0B6F45373A4750 (void);
// 0x000000D6 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControlFastSpeed()
extern void MixedRealityInputSimulationProfile_get_ControlFastSpeed_mE3D6B9EC4BC6637937F83CC1BE00EF772D906B2D (void);
// 0x000000D7 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveHorizontal()
extern void MixedRealityInputSimulationProfile_get_MoveHorizontal_m4DA10B0739A640AA9CC3E4A8115EB727DE94BFD5 (void);
// 0x000000D8 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveVertical()
extern void MixedRealityInputSimulationProfile_get_MoveVertical_m70CC547B11F404DE43F8BA99929C13923F7ABA93 (void);
// 0x000000D9 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveUpDown()
extern void MixedRealityInputSimulationProfile_get_MoveUpDown_m1531E2F16A225AB5E81C407B54DAFC7B1DAF1147 (void);
// 0x000000DA System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LookHorizontal()
extern void MixedRealityInputSimulationProfile_get_LookHorizontal_mFE76A0E5451F8122C9A8799EA933D33B37A0C19D (void);
// 0x000000DB System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LookVertical()
extern void MixedRealityInputSimulationProfile_get_LookVertical_m04B21DB0BC22ACA5148EF567BF1C4CA3254401B7 (void);
// 0x000000DC System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_SimulateEyePosition()
extern void MixedRealityInputSimulationProfile_get_SimulateEyePosition_m7311A20140D5CBB130CE7424222144B61D2CCBC3 (void);
// 0x000000DD Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultEyeGazeSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultEyeGazeSimulationMode_m58E5D39C5436C794B6D87D8C45F2C87BDD3507D1 (void);
// 0x000000DE Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultControllerSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultControllerSimulationMode_mBBD8CD522463D7B420C1E89ACAE481B717817693 (void);
// 0x000000DF Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleLeftControllerKey()
extern void MixedRealityInputSimulationProfile_get_ToggleLeftControllerKey_m743897F7C6EE97EED46E3913AAB412E4455488A5 (void);
// 0x000000E0 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleRightControllerKey()
extern void MixedRealityInputSimulationProfile_get_ToggleRightControllerKey_m4BE85987AECC733E95C8273810E8F4C75BE3B76B (void);
// 0x000000E1 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerHideTimeout()
extern void MixedRealityInputSimulationProfile_get_ControllerHideTimeout_m865984206D3C433C4E81193F7200FE472E75A0A3 (void);
// 0x000000E2 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftControllerManipulationKey()
extern void MixedRealityInputSimulationProfile_get_LeftControllerManipulationKey_m0C961622AB093740BD92AC1708F651B4E08B7377 (void);
// 0x000000E3 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightControllerManipulationKey()
extern void MixedRealityInputSimulationProfile_get_RightControllerManipulationKey_m6B61D1328042D597970CF437387C2EAFD8778EDD (void);
// 0x000000E4 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseControllerRotationSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseControllerRotationSpeed_mCD7DAE3FB4EF4575C53FF01C8C91B4A3A829A3E1 (void);
// 0x000000E5 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerRotateButton()
extern void MixedRealityInputSimulationProfile_get_ControllerRotateButton_mA01DA5CA2A13CAC5E284675C4EF31BFB8DD29273 (void);
// 0x000000E6 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandGesture()
extern void MixedRealityInputSimulationProfile_get_DefaultHandGesture_m564378BD199E2905DCB5E744E716801888BD076B (void);
// 0x000000E7 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_LeftMouseHandGesture_m1DF353F9FEBA7AE0CDB71B37064EA4AFE13C5BD1 (void);
// 0x000000E8 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MiddleMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_MiddleMouseHandGesture_mD555821DAE4CB679C619CDA6734738FDCEE88760 (void);
// 0x000000E9 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_RightMouseHandGesture_mF90A25BEB12D1FBB6D1CBD0F85F5B3BED6A05ED6 (void);
// 0x000000EA System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandGestureAnimationSpeed()
extern void MixedRealityInputSimulationProfile_get_HandGestureAnimationSpeed_mE53836908FB17E9D3919A3C410A486BE4A2B24E4 (void);
// 0x000000EB System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HoldStartDuration()
extern void MixedRealityInputSimulationProfile_get_HoldStartDuration_mA17CB73B9856509192B0FEC381E4D9ED04520369 (void);
// 0x000000EC System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_NavigationStartThreshold()
extern void MixedRealityInputSimulationProfile_get_NavigationStartThreshold_mD0691521CE6CDE700EDB283F7EE3CCA8A8F5E70E (void);
// 0x000000ED System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultControllerDistance()
extern void MixedRealityInputSimulationProfile_get_DefaultControllerDistance_m0D5F59DB4F0D8DFF042FF4D2E4CFE83F031EEA8E (void);
// 0x000000EE System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerDepthMultiplier()
extern void MixedRealityInputSimulationProfile_get_ControllerDepthMultiplier_mCCB8B1C583DAF08368A56959FD862E39EC7C20AE (void);
// 0x000000EF System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerJitterAmount()
extern void MixedRealityInputSimulationProfile_get_ControllerJitterAmount_m53797E798206619D38D1D266C1EC738B4CE00E01 (void);
// 0x000000F0 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerTriggerKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerTriggerKey_mC7202B9DC85EC6DF25495E20E59E04B6C7C19BB7 (void);
// 0x000000F1 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerGrabKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerGrabKey_m8A4252C164FA8588C7454D71BF8B4C6F86F70A3C (void);
// 0x000000F2 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerMenuKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerMenuKey_m752FD34294E38426BE92E4B9CE288FB70A8A5535 (void);
// 0x000000F3 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultHandSimulationMode_mE3A316D1EE705D6B2FDB570B09ED526EB4338872 (void);
// 0x000000F4 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleLeftHandKey()
extern void MixedRealityInputSimulationProfile_get_ToggleLeftHandKey_m2D2A7022E31F9B45314E704339EAFF19E0E3A232 (void);
// 0x000000F5 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleRightHandKey()
extern void MixedRealityInputSimulationProfile_get_ToggleRightHandKey_mF850FBADB8FEE781C7196D5B2A44B07CCA51A096 (void);
// 0x000000F6 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandHideTimeout()
extern void MixedRealityInputSimulationProfile_get_HandHideTimeout_m0DEEB2EB6BD6BC5FCDE9484BDD9DC392251D01C4 (void);
// 0x000000F7 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftHandManipulationKey()
extern void MixedRealityInputSimulationProfile_get_LeftHandManipulationKey_m3413D11CF5699A6B1D42C12B8F9ECDB19A6AFFC0 (void);
// 0x000000F8 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightHandManipulationKey()
extern void MixedRealityInputSimulationProfile_get_RightHandManipulationKey_m7F77E67BA0EC453F4112B14C8A5A05F6E1456194 (void);
// 0x000000F9 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseHandRotationSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseHandRotationSpeed_m46EFE0618F1900C9C1C21015E28CFABE157B7CE1 (void);
// 0x000000FA Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandRotateButton()
extern void MixedRealityInputSimulationProfile_get_HandRotateButton_mFE0B2953B630D0A333FCCB96317AE0D94D9CE00B (void);
// 0x000000FB System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandDistance()
extern void MixedRealityInputSimulationProfile_get_DefaultHandDistance_m78281550082D3C5162AC4C3E0D1D6CF7E26641CA (void);
// 0x000000FC System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandDepthMultiplier()
extern void MixedRealityInputSimulationProfile_get_HandDepthMultiplier_m80FF121A4F725E6AD0979F46EE705E4B67497A05 (void);
// 0x000000FD System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandJitterAmount()
extern void MixedRealityInputSimulationProfile_get_HandJitterAmount_mD118BC566C9B351E1DFE8A89D556CBD159B74512 (void);
// 0x000000FE System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::.ctor()
extern void MixedRealityInputSimulationProfile__ctor_m157F7156089E96E17AEE2EADB380C13FFAC9BF11 (void);
// 0x000000FF System.Boolean Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::get_IsRotating()
extern void MouseRotationProvider_get_IsRotating_m3A38A6B6D95AC6C33ABBFD9D1D59AB20BA863BA9 (void);
// 0x00000100 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::Update(Microsoft.MixedReality.Toolkit.Input.KeyBinding,Microsoft.MixedReality.Toolkit.Input.KeyBinding,System.Boolean)
extern void MouseRotationProvider_Update_m16E586538696BF801DBD90988947FAD755C58407 (void);
// 0x00000101 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::OnStartRotating(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void MouseRotationProvider_OnStartRotating_mFF7967B266C95EB64DC478408C60D701DF99D25F (void);
// 0x00000102 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::OnEndRotating(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void MouseRotationProvider_OnEndRotating_m976F7D5CAB2870088B1C9659D596AD78E5587D7E (void);
// 0x00000103 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::SetWantsMouseJumping(System.Boolean)
extern void MouseRotationProvider_SetWantsMouseJumping_m56D1E5003D076C91B709965A25D2424670F58A15 (void);
// 0x00000104 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::.ctor()
extern void MouseRotationProvider__ctor_mDB741380CEDFA9256E50945178D9636F25D20D59 (void);
// 0x00000105 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::.cctor()
extern void MouseRotationProvider__cctor_m25093073CAA61112C0B570984F622FF1CCFD2F2A (void);
// 0x00000106 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::get_SimulationMode()
extern void SimulatedArticulatedHand_get_SimulationMode_mBBEBDD6302C2BF478BED49801CFB4662A49CFE52 (void);
// 0x00000107 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedArticulatedHand__ctor_mC11D60B9980358338E8A88F45813B61FEAEF8F4B (void);
// 0x00000108 Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::get_DefaultInteractions()
extern void SimulatedArticulatedHand_get_DefaultInteractions_m61AAC02EDC45014DCCF84454B269E5CED05BB07E (void);
// 0x00000109 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedArticulatedHand_UpdateInteractions_mA188894B683F47C8E6146E944F12FB6173C293DD (void);
// 0x0000010A Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::GetGesturePose(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedArticulatedHandPoses_GetGesturePose_mE3DBC4AA1F37232EBCC1C7C4ECD669597ABFA8AE (void);
// 0x0000010B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::SetGesturePose(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId,Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose)
extern void SimulatedArticulatedHandPoses_SetGesturePose_m4D74EBFD2FB2AB3FF816E4F32E3E34CB62F0088B (void);
// 0x0000010C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::LoadDefaultGesturePoses()
extern void SimulatedArticulatedHandPoses_LoadDefaultGesturePoses_m09E0520A0B0425F6C75801ECEEFB50D1A18AD6A8 (void);
// 0x0000010D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::.cctor()
extern void SimulatedArticulatedHandPoses__cctor_m87F894D9972BA341A57F7B6A39E078474DC83A5E (void);
// 0x0000010E Microsoft.MixedReality.Toolkit.Utilities.Handedness Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::get_Handedness()
extern void SimulatedControllerState_get_Handedness_m46C3951983F1995D748F0FBC88B07824CAAC6FC6 (void);
// 0x0000010F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedControllerState__ctor_m56FC25A09678F68D23088ADD40782FF23C2A76A7 (void);
// 0x00000110 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::SimulateInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean,System.Single,System.Single,System.Single)
extern void SimulatedControllerState_SimulateInput_m7AD9F72748CA315743863A953DB690181E5F879F (void);
// 0x00000111 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::ResetPosition(UnityEngine.Vector3)
extern void SimulatedControllerState_ResetPosition_m59000E30DAA3EA12C67F2D3FEFF919A3EB01F813 (void);
// 0x00000112 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::ResetRotation()
// 0x00000113 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::Update()
extern void SimulatedControllerState_Update_mE9D02CF11449E7BFA88D559905A097ADFB665346 (void);
// 0x00000114 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_isSimulatingGaze()
extern void SimulatedControllerDataProvider_get_isSimulatingGaze_m2DAA86B106D0B6D6980A1D21CBE2051889DD75EA (void);
// 0x00000115 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_IsSimulatingLeft()
extern void SimulatedControllerDataProvider_get_IsSimulatingLeft_mEC1D5FBA42D5828A1AFBD511C97EA59EA4AA5C1E (void);
// 0x00000116 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::set_IsSimulatingLeft(System.Boolean)
extern void SimulatedControllerDataProvider_set_IsSimulatingLeft_m2737D03870D76115049925E2FE51ACF3FA9E5BDF (void);
// 0x00000117 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_IsSimulatingRight()
extern void SimulatedControllerDataProvider_get_IsSimulatingRight_mDD8DE7C92F368CBFC129407969EED68ED82CA6A9 (void);
// 0x00000118 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::set_IsSimulatingRight(System.Boolean)
extern void SimulatedControllerDataProvider_set_IsSimulatingRight_m571C0EDD4EEFC1C30049E9D9F9570AD6FE673547 (void);
// 0x00000119 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedControllerDataProvider__ctor_m1F9C770683E39974146F48C8B2958E4F4931207D (void);
// 0x0000011A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::SimulateUserInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedControllerDataProvider_SimulateUserInput_m12DC4B114C85C30FA9BB34513237E0BC5402762D (void);
// 0x0000011B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
// 0x0000011C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedControllerDataProvider_ResetInput_m7541D0993321A3AE32D820778D97EF05EFB9CA4E (void);
// 0x0000011D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedControllerDataProvider_ResetInput_m7FAF6A7B47BC4583E65B8FB23A51339596AE5CC1 (void);
// 0x0000011E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::.cctor()
extern void SimulatedControllerDataProvider__cctor_m7A9A8EDD1D4F46CF785536C68F3966F88ABDE109 (void);
// 0x0000011F Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::get_SimulationMode()
extern void SimulatedGestureHand_get_SimulationMode_m41F11F147B7019064414595EC4ED63F439866AB0 (void);
// 0x00000120 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::get_NavigationDelta()
extern void SimulatedGestureHand_get_NavigationDelta_mA8CB35243425C4B145F87EB5357F06EEC6C97639 (void);
// 0x00000121 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedGestureHand__ctor_m95276B6DDB55268C6A15D70F71B204FE112C3ED8 (void);
// 0x00000122 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::EnsureProfileSettings()
extern void SimulatedGestureHand_EnsureProfileSettings_m918249A2BA9F95008F34F300954269600E8F16AA (void);
// 0x00000123 Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::get_DefaultInteractions()
extern void SimulatedGestureHand_get_DefaultInteractions_m7EA86C9A8C9BA003C343E8AA710E9A5A914D1A59 (void);
// 0x00000124 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedGestureHand_UpdateInteractions_mEA39240329977226BD1F4903037A75E37489B27F (void);
// 0x00000125 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartHold()
extern void SimulatedGestureHand_TryStartHold_m88137D050C4A12181111A45C48C719C106D43625 (void);
// 0x00000126 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteHold()
extern void SimulatedGestureHand_TryCompleteHold_mBF67C25ED325608CCA2F802CD98D2542D4B6D5AE (void);
// 0x00000127 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelHold()
extern void SimulatedGestureHand_TryCancelHold_m38FB29B8D69C05D2059C126AB099E3B151E88DFD (void);
// 0x00000128 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartManipulation()
extern void SimulatedGestureHand_TryStartManipulation_m05724BF0BD0601B4255D77583EF8A5E5914ACD95 (void);
// 0x00000129 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateManipulation()
extern void SimulatedGestureHand_UpdateManipulation_m745633A22C7633A7B26013E5FA80BF3464831EEA (void);
// 0x0000012A System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteManipulation()
extern void SimulatedGestureHand_TryCompleteManipulation_m1052F207737A0DD5B6573D4845504BE479C205DC (void);
// 0x0000012B System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelManipulation()
extern void SimulatedGestureHand_TryCancelManipulation_m1869B76FEFB527BDAE3215DA8C52A75D2D7FA4D6 (void);
// 0x0000012C System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteSelect()
extern void SimulatedGestureHand_TryCompleteSelect_m31871C90BEB9D614E751977C7F83D43DA7BA9C09 (void);
// 0x0000012D System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartNavigation()
extern void SimulatedGestureHand_TryStartNavigation_m61BEBBB6FB5E4203B9506D04C660D083EE94867D (void);
// 0x0000012E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateNavigation()
extern void SimulatedGestureHand_UpdateNavigation_m750BE60979D05EBE01C77C90E40462322B572935 (void);
// 0x0000012F System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteNavigation()
extern void SimulatedGestureHand_TryCompleteNavigation_m5D4945DEE7D9EDB87F92E41EB8B5728AEA744B6F (void);
// 0x00000130 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelNavigation()
extern void SimulatedGestureHand_TryCancelNavigation_m4707A1F7F2CB27D419730EC56EF9AB59AE5D5EDE (void);
// 0x00000131 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateNavigationRails()
extern void SimulatedGestureHand_UpdateNavigationRails_m52DBD1706EFC79CE41980078CC08D7CC16514914 (void);
// 0x00000132 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_IsTracked()
extern void SimulatedHandData_get_IsTracked_m8B8E107EB194B24177CBB3ED817E2FAF7D81511C (void);
// 0x00000133 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[] Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_Joints()
extern void SimulatedHandData_get_Joints_m831297E407F681B0BC9D3A26BEF4B24945A0827D (void);
// 0x00000134 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_IsPinching()
extern void SimulatedHandData_get_IsPinching_mE4C687B3590D8876CF9D719196F7F6A8F2A32CD6 (void);
// 0x00000135 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::Copy(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedHandData_Copy_mF446B3C9178233C7B00278FF7608082D8AD27E5A (void);
// 0x00000136 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::Update(System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator)
extern void SimulatedHandData_Update_mD0EB03CC34D4D55F9C5611C704B211B8DCD84820 (void);
// 0x00000137 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::.ctor()
extern void SimulatedHandData__ctor_mBA5359AE76E1F7A8E04BEDAA9DBED6A3D7FB8ED7 (void);
// 0x00000138 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::.cctor()
extern void SimulatedHandData__cctor_mF3A9FD43E83AFCAF40B65C56EA304BADB0254D29 (void);
// 0x00000139 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::.ctor(System.Object,System.IntPtr)
extern void HandJointDataGenerator__ctor_m81B71E3BF5A0DFBB0305C863ED08ED5C18098A89 (void);
// 0x0000013A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::Invoke(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void HandJointDataGenerator_Invoke_m2CE9BD915445F8B6D9A95EB4F9E47C4E589C7579 (void);
// 0x0000013B System.IAsyncResult Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::BeginInvoke(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[],System.AsyncCallback,System.Object)
extern void HandJointDataGenerator_BeginInvoke_m913DC3C7E08404E370506F12049611CAB87C8873 (void);
// 0x0000013C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::EndInvoke(System.IAsyncResult)
extern void HandJointDataGenerator_EndInvoke_mCFFC7452265CC7BC63E0E74E8DA58C391B127659 (void);
// 0x0000013D Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedHand::get_SimulationMode()
// 0x0000013E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedHand__ctor_mDF127150B4E04F051D7EF4BF5FE3B4EAB933F803 (void);
// 0x0000013F System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&)
extern void SimulatedHand_TryGetJoint_m46705C986BDC9BC060FDC99BCF399470C9BDF234 (void);
// 0x00000140 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::UpdateState(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedHand_UpdateState_m7B261C5737126C9820998380915E0C697DD3568D (void);
// 0x00000141 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
// 0x00000142 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::.cctor()
extern void SimulatedHand__cctor_m191ECB7D227F3E525F9BFDA1C6695EFF0A0A0C1C (void);
// 0x00000143 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_IsPinching()
extern void SimulatedHandState_get_IsPinching_mAAF0661B1D6D9BED569C257A49DFBB8D26FBABAA (void);
// 0x00000144 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_Gesture()
extern void SimulatedHandState_get_Gesture_mF578657C921D4644519E270D90D04EAE48D43374 (void);
// 0x00000145 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::set_Gesture(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedHandState_set_Gesture_mEBFEF75E14FB2E157FCA7D5A8F34DE15C808B0CE (void);
// 0x00000146 System.Single Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_GestureBlending()
extern void SimulatedHandState_get_GestureBlending_m83CC9BAB523F22A0B50A561E8F5DE10D666C60A2 (void);
// 0x00000147 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::set_GestureBlending(System.Single)
extern void SimulatedHandState_set_GestureBlending_m96C35208AF74C83C7B4E5BB880687BF491FC6C5C (void);
// 0x00000148 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedHandState__ctor_m708A7963488340DAB68F30DBB4B246DA1FE7F89A (void);
// 0x00000149 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::ResetGesture()
extern void SimulatedHandState_ResetGesture_mF5E16D7B6471E0EE555B96BDF542E114CF8EBD1A (void);
// 0x0000014A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::ResetRotation()
extern void SimulatedHandState_ResetRotation_m463FA1DABE4E2117F1D50A6D6AB3C78C570AADD3 (void);
// 0x0000014B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::FillCurrentFrame(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void SimulatedHandState_FillCurrentFrame_m166C748751310B276B46A6FE1CF75F270F66A207 (void);
// 0x0000014C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedHandDataProvider__ctor_m759A7F51E7EEE81CEACFD03AEEAD1891A9BE0912 (void);
// 0x0000014D System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::UpdateHandData(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedHandDataProvider_UpdateHandData_mAD23FCEC7AE0967FE6A6368410817B5847EB0C18 (void);
// 0x0000014E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SimulateUserInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedHandDataProvider_SimulateUserInput_m088B9E694FA18B8343582BE439CDF1A901634176 (void);
// 0x0000014F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
extern void SimulatedHandDataProvider_SimulateInput_m047048C1FD5C298AE91FC8B2F21F40CEF7BFAF2A (void);
// 0x00000150 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedHandDataProvider_ResetInput_m913821A6C9337C4F8E747C532955D246A2658BBD (void);
// 0x00000151 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SelectGesture()
extern void SimulatedHandDataProvider_SelectGesture_m549B8B820F980DEDE39977B4F93DD0B081848FA9 (void);
// 0x00000152 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::ToggleGesture(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedHandDataProvider_ToggleGesture_mFF8F87E941E47770D90AB9D3930663059B4ECBC2 (void);
// 0x00000153 Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateLeft()
extern void SimulatedHandDataProvider_get_HandStateLeft_m91D75464AC108266DF529EBA8FC43D0CE45844F8 (void);
// 0x00000154 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateLeft(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateLeft_m4C964F28810FD73AC735BD76BA2F13765BCE7909 (void);
// 0x00000155 Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateRight()
extern void SimulatedHandDataProvider_get_HandStateRight_m5591B296B3BB6E71C85761BD7550D0741A783D66 (void);
// 0x00000156 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateRight(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateRight_m567357AAB6E9831E06A465BF9B06D955410AE5E3 (void);
// 0x00000157 Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateGaze()
extern void SimulatedHandDataProvider_get_HandStateGaze_m731A75BC5C853F4AA4CD01D7F1350071EC11518A (void);
// 0x00000158 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateGaze(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateGaze_mAD4DA8E5A626FBAED0D45648EB54F47716F69FC0 (void);
// 0x00000159 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::CalculateJointRotations(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[],UnityEngine.Quaternion[])
extern void SimulatedHandUtils_CalculateJointRotations_m4EF645221FB34CED93C109FA496F214C6F056FD4 (void);
// 0x0000015A UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmForwardVector(UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmForwardVector_m691C67DE0AEA1712108FF6720DAC920D5556E0D6 (void);
// 0x0000015B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmUpVector(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmUpVector_mE0CBC30A4DF61063AE67A6F0ACD34F8B4BF101BB (void);
// 0x0000015C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmRightVector(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmRightVector_m5F4F8343CD86FEB7A13789016CEADBF56D938B06 (void);
// 0x0000015D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::.ctor()
extern void SimulatedHandUtils__ctor_mD378A3CAF1AA2D3806599507D1D85CEBCC865C9D (void);
// 0x0000015E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::.cctor()
extern void SimulatedHandUtils__cctor_m67E8189F6A9AB996DF5472D91ED4C2A6435E78F1 (void);
// 0x0000015F System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_IsTracked()
extern void SimulatedMotionControllerData_get_IsTracked_m6884EE6590E582A3883E92492831A3FAC88C6313 (void);
// 0x00000160 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_ButtonState()
extern void SimulatedMotionControllerData_get_ButtonState_m20C87E9B843BD63E1B0452D70A2C344E11057910 (void);
// 0x00000161 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_Position()
extern void SimulatedMotionControllerData_get_Position_m9C154E85706D99112116B944EBD1E7CE8A35523A (void);
// 0x00000162 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::set_Position(UnityEngine.Vector3)
extern void SimulatedMotionControllerData_set_Position_mBB23C897BC08DED72639063EE497AB5573C0D5CD (void);
// 0x00000163 UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_Rotation()
extern void SimulatedMotionControllerData_get_Rotation_m7DD9A8AFEA6523AC2AA795B7D97B48163FE0EE95 (void);
// 0x00000164 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::set_Rotation(UnityEngine.Quaternion)
extern void SimulatedMotionControllerData_set_Rotation_m7A58CA5F499A8E23211E3D3AE0AAD4A85A3E6EC0 (void);
// 0x00000165 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::Update(System.Boolean,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater)
extern void SimulatedMotionControllerData_Update_m063F8CE978B472902814F80FF2EB03832DDEBC84 (void);
// 0x00000166 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::.ctor()
extern void SimulatedMotionControllerData__ctor_mDA5BEA2A4EBEE7F76C45F87245A784FD1311D428 (void);
// 0x00000167 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::.ctor(System.Object,System.IntPtr)
extern void MotionControllerPoseUpdater__ctor_m88F203AEF0EE969AFA6251B91896FBED4CA4F365 (void);
// 0x00000168 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::Invoke()
extern void MotionControllerPoseUpdater_Invoke_m7CFA23104E339E9B17C851684974374D34784538 (void);
// 0x00000169 System.IAsyncResult Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::BeginInvoke(System.AsyncCallback,System.Object)
extern void MotionControllerPoseUpdater_BeginInvoke_mFC57A62A5480A896D5C845C3073BECFCACE11B57 (void);
// 0x0000016A Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::EndInvoke(System.IAsyncResult)
extern void MotionControllerPoseUpdater_EndInvoke_m08BE44167D9F904339BA962D477809D1E435B4C8 (void);
// 0x0000016B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedMotionController__ctor_mEFDDF50F0018B5E135CECF3C13C13FB760B81D17 (void);
// 0x0000016C Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController::get_DefaultInteractions()
extern void SimulatedMotionController_get_DefaultInteractions_mE433893F5E3CB010573A31080973C42A6DE4DA51 (void);
// 0x0000016D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController::UpdateState(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData)
extern void SimulatedMotionController_UpdateState_m9FEA78FC72EFE99682BE10269672E9DD2EFB1C71 (void);
// 0x0000016E System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::Equals(System.Object)
extern void SimulatedMotionControllerButtonState_Equals_mD347078799A5A8DAEC49169AFCB664D7A1B726DC (void);
// 0x0000016F System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::Equals(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_Equals_mBE4BBEA32192B61DD8320254C1110073198AC8C1 (void);
// 0x00000170 System.Int32 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::GetHashCode()
extern void SimulatedMotionControllerButtonState_GetHashCode_mBD209918615EB0B16D224253BF0FF2047C315440 (void);
// 0x00000171 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::op_Equality(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_op_Equality_mE5CC94395D714B0687635BF18C3F4EEE2E5C2E0B (void);
// 0x00000172 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::op_Inequality(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_op_Inequality_m1C5F4A34FA2F1FF81E230F7CC396DADCF32A9DE3 (void);
// 0x00000173 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::get_ButtonState()
extern void SimulatedMotionControllerState_get_ButtonState_m8B5B7449DE074DCEA5F690146E385B9AC2F64E05 (void);
// 0x00000174 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::set_ButtonState(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerState_set_ButtonState_m20336CF5A9C8735C978B3C857C2730A38A275410 (void);
// 0x00000175 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedMotionControllerState__ctor_m6051CB0EB48FD1888F2BD01300348D728C13B660 (void);
// 0x00000176 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::ResetRotation()
extern void SimulatedMotionControllerState_ResetRotation_mFC493B2D24F26CD1F548CA7C60DFFDFDA6E10D09 (void);
// 0x00000177 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::ResetButtonStates()
extern void SimulatedMotionControllerState_ResetButtonStates_mBA8DAB516836F441B4E47338A6D1B7D36CAB940D (void);
// 0x00000178 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::UpdateControllerPose()
extern void SimulatedMotionControllerState_UpdateControllerPose_mF3C67E965F33F01E74563399516A1F454499985C (void);
// 0x00000179 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedMotionControllerDataProvider__ctor_m4A7D7AAF426E7C419EDA91B9A207D83D6C834BD1 (void);
// 0x0000017A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
extern void SimulatedMotionControllerDataProvider_SimulateInput_m5E722B5B8FBDB58DF0EC6BA71EEFF160F089866F (void);
// 0x0000017B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::UpdateControllerData(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedMotionControllerDataProvider_UpdateControllerData_m78D7635BF2E582CE1F92636D6BA5D18146CE9C79 (void);
// 0x0000017C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedMotionControllerDataProvider_ResetInput_mAD01C92BD1B2619E2E8FC8339C96593025F2E4C2 (void);
static Il2CppMethodPointer s_methodPointers[380] = 
{
	InputSimulationHelpGuide_Start_m475D23A76644A5672FF24FB4EDB04FAC31992C75,
	InputSimulationHelpGuide_Update_m4827FBD5BA077C8598B20A9A8E0787418DAA949D,
	InputSimulationHelpGuide__ctor_m7A8BB53944392590E7D584F8000E7C40CC6A709D,
	BaseInputSimulationService_GetActiveControllers_mF2C747162AA7163685B3FAA4ACF31A905F47F271,
	BaseInputSimulationService__ctor_m85F8191D8511AF069ADA87DBE8D78C9BFFE9B764,
	BaseInputSimulationService__ctor_m064170922D0E2DFDE990B5C79B3BFDFDE6B0EC14,
	BaseInputSimulationService_UpdateControllerDevice_mFFB0DC32268DF3ACD93A6ACB48952FB158D3E223,
	BaseInputSimulationService_GetControllerDevice_mB7259909FFC45776FD94AEC20D2FEE4D9FE9FFBF,
	BaseInputSimulationService_GetOrAddControllerDevice_m5DB8C6ED98633335D0326B034372000D1D540E9F,
	BaseInputSimulationService_RemoveControllerDevice_m13E89A8C09037EF57187F2CE6F8EE851FC5C04D6,
	BaseInputSimulationService_RemoveAllControllerDevices_mF9A6C46723EA6668367C422CB905DE90934BC72D,
	BaseInputSimulationService_UpdateActiveControllers_m3A7CD05A5E317FEE8779541575C0BA0633AE7A76,
	BaseInputSimulationService_UpdateHandDevice_mA9B830A183C7808F0514325287FF4E07E456ACB9,
	BaseInputSimulationService_GetHandDevice_m19FAEDB37CF5335D58F0CED05E63BDFF5541CB63,
	BaseInputSimulationService_GetOrAddHandDevice_m5BC2E9478A6C57B3E1A642F3E70A638623BE6FE8,
	BaseInputSimulationService_RemoveHandDevice_m456384E54E1CE46C9FE79E62BD521ACE2B902A18,
	BaseInputSimulationService_RemoveAllHandDevices_m59A8A8B8DA7C43183728D418893DC6EC9613A291,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InputPlaybackService_get_HandDataLeft_m84B178B9A58B7210C43A7C7236F32B5A315A5711,
	InputPlaybackService_get_HandDataRight_mDD7220C2A41B148F35D3507D343182ED25487C02,
	InputPlaybackService__ctor_m12F6326FCAAF459F37C614A6B5D04AB45C836451,
	InputPlaybackService__ctor_m197A16D87DB4C61DED00B0FAD1417B05493B51AF,
	InputPlaybackService_CheckCapability_mE6BC4ACBF32E7F3B780EDB26C3AB3A3FB327A768,
	InputPlaybackService_get_Animation_m4FEEABABE429ECE8023A2407F1F811F691FF4DF9,
	InputPlaybackService_set_Animation_m06C3761D40C27537DE5225AC425EC1189BFBC3F2,
	InputPlaybackService_get_Duration_mA495B0C2583F9A743770E55069C07010F95F4AE6,
	InputPlaybackService_get_IsPlaying_m7E5D50CF258830BF3A9483ADA521D225A6E9BDFA,
	InputPlaybackService_get_LocalTime_m6738B6E1060DBFD07C01965DB911B19B828075F2,
	InputPlaybackService_set_LocalTime_mE3CB0B7AF06AC479B0B935D8054D9EE5DBCEB61E,
	InputPlaybackService_Play_m254C2F7E3DA591DFBBCD08EE99844E3137EF66B2,
	InputPlaybackService_Stop_m6BE0A0CB07CE43F891CECD64D4E241CFB4E893A3,
	InputPlaybackService_Pause_m73982C1B88BA07066EC6A9970ADC5BC07E002891,
	InputPlaybackService_Update_m68ACD2512F93D170C48DA7F983980E2CB9582F95,
	InputPlaybackService_SetPlaying_m197E71420EC3D24BED0CF4B222271147AE79C770,
	InputPlaybackService_Evaluate_m3DA25CD6A4C5B15ABA60A5DC0B7AF6A94C600AEA,
	InputPlaybackService_EvaluateHandData_m4237F623E0B98F0883751BE03995305179A4903A,
	InputPlaybackService_LoadInputAnimation_m222F6A84CF1D8C2DC9ADBD5C70ED1AC7512E7704,
	InputPlaybackService__cctor_mB21D1F0ECB88CDC6CEDE2466014117F4F383A2C7,
	U3CU3Ec__DisplayClass29_0__ctor_m781FEF0197866BE561E9FA465EDCB66899023664,
	U3CU3Ec__DisplayClass29_0_U3CEvaluateHandDataU3Eb__0_mB2BCB396F2FACD240A54DD2F48A53835CC070792,
	MouseDelta_Reset_m0AEA879CA11D8CCE6D6421CCC6FD94045026162A,
	MouseDelta__ctor_m28C03A62F23F401F2C5297C644D878F379F3536D,
	InputSimulationService_get_ControllerSimulationMode_m5C2BB99A87CF427718C93AFADD5014C77E50650C,
	InputSimulationService_set_ControllerSimulationMode_m9A00A586B8B5DA4C9DECC85C46D033FD96A723E4,
	InputSimulationService_get_HandDataLeft_m98B0BD5D16C143A60CE897776A3BD80421CA4F8A,
	InputSimulationService_get_HandDataRight_m8DBFB8C070C84935B56366331CE068E3DF1F04B0,
	InputSimulationService_get_HandDataGaze_mF09C4A235A5954C7BD9D40F4CF24230534242861,
	InputSimulationService_get_MotionControllerDataLeft_m9ED5154B1B8879A8B389139AFE614354F85EFEFB,
	InputSimulationService_get_MotionControllerDataRight_m9079E5F1EC6D4562D556E6478863DD981B89538E,
	InputSimulationService_get_IsSimulatingControllerLeft_m140240A942EFE9D9E953B13AB78DC2E1EA869B99,
	InputSimulationService_get_IsSimulatingControllerRight_m7DC2E6087E5DDAEF49B14C1D1215A1DDA530CAA6,
	InputSimulationService_get_IsAlwaysVisibleControllerLeft_mA1EC5E8D5E6D7C27D4367F4B7403B3A4A3D67265,
	InputSimulationService_set_IsAlwaysVisibleControllerLeft_mE008C1FB8CB2466C82357AC5B3ABA6912C22FB20,
	InputSimulationService_get_IsAlwaysVisibleControllerRight_m29AA450588031AED3CA89E88B199AC2753F7D4B8,
	InputSimulationService_set_IsAlwaysVisibleControllerRight_m53069AE80CE41A3167A4D02AA313493BB00C77B2,
	InputSimulationService_get_ControllerPositionLeft_m3A88C8308D00998CE5014A30D3272B829ECD9E1C,
	InputSimulationService_set_ControllerPositionLeft_mD63860DDB9C5B67260933CFC89935B449100F748,
	InputSimulationService_get_ControllerPositionRight_m549BBFDB881C36C1B2C75064045B6FF44267DD52,
	InputSimulationService_set_ControllerPositionRight_m97D54623C77669DDC3BC30647B94756B1C4670E4,
	InputSimulationService_get_ControllerRotationLeft_mA22D1BA8514159CD44C9D5B5BC004B69B46516A1,
	InputSimulationService_set_ControllerRotationLeft_m3C68959F080B755DA57D343C1C4C0FCB36ED2462,
	InputSimulationService_get_ControllerRotationRight_m7A2DD07B5A831AC7610DC3DAC60CEC27ADEF97FE,
	InputSimulationService_set_ControllerRotationRight_mCEF83C015F5DEEA7A93144FD6D4DC291E9048927,
	InputSimulationService_ResetControllerLeft_m51C82FFF6F2818A1D428946D650A8459666DDE3F,
	InputSimulationService_ResetControllerRight_m2B80BF8F9DBDF17A629A360E372F806B37D0647D,
	InputSimulationService_get_SimulateEyePosition_m40BD0C6709D6228F1237DA4E3C85FD78978C662B,
	InputSimulationService_set_SimulateEyePosition_m63DCFD8AA884675583BEA3AE0C2CB455E3888C0A,
	InputSimulationService_get_EyeGazeSimulationMode_m5BE2C0C42FD96604390D433B86E8CC7A13D29F30,
	InputSimulationService_set_EyeGazeSimulationMode_m2892153046A23A7B9A27A9E8688BDAF3BE3631DE,
	InputSimulationService_get_UserInputEnabled_m6D63571F3821DB7485C628876E3E844BFE71EC5C,
	InputSimulationService_set_UserInputEnabled_mE857BC356F694BE2A5E2F11D7D1883CBB800D89D,
	InputSimulationService__ctor_mEB564D75663C0501B3AC27B8BD5B7EE55F9768ED,
	InputSimulationService__ctor_m5A3710F1EB46D0040F0F222DAD33A98DA9152584,
	InputSimulationService_CheckCapability_m96473FE081D388EA62FAE70A281E8ACCA7E6C268,
	InputSimulationService_Initialize_mD50874D0B717748BCA596E1B054D50AF599A391F,
	InputSimulationService_Destroy_mE346FF8815BBF076E30E294873FD6B6C0C65A349,
	InputSimulationService_Enable_mAD1952BC2B055623356DF6475386007F802BF85C,
	InputSimulationService_Disable_mD92589DC3819D3462F1F06A9FEB864C602F9F84F,
	InputSimulationService_Update_m7229E7271E0B770B520411287231EE9DC7035C2E,
	InputSimulationService_LateUpdate_m6C95257157FA234363CDD509B6CF1F776068D3EF,
	InputSimulationService_get_InputSimulationProfile_m3BA9686028141B76C8EC24BB81575A9B8079EA46,
	InputSimulationService_set_InputSimulationProfile_m1EB8A508743BC2D2C61B8FC4F15966D438709CC9,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SaccadeProvider_m5242992A09AF642B57754EBA46D2BAF74663B9B3,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SmoothEyeTracking_m85838FAF50D3898C7EF4CF565881E1C613DE8433,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_set_SmoothEyeTracking_mD05F38EA97F499C4C55493A20ABAA27914D05110,
	InputSimulationService_EnableCameraControl_mB3008B5BC7DC66F0A287C6BDC1161D0DCCBD7D6D,
	InputSimulationService_DisableCameraControl_m72D2FE758D4BBDDCAF070C7DFF8DB8830A2B2634,
	InputSimulationService_EnableHandSimulation_m0C764704AD1B9D1F11B0390369B0204C610435D2,
	InputSimulationService_EnableMotionControllerSimulation_mAA79A753F0F5DB1911C90844131BB4F64FB56D8F,
	InputSimulationService_DisableControllerSimulation_m81D1CBCE97E029319CB68353F9D5C525A33BCD59,
	InputSimulationService_ResetMouseDelta_mC5931363ACD613134D328073437059A0E10EDB66,
	InputSimulationService_UpdateMouseDelta_m28D4BF91691EF0F1A1A2076692653ED3F921EE94,
	InputSimulationService_ScreenToWorld_m03047E7359DDFA55700C57BBC07168529A161208,
	InputSimulationService_WorldToScreen_m8130A54068206AB76FECD02D7696BFE74A77D907,
	InputSimulationService_WorldToViewport_m251E12624E64A31AB829974FC4FA6354C4003E1C,
	InputSimulationService_get_HandSimulationMode_mD980C86DBF185DDD04F3E4317E41398C62FF3BFF,
	InputSimulationService_set_HandSimulationMode_mEFBCF4330009D2E3D93A16AA1DFEFCD044187BFF,
	InputSimulationService_get_IsSimulatingHandLeft_m2EF6AF08EDC24EE783E853087A45B8857179BD52,
	InputSimulationService_get_IsSimulatingHandRight_mC7F71CC417973EE053118F762128B00BC3F054E3,
	InputSimulationService_get_IsAlwaysVisibleHandLeft_mAA29EB526F329E0CAB047A3D64BF9DC11C7ED3F9,
	InputSimulationService_set_IsAlwaysVisibleHandLeft_m3A3DD8B53DC009A6FEA981E59612EC0AAB7D95AE,
	InputSimulationService_get_IsAlwaysVisibleHandRight_m799817502E4A06157EC9935C7B6A2DA6C09BB9E4,
	InputSimulationService_set_IsAlwaysVisibleHandRight_mA9FFB5CB0848C4E3791D0FEA87185636AA768EB5,
	InputSimulationService_get_HandPositionLeft_m199BB1F741E460485467E811D983167A860C38EE,
	InputSimulationService_set_HandPositionLeft_mF541BC38C4CA9FEEE3BDAA4B46C12871EEE86DBA,
	InputSimulationService_get_HandPositionRight_m53704AA4BEAAFEE0EBC2AE7D2F4A31305F961A79,
	InputSimulationService_set_HandPositionRight_m19FF605A0DF226BFDB561AB77C61CDB7043215B4,
	InputSimulationService_get_HandRotationLeft_m001A59786A486D00C97F0E633D76B88F39D0DED8,
	InputSimulationService_set_HandRotationLeft_mE7C0C362D7DA0C94CBCFB3E8B74C44BB585C4E32,
	InputSimulationService_get_HandRotationRight_m2E4C13B6DA382C98EDC93C0C84EB7ED1A641B64E,
	InputSimulationService_set_HandRotationRight_mAF2B18F0AD9EC629C539019F3C89C65F58939CD3,
	InputSimulationService_ResetHandLeft_mAEEEF1FF233623898722AB90A8A78B8503D03F2F,
	InputSimulationService_ResetHandRight_m1479E339384C357A7406167E0FDC1678BF296A36,
	KeyBinding__cctor_m0C3F262A778D96E16F8FC6EB1BDEF96C8D640315,
	KeyBinding_get_BindingType_m4865FD00DE59B37BAE922C5DFF37C39BB100CF5E,
	KeyBinding_ToString_m19E5162244657C0CAF1CE542F4C695EA15F6C296,
	KeyBinding_TryGetKeyCode_mC94C514E2FE9CFC8372A6BD671DFA68DF8336085,
	KeyBinding_TryGetMouseButton_m0726DFBCCF20C5726E68B5766CAC8F991ECA91D5,
	KeyBinding_TryGetMouseButton_m83C8B35064098A2CBFA7D634C7C4211C89935B99,
	KeyBinding_Unbound_m6A91AB82DA79FE7B6B045A7EC3384FF75798D1E6,
	KeyBinding_FromKey_m6585166FD1FE2AF96428212F8EB6C119902E87A3,
	KeyBinding_FromMouseButton_mFF74F0829CA9BD73D1FE8CCA59C3BF4F704435D2,
	KeyBinding_FromMouseButton_m95365EEB21C9242EC716CDC2B7F785B60E210876,
	U3CU3Ec__DisplayClass5_0__ctor_m3B4B239601AEA7ABB3B9B2C679813D68E5C01D6B,
	U3CU3Ec__DisplayClass5_0_U3C_cctorU3Eb__0_m988961A53043F6B6572DC32C2F713662FC5443A3,
	KeyInputSystem_get_SimulatingInput_mF3CB8811D973827E2C83ECAA82F5F6E46F56EC95,
	KeyInputSystem_StartKeyInputStimulation_m16EC395569C1DFF113C2E5F7328BF32BBCF1BDFA,
	KeyInputSystem_StopKeyInputSimulation_mCA54EA6107A8901AE525DE5C25DEA32331B389A5,
	KeyInputSystem_ResetKeyInputSimulation_mAA38947B5A4DDCD416BD7175A433A1F7659FA51F,
	KeyInputSystem_PressKey_mF5901FC758ABBEEB118CB05100DEEB144D33B047,
	KeyInputSystem_ReleaseKey_m3265592C4F467DCEB5A181EE17AC607B5BE3FFC2,
	KeyInputSystem_AdvanceSimulation_mFA3DC653C807E268AE916981BAB816FC7E0B47DA,
	KeyInputSystem_GetKey_m6CBF61B319ECB14A403BE0CEB2B43D86C0080F3F,
	KeyInputSystem_GetKeyDown_m6EECCE6FA9BEC21565289E40EFC7BCF91E5E2FA2,
	KeyInputSystem_GetKeyUp_m7E47BC465852FAB6253B990E0905CCF89A039EFB,
	ManualCameraControl__ctor_m3514F7F7A420CCD77AD286A805FB19CDA94F313B,
	ManualCameraControl_InputCurve_mCE2F23BF1974155B5FC22C087BB17BED87112A62,
	ManualCameraControl_SetInitialTransform_m19950240F3173F1FF9ADD9A6C328EC4BA0427251,
	ManualCameraControl_UpdateTransform_m0FE2241819406CAA729621C5AD10B873190F1A2F,
	ManualCameraControl_GetKeyDir_mABC2C9AAAEE2695100B4E08C421C4573202F2A69,
	ManualCameraControl_GetCameraControlTranslation_m35450DDF7810FD1A41E704CE8E5C3FBB3439465A,
	ManualCameraControl_GetCameraControlRotation_mD52B1A6A77508F50D874E917BCF87875B822C7F3,
	ManualCameraControl__cctor_m29898A871CA03C44B849640D8FA07517FDB0B18E,
	MixedRealityInputSimulationProfile_get_IndicatorsPrefab_m0D34BBBA318A5773011EA6254B3BEC1A44DBB0EF,
	MixedRealityInputSimulationProfile_get_MouseRotationSensitivity_m2EE0E80F81ECB00732D8366B50CC28B28D518E9F,
	MixedRealityInputSimulationProfile_get_MouseX_m48F663A219D12611B54B03CA43CDF1C32BEDD2BE,
	MixedRealityInputSimulationProfile_get_MouseY_m4E9685C10A13F2A03C08DE4E4A24741BBD531C89,
	MixedRealityInputSimulationProfile_get_MouseScroll_m98A4BDE515DA26AD083F79F26E91EFF7EF614AFC,
	MixedRealityInputSimulationProfile_get_InteractionButton_m41A86704A8559A467B46029A3E28931C8CCDCC98,
	MixedRealityInputSimulationProfile_get_DoublePressTime_m1E73696EA0E5DDEB0558ABFB8C40B5F38230C134,
	MixedRealityInputSimulationProfile_get_IsHandsFreeInputEnabled_m2E81793895EFE29DFD19DDBEE38966D5BF0D0B22,
	MixedRealityInputSimulationProfile_get_IsCameraControlEnabled_mAB0618F98A37CC4315881F11F416519FDC3B12BE,
	MixedRealityInputSimulationProfile_get_MouseLookSpeed_mD6823B4E6143841B971F82D65C31F93076CCE6F7,
	MixedRealityInputSimulationProfile_get_MouseLookButton_mFE38DE138E579ADD658C8BBC26D97ECC7C3D9E0A,
	MixedRealityInputSimulationProfile_get_MouseLookToggle_mD962C2FDD3B8AE5C5B8E5D694CD28D38AE76039C,
	MixedRealityInputSimulationProfile_get_IsControllerLookInverted_m790B6026A8750F5E63B95906636F27AA414AF65F,
	MixedRealityInputSimulationProfile_get_CameraOriginOffset_m0577C5510AB8CD9A3B2CB624C6A2DEAE2CDE2058,
	MixedRealityInputSimulationProfile_get_CurrentControlMode_m6837CB72AC67A99846DAEDBD9B1B27AE8BEC5B58,
	MixedRealityInputSimulationProfile_get_FastControlKey_mA19B636CBA3D5E96DF17A30FF97C6FF185C9CEF1,
	MixedRealityInputSimulationProfile_get_ControlSlowSpeed_mA9A52A02B3279B004A42770ECC0B6F45373A4750,
	MixedRealityInputSimulationProfile_get_ControlFastSpeed_mE3D6B9EC4BC6637937F83CC1BE00EF772D906B2D,
	MixedRealityInputSimulationProfile_get_MoveHorizontal_m4DA10B0739A640AA9CC3E4A8115EB727DE94BFD5,
	MixedRealityInputSimulationProfile_get_MoveVertical_m70CC547B11F404DE43F8BA99929C13923F7ABA93,
	MixedRealityInputSimulationProfile_get_MoveUpDown_m1531E2F16A225AB5E81C407B54DAFC7B1DAF1147,
	MixedRealityInputSimulationProfile_get_LookHorizontal_mFE76A0E5451F8122C9A8799EA933D33B37A0C19D,
	MixedRealityInputSimulationProfile_get_LookVertical_m04B21DB0BC22ACA5148EF567BF1C4CA3254401B7,
	MixedRealityInputSimulationProfile_get_SimulateEyePosition_m7311A20140D5CBB130CE7424222144B61D2CCBC3,
	MixedRealityInputSimulationProfile_get_DefaultEyeGazeSimulationMode_m58E5D39C5436C794B6D87D8C45F2C87BDD3507D1,
	MixedRealityInputSimulationProfile_get_DefaultControllerSimulationMode_mBBD8CD522463D7B420C1E89ACAE481B717817693,
	MixedRealityInputSimulationProfile_get_ToggleLeftControllerKey_m743897F7C6EE97EED46E3913AAB412E4455488A5,
	MixedRealityInputSimulationProfile_get_ToggleRightControllerKey_m4BE85987AECC733E95C8273810E8F4C75BE3B76B,
	MixedRealityInputSimulationProfile_get_ControllerHideTimeout_m865984206D3C433C4E81193F7200FE472E75A0A3,
	MixedRealityInputSimulationProfile_get_LeftControllerManipulationKey_m0C961622AB093740BD92AC1708F651B4E08B7377,
	MixedRealityInputSimulationProfile_get_RightControllerManipulationKey_m6B61D1328042D597970CF437387C2EAFD8778EDD,
	MixedRealityInputSimulationProfile_get_MouseControllerRotationSpeed_mCD7DAE3FB4EF4575C53FF01C8C91B4A3A829A3E1,
	MixedRealityInputSimulationProfile_get_ControllerRotateButton_mA01DA5CA2A13CAC5E284675C4EF31BFB8DD29273,
	MixedRealityInputSimulationProfile_get_DefaultHandGesture_m564378BD199E2905DCB5E744E716801888BD076B,
	MixedRealityInputSimulationProfile_get_LeftMouseHandGesture_m1DF353F9FEBA7AE0CDB71B37064EA4AFE13C5BD1,
	MixedRealityInputSimulationProfile_get_MiddleMouseHandGesture_mD555821DAE4CB679C619CDA6734738FDCEE88760,
	MixedRealityInputSimulationProfile_get_RightMouseHandGesture_mF90A25BEB12D1FBB6D1CBD0F85F5B3BED6A05ED6,
	MixedRealityInputSimulationProfile_get_HandGestureAnimationSpeed_mE53836908FB17E9D3919A3C410A486BE4A2B24E4,
	MixedRealityInputSimulationProfile_get_HoldStartDuration_mA17CB73B9856509192B0FEC381E4D9ED04520369,
	MixedRealityInputSimulationProfile_get_NavigationStartThreshold_mD0691521CE6CDE700EDB283F7EE3CCA8A8F5E70E,
	MixedRealityInputSimulationProfile_get_DefaultControllerDistance_m0D5F59DB4F0D8DFF042FF4D2E4CFE83F031EEA8E,
	MixedRealityInputSimulationProfile_get_ControllerDepthMultiplier_mCCB8B1C583DAF08368A56959FD862E39EC7C20AE,
	MixedRealityInputSimulationProfile_get_ControllerJitterAmount_m53797E798206619D38D1D266C1EC738B4CE00E01,
	MixedRealityInputSimulationProfile_get_MotionControllerTriggerKey_mC7202B9DC85EC6DF25495E20E59E04B6C7C19BB7,
	MixedRealityInputSimulationProfile_get_MotionControllerGrabKey_m8A4252C164FA8588C7454D71BF8B4C6F86F70A3C,
	MixedRealityInputSimulationProfile_get_MotionControllerMenuKey_m752FD34294E38426BE92E4B9CE288FB70A8A5535,
	MixedRealityInputSimulationProfile_get_DefaultHandSimulationMode_mE3A316D1EE705D6B2FDB570B09ED526EB4338872,
	MixedRealityInputSimulationProfile_get_ToggleLeftHandKey_m2D2A7022E31F9B45314E704339EAFF19E0E3A232,
	MixedRealityInputSimulationProfile_get_ToggleRightHandKey_mF850FBADB8FEE781C7196D5B2A44B07CCA51A096,
	MixedRealityInputSimulationProfile_get_HandHideTimeout_m0DEEB2EB6BD6BC5FCDE9484BDD9DC392251D01C4,
	MixedRealityInputSimulationProfile_get_LeftHandManipulationKey_m3413D11CF5699A6B1D42C12B8F9ECDB19A6AFFC0,
	MixedRealityInputSimulationProfile_get_RightHandManipulationKey_m7F77E67BA0EC453F4112B14C8A5A05F6E1456194,
	MixedRealityInputSimulationProfile_get_MouseHandRotationSpeed_m46EFE0618F1900C9C1C21015E28CFABE157B7CE1,
	MixedRealityInputSimulationProfile_get_HandRotateButton_mFE0B2953B630D0A333FCCB96317AE0D94D9CE00B,
	MixedRealityInputSimulationProfile_get_DefaultHandDistance_m78281550082D3C5162AC4C3E0D1D6CF7E26641CA,
	MixedRealityInputSimulationProfile_get_HandDepthMultiplier_m80FF121A4F725E6AD0979F46EE705E4B67497A05,
	MixedRealityInputSimulationProfile_get_HandJitterAmount_mD118BC566C9B351E1DFE8A89D556CBD159B74512,
	MixedRealityInputSimulationProfile__ctor_m157F7156089E96E17AEE2EADB380C13FFAC9BF11,
	MouseRotationProvider_get_IsRotating_m3A38A6B6D95AC6C33ABBFD9D1D59AB20BA863BA9,
	MouseRotationProvider_Update_m16E586538696BF801DBD90988947FAD755C58407,
	MouseRotationProvider_OnStartRotating_mFF7967B266C95EB64DC478408C60D701DF99D25F,
	MouseRotationProvider_OnEndRotating_m976F7D5CAB2870088B1C9659D596AD78E5587D7E,
	MouseRotationProvider_SetWantsMouseJumping_m56D1E5003D076C91B709965A25D2424670F58A15,
	MouseRotationProvider__ctor_mDB741380CEDFA9256E50945178D9636F25D20D59,
	MouseRotationProvider__cctor_m25093073CAA61112C0B570984F622FF1CCFD2F2A,
	SimulatedArticulatedHand_get_SimulationMode_mBBEBDD6302C2BF478BED49801CFB4662A49CFE52,
	SimulatedArticulatedHand__ctor_mC11D60B9980358338E8A88F45813B61FEAEF8F4B,
	SimulatedArticulatedHand_get_DefaultInteractions_m61AAC02EDC45014DCCF84454B269E5CED05BB07E,
	SimulatedArticulatedHand_UpdateInteractions_mA188894B683F47C8E6146E944F12FB6173C293DD,
	SimulatedArticulatedHandPoses_GetGesturePose_mE3DBC4AA1F37232EBCC1C7C4ECD669597ABFA8AE,
	SimulatedArticulatedHandPoses_SetGesturePose_m4D74EBFD2FB2AB3FF816E4F32E3E34CB62F0088B,
	SimulatedArticulatedHandPoses_LoadDefaultGesturePoses_m09E0520A0B0425F6C75801ECEEFB50D1A18AD6A8,
	SimulatedArticulatedHandPoses__cctor_m87F894D9972BA341A57F7B6A39E078474DC83A5E,
	SimulatedControllerState_get_Handedness_m46C3951983F1995D748F0FBC88B07824CAAC6FC6,
	SimulatedControllerState__ctor_m56FC25A09678F68D23088ADD40782FF23C2A76A7,
	SimulatedControllerState_SimulateInput_m7AD9F72748CA315743863A953DB690181E5F879F,
	SimulatedControllerState_ResetPosition_m59000E30DAA3EA12C67F2D3FEFF919A3EB01F813,
	NULL,
	SimulatedControllerState_Update_mE9D02CF11449E7BFA88D559905A097ADFB665346,
	SimulatedControllerDataProvider_get_isSimulatingGaze_m2DAA86B106D0B6D6980A1D21CBE2051889DD75EA,
	SimulatedControllerDataProvider_get_IsSimulatingLeft_mEC1D5FBA42D5828A1AFBD511C97EA59EA4AA5C1E,
	SimulatedControllerDataProvider_set_IsSimulatingLeft_m2737D03870D76115049925E2FE51ACF3FA9E5BDF,
	SimulatedControllerDataProvider_get_IsSimulatingRight_mDD8DE7C92F368CBFC129407969EED68ED82CA6A9,
	SimulatedControllerDataProvider_set_IsSimulatingRight_m571C0EDD4EEFC1C30049E9D9F9570AD6FE673547,
	SimulatedControllerDataProvider__ctor_m1F9C770683E39974146F48C8B2958E4F4931207D,
	SimulatedControllerDataProvider_SimulateUserInput_m12DC4B114C85C30FA9BB34513237E0BC5402762D,
	NULL,
	SimulatedControllerDataProvider_ResetInput_m7541D0993321A3AE32D820778D97EF05EFB9CA4E,
	SimulatedControllerDataProvider_ResetInput_m7FAF6A7B47BC4583E65B8FB23A51339596AE5CC1,
	SimulatedControllerDataProvider__cctor_m7A9A8EDD1D4F46CF785536C68F3966F88ABDE109,
	SimulatedGestureHand_get_SimulationMode_m41F11F147B7019064414595EC4ED63F439866AB0,
	SimulatedGestureHand_get_NavigationDelta_mA8CB35243425C4B145F87EB5357F06EEC6C97639,
	SimulatedGestureHand__ctor_m95276B6DDB55268C6A15D70F71B204FE112C3ED8,
	SimulatedGestureHand_EnsureProfileSettings_m918249A2BA9F95008F34F300954269600E8F16AA,
	SimulatedGestureHand_get_DefaultInteractions_m7EA86C9A8C9BA003C343E8AA710E9A5A914D1A59,
	SimulatedGestureHand_UpdateInteractions_mEA39240329977226BD1F4903037A75E37489B27F,
	SimulatedGestureHand_TryStartHold_m88137D050C4A12181111A45C48C719C106D43625,
	SimulatedGestureHand_TryCompleteHold_mBF67C25ED325608CCA2F802CD98D2542D4B6D5AE,
	SimulatedGestureHand_TryCancelHold_m38FB29B8D69C05D2059C126AB099E3B151E88DFD,
	SimulatedGestureHand_TryStartManipulation_m05724BF0BD0601B4255D77583EF8A5E5914ACD95,
	SimulatedGestureHand_UpdateManipulation_m745633A22C7633A7B26013E5FA80BF3464831EEA,
	SimulatedGestureHand_TryCompleteManipulation_m1052F207737A0DD5B6573D4845504BE479C205DC,
	SimulatedGestureHand_TryCancelManipulation_m1869B76FEFB527BDAE3215DA8C52A75D2D7FA4D6,
	SimulatedGestureHand_TryCompleteSelect_m31871C90BEB9D614E751977C7F83D43DA7BA9C09,
	SimulatedGestureHand_TryStartNavigation_m61BEBBB6FB5E4203B9506D04C660D083EE94867D,
	SimulatedGestureHand_UpdateNavigation_m750BE60979D05EBE01C77C90E40462322B572935,
	SimulatedGestureHand_TryCompleteNavigation_m5D4945DEE7D9EDB87F92E41EB8B5728AEA744B6F,
	SimulatedGestureHand_TryCancelNavigation_m4707A1F7F2CB27D419730EC56EF9AB59AE5D5EDE,
	SimulatedGestureHand_UpdateNavigationRails_m52DBD1706EFC79CE41980078CC08D7CC16514914,
	SimulatedHandData_get_IsTracked_m8B8E107EB194B24177CBB3ED817E2FAF7D81511C,
	SimulatedHandData_get_Joints_m831297E407F681B0BC9D3A26BEF4B24945A0827D,
	SimulatedHandData_get_IsPinching_mE4C687B3590D8876CF9D719196F7F6A8F2A32CD6,
	SimulatedHandData_Copy_mF446B3C9178233C7B00278FF7608082D8AD27E5A,
	SimulatedHandData_Update_mD0EB03CC34D4D55F9C5611C704B211B8DCD84820,
	SimulatedHandData__ctor_mBA5359AE76E1F7A8E04BEDAA9DBED6A3D7FB8ED7,
	SimulatedHandData__cctor_mF3A9FD43E83AFCAF40B65C56EA304BADB0254D29,
	HandJointDataGenerator__ctor_m81B71E3BF5A0DFBB0305C863ED08ED5C18098A89,
	HandJointDataGenerator_Invoke_m2CE9BD915445F8B6D9A95EB4F9E47C4E589C7579,
	HandJointDataGenerator_BeginInvoke_m913DC3C7E08404E370506F12049611CAB87C8873,
	HandJointDataGenerator_EndInvoke_mCFFC7452265CC7BC63E0E74E8DA58C391B127659,
	NULL,
	SimulatedHand__ctor_mDF127150B4E04F051D7EF4BF5FE3B4EAB933F803,
	SimulatedHand_TryGetJoint_m46705C986BDC9BC060FDC99BCF399470C9BDF234,
	SimulatedHand_UpdateState_m7B261C5737126C9820998380915E0C697DD3568D,
	NULL,
	SimulatedHand__cctor_m191ECB7D227F3E525F9BFDA1C6695EFF0A0A0C1C,
	SimulatedHandState_get_IsPinching_mAAF0661B1D6D9BED569C257A49DFBB8D26FBABAA,
	SimulatedHandState_get_Gesture_mF578657C921D4644519E270D90D04EAE48D43374,
	SimulatedHandState_set_Gesture_mEBFEF75E14FB2E157FCA7D5A8F34DE15C808B0CE,
	SimulatedHandState_get_GestureBlending_m83CC9BAB523F22A0B50A561E8F5DE10D666C60A2,
	SimulatedHandState_set_GestureBlending_m96C35208AF74C83C7B4E5BB880687BF491FC6C5C,
	SimulatedHandState__ctor_m708A7963488340DAB68F30DBB4B246DA1FE7F89A,
	SimulatedHandState_ResetGesture_mF5E16D7B6471E0EE555B96BDF542E114CF8EBD1A,
	SimulatedHandState_ResetRotation_m463FA1DABE4E2117F1D50A6D6AB3C78C570AADD3,
	SimulatedHandState_FillCurrentFrame_m166C748751310B276B46A6FE1CF75F270F66A207,
	SimulatedHandDataProvider__ctor_m759A7F51E7EEE81CEACFD03AEEAD1891A9BE0912,
	SimulatedHandDataProvider_UpdateHandData_mAD23FCEC7AE0967FE6A6368410817B5847EB0C18,
	SimulatedHandDataProvider_SimulateUserInput_m088B9E694FA18B8343582BE439CDF1A901634176,
	SimulatedHandDataProvider_SimulateInput_m047048C1FD5C298AE91FC8B2F21F40CEF7BFAF2A,
	SimulatedHandDataProvider_ResetInput_m913821A6C9337C4F8E747C532955D246A2658BBD,
	SimulatedHandDataProvider_SelectGesture_m549B8B820F980DEDE39977B4F93DD0B081848FA9,
	SimulatedHandDataProvider_ToggleGesture_mFF8F87E941E47770D90AB9D3930663059B4ECBC2,
	SimulatedHandDataProvider_get_HandStateLeft_m91D75464AC108266DF529EBA8FC43D0CE45844F8,
	SimulatedHandDataProvider_set_HandStateLeft_m4C964F28810FD73AC735BD76BA2F13765BCE7909,
	SimulatedHandDataProvider_get_HandStateRight_m5591B296B3BB6E71C85761BD7550D0741A783D66,
	SimulatedHandDataProvider_set_HandStateRight_m567357AAB6E9831E06A465BF9B06D955410AE5E3,
	SimulatedHandDataProvider_get_HandStateGaze_m731A75BC5C853F4AA4CD01D7F1350071EC11518A,
	SimulatedHandDataProvider_set_HandStateGaze_mAD4DA8E5A626FBAED0D45648EB54F47716F69FC0,
	SimulatedHandUtils_CalculateJointRotations_m4EF645221FB34CED93C109FA496F214C6F056FD4,
	SimulatedHandUtils_GetPalmForwardVector_m691C67DE0AEA1712108FF6720DAC920D5556E0D6,
	SimulatedHandUtils_GetPalmUpVector_mE0CBC30A4DF61063AE67A6F0ACD34F8B4BF101BB,
	SimulatedHandUtils_GetPalmRightVector_m5F4F8343CD86FEB7A13789016CEADBF56D938B06,
	SimulatedHandUtils__ctor_mD378A3CAF1AA2D3806599507D1D85CEBCC865C9D,
	SimulatedHandUtils__cctor_m67E8189F6A9AB996DF5472D91ED4C2A6435E78F1,
	SimulatedMotionControllerData_get_IsTracked_m6884EE6590E582A3883E92492831A3FAC88C6313,
	SimulatedMotionControllerData_get_ButtonState_m20C87E9B843BD63E1B0452D70A2C344E11057910,
	SimulatedMotionControllerData_get_Position_m9C154E85706D99112116B944EBD1E7CE8A35523A,
	SimulatedMotionControllerData_set_Position_mBB23C897BC08DED72639063EE497AB5573C0D5CD,
	SimulatedMotionControllerData_get_Rotation_m7DD9A8AFEA6523AC2AA795B7D97B48163FE0EE95,
	SimulatedMotionControllerData_set_Rotation_m7A58CA5F499A8E23211E3D3AE0AAD4A85A3E6EC0,
	SimulatedMotionControllerData_Update_m063F8CE978B472902814F80FF2EB03832DDEBC84,
	SimulatedMotionControllerData__ctor_mDA5BEA2A4EBEE7F76C45F87245A784FD1311D428,
	MotionControllerPoseUpdater__ctor_m88F203AEF0EE969AFA6251B91896FBED4CA4F365,
	MotionControllerPoseUpdater_Invoke_m7CFA23104E339E9B17C851684974374D34784538,
	MotionControllerPoseUpdater_BeginInvoke_mFC57A62A5480A896D5C845C3073BECFCACE11B57,
	MotionControllerPoseUpdater_EndInvoke_m08BE44167D9F904339BA962D477809D1E435B4C8,
	SimulatedMotionController__ctor_mEFDDF50F0018B5E135CECF3C13C13FB760B81D17,
	SimulatedMotionController_get_DefaultInteractions_mE433893F5E3CB010573A31080973C42A6DE4DA51,
	SimulatedMotionController_UpdateState_m9FEA78FC72EFE99682BE10269672E9DD2EFB1C71,
	SimulatedMotionControllerButtonState_Equals_mD347078799A5A8DAEC49169AFCB664D7A1B726DC,
	SimulatedMotionControllerButtonState_Equals_mBE4BBEA32192B61DD8320254C1110073198AC8C1,
	SimulatedMotionControllerButtonState_GetHashCode_mBD209918615EB0B16D224253BF0FF2047C315440,
	SimulatedMotionControllerButtonState_op_Equality_mE5CC94395D714B0687635BF18C3F4EEE2E5C2E0B,
	SimulatedMotionControllerButtonState_op_Inequality_m1C5F4A34FA2F1FF81E230F7CC396DADCF32A9DE3,
	SimulatedMotionControllerState_get_ButtonState_m8B5B7449DE074DCEA5F690146E385B9AC2F64E05,
	SimulatedMotionControllerState_set_ButtonState_m20336CF5A9C8735C978B3C857C2730A38A275410,
	SimulatedMotionControllerState__ctor_m6051CB0EB48FD1888F2BD01300348D728C13B660,
	SimulatedMotionControllerState_ResetRotation_mFC493B2D24F26CD1F548CA7C60DFFDFDA6E10D09,
	SimulatedMotionControllerState_ResetButtonStates_mBA8DAB516836F441B4E47338A6D1B7D36CAB940D,
	SimulatedMotionControllerState_UpdateControllerPose_mF3C67E965F33F01E74563399516A1F454499985C,
	SimulatedMotionControllerDataProvider__ctor_m4A7D7AAF426E7C419EDA91B9A207D83D6C834BD1,
	SimulatedMotionControllerDataProvider_SimulateInput_m5E722B5B8FBDB58DF0EC6BA71EEFF160F089866F,
	SimulatedMotionControllerDataProvider_UpdateControllerData_m78D7635BF2E582CE1F92636D6BA5D18146CE9C79,
	SimulatedMotionControllerDataProvider_ResetInput_mAD01C92BD1B2619E2E8FC8339C96593025F2E4C2,
};
extern void KeyBinding_get_BindingType_m4865FD00DE59B37BAE922C5DFF37C39BB100CF5E_AdjustorThunk (void);
extern void KeyBinding_ToString_m19E5162244657C0CAF1CE542F4C695EA15F6C296_AdjustorThunk (void);
extern void KeyBinding_TryGetKeyCode_mC94C514E2FE9CFC8372A6BD671DFA68DF8336085_AdjustorThunk (void);
extern void KeyBinding_TryGetMouseButton_m0726DFBCCF20C5726E68B5766CAC8F991ECA91D5_AdjustorThunk (void);
extern void KeyBinding_TryGetMouseButton_m83C8B35064098A2CBFA7D634C7C4211C89935B99_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_Equals_mD347078799A5A8DAEC49169AFCB664D7A1B726DC_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_Equals_mBE4BBEA32192B61DD8320254C1110073198AC8C1_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_GetHashCode_mBD209918615EB0B16D224253BF0FF2047C315440_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x060000A8, KeyBinding_get_BindingType_m4865FD00DE59B37BAE922C5DFF37C39BB100CF5E_AdjustorThunk },
	{ 0x060000A9, KeyBinding_ToString_m19E5162244657C0CAF1CE542F4C695EA15F6C296_AdjustorThunk },
	{ 0x060000AA, KeyBinding_TryGetKeyCode_mC94C514E2FE9CFC8372A6BD671DFA68DF8336085_AdjustorThunk },
	{ 0x060000AB, KeyBinding_TryGetMouseButton_m0726DFBCCF20C5726E68B5766CAC8F991ECA91D5_AdjustorThunk },
	{ 0x060000AC, KeyBinding_TryGetMouseButton_m83C8B35064098A2CBFA7D634C7C4211C89935B99_AdjustorThunk },
	{ 0x0600016E, SimulatedMotionControllerButtonState_Equals_mD347078799A5A8DAEC49169AFCB664D7A1B726DC_AdjustorThunk },
	{ 0x0600016F, SimulatedMotionControllerButtonState_Equals_mBE4BBEA32192B61DD8320254C1110073198AC8C1_AdjustorThunk },
	{ 0x06000170, SimulatedMotionControllerButtonState_GetHashCode_mBD209918615EB0B16D224253BF0FF2047C315440_AdjustorThunk },
};
static const int32_t s_InvokerIndices[380] = 
{
	4406,
	4406,
	4406,
	4326,
	330,
	834,
	1189,
	2850,
	1529,
	3670,
	4406,
	4406,
	1189,
	2850,
	1529,
	3670,
	4406,
	4326,
	4295,
	3604,
	4295,
	3604,
	4326,
	4326,
	4326,
	4326,
	4364,
	3670,
	4364,
	4364,
	4364,
	3670,
	4364,
	3670,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4406,
	4406,
	4295,
	3604,
	4364,
	4364,
	4364,
	3670,
	4364,
	3670,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4406,
	4406,
	4326,
	3635,
	4364,
	4371,
	3676,
	4406,
	4406,
	4406,
	3176,
	4326,
	4326,
	330,
	834,
	3144,
	4326,
	3635,
	4371,
	4364,
	4371,
	3676,
	4406,
	4406,
	4406,
	4406,
	3670,
	4406,
	2174,
	3176,
	6303,
	4406,
	3635,
	4406,
	4406,
	4295,
	3604,
	4326,
	4326,
	4326,
	4326,
	4326,
	4364,
	4364,
	4364,
	3670,
	4364,
	3670,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4406,
	4406,
	4364,
	3670,
	4295,
	3604,
	4364,
	3670,
	330,
	834,
	3144,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4326,
	3635,
	4326,
	4364,
	3670,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	3367,
	3366,
	3366,
	4295,
	3604,
	4364,
	4364,
	4364,
	3670,
	4364,
	3670,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4401,
	3706,
	4406,
	4406,
	6303,
	4295,
	4326,
	3070,
	3070,
	3070,
	6271,
	6045,
	6045,
	6045,
	4406,
	2003,
	6290,
	6303,
	6303,
	6303,
	6216,
	6216,
	6303,
	6154,
	6154,
	6154,
	3635,
	6182,
	3635,
	2172,
	5772,
	3370,
	3370,
	6303,
	4326,
	4371,
	4326,
	4326,
	4326,
	4302,
	4371,
	4364,
	4364,
	4371,
	4302,
	4364,
	4364,
	4401,
	4295,
	4302,
	4371,
	4371,
	4326,
	4326,
	4326,
	4326,
	4326,
	4364,
	4295,
	4295,
	4302,
	4302,
	4371,
	4302,
	4302,
	4371,
	4302,
	4295,
	4295,
	4295,
	4295,
	4371,
	4371,
	4371,
	4371,
	4371,
	4371,
	4302,
	4302,
	4302,
	4295,
	4302,
	4302,
	4371,
	4302,
	4302,
	4371,
	4302,
	4371,
	4371,
	4371,
	4406,
	4364,
	1202,
	6216,
	6216,
	6221,
	4406,
	6303,
	4295,
	800,
	4326,
	3635,
	6102,
	5851,
	6303,
	6303,
	4364,
	3670,
	337,
	3706,
	4406,
	4406,
	4364,
	4364,
	3670,
	4364,
	3670,
	3635,
	3635,
	168,
	3670,
	2174,
	6303,
	4295,
	4401,
	800,
	4406,
	4326,
	3635,
	4364,
	4364,
	4364,
	4364,
	4406,
	4364,
	4364,
	4364,
	4364,
	4406,
	4364,
	4364,
	4406,
	4364,
	4326,
	4364,
	3635,
	1129,
	4406,
	6303,
	2169,
	3635,
	1003,
	3635,
	4295,
	800,
	1667,
	3635,
	3635,
	6303,
	4364,
	4295,
	3604,
	4371,
	3676,
	3670,
	4406,
	4406,
	3635,
	3635,
	747,
	3635,
	168,
	2174,
	4295,
	2630,
	4326,
	3635,
	4326,
	3635,
	4326,
	3635,
	5490,
	6197,
	5813,
	5813,
	4406,
	6303,
	4364,
	4370,
	4401,
	3706,
	4339,
	3650,
	1130,
	4406,
	2169,
	4321,
	1524,
	2821,
	800,
	4326,
	3635,
	3176,
	3219,
	4295,
	5745,
	5745,
	4370,
	3675,
	3670,
	4406,
	4406,
	4321,
	3635,
	168,
	1238,
	2174,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_InputSimulation_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSimulation_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSimulation_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.InputSimulation.dll",
	380,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Services_InputSimulation_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
