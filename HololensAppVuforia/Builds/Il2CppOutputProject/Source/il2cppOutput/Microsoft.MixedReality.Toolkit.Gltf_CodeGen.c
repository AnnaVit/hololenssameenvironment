﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::get_Model()
extern void GltfAsset_get_Model_m0EEB65F75A0382F5FEE4C128E8D4FC2279362E57 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::set_Model(UnityEngine.GameObject)
extern void GltfAsset_set_Model_m9FAED845DCCCAD5150A853E235699F6A4988C7CC (void);
// 0x00000003 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::get_GltfObject()
extern void GltfAsset_get_GltfObject_m93DAFC6A3D2F65D2DD53F6DEBF3B9C35EF2D54E6 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::set_GltfObject(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void GltfAsset_set_GltfObject_mF442883BEC9B6E6A31673452C3C7E425132B7DD1 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::.ctor()
extern void GltfAsset__ctor_m7E2A448086DB451A81382FFB372B0BF4E4427BCB (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::Construct(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void ConstructGltf_Construct_m7AF376B19D93867D068C7070283A9105C75D2D7C (void);
// 0x00000007 System.Threading.Tasks.Task`1<UnityEngine.GameObject> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void ConstructGltf_ConstructAsync_m975484D3239B0ECD7FC1DBA2585C9D8825375ED1 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructBufferView(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView)
extern void ConstructGltf_ConstructBufferView_mD3430888FB4600BD2C23383AE209CF65D5B0ECAC (void);
// 0x00000009 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructTextureAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture)
extern void ConstructGltf_ConstructTextureAsync_m0AD41CB23C8FDB01E726E696D66CFBB53EB34EC7 (void);
// 0x0000000A System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMaterialAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_ConstructMaterialAsync_mBBF26E2C2B1E3E31056532A04980CE8C75307F7D (void);
// 0x0000000B System.Threading.Tasks.Task`1<UnityEngine.Material> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateMRTKShaderMaterial(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_CreateMRTKShaderMaterial_mB15031444B7BBF9B2CFBFB017A5FF11B88803B52 (void);
// 0x0000000C System.Threading.Tasks.Task`1<UnityEngine.Material> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateStandardShaderMaterial(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_CreateStandardShaderMaterial_mA98881D421ED3BF1EF06DEC0E132FE0D668758C6 (void);
// 0x0000000D System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructSceneAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfScene,UnityEngine.GameObject)
extern void ConstructGltf_ConstructSceneAsync_m8F37C14372A58396B8D6E34EAF480C6899FE6D6D (void);
// 0x0000000E System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructNodeAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode,System.Int32,UnityEngine.Transform)
extern void ConstructGltf_ConstructNodeAsync_mF82D527D7FB0E5BA869EAF57620CC293463A063D (void);
// 0x0000000F System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMeshAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,UnityEngine.GameObject,System.Int32)
extern void ConstructGltf_ConstructMeshAsync_m423EB9E6373AF0AF3A069BE5F2CFA234F97E16B5 (void);
// 0x00000010 System.Threading.Tasks.Task`1<UnityEngine.Mesh> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMeshPrimitiveAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive)
extern void ConstructGltf_ConstructMeshPrimitiveAsync_mE4B96E500D485F6546F86912ED5FD7C516399463 (void);
// 0x00000011 UnityEngine.BoneWeight[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateBoneWeightArray(UnityEngine.Vector4[],UnityEngine.Vector4[],System.Int32)
extern void ConstructGltf_CreateBoneWeightArray_m837F9B06B6EEC90587F1724CD00A2FDF7EDC74D4 (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::NormalizeBoneWeightArray(UnityEngine.Vector4[])
extern void ConstructGltf_NormalizeBoneWeightArray_m7D3A155C7480BF81FBB6AAD3E182F189528FF752 (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::.cctor()
extern void ConstructGltf__cctor_mFF81A816DA2AE82E55822CAD6A43BD936CFF1962 (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<Construct>d__18::MoveNext()
extern void U3CConstructU3Ed__18_MoveNext_m245CB5F41374271A3C6081AE92F6874D98B8EC32 (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<Construct>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A (void);
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructAsync>d__19::MoveNext()
extern void U3CConstructAsyncU3Ed__19_MoveNext_mF08700731B540D0E376A2F68F6AFA1727E85C8EE (void);
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructAsync>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructTextureAsync>d__21::MoveNext()
extern void U3CConstructTextureAsyncU3Ed__21_MoveNext_m5D0FB162E9746A292527BF6D4ECA72CA00B59EEE (void);
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructTextureAsync>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3 (void);
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMaterialAsync>d__22::MoveNext()
extern void U3CConstructMaterialAsyncU3Ed__22_MoveNext_m2D0A844441E9A198ED982ECDE9EFBA6921CF64DB (void);
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMaterialAsync>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3 (void);
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateMRTKShaderMaterial>d__23::MoveNext()
extern void U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mF7839D1592154510BF77F591CEC84980BAADD950 (void);
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateMRTKShaderMaterial>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateStandardShaderMaterial>d__24::MoveNext()
extern void U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m147AA34FADDB3E629F6D267B66BB9C5F66E64436 (void);
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateStandardShaderMaterial>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D (void);
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructSceneAsync>d__25::MoveNext()
extern void U3CConstructSceneAsyncU3Ed__25_MoveNext_mE3B45AFA3B6493146C2517F8D606C2867D10A3CE (void);
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructSceneAsync>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D (void);
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructNodeAsync>d__26::MoveNext()
extern void U3CConstructNodeAsyncU3Ed__26_MoveNext_m21B676BF91C3AAD335FF663CFA0F2E1D4AE0490B (void);
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructNodeAsync>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4 (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshAsync>d__27::MoveNext()
extern void U3CConstructMeshAsyncU3Ed__27_MoveNext_mFD2EE8133BA30C7EF6E9C34A6BF2081F92857EEC (void);
// 0x00000025 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshAsync>d__27::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C (void);
// 0x00000026 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshPrimitiveAsync>d__28::MoveNext()
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m1369979E70F7F0C20A06EC7D57F7FDFEC675C4E7 (void);
// 0x00000027 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshPrimitiveAsync>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F (void);
// 0x00000028 UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTrsProperties(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void GltfConversions_GetTrsProperties_mF9A1951C1A275E671469A6B2904F806EF2B68025 (void);
// 0x00000029 UnityEngine.Color Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetColorValue(System.Single[])
extern void GltfConversions_GetColorValue_mA8DE199A1E0E8619C609B1B7E9B053DA1C5A9485 (void);
// 0x0000002A System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetColorValue(UnityEngine.Color)
extern void GltfConversions_SetColorValue_mBE5BBBFE9609A1BAD3C96405827DB679A285B027 (void);
// 0x0000002B UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector2Value(System.Single[])
extern void GltfConversions_GetVector2Value_m0EB26D3A83BF2A1E25073D2FB9167D2E301ADD15 (void);
// 0x0000002C System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetVector2Value(UnityEngine.Vector2)
extern void GltfConversions_SetVector2Value_m12C35C5BD06A02AFB687689BF130206BDC1FCADA (void);
// 0x0000002D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector3Value(System.Single[],System.Boolean)
extern void GltfConversions_GetVector3Value_m6CF7A32C1560ACF10D3136F4EAAA477855A6038B (void);
// 0x0000002E System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetVector3Value(UnityEngine.Vector3,System.Boolean)
extern void GltfConversions_SetVector3Value_mD5213CB43BEEA3F3FF50166029B1F74EE91ECBE7 (void);
// 0x0000002F UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetQuaternionValue(System.Single[],System.Boolean)
extern void GltfConversions_GetQuaternionValue_m66B59B6DD5BFE3B7A940B5549E107D6C0A72FC3F (void);
// 0x00000030 System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetQuaternionValue(UnityEngine.Quaternion,System.Boolean)
extern void GltfConversions_SetQuaternionValue_m3E99AB48CFA0534C6F0692FBA4EE9910911330D0 (void);
// 0x00000031 UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetMatrix4X4Value(System.Double[])
extern void GltfConversions_GetMatrix4X4Value_m73C21D9B0361BE428B312A3B3208F9FA6E5FC86D (void);
// 0x00000032 System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetMatrix4X4Value(UnityEngine.Matrix4x4)
extern void GltfConversions_SetMatrix4X4Value_m4AFB5EF4730DF3AAD7449FE549A7EA9A36E65E15 (void);
// 0x00000033 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTrsProperties(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void GltfConversions_GetTrsProperties_mA735690429F6A833A957E4F43FF1C75EA311B443 (void);
// 0x00000034 System.Int32[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetIntArray(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetIntArray_mDE81E177A1373A3F217EF11E2D42E60F36A843B3 (void);
// 0x00000035 UnityEngine.Vector2[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector2Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector2Array_mD2DC81532D69A36F5BC7DD9F499DF39D7725343E (void);
// 0x00000036 UnityEngine.Vector3[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector3Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector3Array_mD4A3CDFD86163EFE8149003B05FF9B3C57FF3D81 (void);
// 0x00000037 UnityEngine.Vector4[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector4Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector4Array_m51609A4B6E68E106C92D189C54D8BAAA6E34823B (void);
// 0x00000038 UnityEngine.Color[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetColorArray(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor)
extern void GltfConversions_GetColorArray_m520810F7A52EA5CC593D3F74C925C511304BD1A9 (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTypeDetails(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType,System.Int32&,System.Single&)
extern void GltfConversions_GetTypeDetails_mD88D20B21B07B14EE8FC213BB7725C41247D79E3 (void);
// 0x0000003A System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetDiscreteElement(System.Byte[],System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfConversions_GetDiscreteElement_mC7877141EB7B3E47E31B4C3F6AA73191EBA67C3A (void);
// 0x0000003B System.UInt32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetDiscreteUnsignedElement(System.Byte[],System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfConversions_GetDiscreteUnsignedElement_m2995233EE3982E932671DD1E33B68D20CD4FB71C (void);
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::.cctor()
extern void GltfConversions__cctor_m20C27DD35893F7C906CA955FD596232C598B896E (void);
// 0x0000003D System.Threading.Tasks.Task`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::ImportGltfObjectFromPathAsync(System.String)
extern void GltfUtility_ImportGltfObjectFromPathAsync_m3F1F281E883DC4B6BD5B8588ED833FD8E3CB8A74 (void);
// 0x0000003E Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfObjectFromJson(System.String)
extern void GltfUtility_GetGltfObjectFromJson_m52D43312EAE9010FC8D47BCBE2C95020B5A05A49 (void);
// 0x0000003F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfObjectFromGlb(System.Byte[])
extern void GltfUtility_GetGltfObjectFromGlb_m72080FB7D1B2483D0C1E7490E25AFF20452FD42F (void);
// 0x00000040 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetJsonObject(System.String,System.String)
extern void GltfUtility_GetJsonObject_mB2354AC310894EC09CF06202FFF898423C11925F (void);
// 0x00000041 System.Collections.Generic.List`1<System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfMeshPrimitiveAttributes(System.String)
extern void GltfUtility_GetGltfMeshPrimitiveAttributes_m0A7D4957258FC3E04CADE81651F245E39C3C2E95 (void);
// 0x00000042 System.Collections.Generic.List`1<System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfMeshPrimitiveAttributes(System.String,System.Text.RegularExpressions.Regex)
extern void GltfUtility_GetGltfMeshPrimitiveAttributes_mE02C7DA1CE33982ECB497738B3E72C305052973A (void);
// 0x00000043 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtensionObjects(System.String,System.String)
extern void GltfUtility_GetGltfExtensionObjects_m5B16C1462DABCED24175761477970B0C4C3A2BEB (void);
// 0x00000044 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtraObjects(System.String,System.String)
extern void GltfUtility_GetGltfExtraObjects_m8B7F5603BBEA0ADF53E4F5890372801800044E16 (void);
// 0x00000045 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtensions(System.String,System.Text.RegularExpressions.Regex)
extern void GltfUtility_GetGltfExtensions_mE10E37EA7E69B686AF546BD44B824FED47866B7F (void);
// 0x00000046 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetJsonObject(System.String,System.Int32)
extern void GltfUtility_GetJsonObject_m44B34164AE6A2DCF757102EDCD2242F2DA3A4A7F (void);
// 0x00000047 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfNodeName(System.String)
extern void GltfUtility_GetGltfNodeName_m4F69B213CEF1E7EB2146E459B56063E5B0024B82 (void);
// 0x00000048 System.Collections.Generic.Dictionary`2<System.String,System.Int32> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::StringIntDictionaryFromJson(System.String)
extern void GltfUtility_StringIntDictionaryFromJson_mCAAFEB6738615CF719A55060F5EE738E03223CCA (void);
// 0x00000049 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::JsonDictionaryToArray(System.String)
extern void GltfUtility_JsonDictionaryToArray_m92AE40A584CC17938219B95D2E2B632A24E4A41F (void);
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::.cctor()
extern void GltfUtility__cctor_m58658EF87ABBEDDE69F6CB0DB7F7AA55470E11BA (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/StringKeyValue::.ctor()
extern void StringKeyValue__ctor_m45F1F6BE5216ED7EB58B63D0205E7BF5589B64D6 (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/StringIntKeyValueArray::.ctor()
extern void StringIntKeyValueArray__ctor_mCB2E1185BB648F171E707AF0276825551106B356 (void);
// 0x0000004D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/<ImportGltfObjectFromPathAsync>d__4::MoveNext()
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_mF311DC416E7D3ECED94E034D843532C2778A8C0F (void);
// 0x0000004E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/<ImportGltfObjectFromPathAsync>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097 (void);
// 0x0000004F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::get_BufferView()
extern void GltfAccessor_get_BufferView_mBF991561A98B0BB5FD0E3A655D5E8616961DBF93 (void);
// 0x00000050 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::set_BufferView(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView)
extern void GltfAccessor_set_BufferView_mA1BE80D7B2D106A658A1292327A86C65FF53E4E5 (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::.ctor()
extern void GltfAccessor__ctor_mCCE33A115F791DB6BFB69C52D8CD3E5E9C143904 (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparse::.ctor()
extern void GltfAccessorSparse__ctor_m77979024088AB47E71DCF650C474ACC522EC0CC3 (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::.ctor()
extern void GltfAccessorSparseIndices__ctor_m320598C1295C386E39D29EEDDD23A9593E7E479D (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseValues::.ctor()
extern void GltfAccessorSparseValues__ctor_mB5505869103BD63D3DC8FE2A4BF955AB9B3DCFF6 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimation::.ctor()
extern void GltfAnimation__ctor_m844022C2F66F8A9AB76C088EEF5F14387F4C6062 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannel::.ctor()
extern void GltfAnimationChannel__ctor_m7A23642184A30691D9183FBBAE1908FF286611F9 (void);
// 0x00000057 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::.ctor()
extern void GltfAnimationChannelTarget__ctor_m02CE15840D03520EBDCB56AE3E17D310B1632EA0 (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::.ctor()
extern void GltfAnimationSampler__ctor_mD856E3AA62E5AE489C2F43B56BDF0ABEAE783D7C (void);
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAssetInfo::.ctor()
extern void GltfAssetInfo__ctor_mB144474A6F1C90EB2348DA0B661C49E37AD3BE47 (void);
// 0x0000005A System.Byte[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::get_BufferData()
extern void GltfBuffer_get_BufferData_m5FBC24C211FFC93370C4CA56C07809DA7DF59257 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::set_BufferData(System.Byte[])
extern void GltfBuffer_set_BufferData_mB3E1712AF73DCFEF5407692BAACD1FBD1384DC41 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::.ctor()
extern void GltfBuffer__ctor_m651C75471E97180EAE30FFA0F7A4FE4A6A99579C (void);
// 0x0000005D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::get_Buffer()
extern void GltfBufferView_get_Buffer_m47253FD5C18EC856A56348EC0B9855C42B0A9EE0 (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::set_Buffer(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer)
extern void GltfBufferView_set_Buffer_m3CF4046017E0B5AFE7496913FD3FCA25C962EF9A (void);
// 0x0000005F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::.ctor()
extern void GltfBufferView__ctor_mE94B77D319F5E04B069765D251933375CB7AC591 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::.ctor()
extern void GltfCamera__ctor_mD426973F5FB04C36B2437EC5AB504156EF0F2670 (void);
// 0x00000061 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraOrthographic::.ctor()
extern void GltfCameraOrthographic__ctor_mA636B9B7A7052134E154DBE585C264D35F98E3C3 (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraPerspective::.ctor()
extern void GltfCameraPerspective__ctor_m36B921EEF1C0078FEDF209D12F3CCB4CE544298E (void);
// 0x00000063 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfChildOfRootProperty::.ctor()
extern void GltfChildOfRootProperty__ctor_mB5338B3E23EB6600BC95FBD669A0BBCBD572CE56 (void);
// 0x00000064 UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::get_Texture()
extern void GltfImage_get_Texture_mD779E699883558093A72BCA8A13B77149310DDBD (void);
// 0x00000065 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::set_Texture(UnityEngine.Texture2D)
extern void GltfImage_set_Texture_mEE09E29892BC1EB99FE4F516C6A79ADB2801061A (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::.ctor()
extern void GltfImage__ctor_m4726027B019425653A1C8ACB26589B7C2594DFBC (void);
// 0x00000067 UnityEngine.Material Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::get_Material()
extern void GltfMaterial_get_Material_mFA2E7C0146232BED65AFA5E64EB1F01388141C20 (void);
// 0x00000068 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::set_Material(UnityEngine.Material)
extern void GltfMaterial_set_Material_mBB2F569AFBBCD2A8F7BB8AF6EC002CDCA0456B7B (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::.ctor()
extern void GltfMaterial__ctor_mF9208CFE8B5C498F418B72CFBB5A3A95C8AFDC44 (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterialCommonConstant::.ctor()
extern void GltfMaterialCommonConstant__ctor_mEE6A5EA764BEC71FF7AB810D3FBF87E552805863 (void);
// 0x0000006B UnityEngine.Mesh Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::get_Mesh()
extern void GltfMesh_get_Mesh_mDEE88954BB0F1F79F13A090C0FFCCF604F1836E7 (void);
// 0x0000006C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::set_Mesh(UnityEngine.Mesh)
extern void GltfMesh_set_Mesh_m21306B466FCECF334B5CCC1010AE69F2644D6955 (void);
// 0x0000006D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::.ctor()
extern void GltfMesh__ctor_m26F2DC3DD72BFC565EB0D32479B34EA0BBC105A2 (void);
// 0x0000006E System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Int32>> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_Targets()
extern void GltfMeshPrimitive_get_Targets_m1C5161E796B842241E9220E5CDF6FE3C7730321A (void);
// 0x0000006F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_Targets(System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Int32>>)
extern void GltfMeshPrimitive_set_Targets_m47A1CF3F7C5812BA6622E06E983D6C6247A56427 (void);
// 0x00000070 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_Attributes()
extern void GltfMeshPrimitive_get_Attributes_m937303E06650F7DA0F1DEC3DA0BAF7A65C7D32E9 (void);
// 0x00000071 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_Attributes(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes)
extern void GltfMeshPrimitive_set_Attributes_m55351ED659CB203206754110C2E2EE96F8FA32FA (void);
// 0x00000072 UnityEngine.Mesh Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_SubMesh()
extern void GltfMeshPrimitive_get_SubMesh_mFC5DBA13B54CC0970537C50F5E63AD9588906EEF (void);
// 0x00000073 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_SubMesh(UnityEngine.Mesh)
extern void GltfMeshPrimitive_set_SubMesh_mF33518F7D3AC143DF21ACA3BA58BC88F27C58795 (void);
// 0x00000074 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::.ctor()
extern void GltfMeshPrimitive__ctor_mC5379DF50E3B74C2F65CD3A4ECD5BA48D1CEFD03 (void);
// 0x00000075 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Int32>)
extern void GltfMeshPrimitiveAttributes__ctor_m4091BE30DE171D21B52D7FBA594A16287786C19C (void);
// 0x00000076 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::TryGetDefault(System.String,System.Int32)
extern void GltfMeshPrimitiveAttributes_TryGetDefault_m9BDFED10783734F8E67D546A2EB0CF4C90CD5892 (void);
// 0x00000077 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_POSITION()
extern void GltfMeshPrimitiveAttributes_get_POSITION_m5913C86E735FA4278C385108B4128163630ABA52 (void);
// 0x00000078 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_NORMAL()
extern void GltfMeshPrimitiveAttributes_get_NORMAL_mBCCD4E51D477BC7A3413E278723CBDCAE570DC1D (void);
// 0x00000079 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_0()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_0_mAAABFF295B94B889928FFCCDBBC72BD49156137F (void);
// 0x0000007A System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_1()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_1_mA7383BECF396AB6FD912F8D13ED4D0C55A7CA4D6 (void);
// 0x0000007B System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_2()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_2_m5CCCBC040F66312BA33EB7B507EFF2D290458A04 (void);
// 0x0000007C System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_3()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_3_m0BFBB9EED9A7063FC002629EFD3CF9E19457A31F (void);
// 0x0000007D System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_COLOR_0()
extern void GltfMeshPrimitiveAttributes_get_COLOR_0_m3578B4E478BE41DDEADC1BF2ACE4B07D669FC2D7 (void);
// 0x0000007E System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TANGENT()
extern void GltfMeshPrimitiveAttributes_get_TANGENT_mAEBDBDEE53A5246AD636A9EB57E5E24976E46634 (void);
// 0x0000007F System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_WEIGHTS_0()
extern void GltfMeshPrimitiveAttributes_get_WEIGHTS_0_mD58B5E52621F1A6C2A8330C40FE67EFEB012A58D (void);
// 0x00000080 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_JOINTS_0()
extern void GltfMeshPrimitiveAttributes_get_JOINTS_0_m1747B821876AC421F9ABC0D90E8935AAD66B789C (void);
// 0x00000081 UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::get_Matrix()
extern void GltfNode_get_Matrix_m1B810ECC2DC3F1D93783DC05A31EBA8AB578E3C5 (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::set_Matrix(UnityEngine.Matrix4x4)
extern void GltfNode_set_Matrix_m3248A545B8361419A301F284ACF9BBA135F85051 (void);
// 0x00000083 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::.ctor()
extern void GltfNode__ctor_m61057DF0B77B922B9B05702B7C68073E13828E9E (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNormalTextureInfo::.ctor()
extern void GltfNormalTextureInfo__ctor_m62DB88F06ED2B079B3CD40C2A32DAC35F6A4DB0A (void);
// 0x00000085 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_Name()
extern void GltfObject_get_Name_m6EEDCBC4E03E20908BCAD19D627DB2E707766178 (void);
// 0x00000086 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_Name(System.String)
extern void GltfObject_set_Name_m47895CAC77FC47B9000CFDCBAECE315E37444B4A (void);
// 0x00000087 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_Uri()
extern void GltfObject_get_Uri_m63C0AA0B7A6CF31A2EFD26C8A6645F4E37B059F3 (void);
// 0x00000088 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_Uri(System.String)
extern void GltfObject_set_Uri_m735F5DA5B8E21AF02E397BCC7995D8A9468E9D41 (void);
// 0x00000089 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_GameObjectReference()
extern void GltfObject_get_GameObjectReference_m2B3785589D407870E1EDF983FA2EADC74121CB19 (void);
// 0x0000008A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_GameObjectReference(UnityEngine.GameObject)
extern void GltfObject_set_GameObjectReference_m006D03F1F66EA53E5FBD779059C4AEFD5FD0308A (void);
// 0x0000008B System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_RegisteredExtensions()
extern void GltfObject_get_RegisteredExtensions_m429D2F362F9FADA1237D3B571A03BD432503F507 (void);
// 0x0000008C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_RegisteredExtensions(System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension>)
extern void GltfObject_set_RegisteredExtensions_mF87255C41D50B99E1FE43201F1E3796C6376E976 (void);
// 0x0000008D System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_UseBackgroundThread()
extern void GltfObject_get_UseBackgroundThread_m921E5B1288396728EAB6AA773BAB9B07E09B1AEF (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_UseBackgroundThread(System.Boolean)
extern void GltfObject_set_UseBackgroundThread_mCD113787FF499BD925D6B5543F0340DFB8AA36A7 (void);
// 0x0000008F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::GetAccessor(System.Int32)
extern void GltfObject_GetAccessor_mED951A69BDB4C8E3450D5D3CF657F2FD9B52E133 (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::.ctor()
extern void GltfObject__ctor_m89A967DB8CBF8F9B7C4DA67453B515678246F694 (void);
// 0x00000091 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfOcclusionTextureInfo::.ctor()
extern void GltfOcclusionTextureInfo__ctor_mF8E71A6B7D80DB33B724FE7DC00F615AD08EAEFA (void);
// 0x00000092 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfPbrMetallicRoughness::.ctor()
extern void GltfPbrMetallicRoughness__ctor_mE67BE46E07C7CB43C7894C78694E682835521435 (void);
// 0x00000093 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfProperty::.ctor()
extern void GltfProperty__ctor_m39A491CE9073387E57446DAAE7B09EAE671C6A88 (void);
// 0x00000094 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::.ctor()
extern void GltfSampler__ctor_mD1CBD10E582CC874D88BFD197C31572C00F78696 (void);
// 0x00000095 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfScene::.ctor()
extern void GltfScene__ctor_m3860DB75050ABC6EF5E09D63FFF1554604C0B33D (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSkin::.ctor()
extern void GltfSkin__ctor_mD23C21FF4D23229F9909376935604EB1600548F8 (void);
// 0x00000097 UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::get_Texture()
extern void GltfTexture_get_Texture_m95F363CA33062CDB544D6131F824ED6C2D59F69B (void);
// 0x00000098 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::set_Texture(UnityEngine.Texture2D)
extern void GltfTexture_set_Texture_mD812992D23C0E4FEB265F281EC31C14523BF5EB2 (void);
// 0x00000099 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::.ctor()
extern void GltfTexture__ctor_mA66CBD7D8639F2E3C28D7771ACFC6C0DB1E6D50E (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTextureInfo::.ctor()
extern void GltfTextureInfo__ctor_mD78DB2C898E69E1E1E09B4384016327CC13234EB (void);
// 0x0000009B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension::.ctor()
extern void GltfExtension__ctor_mE94C25127141B718F640D17D1C80170C070CFAF5 (void);
// 0x0000009C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.KHR_Materials_PbrSpecularGlossiness::.ctor()
extern void KHR_Materials_PbrSpecularGlossiness__ctor_mC016AC6361F9868BC15758E60B8EDBAD46DBDE59 (void);
static Il2CppMethodPointer s_methodPointers[156] = 
{
	GltfAsset_get_Model_m0EEB65F75A0382F5FEE4C128E8D4FC2279362E57,
	GltfAsset_set_Model_m9FAED845DCCCAD5150A853E235699F6A4988C7CC,
	GltfAsset_get_GltfObject_m93DAFC6A3D2F65D2DD53F6DEBF3B9C35EF2D54E6,
	GltfAsset_set_GltfObject_mF442883BEC9B6E6A31673452C3C7E425132B7DD1,
	GltfAsset__ctor_m7E2A448086DB451A81382FFB372B0BF4E4427BCB,
	ConstructGltf_Construct_m7AF376B19D93867D068C7070283A9105C75D2D7C,
	ConstructGltf_ConstructAsync_m975484D3239B0ECD7FC1DBA2585C9D8825375ED1,
	ConstructGltf_ConstructBufferView_mD3430888FB4600BD2C23383AE209CF65D5B0ECAC,
	ConstructGltf_ConstructTextureAsync_m0AD41CB23C8FDB01E726E696D66CFBB53EB34EC7,
	ConstructGltf_ConstructMaterialAsync_mBBF26E2C2B1E3E31056532A04980CE8C75307F7D,
	ConstructGltf_CreateMRTKShaderMaterial_mB15031444B7BBF9B2CFBFB017A5FF11B88803B52,
	ConstructGltf_CreateStandardShaderMaterial_mA98881D421ED3BF1EF06DEC0E132FE0D668758C6,
	ConstructGltf_ConstructSceneAsync_m8F37C14372A58396B8D6E34EAF480C6899FE6D6D,
	ConstructGltf_ConstructNodeAsync_mF82D527D7FB0E5BA869EAF57620CC293463A063D,
	ConstructGltf_ConstructMeshAsync_m423EB9E6373AF0AF3A069BE5F2CFA234F97E16B5,
	ConstructGltf_ConstructMeshPrimitiveAsync_mE4B96E500D485F6546F86912ED5FD7C516399463,
	ConstructGltf_CreateBoneWeightArray_m837F9B06B6EEC90587F1724CD00A2FDF7EDC74D4,
	ConstructGltf_NormalizeBoneWeightArray_m7D3A155C7480BF81FBB6AAD3E182F189528FF752,
	ConstructGltf__cctor_mFF81A816DA2AE82E55822CAD6A43BD936CFF1962,
	U3CConstructU3Ed__18_MoveNext_m245CB5F41374271A3C6081AE92F6874D98B8EC32,
	U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A,
	U3CConstructAsyncU3Ed__19_MoveNext_mF08700731B540D0E376A2F68F6AFA1727E85C8EE,
	U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88,
	U3CConstructTextureAsyncU3Ed__21_MoveNext_m5D0FB162E9746A292527BF6D4ECA72CA00B59EEE,
	U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3,
	U3CConstructMaterialAsyncU3Ed__22_MoveNext_m2D0A844441E9A198ED982ECDE9EFBA6921CF64DB,
	U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3,
	U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mF7839D1592154510BF77F591CEC84980BAADD950,
	U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B,
	U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m147AA34FADDB3E629F6D267B66BB9C5F66E64436,
	U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D,
	U3CConstructSceneAsyncU3Ed__25_MoveNext_mE3B45AFA3B6493146C2517F8D606C2867D10A3CE,
	U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D,
	U3CConstructNodeAsyncU3Ed__26_MoveNext_m21B676BF91C3AAD335FF663CFA0F2E1D4AE0490B,
	U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4,
	U3CConstructMeshAsyncU3Ed__27_MoveNext_mFD2EE8133BA30C7EF6E9C34A6BF2081F92857EEC,
	U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m1369979E70F7F0C20A06EC7D57F7FDFEC675C4E7,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F,
	GltfConversions_GetTrsProperties_mF9A1951C1A275E671469A6B2904F806EF2B68025,
	GltfConversions_GetColorValue_mA8DE199A1E0E8619C609B1B7E9B053DA1C5A9485,
	GltfConversions_SetColorValue_mBE5BBBFE9609A1BAD3C96405827DB679A285B027,
	GltfConversions_GetVector2Value_m0EB26D3A83BF2A1E25073D2FB9167D2E301ADD15,
	GltfConversions_SetVector2Value_m12C35C5BD06A02AFB687689BF130206BDC1FCADA,
	GltfConversions_GetVector3Value_m6CF7A32C1560ACF10D3136F4EAAA477855A6038B,
	GltfConversions_SetVector3Value_mD5213CB43BEEA3F3FF50166029B1F74EE91ECBE7,
	GltfConversions_GetQuaternionValue_m66B59B6DD5BFE3B7A940B5549E107D6C0A72FC3F,
	GltfConversions_SetQuaternionValue_m3E99AB48CFA0534C6F0692FBA4EE9910911330D0,
	GltfConversions_GetMatrix4X4Value_m73C21D9B0361BE428B312A3B3208F9FA6E5FC86D,
	GltfConversions_SetMatrix4X4Value_m4AFB5EF4730DF3AAD7449FE549A7EA9A36E65E15,
	GltfConversions_GetTrsProperties_mA735690429F6A833A957E4F43FF1C75EA311B443,
	GltfConversions_GetIntArray_mDE81E177A1373A3F217EF11E2D42E60F36A843B3,
	GltfConversions_GetVector2Array_mD2DC81532D69A36F5BC7DD9F499DF39D7725343E,
	GltfConversions_GetVector3Array_mD4A3CDFD86163EFE8149003B05FF9B3C57FF3D81,
	GltfConversions_GetVector4Array_m51609A4B6E68E106C92D189C54D8BAAA6E34823B,
	GltfConversions_GetColorArray_m520810F7A52EA5CC593D3F74C925C511304BD1A9,
	GltfConversions_GetTypeDetails_mD88D20B21B07B14EE8FC213BB7725C41247D79E3,
	GltfConversions_GetDiscreteElement_mC7877141EB7B3E47E31B4C3F6AA73191EBA67C3A,
	GltfConversions_GetDiscreteUnsignedElement_m2995233EE3982E932671DD1E33B68D20CD4FB71C,
	GltfConversions__cctor_m20C27DD35893F7C906CA955FD596232C598B896E,
	GltfUtility_ImportGltfObjectFromPathAsync_m3F1F281E883DC4B6BD5B8588ED833FD8E3CB8A74,
	GltfUtility_GetGltfObjectFromJson_m52D43312EAE9010FC8D47BCBE2C95020B5A05A49,
	GltfUtility_GetGltfObjectFromGlb_m72080FB7D1B2483D0C1E7490E25AFF20452FD42F,
	GltfUtility_GetJsonObject_mB2354AC310894EC09CF06202FFF898423C11925F,
	GltfUtility_GetGltfMeshPrimitiveAttributes_m0A7D4957258FC3E04CADE81651F245E39C3C2E95,
	GltfUtility_GetGltfMeshPrimitiveAttributes_mE02C7DA1CE33982ECB497738B3E72C305052973A,
	GltfUtility_GetGltfExtensionObjects_m5B16C1462DABCED24175761477970B0C4C3A2BEB,
	GltfUtility_GetGltfExtraObjects_m8B7F5603BBEA0ADF53E4F5890372801800044E16,
	GltfUtility_GetGltfExtensions_mE10E37EA7E69B686AF546BD44B824FED47866B7F,
	GltfUtility_GetJsonObject_m44B34164AE6A2DCF757102EDCD2242F2DA3A4A7F,
	GltfUtility_GetGltfNodeName_m4F69B213CEF1E7EB2146E459B56063E5B0024B82,
	GltfUtility_StringIntDictionaryFromJson_mCAAFEB6738615CF719A55060F5EE738E03223CCA,
	GltfUtility_JsonDictionaryToArray_m92AE40A584CC17938219B95D2E2B632A24E4A41F,
	GltfUtility__cctor_m58658EF87ABBEDDE69F6CB0DB7F7AA55470E11BA,
	StringKeyValue__ctor_m45F1F6BE5216ED7EB58B63D0205E7BF5589B64D6,
	StringIntKeyValueArray__ctor_mCB2E1185BB648F171E707AF0276825551106B356,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_mF311DC416E7D3ECED94E034D843532C2778A8C0F,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097,
	GltfAccessor_get_BufferView_mBF991561A98B0BB5FD0E3A655D5E8616961DBF93,
	GltfAccessor_set_BufferView_mA1BE80D7B2D106A658A1292327A86C65FF53E4E5,
	GltfAccessor__ctor_mCCE33A115F791DB6BFB69C52D8CD3E5E9C143904,
	GltfAccessorSparse__ctor_m77979024088AB47E71DCF650C474ACC522EC0CC3,
	GltfAccessorSparseIndices__ctor_m320598C1295C386E39D29EEDDD23A9593E7E479D,
	GltfAccessorSparseValues__ctor_mB5505869103BD63D3DC8FE2A4BF955AB9B3DCFF6,
	GltfAnimation__ctor_m844022C2F66F8A9AB76C088EEF5F14387F4C6062,
	GltfAnimationChannel__ctor_m7A23642184A30691D9183FBBAE1908FF286611F9,
	GltfAnimationChannelTarget__ctor_m02CE15840D03520EBDCB56AE3E17D310B1632EA0,
	GltfAnimationSampler__ctor_mD856E3AA62E5AE489C2F43B56BDF0ABEAE783D7C,
	GltfAssetInfo__ctor_mB144474A6F1C90EB2348DA0B661C49E37AD3BE47,
	GltfBuffer_get_BufferData_m5FBC24C211FFC93370C4CA56C07809DA7DF59257,
	GltfBuffer_set_BufferData_mB3E1712AF73DCFEF5407692BAACD1FBD1384DC41,
	GltfBuffer__ctor_m651C75471E97180EAE30FFA0F7A4FE4A6A99579C,
	GltfBufferView_get_Buffer_m47253FD5C18EC856A56348EC0B9855C42B0A9EE0,
	GltfBufferView_set_Buffer_m3CF4046017E0B5AFE7496913FD3FCA25C962EF9A,
	GltfBufferView__ctor_mE94B77D319F5E04B069765D251933375CB7AC591,
	GltfCamera__ctor_mD426973F5FB04C36B2437EC5AB504156EF0F2670,
	GltfCameraOrthographic__ctor_mA636B9B7A7052134E154DBE585C264D35F98E3C3,
	GltfCameraPerspective__ctor_m36B921EEF1C0078FEDF209D12F3CCB4CE544298E,
	GltfChildOfRootProperty__ctor_mB5338B3E23EB6600BC95FBD669A0BBCBD572CE56,
	GltfImage_get_Texture_mD779E699883558093A72BCA8A13B77149310DDBD,
	GltfImage_set_Texture_mEE09E29892BC1EB99FE4F516C6A79ADB2801061A,
	GltfImage__ctor_m4726027B019425653A1C8ACB26589B7C2594DFBC,
	GltfMaterial_get_Material_mFA2E7C0146232BED65AFA5E64EB1F01388141C20,
	GltfMaterial_set_Material_mBB2F569AFBBCD2A8F7BB8AF6EC002CDCA0456B7B,
	GltfMaterial__ctor_mF9208CFE8B5C498F418B72CFBB5A3A95C8AFDC44,
	GltfMaterialCommonConstant__ctor_mEE6A5EA764BEC71FF7AB810D3FBF87E552805863,
	GltfMesh_get_Mesh_mDEE88954BB0F1F79F13A090C0FFCCF604F1836E7,
	GltfMesh_set_Mesh_m21306B466FCECF334B5CCC1010AE69F2644D6955,
	GltfMesh__ctor_m26F2DC3DD72BFC565EB0D32479B34EA0BBC105A2,
	GltfMeshPrimitive_get_Targets_m1C5161E796B842241E9220E5CDF6FE3C7730321A,
	GltfMeshPrimitive_set_Targets_m47A1CF3F7C5812BA6622E06E983D6C6247A56427,
	GltfMeshPrimitive_get_Attributes_m937303E06650F7DA0F1DEC3DA0BAF7A65C7D32E9,
	GltfMeshPrimitive_set_Attributes_m55351ED659CB203206754110C2E2EE96F8FA32FA,
	GltfMeshPrimitive_get_SubMesh_mFC5DBA13B54CC0970537C50F5E63AD9588906EEF,
	GltfMeshPrimitive_set_SubMesh_mF33518F7D3AC143DF21ACA3BA58BC88F27C58795,
	GltfMeshPrimitive__ctor_mC5379DF50E3B74C2F65CD3A4ECD5BA48D1CEFD03,
	GltfMeshPrimitiveAttributes__ctor_m4091BE30DE171D21B52D7FBA594A16287786C19C,
	GltfMeshPrimitiveAttributes_TryGetDefault_m9BDFED10783734F8E67D546A2EB0CF4C90CD5892,
	GltfMeshPrimitiveAttributes_get_POSITION_m5913C86E735FA4278C385108B4128163630ABA52,
	GltfMeshPrimitiveAttributes_get_NORMAL_mBCCD4E51D477BC7A3413E278723CBDCAE570DC1D,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_0_mAAABFF295B94B889928FFCCDBBC72BD49156137F,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_1_mA7383BECF396AB6FD912F8D13ED4D0C55A7CA4D6,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_2_m5CCCBC040F66312BA33EB7B507EFF2D290458A04,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_3_m0BFBB9EED9A7063FC002629EFD3CF9E19457A31F,
	GltfMeshPrimitiveAttributes_get_COLOR_0_m3578B4E478BE41DDEADC1BF2ACE4B07D669FC2D7,
	GltfMeshPrimitiveAttributes_get_TANGENT_mAEBDBDEE53A5246AD636A9EB57E5E24976E46634,
	GltfMeshPrimitiveAttributes_get_WEIGHTS_0_mD58B5E52621F1A6C2A8330C40FE67EFEB012A58D,
	GltfMeshPrimitiveAttributes_get_JOINTS_0_m1747B821876AC421F9ABC0D90E8935AAD66B789C,
	GltfNode_get_Matrix_m1B810ECC2DC3F1D93783DC05A31EBA8AB578E3C5,
	GltfNode_set_Matrix_m3248A545B8361419A301F284ACF9BBA135F85051,
	GltfNode__ctor_m61057DF0B77B922B9B05702B7C68073E13828E9E,
	GltfNormalTextureInfo__ctor_m62DB88F06ED2B079B3CD40C2A32DAC35F6A4DB0A,
	GltfObject_get_Name_m6EEDCBC4E03E20908BCAD19D627DB2E707766178,
	GltfObject_set_Name_m47895CAC77FC47B9000CFDCBAECE315E37444B4A,
	GltfObject_get_Uri_m63C0AA0B7A6CF31A2EFD26C8A6645F4E37B059F3,
	GltfObject_set_Uri_m735F5DA5B8E21AF02E397BCC7995D8A9468E9D41,
	GltfObject_get_GameObjectReference_m2B3785589D407870E1EDF983FA2EADC74121CB19,
	GltfObject_set_GameObjectReference_m006D03F1F66EA53E5FBD779059C4AEFD5FD0308A,
	GltfObject_get_RegisteredExtensions_m429D2F362F9FADA1237D3B571A03BD432503F507,
	GltfObject_set_RegisteredExtensions_mF87255C41D50B99E1FE43201F1E3796C6376E976,
	GltfObject_get_UseBackgroundThread_m921E5B1288396728EAB6AA773BAB9B07E09B1AEF,
	GltfObject_set_UseBackgroundThread_mCD113787FF499BD925D6B5543F0340DFB8AA36A7,
	GltfObject_GetAccessor_mED951A69BDB4C8E3450D5D3CF657F2FD9B52E133,
	GltfObject__ctor_m89A967DB8CBF8F9B7C4DA67453B515678246F694,
	GltfOcclusionTextureInfo__ctor_mF8E71A6B7D80DB33B724FE7DC00F615AD08EAEFA,
	GltfPbrMetallicRoughness__ctor_mE67BE46E07C7CB43C7894C78694E682835521435,
	GltfProperty__ctor_m39A491CE9073387E57446DAAE7B09EAE671C6A88,
	GltfSampler__ctor_mD1CBD10E582CC874D88BFD197C31572C00F78696,
	GltfScene__ctor_m3860DB75050ABC6EF5E09D63FFF1554604C0B33D,
	GltfSkin__ctor_mD23C21FF4D23229F9909376935604EB1600548F8,
	GltfTexture_get_Texture_m95F363CA33062CDB544D6131F824ED6C2D59F69B,
	GltfTexture_set_Texture_mD812992D23C0E4FEB265F281EC31C14523BF5EB2,
	GltfTexture__ctor_mA66CBD7D8639F2E3C28D7771ACFC6C0DB1E6D50E,
	GltfTextureInfo__ctor_mD78DB2C898E69E1E1E09B4384016327CC13234EB,
	GltfExtension__ctor_mE94C25127141B718F640D17D1C80170C070CFAF5,
	KHR_Materials_PbrSpecularGlossiness__ctor_mC016AC6361F9868BC15758E60B8EDBAD46DBDE59,
};
extern void U3CConstructU3Ed__18_MoveNext_m245CB5F41374271A3C6081AE92F6874D98B8EC32_AdjustorThunk (void);
extern void U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A_AdjustorThunk (void);
extern void U3CConstructAsyncU3Ed__19_MoveNext_mF08700731B540D0E376A2F68F6AFA1727E85C8EE_AdjustorThunk (void);
extern void U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88_AdjustorThunk (void);
extern void U3CConstructTextureAsyncU3Ed__21_MoveNext_m5D0FB162E9746A292527BF6D4ECA72CA00B59EEE_AdjustorThunk (void);
extern void U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3_AdjustorThunk (void);
extern void U3CConstructMaterialAsyncU3Ed__22_MoveNext_m2D0A844441E9A198ED982ECDE9EFBA6921CF64DB_AdjustorThunk (void);
extern void U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3_AdjustorThunk (void);
extern void U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mF7839D1592154510BF77F591CEC84980BAADD950_AdjustorThunk (void);
extern void U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B_AdjustorThunk (void);
extern void U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m147AA34FADDB3E629F6D267B66BB9C5F66E64436_AdjustorThunk (void);
extern void U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D_AdjustorThunk (void);
extern void U3CConstructSceneAsyncU3Ed__25_MoveNext_mE3B45AFA3B6493146C2517F8D606C2867D10A3CE_AdjustorThunk (void);
extern void U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D_AdjustorThunk (void);
extern void U3CConstructNodeAsyncU3Ed__26_MoveNext_m21B676BF91C3AAD335FF663CFA0F2E1D4AE0490B_AdjustorThunk (void);
extern void U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4_AdjustorThunk (void);
extern void U3CConstructMeshAsyncU3Ed__27_MoveNext_mFD2EE8133BA30C7EF6E9C34A6BF2081F92857EEC_AdjustorThunk (void);
extern void U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C_AdjustorThunk (void);
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m1369979E70F7F0C20A06EC7D57F7FDFEC675C4E7_AdjustorThunk (void);
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F_AdjustorThunk (void);
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_mF311DC416E7D3ECED94E034D843532C2778A8C0F_AdjustorThunk (void);
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[22] = 
{
	{ 0x06000014, U3CConstructU3Ed__18_MoveNext_m245CB5F41374271A3C6081AE92F6874D98B8EC32_AdjustorThunk },
	{ 0x06000015, U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A_AdjustorThunk },
	{ 0x06000016, U3CConstructAsyncU3Ed__19_MoveNext_mF08700731B540D0E376A2F68F6AFA1727E85C8EE_AdjustorThunk },
	{ 0x06000017, U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88_AdjustorThunk },
	{ 0x06000018, U3CConstructTextureAsyncU3Ed__21_MoveNext_m5D0FB162E9746A292527BF6D4ECA72CA00B59EEE_AdjustorThunk },
	{ 0x06000019, U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3_AdjustorThunk },
	{ 0x0600001A, U3CConstructMaterialAsyncU3Ed__22_MoveNext_m2D0A844441E9A198ED982ECDE9EFBA6921CF64DB_AdjustorThunk },
	{ 0x0600001B, U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3_AdjustorThunk },
	{ 0x0600001C, U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mF7839D1592154510BF77F591CEC84980BAADD950_AdjustorThunk },
	{ 0x0600001D, U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B_AdjustorThunk },
	{ 0x0600001E, U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m147AA34FADDB3E629F6D267B66BB9C5F66E64436_AdjustorThunk },
	{ 0x0600001F, U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D_AdjustorThunk },
	{ 0x06000020, U3CConstructSceneAsyncU3Ed__25_MoveNext_mE3B45AFA3B6493146C2517F8D606C2867D10A3CE_AdjustorThunk },
	{ 0x06000021, U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D_AdjustorThunk },
	{ 0x06000022, U3CConstructNodeAsyncU3Ed__26_MoveNext_m21B676BF91C3AAD335FF663CFA0F2E1D4AE0490B_AdjustorThunk },
	{ 0x06000023, U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4_AdjustorThunk },
	{ 0x06000024, U3CConstructMeshAsyncU3Ed__27_MoveNext_mFD2EE8133BA30C7EF6E9C34A6BF2081F92857EEC_AdjustorThunk },
	{ 0x06000025, U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C_AdjustorThunk },
	{ 0x06000026, U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m1369979E70F7F0C20A06EC7D57F7FDFEC675C4E7_AdjustorThunk },
	{ 0x06000027, U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F_AdjustorThunk },
	{ 0x0600004D, U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_mF311DC416E7D3ECED94E034D843532C2778A8C0F_AdjustorThunk },
	{ 0x0600004E, U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097_AdjustorThunk },
};
static const int32_t s_InvokerIndices[156] = 
{
	4326,
	3635,
	4326,
	3635,
	4406,
	6218,
	6106,
	5873,
	5647,
	5314,
	5314,
	5314,
	5315,
	5047,
	5314,
	5647,
	5314,
	6218,
	6303,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	4406,
	3635,
	5017,
	5956,
	6091,
	6191,
	6116,
	5807,
	5662,
	5669,
	5652,
	6049,
	6105,
	5159,
	5648,
	5648,
	5648,
	5648,
	6106,
	5439,
	5244,
	5244,
	6303,
	6106,
	6106,
	6106,
	5647,
	6106,
	5647,
	5647,
	5647,
	5647,
	5642,
	6106,
	6106,
	6106,
	6303,
	4406,
	4406,
	4406,
	3635,
	4326,
	3635,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4326,
	3635,
	4406,
	4326,
	3635,
	4406,
	4406,
	4406,
	4406,
	4406,
	4326,
	3635,
	4406,
	4326,
	3635,
	4406,
	4406,
	4326,
	3635,
	4406,
	4326,
	3635,
	4326,
	3635,
	4326,
	3635,
	4406,
	3635,
	1417,
	4295,
	4295,
	4295,
	4295,
	4295,
	4295,
	4295,
	4295,
	4295,
	4295,
	4311,
	3618,
	4406,
	4406,
	4326,
	3635,
	4326,
	3635,
	4326,
	3635,
	4326,
	3635,
	4364,
	3670,
	2843,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4326,
	3635,
	4406,
	4406,
	4406,
	4406,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Gltf_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Gltf_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Gltf_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Gltf.dll",
	156,
	s_methodPointers,
	22,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Gltf_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
