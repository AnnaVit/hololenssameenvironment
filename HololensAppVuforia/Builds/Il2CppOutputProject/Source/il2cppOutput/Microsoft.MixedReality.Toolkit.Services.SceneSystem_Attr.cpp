﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HelpURLAttribute
struct  HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct  AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void Microsoft_MixedReality_Toolkit_Services_SceneSystem_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[2];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[3];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x20\x61\x69\x70\x6D\x72\x61\x67\x65\x6E\x74\x5F\x77\x6F\x72\x6B"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x35\x2E\x33\x2E\x30"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[5];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x6D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x54\x6F\x6F\x6C\x6B\x69\x74\x2D\x55\x6E\x69\x74\x79\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x53\x63\x65\x6E\x65\x53\x79\x73\x74\x65\x6D\x2F\x53\x63\x65\x6E\x65\x53\x79\x73\x74\x65\x6D\x47\x65\x74\x74\x69\x6E\x67\x53\x74\x61\x72\x74\x65\x64\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadContentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnContentLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadContentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnContentUnloadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadLightingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnLightingLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadLightingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnLightingUnloadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadSceneU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnSceneLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadSceneU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnSceneUnloadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSceneOperationInProgressU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSceneOperationProgressU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CLightingOperationInProgressU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CLightingOperationProgressU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CActiveLightingSceneU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CWaitingToProceedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSourceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSourceNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem__ctor_m20B983EFDD37E3A74A513AC9A66AE66266B020B0(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x28\x72\x65\x67\x69\x73\x74\x72\x61\x72\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x72\x65\x71\x75\x69\x72\x65\x64\x29\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x61\x20\x66\x75\x74\x75\x72\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x2E"), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_Name_mD81B3B234C5D0040C29DD4501739571D99B8E756(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_Name_mEFB190232F67C4F4AE0CAAEB9B5C311CA0177C0F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadContent_mA0E78EE6DEB7B41316A5D64EDF9640E828F661E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadContent_m2E22007CC25BCC9F31315743EE968B5EE2A28023(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnContentLoaded_m92D4A61485E05E69A328A68A781F59B36BEE29BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnContentLoaded_mC78F377CFC8C798A3FE1EDB0EEE04F44297D75B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadContent_mCA25A1E2B90D4AAC46BADE27A31CBE5A731CC656(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadContent_mC47F0EE4655BF5A22212DB14611128B878C62D34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnContentUnloaded_m7FA0D4D4EB1D4C2CCC49BE019E3F4BA579946179(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnContentUnloaded_mB63CF5824DB411292D58E082319F8FFEAB07EF42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadLighting_m1464D754A9FDB44BAD65496339741E73137CEE45(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadLighting_m59B78EB019377FC0F5095C32030AAF928F9C246C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnLightingLoaded_mBE0BF6596B47D1209FDCECE4E327C31B887AF6D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnLightingLoaded_m4172CC23D3D11A18A646F6BBA77D40AF3836D516(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadLighting_m8BE05DB6F3B235E59975FFE1368ABD036D74B9AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadLighting_mBB7410FC221830930A467518F8D6BB4790690E90(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnLightingUnloaded_m8C636AC30A492C24EFEA6705DEAD5B3AF0A8C59B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnLightingUnloaded_m24F7D7DAA7CFE7E0B8C2B33D5F3A3092B7B60691(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadScene_m2439D59A3DAF9F099A078D4049A0A5CBE444E6A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadScene_mC0FD0F368CEB2547F09EF2B2DC8188A04677154F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnSceneLoaded_mB39756B7F280897525262E5E8A6C1C7BC59A4781(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnSceneLoaded_m74BC1E0ECB972E63635F4DB585DEB82AA696B89D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadScene_mC5EC78C1A062949FAB93BF07D357B83BCB3E021B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadScene_m56A38E364A91F979EE1AF65EBBBA5ABA6EAB8BB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnSceneUnloaded_mB6F767C24A7FE03E177B3CF342D2525110966DFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnSceneUnloaded_m7944E323E43912240891BC9557F8C1326C4F2848(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SceneOperationInProgress_mC58AE6B423737A75D54C9CF25622042876AFCCBB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_SceneOperationInProgress_mFF583414C6EC145D71B4ADA0E9F0C220F9F57DAB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SceneOperationProgress_m8DB01E2F5D469A8DFC429FB911E57C747B78DC13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_SceneOperationProgress_mFE115B9854BA68AF845497B36AAF3BE1D4CBDD3F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_LightingOperationInProgress_m7CABCBEB6BD0E62099A24684B74B1FFD6A4B92C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_LightingOperationInProgress_m9C00A8A302719E4ADEF5F11D9B447CD2CBBAB0EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_LightingOperationProgress_mFEEB065447C1BD1EA599059B096288F02188437A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_LightingOperationProgress_m2BB3346279965EB59FC6B9AA4BC2FA87C18AEAAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_ActiveLightingScene_mE436F865C3B75810B3AC1D9D3C673B59F3170906(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_ActiveLightingScene_m3C25E5C5E874F233684FAB97AD7988E695E6DD50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_WaitingToProceed_mEB034243177572389CA8284EE9544915CC48C237(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_WaitingToProceed_mEC08BBFBEFEECEDE5A807F8D0FEA9696973187E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SourceId_m713DDCC50FD852186D8CE49666106BBB6CBABC39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SourceName_mF92BD92E3498905F42B28EDD326A073AE0DBE845(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadNextContent_mDCC40F19871FD1D7ED3BBD18674DCD1245B39736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadPrevContent_mC75F54BD8BE6682F8B802E5A2C1C5A8E06151B07(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContent_m3916C2F12DDF63CCF36C0881FF72207F5F41A90A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContent_mF5D1CBAB497EFF0767FAE3C7C3DCBD8F03E1E5BF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContentByTag_m0266C8F5B8E2338D9AB278FC9FEC9A428339533A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContentByTag_m2E3CDC20F49C80C9A6E5FEC05F962735D422A9DC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContent_m4EDF43EA100331FBD324BDD701C6D6D3F4FB5446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContent_m507B7DC383DCF1744EFE0E17573D8E6814A5183C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_SetLightingScene_mB479CDE23B195F98EDA2A283BE050419496F1E79(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_SetManagerScene_m7C377C8ADBE48EE1ECC8E8202F612BCE0BC38B01(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadScenesInternal_m6592ED405CD85972164285228DF19245E96C525B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadScenesInternal_m80D2F08193FC1286A06BA550F7807B8AD0CA76B9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_0_0_0_var), NULL);
	}
}
static void MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_GetScenes_mF9EABC9B6D09C4EEDE196E431258FA6ED8BF472C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t5877D71A3EE34AABA4CB9A17615AFF5A07D4D78F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_CustomAttributesCacheGenerator_U3CLoadNextContentU3Ed__105_SetStateMachine_m5EE6A93BFAD98736BCAF11E3E5EEBB9F7F589CD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_CustomAttributesCacheGenerator_U3CLoadPrevContentU3Ed__107_SetStateMachine_m48A8671DDE812CD8B07B41E84CDC91ECDCE4462F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_CustomAttributesCacheGenerator_U3CLoadContentU3Ed__108_SetStateMachine_mB62B3A2AABA3E84EA0374AE56F56B8C1C1DB7922(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_CustomAttributesCacheGenerator_U3CUnloadContentU3Ed__109_SetStateMachine_m859DE916921C26FE183A3D30F1D9A8270D22AAB1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_CustomAttributesCacheGenerator_U3CLoadContentByTagU3Ed__111_SetStateMachine_mC39EA78D20A90BC4BC2B4C1D1C65A8F7EF70B50B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_CustomAttributesCacheGenerator_U3CUnloadContentByTagU3Ed__113_SetStateMachine_mDC115D8308CB0AB966D86B2F90AB397A875A22DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_CustomAttributesCacheGenerator_U3CLoadContentU3Ed__115_SetStateMachine_m2B64B541E3C5EEA258EBEB3CB9C414425331780D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_CustomAttributesCacheGenerator_U3CUnloadContentU3Ed__117_SetStateMachine_mCB473D20105ADE16E0CB4353DE1F6E05EB5A2126(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_CustomAttributesCacheGenerator_U3CSetLightingSceneU3Ed__121_SetStateMachine_m1C5AA73D73F655989319C1CBC0A67B9F2222D1D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_CustomAttributesCacheGenerator_U3CSetManagerSceneU3Ed__123_SetStateMachine_m53A84B1181DDF782DCC09EB8A0C9A2F112D000DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_CustomAttributesCacheGenerator_U3CLoadScenesInternalU3Ed__125_SetStateMachine_m4330D4C90A2F5E1900C6B644617012EBB4AE8E98(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_CustomAttributesCacheGenerator_U3CUnloadScenesInternalU3Ed__127_SetStateMachine_m250E2C64DB0940C2F33A6305388D139A46FD0DDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138__ctor_m93274A5DC721862E88BAEAEA16C7D28918A2AFE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_IDisposable_Dispose_mF97B61AA1D8437A1C3B82FF89596E934A003FCA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_Generic_IEnumeratorU3CUnityEngine_SceneManagement_SceneU3E_get_Current_mD4174E5E6186946EDF6F26078BBF34FBDA91E57A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerator_Reset_m886485B187E58B8C77EB13F9A8F94CE78FD64728(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerator_get_Current_m3A5D52B205D24D1DD870EBBB6DBAC43D5E6D9E43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_Generic_IEnumerableU3CUnityEngine_SceneManagement_SceneU3E_GetEnumerator_m5EE1C17BFC274F7A2F13043278771134415A9638(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerable_GetEnumerator_m607FBDE4BC1CACA7488A07F55897D339C9940F81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_SceneSystem_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_SceneSystem_AttributeGenerators[110] = 
{
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator,
	U3CU3Ec_t5877D71A3EE34AABA4CB9A17615AFF5A07D4D78F_CustomAttributesCacheGenerator,
	U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_CustomAttributesCacheGenerator,
	U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_CustomAttributesCacheGenerator,
	U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_CustomAttributesCacheGenerator,
	U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_CustomAttributesCacheGenerator,
	U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_CustomAttributesCacheGenerator,
	U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_CustomAttributesCacheGenerator,
	U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_CustomAttributesCacheGenerator,
	U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_CustomAttributesCacheGenerator,
	U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_CustomAttributesCacheGenerator,
	U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_CustomAttributesCacheGenerator,
	U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_CustomAttributesCacheGenerator,
	U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_CustomAttributesCacheGenerator,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadContentU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnContentLoadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadContentU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnContentUnloadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadLightingU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnLightingLoadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadLightingU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnLightingUnloadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillLoadSceneU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnSceneLoadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnWillUnloadSceneU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3COnSceneUnloadedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSceneOperationInProgressU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSceneOperationProgressU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CLightingOperationInProgressU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CLightingOperationProgressU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CActiveLightingSceneU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CWaitingToProceedU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSourceIdU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_U3CSourceNameU3Ek__BackingField,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem__ctor_m20B983EFDD37E3A74A513AC9A66AE66266B020B0,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_Name_mD81B3B234C5D0040C29DD4501739571D99B8E756,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_Name_mEFB190232F67C4F4AE0CAAEB9B5C311CA0177C0F,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadContent_mA0E78EE6DEB7B41316A5D64EDF9640E828F661E0,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadContent_m2E22007CC25BCC9F31315743EE968B5EE2A28023,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnContentLoaded_m92D4A61485E05E69A328A68A781F59B36BEE29BA,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnContentLoaded_mC78F377CFC8C798A3FE1EDB0EEE04F44297D75B5,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadContent_mCA25A1E2B90D4AAC46BADE27A31CBE5A731CC656,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadContent_mC47F0EE4655BF5A22212DB14611128B878C62D34,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnContentUnloaded_m7FA0D4D4EB1D4C2CCC49BE019E3F4BA579946179,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnContentUnloaded_mB63CF5824DB411292D58E082319F8FFEAB07EF42,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadLighting_m1464D754A9FDB44BAD65496339741E73137CEE45,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadLighting_m59B78EB019377FC0F5095C32030AAF928F9C246C,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnLightingLoaded_mBE0BF6596B47D1209FDCECE4E327C31B887AF6D7,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnLightingLoaded_m4172CC23D3D11A18A646F6BBA77D40AF3836D516,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadLighting_m8BE05DB6F3B235E59975FFE1368ABD036D74B9AA,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadLighting_mBB7410FC221830930A467518F8D6BB4790690E90,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnLightingUnloaded_m8C636AC30A492C24EFEA6705DEAD5B3AF0A8C59B,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnLightingUnloaded_m24F7D7DAA7CFE7E0B8C2B33D5F3A3092B7B60691,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillLoadScene_m2439D59A3DAF9F099A078D4049A0A5CBE444E6A1,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillLoadScene_mC0FD0F368CEB2547F09EF2B2DC8188A04677154F,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnSceneLoaded_mB39756B7F280897525262E5E8A6C1C7BC59A4781,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnSceneLoaded_m74BC1E0ECB972E63635F4DB585DEB82AA696B89D,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnWillUnloadScene_mC5EC78C1A062949FAB93BF07D357B83BCB3E021B,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnWillUnloadScene_m56A38E364A91F979EE1AF65EBBBA5ABA6EAB8BB8,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_OnSceneUnloaded_mB6F767C24A7FE03E177B3CF342D2525110966DFE,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_OnSceneUnloaded_m7944E323E43912240891BC9557F8C1326C4F2848,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SceneOperationInProgress_mC58AE6B423737A75D54C9CF25622042876AFCCBB,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_SceneOperationInProgress_mFF583414C6EC145D71B4ADA0E9F0C220F9F57DAB,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SceneOperationProgress_m8DB01E2F5D469A8DFC429FB911E57C747B78DC13,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_SceneOperationProgress_mFE115B9854BA68AF845497B36AAF3BE1D4CBDD3F,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_LightingOperationInProgress_m7CABCBEB6BD0E62099A24684B74B1FFD6A4B92C4,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_LightingOperationInProgress_m9C00A8A302719E4ADEF5F11D9B447CD2CBBAB0EC,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_LightingOperationProgress_mFEEB065447C1BD1EA599059B096288F02188437A,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_LightingOperationProgress_m2BB3346279965EB59FC6B9AA4BC2FA87C18AEAAF,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_ActiveLightingScene_mE436F865C3B75810B3AC1D9D3C673B59F3170906,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_ActiveLightingScene_m3C25E5C5E874F233684FAB97AD7988E695E6DD50,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_WaitingToProceed_mEB034243177572389CA8284EE9544915CC48C237,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_set_WaitingToProceed_mEC08BBFBEFEECEDE5A807F8D0FEA9696973187E9,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SourceId_m713DDCC50FD852186D8CE49666106BBB6CBABC39,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_get_SourceName_mF92BD92E3498905F42B28EDD326A073AE0DBE845,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadNextContent_mDCC40F19871FD1D7ED3BBD18674DCD1245B39736,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadPrevContent_mC75F54BD8BE6682F8B802E5A2C1C5A8E06151B07,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContent_m3916C2F12DDF63CCF36C0881FF72207F5F41A90A,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContent_mF5D1CBAB497EFF0767FAE3C7C3DCBD8F03E1E5BF,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContentByTag_m0266C8F5B8E2338D9AB278FC9FEC9A428339533A,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContentByTag_m2E3CDC20F49C80C9A6E5FEC05F962735D422A9DC,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadContent_m4EDF43EA100331FBD324BDD701C6D6D3F4FB5446,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadContent_m507B7DC383DCF1744EFE0E17573D8E6814A5183C,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_SetLightingScene_mB479CDE23B195F98EDA2A283BE050419496F1E79,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_SetManagerScene_m7C377C8ADBE48EE1ECC8E8202F612BCE0BC38B01,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_LoadScenesInternal_m6592ED405CD85972164285228DF19245E96C525B,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_UnloadScenesInternal_m80D2F08193FC1286A06BA550F7807B8AD0CA76B9,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_CustomAttributesCacheGenerator_MixedRealitySceneSystem_GetScenes_mF9EABC9B6D09C4EEDE196E431258FA6ED8BF472C,
	U3CLoadNextContentU3Ed__105_tD0F119CDA56EB9EA7C53A3DCE3A667E241697204_CustomAttributesCacheGenerator_U3CLoadNextContentU3Ed__105_SetStateMachine_m5EE6A93BFAD98736BCAF11E3E5EEBB9F7F589CD7,
	U3CLoadPrevContentU3Ed__107_t1133FED1964F5089181AC28681AC3DF6A13AEF1C_CustomAttributesCacheGenerator_U3CLoadPrevContentU3Ed__107_SetStateMachine_m48A8671DDE812CD8B07B41E84CDC91ECDCE4462F,
	U3CLoadContentU3Ed__108_t8F44E39A8FAA7CD793DBB7AEBD29A36F52EB1812_CustomAttributesCacheGenerator_U3CLoadContentU3Ed__108_SetStateMachine_mB62B3A2AABA3E84EA0374AE56F56B8C1C1DB7922,
	U3CUnloadContentU3Ed__109_tAC6BDEE48633D618EAD1B20B86577374115467A4_CustomAttributesCacheGenerator_U3CUnloadContentU3Ed__109_SetStateMachine_m859DE916921C26FE183A3D30F1D9A8270D22AAB1,
	U3CLoadContentByTagU3Ed__111_tC7DB0D483ECF4079F0818F59A268621A27E19B17_CustomAttributesCacheGenerator_U3CLoadContentByTagU3Ed__111_SetStateMachine_mC39EA78D20A90BC4BC2B4C1D1C65A8F7EF70B50B,
	U3CUnloadContentByTagU3Ed__113_t9E1E33796D3E3FFCB9314E256A42875B49CC34AE_CustomAttributesCacheGenerator_U3CUnloadContentByTagU3Ed__113_SetStateMachine_mDC115D8308CB0AB966D86B2F90AB397A875A22DF,
	U3CLoadContentU3Ed__115_tCCEF1C5EA22567AA23A1BA65FA468ECCD9448D46_CustomAttributesCacheGenerator_U3CLoadContentU3Ed__115_SetStateMachine_m2B64B541E3C5EEA258EBEB3CB9C414425331780D,
	U3CUnloadContentU3Ed__117_t10B4882573E45FCF1D3D485082F7ED37557F7444_CustomAttributesCacheGenerator_U3CUnloadContentU3Ed__117_SetStateMachine_mCB473D20105ADE16E0CB4353DE1F6E05EB5A2126,
	U3CSetLightingSceneU3Ed__121_t96ACF2D304E0A5E645932689F99FBCC6CAA8238C_CustomAttributesCacheGenerator_U3CSetLightingSceneU3Ed__121_SetStateMachine_m1C5AA73D73F655989319C1CBC0A67B9F2222D1D3,
	U3CSetManagerSceneU3Ed__123_tD752F4D175CB0CCABFDEEAE50128FC7AF4355959_CustomAttributesCacheGenerator_U3CSetManagerSceneU3Ed__123_SetStateMachine_m53A84B1181DDF782DCC09EB8A0C9A2F112D000DC,
	U3CLoadScenesInternalU3Ed__125_t8AB02444C722C8AF49834C6EB8434A4380EE8235_CustomAttributesCacheGenerator_U3CLoadScenesInternalU3Ed__125_SetStateMachine_m4330D4C90A2F5E1900C6B644617012EBB4AE8E98,
	U3CUnloadScenesInternalU3Ed__127_tD551AFA0B70760ABB848631538852B51C4F5D0B0_CustomAttributesCacheGenerator_U3CUnloadScenesInternalU3Ed__127_SetStateMachine_m250E2C64DB0940C2F33A6305388D139A46FD0DDD,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138__ctor_m93274A5DC721862E88BAEAEA16C7D28918A2AFE0,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_IDisposable_Dispose_mF97B61AA1D8437A1C3B82FF89596E934A003FCA2,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_Generic_IEnumeratorU3CUnityEngine_SceneManagement_SceneU3E_get_Current_mD4174E5E6186946EDF6F26078BBF34FBDA91E57A,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerator_Reset_m886485B187E58B8C77EB13F9A8F94CE78FD64728,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerator_get_Current_m3A5D52B205D24D1DD870EBBB6DBAC43D5E6D9E43,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_Generic_IEnumerableU3CUnityEngine_SceneManagement_SceneU3E_GetEnumerator_m5EE1C17BFC274F7A2F13043278771134415A9638,
	U3CGetScenesU3Ed__138_tEC5EE89879A513EDFDE44985BC1E67DA4DE166B7_CustomAttributesCacheGenerator_U3CGetScenesU3Ed__138_System_Collections_IEnumerable_GetEnumerator_m607FBDE4BC1CACA7488A07F55897D339C9940F81,
	Microsoft_MixedReality_Toolkit_Services_SceneSystem_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
