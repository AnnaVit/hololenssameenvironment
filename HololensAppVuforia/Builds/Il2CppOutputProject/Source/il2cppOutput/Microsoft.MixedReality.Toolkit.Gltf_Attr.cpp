﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct  AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void Microsoft_MixedReality_Toolkit_Gltf_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[2];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[3];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x35\x2E\x33\x2E\x30"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[5];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x20\x61\x69\x70\x6D\x72\x61\x67\x65\x6E\x74\x5F\x77\x6F\x72\x6B"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[6];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x54\x6F\x6F\x6C\x6B\x69\x74\x2E\x47\x6C\x74\x66\x2E\x49\x6D\x70\x6F\x72\x74\x65\x72\x73"), NULL);
	}
}
static void GltfAsset_t8D2643213A643AE14DBF3148080A2F4C3F4BECA5_CustomAttributesCacheGenerator_model(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GltfAsset_t8D2643213A643AE14DBF3148080A2F4C3F4BECA5_CustomAttributesCacheGenerator_gltfObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_Construct_m7AF376B19D93867D068C7070283A9105C75D2D7C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructAsync_m975484D3239B0ECD7FC1DBA2585C9D8825375ED1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructBufferView_mD3430888FB4600BD2C23383AE209CF65D5B0ECAC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructTextureAsync_m0AD41CB23C8FDB01E726E696D66CFBB53EB34EC7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMaterialAsync_mBBF26E2C2B1E3E31056532A04980CE8C75307F7D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_CreateMRTKShaderMaterial_mB15031444B7BBF9B2CFBFB017A5FF11B88803B52(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_CreateStandardShaderMaterial_mA98881D421ED3BF1EF06DEC0E132FE0D668758C6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructSceneAsync_m8F37C14372A58396B8D6E34EAF480C6899FE6D6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructNodeAsync_mF82D527D7FB0E5BA869EAF57620CC293463A063D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMeshAsync_m423EB9E6373AF0AF3A069BE5F2CFA234F97E16B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_0_0_0_var), NULL);
	}
}
static void ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMeshPrimitiveAsync_mE4B96E500D485F6546F86912ED5FD7C516399463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_0_0_0_var), NULL);
	}
}
static void U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_CustomAttributesCacheGenerator_U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_CustomAttributesCacheGenerator_U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_CustomAttributesCacheGenerator_U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_CustomAttributesCacheGenerator_U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_CustomAttributesCacheGenerator_U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_CustomAttributesCacheGenerator_U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_CustomAttributesCacheGenerator_U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_CustomAttributesCacheGenerator_U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_CustomAttributesCacheGenerator_U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_CustomAttributesCacheGenerator_U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetTrsProperties_mF9A1951C1A275E671469A6B2904F806EF2B68025(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetColorValue_mA8DE199A1E0E8619C609B1B7E9B053DA1C5A9485(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetColorValue_mBE5BBBFE9609A1BAD3C96405827DB679A285B027(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector2Value_m0EB26D3A83BF2A1E25073D2FB9167D2E301ADD15(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetVector2Value_m12C35C5BD06A02AFB687689BF130206BDC1FCADA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector3Value_m6CF7A32C1560ACF10D3136F4EAAA477855A6038B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetVector3Value_mD5213CB43BEEA3F3FF50166029B1F74EE91ECBE7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetQuaternionValue_m66B59B6DD5BFE3B7A940B5549E107D6C0A72FC3F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetQuaternionValue_m3E99AB48CFA0534C6F0692FBA4EE9910911330D0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetMatrix4X4Value_m73C21D9B0361BE428B312A3B3208F9FA6E5FC86D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetMatrix4X4Value_m4AFB5EF4730DF3AAD7449FE549A7EA9A36E65E15(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetTrsProperties_mA735690429F6A833A957E4F43FF1C75EA311B443(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetIntArray_mDE81E177A1373A3F217EF11E2D42E60F36A843B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector2Array_mD2DC81532D69A36F5BC7DD9F499DF39D7725343E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector3Array_mD4A3CDFD86163EFE8149003B05FF9B3C57FF3D81(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector4Array_m51609A4B6E68E106C92D189C54D8BAAA6E34823B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetColorArray_m520810F7A52EA5CC593D3F74C925C511304BD1A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GltfUtility_tAA44ED2D01F578519AB192D2098F2A6265DACFA7_CustomAttributesCacheGenerator_GltfUtility_ImportGltfObjectFromPathAsync_m3F1F281E883DC4B6BD5B8588ED833FD8E3CB8A74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_0_0_0_var), NULL);
	}
}
static void U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_CustomAttributesCacheGenerator_U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_U3CBufferViewU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_GltfAccessor_get_BufferView_mBF991561A98B0BB5FD0E3A655D5E8616961DBF93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_GltfAccessor_set_BufferView_mA1BE80D7B2D106A658A1292327A86C65FF53E4E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_U3CBufferDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_GltfBuffer_get_BufferData_m5FBC24C211FFC93370C4CA56C07809DA7DF59257(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_GltfBuffer_set_BufferData_mB3E1712AF73DCFEF5407692BAACD1FBD1384DC41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_U3CBufferU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_GltfBufferView_get_Buffer_m47253FD5C18EC856A56348EC0B9855C42B0A9EE0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_GltfBufferView_set_Buffer_m3CF4046017E0B5AFE7496913FD3FCA25C962EF9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_GltfImage_get_Texture_mD779E699883558093A72BCA8A13B77149310DDBD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_GltfImage_set_Texture_mEE09E29892BC1EB99FE4F516C6A79ADB2801061A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_U3CMaterialU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_GltfMaterial_get_Material_mFA2E7C0146232BED65AFA5E64EB1F01388141C20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_GltfMaterial_set_Material_mBB2F569AFBBCD2A8F7BB8AF6EC002CDCA0456B7B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_U3CMeshU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_GltfMesh_get_Mesh_mDEE88954BB0F1F79F13A090C0FFCCF604F1836E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_GltfMesh_set_Mesh_m21306B466FCECF334B5CCC1010AE69F2644D6955(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CTargetsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CSubMeshU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_Targets_m1C5161E796B842241E9220E5CDF6FE3C7730321A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_Targets_m47A1CF3F7C5812BA6622E06E983D6C6247A56427(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_Attributes_m937303E06650F7DA0F1DEC3DA0BAF7A65C7D32E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_Attributes_m55351ED659CB203206754110C2E2EE96F8FA32FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_SubMesh_mFC5DBA13B54CC0970537C50F5E63AD9588906EEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_SubMesh_mF33518F7D3AC143DF21ACA3BA58BC88F27C58795(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_U3CMatrixU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_GltfNode_get_Matrix_m1B810ECC2DC3F1D93783DC05A31EBA8AB578E3C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_GltfNode_set_Matrix_m3248A545B8361419A301F284ACF9BBA135F85051(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CUriU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CGameObjectReferenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CRegisteredExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CUseBackgroundThreadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_Name_m6EEDCBC4E03E20908BCAD19D627DB2E707766178(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_Name_m47895CAC77FC47B9000CFDCBAECE315E37444B4A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_Uri_m63C0AA0B7A6CF31A2EFD26C8A6645F4E37B059F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_Uri_m735F5DA5B8E21AF02E397BCC7995D8A9468E9D41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_GameObjectReference_m2B3785589D407870E1EDF983FA2EADC74121CB19(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_GameObjectReference_m006D03F1F66EA53E5FBD779059C4AEFD5FD0308A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_RegisteredExtensions_m429D2F362F9FADA1237D3B571A03BD432503F507(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_RegisteredExtensions_mF87255C41D50B99E1FE43201F1E3796C6376E976(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_UseBackgroundThread_m921E5B1288396728EAB6AA773BAB9B07E09B1AEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_UseBackgroundThread_mCD113787FF499BD925D6B5543F0340DFB8AA36A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_GltfTexture_get_Texture_m95F363CA33062CDB544D6131F824ED6C2D59F69B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_GltfTexture_set_Texture_mD812992D23C0E4FEB265F281EC31C14523BF5EB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tBF24E567929A08A20BB408ADBFB74BC86FC2F136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Gltf_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Gltf_AttributeGenerators[105] = 
{
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator,
	U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_CustomAttributesCacheGenerator,
	U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_CustomAttributesCacheGenerator,
	U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_CustomAttributesCacheGenerator,
	U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_CustomAttributesCacheGenerator,
	U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_CustomAttributesCacheGenerator,
	U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_CustomAttributesCacheGenerator,
	U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_CustomAttributesCacheGenerator,
	U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_CustomAttributesCacheGenerator,
	U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_CustomAttributesCacheGenerator,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_CustomAttributesCacheGenerator,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tBF24E567929A08A20BB408ADBFB74BC86FC2F136_CustomAttributesCacheGenerator,
	GltfAsset_t8D2643213A643AE14DBF3148080A2F4C3F4BECA5_CustomAttributesCacheGenerator_model,
	GltfAsset_t8D2643213A643AE14DBF3148080A2F4C3F4BECA5_CustomAttributesCacheGenerator_gltfObject,
	GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_U3CBufferViewU3Ek__BackingField,
	GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_U3CBufferDataU3Ek__BackingField,
	GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_U3CBufferU3Ek__BackingField,
	GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField,
	GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_U3CMaterialU3Ek__BackingField,
	GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_U3CMeshU3Ek__BackingField,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CTargetsU3Ek__BackingField,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CAttributesU3Ek__BackingField,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_U3CSubMeshU3Ek__BackingField,
	GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_U3CMatrixU3Ek__BackingField,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CUriU3Ek__BackingField,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CGameObjectReferenceU3Ek__BackingField,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CRegisteredExtensionsU3Ek__BackingField,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_U3CUseBackgroundThreadU3Ek__BackingField,
	GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_Construct_m7AF376B19D93867D068C7070283A9105C75D2D7C,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructAsync_m975484D3239B0ECD7FC1DBA2585C9D8825375ED1,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructBufferView_mD3430888FB4600BD2C23383AE209CF65D5B0ECAC,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructTextureAsync_m0AD41CB23C8FDB01E726E696D66CFBB53EB34EC7,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMaterialAsync_mBBF26E2C2B1E3E31056532A04980CE8C75307F7D,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_CreateMRTKShaderMaterial_mB15031444B7BBF9B2CFBFB017A5FF11B88803B52,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_CreateStandardShaderMaterial_mA98881D421ED3BF1EF06DEC0E132FE0D668758C6,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructSceneAsync_m8F37C14372A58396B8D6E34EAF480C6899FE6D6D,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructNodeAsync_mF82D527D7FB0E5BA869EAF57620CC293463A063D,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMeshAsync_m423EB9E6373AF0AF3A069BE5F2CFA234F97E16B5,
	ConstructGltf_tA4AE1F3E4F316EA956847E51ED7484FA5929EDE8_CustomAttributesCacheGenerator_ConstructGltf_ConstructMeshPrimitiveAsync_mE4B96E500D485F6546F86912ED5FD7C516399463,
	U3CConstructU3Ed__18_tD80642C6CAA34BB14419DE5A5E4D3CFCD8A2CAEA_CustomAttributesCacheGenerator_U3CConstructU3Ed__18_SetStateMachine_m81D459B2F40B510BB034FECB1D63754FD601DF6A,
	U3CConstructAsyncU3Ed__19_t6B6EFA5F2C35253DBC798AEE608DE61D9F4BE179_CustomAttributesCacheGenerator_U3CConstructAsyncU3Ed__19_SetStateMachine_m3B1C8962D7FB4DD674090BC5CA48DAB984405F88,
	U3CConstructTextureAsyncU3Ed__21_t2056F25D46F52435D966F060F8ED4A75CA96721D_CustomAttributesCacheGenerator_U3CConstructTextureAsyncU3Ed__21_SetStateMachine_mC62B3B1EF03E746394744168CE45670751F4CFE3,
	U3CConstructMaterialAsyncU3Ed__22_tCC0FB3F76EFD2F8870210EF31934D9BD4A9D6FA2_CustomAttributesCacheGenerator_U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m043F29037FE24049E7C412ADAFF408935E9BD4B3,
	U3CCreateMRTKShaderMaterialU3Ed__23_tCE581E4A314DBFA178F7BC861C239A86C2081156_CustomAttributesCacheGenerator_U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m12FE2642562DDA307F35491DFDA9D6EB8A6E7F4B,
	U3CCreateStandardShaderMaterialU3Ed__24_tD18948B8A6E58E8BAE66DE5CBA1EDEC29BBB3138_CustomAttributesCacheGenerator_U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mBA250736ED58AAFDD2D184363101F4FDD848339D,
	U3CConstructSceneAsyncU3Ed__25_t801966EED50916FA705785DAB774F245906D75C9_CustomAttributesCacheGenerator_U3CConstructSceneAsyncU3Ed__25_SetStateMachine_mD5F275CD6A8C1EC8FF101A02D54E07CB1A95501D,
	U3CConstructNodeAsyncU3Ed__26_t870664D024668808C5745EEC2A4DAFA07B52B0DC_CustomAttributesCacheGenerator_U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m429086DF002E844EAF5F5524CEF3A0C1218856C4,
	U3CConstructMeshAsyncU3Ed__27_tFD13F57C3FB95750F1E0E52CA6F1A328038B470D_CustomAttributesCacheGenerator_U3CConstructMeshAsyncU3Ed__27_SetStateMachine_mDB788EFAEECC2CE6AD9059CE5FC0BEB3E418015C,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_tD9F28C5378DEEB590E2C2D8F0116DB15F208CBF0_CustomAttributesCacheGenerator_U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m9C138D8A05E4F0DFB0C6B6B82BBABF028ED7A71F,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetTrsProperties_mF9A1951C1A275E671469A6B2904F806EF2B68025,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetColorValue_mA8DE199A1E0E8619C609B1B7E9B053DA1C5A9485,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetColorValue_mBE5BBBFE9609A1BAD3C96405827DB679A285B027,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector2Value_m0EB26D3A83BF2A1E25073D2FB9167D2E301ADD15,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetVector2Value_m12C35C5BD06A02AFB687689BF130206BDC1FCADA,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector3Value_m6CF7A32C1560ACF10D3136F4EAAA477855A6038B,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetVector3Value_mD5213CB43BEEA3F3FF50166029B1F74EE91ECBE7,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetQuaternionValue_m66B59B6DD5BFE3B7A940B5549E107D6C0A72FC3F,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetQuaternionValue_m3E99AB48CFA0534C6F0692FBA4EE9910911330D0,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetMatrix4X4Value_m73C21D9B0361BE428B312A3B3208F9FA6E5FC86D,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_SetMatrix4X4Value_m4AFB5EF4730DF3AAD7449FE549A7EA9A36E65E15,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetTrsProperties_mA735690429F6A833A957E4F43FF1C75EA311B443,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetIntArray_mDE81E177A1373A3F217EF11E2D42E60F36A843B3,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector2Array_mD2DC81532D69A36F5BC7DD9F499DF39D7725343E,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector3Array_mD4A3CDFD86163EFE8149003B05FF9B3C57FF3D81,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetVector4Array_m51609A4B6E68E106C92D189C54D8BAAA6E34823B,
	GltfConversions_t5192F1A9C9CA9E60017514FBF165CDC3076383EB_CustomAttributesCacheGenerator_GltfConversions_GetColorArray_m520810F7A52EA5CC593D3F74C925C511304BD1A9,
	GltfUtility_tAA44ED2D01F578519AB192D2098F2A6265DACFA7_CustomAttributesCacheGenerator_GltfUtility_ImportGltfObjectFromPathAsync_m3F1F281E883DC4B6BD5B8588ED833FD8E3CB8A74,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_t5F2D52F46443357658667E5DFD6A8B16EE1823FB_CustomAttributesCacheGenerator_U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m708F203C7D63CF121EB678728B321A57509E2097,
	GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_GltfAccessor_get_BufferView_mBF991561A98B0BB5FD0E3A655D5E8616961DBF93,
	GltfAccessor_tC009E45AE97D3ED63FCE294678F2563F62B8BAB5_CustomAttributesCacheGenerator_GltfAccessor_set_BufferView_mA1BE80D7B2D106A658A1292327A86C65FF53E4E5,
	GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_GltfBuffer_get_BufferData_m5FBC24C211FFC93370C4CA56C07809DA7DF59257,
	GltfBuffer_tCF5ED34530745B1A372D7F2DEBF5828E262CDE5F_CustomAttributesCacheGenerator_GltfBuffer_set_BufferData_mB3E1712AF73DCFEF5407692BAACD1FBD1384DC41,
	GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_GltfBufferView_get_Buffer_m47253FD5C18EC856A56348EC0B9855C42B0A9EE0,
	GltfBufferView_t05A6058BBC1BBAD339313A6FC6C3B0627D8A34CA_CustomAttributesCacheGenerator_GltfBufferView_set_Buffer_m3CF4046017E0B5AFE7496913FD3FCA25C962EF9A,
	GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_GltfImage_get_Texture_mD779E699883558093A72BCA8A13B77149310DDBD,
	GltfImage_tFB309FAE74AFCAA5BE4D9AB14A5879844A3378C5_CustomAttributesCacheGenerator_GltfImage_set_Texture_mEE09E29892BC1EB99FE4F516C6A79ADB2801061A,
	GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_GltfMaterial_get_Material_mFA2E7C0146232BED65AFA5E64EB1F01388141C20,
	GltfMaterial_tF60A19CEF72BDCF67397E5DDFBADD0EF75EBF50D_CustomAttributesCacheGenerator_GltfMaterial_set_Material_mBB2F569AFBBCD2A8F7BB8AF6EC002CDCA0456B7B,
	GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_GltfMesh_get_Mesh_mDEE88954BB0F1F79F13A090C0FFCCF604F1836E7,
	GltfMesh_t031F3A3814DAEC2B89FCF502701F70A79C966271_CustomAttributesCacheGenerator_GltfMesh_set_Mesh_m21306B466FCECF334B5CCC1010AE69F2644D6955,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_Targets_m1C5161E796B842241E9220E5CDF6FE3C7730321A,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_Targets_m47A1CF3F7C5812BA6622E06E983D6C6247A56427,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_Attributes_m937303E06650F7DA0F1DEC3DA0BAF7A65C7D32E9,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_Attributes_m55351ED659CB203206754110C2E2EE96F8FA32FA,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_get_SubMesh_mFC5DBA13B54CC0970537C50F5E63AD9588906EEF,
	GltfMeshPrimitive_t6EB7D1B49E90BBFB2070B431E2E8DD27321E6C42_CustomAttributesCacheGenerator_GltfMeshPrimitive_set_SubMesh_mF33518F7D3AC143DF21ACA3BA58BC88F27C58795,
	GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_GltfNode_get_Matrix_m1B810ECC2DC3F1D93783DC05A31EBA8AB578E3C5,
	GltfNode_t7E92F3A3061F1F06F40D298AA3EF98C71B8FAFC3_CustomAttributesCacheGenerator_GltfNode_set_Matrix_m3248A545B8361419A301F284ACF9BBA135F85051,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_Name_m6EEDCBC4E03E20908BCAD19D627DB2E707766178,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_Name_m47895CAC77FC47B9000CFDCBAECE315E37444B4A,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_Uri_m63C0AA0B7A6CF31A2EFD26C8A6645F4E37B059F3,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_Uri_m735F5DA5B8E21AF02E397BCC7995D8A9468E9D41,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_GameObjectReference_m2B3785589D407870E1EDF983FA2EADC74121CB19,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_GameObjectReference_m006D03F1F66EA53E5FBD779059C4AEFD5FD0308A,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_RegisteredExtensions_m429D2F362F9FADA1237D3B571A03BD432503F507,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_RegisteredExtensions_mF87255C41D50B99E1FE43201F1E3796C6376E976,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_get_UseBackgroundThread_m921E5B1288396728EAB6AA773BAB9B07E09B1AEF,
	GltfObject_t6CE487F5D10F109BD5FD297A08EBD799EF6314B2_CustomAttributesCacheGenerator_GltfObject_set_UseBackgroundThread_mCD113787FF499BD925D6B5543F0340DFB8AA36A7,
	GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_GltfTexture_get_Texture_m95F363CA33062CDB544D6131F824ED6C2D59F69B,
	GltfTexture_t7DFD642A8BD043698BF815547E971121B5D18D58_CustomAttributesCacheGenerator_GltfTexture_set_Texture_mD812992D23C0E4FEB265F281EC31C14523BF5EB2,
	Microsoft_MixedReality_Toolkit_Gltf_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
