﻿using Assets.Externals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    static class DataReceiver
    {
        public enum ServerPackets 
        {
            SWelcomeMessage = 1,

            SRemoveControls = 2,

            SRestoreControls = 3,

            SUpdatePosition = 4,

            SUpdateRotation = 5,

            SUpdateScale = 6,

            SUpdateEulerAngle = 7,

        }

        public class Transform
        {
            public Vector3 position { get; set; }
            public Quaternion rotation { get; set; }
            public Vector3 scale { get; set; }
        }

        private static HandleControls handleControls;
        private static UpdateObject updateObject;

        public static HandleControls HandleControls { get => handleControls; set => handleControls = value; }
        public static UpdateObject UpdateObject { get => updateObject; set => updateObject = value; }

        public static void HandleWelcomeMsg(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            switch (msg)
            {
                case "Blue":
                    updateObject.MyColor = Color.blue;
                    break;
                case "Cyan":
                    updateObject.MyColor = Color.cyan;
                    break;
                case "Green":
                    updateObject.MyColor = Color.green;
                    break;
                case "Magenta":
                    updateObject.MyColor = Color.magenta;
                    break;
                case "Red":
                    updateObject.MyColor = Color.red;
                    break;
                case "Yellow":
                    updateObject.MyColor = Color.yellow;
                    break;
                case "Gray":
                    updateObject.MyColor = Color.gray;
                    break;
            }

            updateObject.ShowHologram();

            Debug.Log(msg);
            DataSender.SendHelloServer();
        }

        public static void HandleRemoveControls(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            handleControls.RemoveControlsComponent(JsonUtility.FromJson<Color>(msg));

            Debug.Log(msg);
            
        }

        public static void HandleRestoreControls(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            handleControls.RestoreControlsComponent();

            Debug.Log(msg);

        }

        public static void HandleUpdatePosition(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            updateObject.UpdatePosition(JsonUtility.FromJson<Vector3>(msg));

            Debug.Log(msg);

        }

        public static void HandleUpdateRotation(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            updateObject.UpdateRotation(JsonUtility.FromJson<Quaternion>(msg));

            Debug.Log(msg);

        }

        public static void HandleUpdateScale(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            updateObject.UpdateScale(JsonUtility.FromJson<Vector3>(msg));

            Debug.Log(msg);

        }

        public static void HandleUpdateEulerAngle(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            string msg = buffer.ReadString();
            buffer.Dispose();

            updateObject.UpdateEulerAngle(JsonUtility.FromJson<Vector3>(msg));

            Debug.Log(msg);

        }
    }
}
