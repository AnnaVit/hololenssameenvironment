﻿using Assets.Scripts;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleControls : MonoBehaviour
{
    [SerializeField]
    GameObject hologram;

    private void Start()
    {
        DataReceiver.HandleControls = this;
    }


    public void RemoveControlsComponent(Color colorMaster)
    {
        hologram.GetComponent<BoundsControl>().enabled = false;

        hologram.GetComponent<DimBoxes.BoundBox>().enabled = true;
        hologram.GetComponent<DimBoxes.BoundBox>().lineColor = colorMaster;
        hologram.GetComponent<DimBoxes.BoundBox>().init();
       
    }

    public void RestoreControlsComponent()
    {
        hologram.GetComponent<BoundsControl>().enabled = true;

        hologram.GetComponent<DimBoxes.BoundBox>().enabled = false;
    }
}
