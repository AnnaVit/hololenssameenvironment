﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::OnValidate()
extern void DisplayInputResult_OnValidate_mE7EDD50BC3E850DD16163BE181F321D1AD2C2E11 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::Update()
extern void DisplayInputResult_Update_m8D4CEDF6D2D78D917B732BB689BBC5F5AFCA819F (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::.ctor()
extern void DisplayInputResult__ctor_m12D77C1F4B5939FA7DE5213BA396F09365ACC9D3 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::Update()
extern void ListInputFeatureUsages_Update_mC0E120575E1F9C4316408558050A6E9FDFA4964E (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::.ctor()
extern void ListInputFeatureUsages__ctor_mCE9C6B5EFA9966396CF688348915A88F115AF50E (void);
static Il2CppMethodPointer s_methodPointers[5] = 
{
	DisplayInputResult_OnValidate_mE7EDD50BC3E850DD16163BE181F321D1AD2C2E11,
	DisplayInputResult_Update_m8D4CEDF6D2D78D917B732BB689BBC5F5AFCA819F,
	DisplayInputResult__ctor_m12D77C1F4B5939FA7DE5213BA396F09365ACC9D3,
	ListInputFeatureUsages_Update_mC0E120575E1F9C4316408558050A6E9FDFA4964E,
	ListInputFeatureUsages__ctor_mCE9C6B5EFA9966396CF688348915A88F115AF50E,
};
static const int32_t s_InvokerIndices[5] = 
{
	4406,
	4406,
	4406,
	4406,
	4406,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Tools_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Tools_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Tools_Runtime_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Tools.Runtime.dll",
	5,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Tools_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
