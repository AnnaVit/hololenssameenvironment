﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.Collections.Generic.IEnumerable`1<System.String>>
struct Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439;
// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3;
// System.Collections.Generic.Dictionary`2<System.Int32,Assets.Scripts.ClientHandleData/Packet>
struct Dictionary_2_tCEA98A7DE07818B811263424883C65DB7D634076;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.BaseEventSystem/EventHandlerEntry>>
struct Dictionary_2_t8A32D41C781DF6C3B200705B4DAD08DAA0C1E945;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler>
struct EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F;
// System.Func`2<Vuforia.DataSet,System.String>
struct Func_2_tA0026E1C9518F11FE5F6997C002B74851CF25921;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t8B45BBA625F1F9197CEB4999F9B2A963FCE4B92D;
// System.Collections.Generic.List`1<System.Tuple`2<Microsoft.MixedReality.Toolkit.BaseEventSystem/Action,UnityEngine.GameObject>>
struct List_1_tA938D4A4ACBD0B6F06021A86EA1FBAFE0D8885AB;
// System.Collections.Generic.List`1<System.Tuple`3<Microsoft.MixedReality.Toolkit.BaseEventSystem/Action,System.Type,UnityEngine.EventSystems.IEventSystemHandler>>
struct List_1_t6872BA3D1F05A344532B0D53CFA8AF9EB0BA351D;
// System.Collections.Generic.List`1<UnityEngine.Vector3[][]>
struct List_1_t22506B3452F881404307E66213B0A382BA5C1DA3;
// System.Collections.Generic.List`1<UnityEngine.Vector3[0...,0...]>
struct List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.IMixedRealityDataProvider>
struct List_1_t2E3A2D87C80FADCE004ABDAD48585B2693D9B3F3;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F;
// System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>
struct List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E;
// System.Collections.Generic.List`1<Vuforia.ModelTargetBehaviour>
struct List_1_t40D79F96F2804A6662D8C123BA490604CF2D60E9;
// Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject>
struct MixedRealitySpatialAwarenessEventData_1_t00EC2B520F876AA2904782A9299DCBEB60209B8B;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// UnityEngine.LineRenderer[]
struct LineRendererU5BU5D_t65EF16DA3FB3E6D083B247824B04BE10D0B46743;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// VuMarkHandler/AugmentationObject[]
struct AugmentationObjectU5BU5D_t1BC7E9AF1E891CACD8C3D092105858257FDE482D;
// UnityEngine.Vector3[0...,0...]
struct Vector3U5BU2CU5D_tF16B36EC585DBDA6C179E131AE5794A33695CA71;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile
struct BaseMixedRealityProfile_tEE9571E16121313E9A2B8A37D702B862D6D87B9C;
// DimBoxes.BoundBox
struct BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F;
// UnityEngine.BoxCollider
struct BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// Assets.Externals.ByteBuffer
struct ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// HandleControls
struct HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar
struct IMixedRealityServiceRegistrar_tFA3986A2825A8869D5550DEA14F57F4F1981B0EA;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// HoloToolkit.Unity.Interpolator
struct Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9;
// UnityEngine.Light
struct Light_tA2F349FE839781469A0344CF6039B51512394275;
// UnityEngine.LineRenderer
struct LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile
struct MixedRealitySpatialAwarenessSystemProfile_tC7E99BA5DC82D06919DE73FF542CAE4929C41D4B;
// Vuforia.ModelTargetBehaviour
struct ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A;
// ModelTargetsManager
struct ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D;
// ModelTargetsUIManager
struct ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169;
// NetworkManager
struct NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t160A2538024FE3EC707872435D01F1C20B3B1A48;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Vuforia.ObjectTracker
struct ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// Vuforia.StateManager
struct StateManager_t1452558828440FBD9B4570C03085876DD02337E8;
// System.String
struct String_t;
// System.Net.Sockets.TcpClient
struct TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE;
// Microsoft.MixedReality.Toolkit.Teleport.TeleportEventData
struct TeleportEventData_tBBBA556B30350D9370F72039B09B42102FA0681A;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextMesh
struct TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t739912483540FF363C2BC828A6930E0397CAC0A8;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UpdateObject
struct UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42;
// VuMarkHandler
struct VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1;
// Vuforia.VuMarkManager
struct VuMarkManager_t3E90D7E4ABBD0C9AAC4022C35D5FAB2F78770B72;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t99CF96AE99AC2AF110A4BFBB5209B39980C75540;
// VuMarkTrackableStatusUI
struct VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615;
// DimBoxes.maxCamera
struct maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7;
// Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem/SceneContentTracker
struct SceneContentTracker_t9B55B2C7D399F026506AC3039C94B5827E47A67B;
// Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem/SceneLightingExecutor
struct SceneLightingExecutor_tDA0C88C144C1A6CEC68C5BA218EFC6AFB3F85C07;
// VuMarkHandler/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t18E6F43B875C7A6DC8C338B26A304C644263098F 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t4FABEA3CEED916354215300FD2C31C3449FFBBE3 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tE9A3975296CDF88EEC2B5A711AA5BC6FD4B0E156 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t4CBE0AEE5BFE7A6169297CE8CCFF5A2312530C7F 
{
public:

public:
};


// System.Object


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528  : public RuntimeObject
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.BaseService
struct  BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578  : public RuntimeObject
{
public:
	// System.String Microsoft.MixedReality.Toolkit.BaseService::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.UInt32 Microsoft.MixedReality.Toolkit.BaseService::<Priority>k__BackingField
	uint32_t ___U3CPriorityU3Ek__BackingField_2;
	// Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile Microsoft.MixedReality.Toolkit.BaseService::<ConfigurationProfile>k__BackingField
	BaseMixedRealityProfile_tEE9571E16121313E9A2B8A37D702B862D6D87B9C * ___U3CConfigurationProfileU3Ek__BackingField_3;
	// System.Boolean Microsoft.MixedReality.Toolkit.BaseService::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578, ___U3CPriorityU3Ek__BackingField_2)); }
	inline uint32_t get_U3CPriorityU3Ek__BackingField_2() const { return ___U3CPriorityU3Ek__BackingField_2; }
	inline uint32_t* get_address_of_U3CPriorityU3Ek__BackingField_2() { return &___U3CPriorityU3Ek__BackingField_2; }
	inline void set_U3CPriorityU3Ek__BackingField_2(uint32_t value)
	{
		___U3CPriorityU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CConfigurationProfileU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578, ___U3CConfigurationProfileU3Ek__BackingField_3)); }
	inline BaseMixedRealityProfile_tEE9571E16121313E9A2B8A37D702B862D6D87B9C * get_U3CConfigurationProfileU3Ek__BackingField_3() const { return ___U3CConfigurationProfileU3Ek__BackingField_3; }
	inline BaseMixedRealityProfile_tEE9571E16121313E9A2B8A37D702B862D6D87B9C ** get_address_of_U3CConfigurationProfileU3Ek__BackingField_3() { return &___U3CConfigurationProfileU3Ek__BackingField_3; }
	inline void set_U3CConfigurationProfileU3Ek__BackingField_3(BaseMixedRealityProfile_tEE9571E16121313E9A2B8A37D702B862D6D87B9C * value)
	{
		___U3CConfigurationProfileU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConfigurationProfileU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};


// Assets.Externals.ByteBuffer
struct  ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> Assets.Externals.ByteBuffer::buff
	List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * ___buff_0;
	// System.Byte[] Assets.Externals.ByteBuffer::readBuff
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___readBuff_1;
	// System.Int32 Assets.Externals.ByteBuffer::readPos
	int32_t ___readPos_2;
	// System.Boolean Assets.Externals.ByteBuffer::buffUpdated
	bool ___buffUpdated_3;
	// System.Boolean Assets.Externals.ByteBuffer::dispose
	bool ___dispose_4;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623, ___buff_0)); }
	inline List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * get_buff_0() const { return ___buff_0; }
	inline List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(List_1_tD0117BC32B3DBF148E7E9AC108FC376C3D4922CF * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buff_0), (void*)value);
	}

	inline static int32_t get_offset_of_readBuff_1() { return static_cast<int32_t>(offsetof(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623, ___readBuff_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_readBuff_1() const { return ___readBuff_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_readBuff_1() { return &___readBuff_1; }
	inline void set_readBuff_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___readBuff_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___readBuff_1), (void*)value);
	}

	inline static int32_t get_offset_of_readPos_2() { return static_cast<int32_t>(offsetof(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623, ___readPos_2)); }
	inline int32_t get_readPos_2() const { return ___readPos_2; }
	inline int32_t* get_address_of_readPos_2() { return &___readPos_2; }
	inline void set_readPos_2(int32_t value)
	{
		___readPos_2 = value;
	}

	inline static int32_t get_offset_of_buffUpdated_3() { return static_cast<int32_t>(offsetof(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623, ___buffUpdated_3)); }
	inline bool get_buffUpdated_3() const { return ___buffUpdated_3; }
	inline bool* get_address_of_buffUpdated_3() { return &___buffUpdated_3; }
	inline void set_buffUpdated_3(bool value)
	{
		___buffUpdated_3 = value;
	}

	inline static int32_t get_offset_of_dispose_4() { return static_cast<int32_t>(offsetof(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623, ___dispose_4)); }
	inline bool get_dispose_4() const { return ___dispose_4; }
	inline bool* get_address_of_dispose_4() { return &___dispose_4; }
	inline void set_dispose_4(bool value)
	{
		___dispose_4 = value;
	}
};


// HoloToolkit.Unity.CameraCache
struct  CameraCache_t8FE25B8A2D81F52E694B70EF48DE821618DFC033  : public RuntimeObject
{
public:

public:
};

struct CameraCache_t8FE25B8A2D81F52E694B70EF48DE821618DFC033_StaticFields
{
public:
	// UnityEngine.Camera HoloToolkit.Unity.CameraCache::cachedCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cachedCamera_0;

public:
	inline static int32_t get_offset_of_cachedCamera_0() { return static_cast<int32_t>(offsetof(CameraCache_t8FE25B8A2D81F52E694B70EF48DE821618DFC033_StaticFields, ___cachedCamera_0)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_cachedCamera_0() const { return ___cachedCamera_0; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_cachedCamera_0() { return &___cachedCamera_0; }
	inline void set_cachedCamera_0(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___cachedCamera_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cachedCamera_0), (void*)value);
	}
};


// Assets.Scripts.ClientHandleData
struct  ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921  : public RuntimeObject
{
public:

public:
};

struct ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Assets.Scripts.ClientHandleData/Packet> Assets.Scripts.ClientHandleData::packets
	Dictionary_2_tCEA98A7DE07818B811263424883C65DB7D634076 * ___packets_0;
	// Assets.Externals.ByteBuffer Assets.Scripts.ClientHandleData::playerBuffer
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623 * ___playerBuffer_1;

public:
	inline static int32_t get_offset_of_packets_0() { return static_cast<int32_t>(offsetof(ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921_StaticFields, ___packets_0)); }
	inline Dictionary_2_tCEA98A7DE07818B811263424883C65DB7D634076 * get_packets_0() const { return ___packets_0; }
	inline Dictionary_2_tCEA98A7DE07818B811263424883C65DB7D634076 ** get_address_of_packets_0() { return &___packets_0; }
	inline void set_packets_0(Dictionary_2_tCEA98A7DE07818B811263424883C65DB7D634076 * value)
	{
		___packets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___packets_0), (void*)value);
	}

	inline static int32_t get_offset_of_playerBuffer_1() { return static_cast<int32_t>(offsetof(ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921_StaticFields, ___playerBuffer_1)); }
	inline ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623 * get_playerBuffer_1() const { return ___playerBuffer_1; }
	inline ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623 ** get_address_of_playerBuffer_1() { return &___playerBuffer_1; }
	inline void set_playerBuffer_1(ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623 * value)
	{
		___playerBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerBuffer_1), (void*)value);
	}
};


// Assets.Scripts.ClientTCP
struct  ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A  : public RuntimeObject
{
public:

public:
};

struct ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields
{
public:
	// System.Net.Sockets.TcpClient Assets.Scripts.ClientTCP::clientSocket
	TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * ___clientSocket_0;
	// System.Net.Sockets.NetworkStream Assets.Scripts.ClientTCP::stream
	NetworkStream_t160A2538024FE3EC707872435D01F1C20B3B1A48 * ___stream_1;
	// System.Byte[] Assets.Scripts.ClientTCP::recBuffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___recBuffer_2;
	// System.String Assets.Scripts.ClientTCP::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_clientSocket_0() { return static_cast<int32_t>(offsetof(ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields, ___clientSocket_0)); }
	inline TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * get_clientSocket_0() const { return ___clientSocket_0; }
	inline TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE ** get_address_of_clientSocket_0() { return &___clientSocket_0; }
	inline void set_clientSocket_0(TcpClient_t0EEB05EA031F6AFD93D46116F5E33A9C4E3350EE * value)
	{
		___clientSocket_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientSocket_0), (void*)value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields, ___stream_1)); }
	inline NetworkStream_t160A2538024FE3EC707872435D01F1C20B3B1A48 * get_stream_1() const { return ___stream_1; }
	inline NetworkStream_t160A2538024FE3EC707872435D01F1C20B3B1A48 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(NetworkStream_t160A2538024FE3EC707872435D01F1C20B3B1A48 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stream_1), (void*)value);
	}

	inline static int32_t get_offset_of_recBuffer_2() { return static_cast<int32_t>(offsetof(ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields, ___recBuffer_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_recBuffer_2() const { return ___recBuffer_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_recBuffer_2() { return &___recBuffer_2; }
	inline void set_recBuffer_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___recBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recBuffer_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields, ___U3CServerAddressU3Ek__BackingField_3)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_3() const { return ___U3CServerAddressU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_3() { return &___U3CServerAddressU3Ek__BackingField_3; }
	inline void set_U3CServerAddressU3Ek__BackingField_3(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServerAddressU3Ek__BackingField_3), (void*)value);
	}
};


// Assets.Scripts.DataReceiver
struct  DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E  : public RuntimeObject
{
public:

public:
};

struct DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E_StaticFields
{
public:
	// HandleControls Assets.Scripts.DataReceiver::handleControls
	HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984 * ___handleControls_0;
	// UpdateObject Assets.Scripts.DataReceiver::updateObject
	UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B * ___updateObject_1;

public:
	inline static int32_t get_offset_of_handleControls_0() { return static_cast<int32_t>(offsetof(DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E_StaticFields, ___handleControls_0)); }
	inline HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984 * get_handleControls_0() const { return ___handleControls_0; }
	inline HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984 ** get_address_of_handleControls_0() { return &___handleControls_0; }
	inline void set_handleControls_0(HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984 * value)
	{
		___handleControls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handleControls_0), (void*)value);
	}

	inline static int32_t get_offset_of_updateObject_1() { return static_cast<int32_t>(offsetof(DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E_StaticFields, ___updateObject_1)); }
	inline UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B * get_updateObject_1() const { return ___updateObject_1; }
	inline UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B ** get_address_of_updateObject_1() { return &___updateObject_1; }
	inline void set_updateObject_1(UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B * value)
	{
		___updateObject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___updateObject_1), (void*)value);
	}
};


// Assets.Scripts.DataSender
struct  DataSender_t2039D3993C46C8C2D8ED9CA4906D4CC5623FDEBC  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Assets.Scripts.ClientTCP/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t9712E44A4DA708FBA21BD30CC4EC3AF748B55EE0  : public RuntimeObject
{
public:
	// System.Byte[] Assets.Scripts.ClientTCP/<>c__DisplayClass9_0::newBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___newBytes_0;

public:
	inline static int32_t get_offset_of_newBytes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t9712E44A4DA708FBA21BD30CC4EC3AF748B55EE0, ___newBytes_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_newBytes_0() const { return ___newBytes_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_newBytes_0() { return &___newBytes_0; }
	inline void set_newBytes_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___newBytes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newBytes_0), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c
struct  U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE_StaticFields
{
public:
	// Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<>9
	U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// ModelTargetsUIManager/<ClearImageSequencePause>d__21
struct  U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3  : public RuntimeObject
{
public:
	// System.Int32 ModelTargetsUIManager/<ClearImageSequencePause>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ModelTargetsUIManager/<ClearImageSequencePause>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ModelTargetsUIManager ModelTargetsUIManager/<ClearImageSequencePause>d__21::<>4__this
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3, ___U3CU3E4__this_2)); }
	inline ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityThread/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t942BA12B202E70079D8B79ED42A1984B1961BA63  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UnityThread/<>c__DisplayClass12_0::action
	RuntimeObject* ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t942BA12B202E70079D8B79ED42A1984B1961BA63, ___action_0)); }
	inline RuntimeObject* get_action_0() const { return ___action_0; }
	inline RuntimeObject** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(RuntimeObject* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___action_0), (void*)value);
	}
};


// UpdateObject/<DelayTime>d__14
struct  U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3  : public RuntimeObject
{
public:
	// System.Int32 UpdateObject/<DelayTime>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UpdateObject/<DelayTime>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 UpdateObject/<DelayTime>d__14::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Single UpdateObject/<DelayTime>d__14::time
	float ___time_3;
	// System.Single UpdateObject/<DelayTime>d__14::<>3__time
	float ___U3CU3E3__time_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__time_4() { return static_cast<int32_t>(offsetof(U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3, ___U3CU3E3__time_4)); }
	inline float get_U3CU3E3__time_4() const { return ___U3CU3E3__time_4; }
	inline float* get_address_of_U3CU3E3__time_4() { return &___U3CU3E3__time_4; }
	inline void set_U3CU3E3__time_4(float value)
	{
		___U3CU3E3__time_4 = value;
	}
};


// VuMarkHandler/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C  : public RuntimeObject
{
public:
	// Vuforia.VuMarkBehaviour VuMarkHandler/<>c__DisplayClass18_0::vumarkBehaviour
	VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * ___vumarkBehaviour_0;

public:
	inline static int32_t get_offset_of_vumarkBehaviour_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C, ___vumarkBehaviour_0)); }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * get_vumarkBehaviour_0() const { return ___vumarkBehaviour_0; }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 ** get_address_of_vumarkBehaviour_0() { return &___vumarkBehaviour_0; }
	inline void set_vumarkBehaviour_0(VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * value)
	{
		___vumarkBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkBehaviour_0), (void*)value);
	}
};


// VuMarkHandler/<OnVuMarkTargetAvailable>d__18
struct  U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005  : public RuntimeObject
{
public:
	// System.Int32 VuMarkHandler/<OnVuMarkTargetAvailable>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VuMarkHandler/<OnVuMarkTargetAvailable>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Vuforia.VuMarkBehaviour VuMarkHandler/<OnVuMarkTargetAvailable>d__18::vumarkBehaviour
	VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * ___vumarkBehaviour_2;
	// VuMarkHandler VuMarkHandler/<OnVuMarkTargetAvailable>d__18::<>4__this
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * ___U3CU3E4__this_3;
	// VuMarkHandler/<>c__DisplayClass18_0 VuMarkHandler/<OnVuMarkTargetAvailable>d__18::<>8__1
	U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C * ___U3CU3E8__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_vumarkBehaviour_2() { return static_cast<int32_t>(offsetof(U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005, ___vumarkBehaviour_2)); }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * get_vumarkBehaviour_2() const { return ___vumarkBehaviour_2; }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 ** get_address_of_vumarkBehaviour_2() { return &___vumarkBehaviour_2; }
	inline void set_vumarkBehaviour_2(VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * value)
	{
		___vumarkBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkBehaviour_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005, ___U3CU3E4__this_3)); }
	inline VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_4), (void*)value);
	}
};


// VuMarkHandler/<ShowPanelAfter>d__35
struct  U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52  : public RuntimeObject
{
public:
	// System.Int32 VuMarkHandler/<ShowPanelAfter>d__35::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VuMarkHandler/<ShowPanelAfter>d__35::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single VuMarkHandler/<ShowPanelAfter>d__35::seconds
	float ___seconds_2;
	// VuMarkHandler VuMarkHandler/<ShowPanelAfter>d__35::<>4__this
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * ___U3CU3E4__this_3;
	// System.String VuMarkHandler/<ShowPanelAfter>d__35::vuMarkId
	String_t* ___vuMarkId_4;
	// System.String VuMarkHandler/<ShowPanelAfter>d__35::vuMarkDataType
	String_t* ___vuMarkDataType_5;
	// System.String VuMarkHandler/<ShowPanelAfter>d__35::vuMarkDesc
	String_t* ___vuMarkDesc_6;
	// UnityEngine.Sprite VuMarkHandler/<ShowPanelAfter>d__35::vuMarkImage
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___vuMarkImage_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___seconds_2)); }
	inline float get_seconds_2() const { return ___seconds_2; }
	inline float* get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(float value)
	{
		___seconds_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___U3CU3E4__this_3)); }
	inline VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_vuMarkId_4() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___vuMarkId_4)); }
	inline String_t* get_vuMarkId_4() const { return ___vuMarkId_4; }
	inline String_t** get_address_of_vuMarkId_4() { return &___vuMarkId_4; }
	inline void set_vuMarkId_4(String_t* value)
	{
		___vuMarkId_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuMarkId_4), (void*)value);
	}

	inline static int32_t get_offset_of_vuMarkDataType_5() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___vuMarkDataType_5)); }
	inline String_t* get_vuMarkDataType_5() const { return ___vuMarkDataType_5; }
	inline String_t** get_address_of_vuMarkDataType_5() { return &___vuMarkDataType_5; }
	inline void set_vuMarkDataType_5(String_t* value)
	{
		___vuMarkDataType_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuMarkDataType_5), (void*)value);
	}

	inline static int32_t get_offset_of_vuMarkDesc_6() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___vuMarkDesc_6)); }
	inline String_t* get_vuMarkDesc_6() const { return ___vuMarkDesc_6; }
	inline String_t** get_address_of_vuMarkDesc_6() { return &___vuMarkDesc_6; }
	inline void set_vuMarkDesc_6(String_t* value)
	{
		___vuMarkDesc_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuMarkDesc_6), (void*)value);
	}

	inline static int32_t get_offset_of_vuMarkImage_7() { return static_cast<int32_t>(offsetof(U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52, ___vuMarkImage_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_vuMarkImage_7() const { return ___vuMarkImage_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_vuMarkImage_7() { return &___vuMarkImage_7; }
	inline void set_vuMarkImage_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___vuMarkImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuMarkImage_7), (void*)value);
	}
};


// VuMarkHandler/AugmentationObject
struct  AugmentationObject_t591E71F0CFBF761AF0B1F2D96031031B1BBD9C58  : public RuntimeObject
{
public:
	// System.String VuMarkHandler/AugmentationObject::vumarkID
	String_t* ___vumarkID_0;
	// UnityEngine.GameObject VuMarkHandler/AugmentationObject::augmentation
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___augmentation_1;

public:
	inline static int32_t get_offset_of_vumarkID_0() { return static_cast<int32_t>(offsetof(AugmentationObject_t591E71F0CFBF761AF0B1F2D96031031B1BBD9C58, ___vumarkID_0)); }
	inline String_t* get_vumarkID_0() const { return ___vumarkID_0; }
	inline String_t** get_address_of_vumarkID_0() { return &___vumarkID_0; }
	inline void set_vumarkID_0(String_t* value)
	{
		___vumarkID_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkID_0), (void*)value);
	}

	inline static int32_t get_offset_of_augmentation_1() { return static_cast<int32_t>(offsetof(AugmentationObject_t591E71F0CFBF761AF0B1F2D96031031B1BBD9C58, ___augmentation_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_augmentation_1() const { return ___augmentation_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_augmentation_1() { return &___augmentation_1; }
	inline void set_augmentation_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___augmentation_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___augmentation_1), (void*)value);
	}
};


// VuforiaAdditiveSceneLoader/<>c
struct  U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields
{
public:
	// VuforiaAdditiveSceneLoader/<>c VuforiaAdditiveSceneLoader/<>c::<>9
	U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124 * ___U3CU3E9_0;
	// System.Func`2<Vuforia.DataSet,System.String> VuforiaAdditiveSceneLoader/<>c::<>9__4_0
	Func_2_tA0026E1C9518F11FE5F6997C002B74851CF25921 * ___U3CU3E9__4_0_1;
	// System.Func`2<System.String,System.Boolean> VuforiaAdditiveSceneLoader/<>c::<>9__4_1
	Func_2_t8B45BBA625F1F9197CEB4999F9B2A963FCE4B92D * ___U3CU3E9__4_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_tA0026E1C9518F11FE5F6997C002B74851CF25921 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_tA0026E1C9518F11FE5F6997C002B74851CF25921 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_tA0026E1C9518F11FE5F6997C002B74851CF25921 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields, ___U3CU3E9__4_1_2)); }
	inline Func_2_t8B45BBA625F1F9197CEB4999F9B2A963FCE4B92D * get_U3CU3E9__4_1_2() const { return ___U3CU3E9__4_1_2; }
	inline Func_2_t8B45BBA625F1F9197CEB4999F9B2A963FCE4B92D ** get_address_of_U3CU3E9__4_1_2() { return &___U3CU3E9__4_1_2; }
	inline void set_U3CU3E9__4_1_2(Func_2_t8B45BBA625F1F9197CEB4999F9B2A963FCE4B92D * value)
	{
		___U3CU3E9__4_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_1_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Microsoft.MixedReality.Toolkit.Utilities.AxisType
struct  AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Bounds
struct  Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// Assets.Scripts.ClientPackets
struct  ClientPackets_tBB32DF50AC986BFFFCD804B6D01A3FEA857939D3 
{
public:
	// System.Int32 Assets.Scripts.ClientPackets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientPackets_tBB32DF50AC986BFFFCD804B6D01A3FEA857939D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Manipulation
struct  Manipulation_t7DD93A854132277D32A7980DEEA84AB82904213E 
{
public:
	// System.Int32 Manipulation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Manipulation_t7DD93A854132277D32A7980DEEA84AB82904213E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// HoloToolkit.Unity.PivotAxis
struct  PivotAxis_t566603A3C8D29430881F470E06AC82E308AF2D9B 
{
public:
	// System.Int32 HoloToolkit.Unity.PivotAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PivotAxis_t566603A3C8D29430881F470E06AC82E308AF2D9B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Profiling.ProfilerMarker
struct  ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// DimBoxes.BoundBox/BoundSource
struct  BoundSource_t6A08E1F647A907F6EB5116F9AEE56F24C24957AF 
{
public:
	// System.Int32 DimBoxes.BoundBox/BoundSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundSource_t6A08E1F647A907F6EB5116F9AEE56F24C24957AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CustomTurnOffBehaviour/TurnOffRendering
struct  TurnOffRendering_t00AD57AC5E1944F728909CA8B78D5298B0D6F20B 
{
public:
	// System.Int32 CustomTurnOffBehaviour/TurnOffRendering::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TurnOffRendering_t00AD57AC5E1944F728909CA8B78D5298B0D6F20B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Assets.Scripts.DataReceiver/ServerPackets
struct  ServerPackets_tFEABB95C6CCC979FDA138BE5CF3B43FA1F4678AD 
{
public:
	// System.Int32 Assets.Scripts.DataReceiver/ServerPackets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ServerPackets_tFEABB95C6CCC979FDA138BE5CF3B43FA1F4678AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Assets.Scripts.DataReceiver/Transform
struct  Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Assets.Scripts.DataReceiver/Transform::<position>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CpositionU3Ek__BackingField_0;
	// UnityEngine.Quaternion Assets.Scripts.DataReceiver/Transform::<rotation>k__BackingField
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CrotationU3Ek__BackingField_1;
	// UnityEngine.Vector3 Assets.Scripts.DataReceiver/Transform::<scale>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CscaleU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185, ___U3CpositionU3Ek__BackingField_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CpositionU3Ek__BackingField_0() const { return ___U3CpositionU3Ek__BackingField_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CpositionU3Ek__BackingField_0() { return &___U3CpositionU3Ek__BackingField_0; }
	inline void set_U3CpositionU3Ek__BackingField_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CpositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CrotationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185, ___U3CrotationU3Ek__BackingField_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_U3CrotationU3Ek__BackingField_1() const { return ___U3CrotationU3Ek__BackingField_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_U3CrotationU3Ek__BackingField_1() { return &___U3CrotationU3Ek__BackingField_1; }
	inline void set_U3CrotationU3Ek__BackingField_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___U3CrotationU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CscaleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185, ___U3CscaleU3Ek__BackingField_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CscaleU3Ek__BackingField_2() const { return ___U3CscaleU3Ek__BackingField_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CscaleU3Ek__BackingField_2() { return &___U3CscaleU3Ek__BackingField_2; }
	inline void set_U3CscaleU3Ek__BackingField_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CscaleU3Ek__BackingField_2 = value;
	}
};


// DefaultTrackableEventHandler/TrackingStatusFilter
struct  TrackingStatusFilter_tC97280BCF7888F24C882F6B2CC9C30CC4FBFE134 
{
public:
	// System.Int32 DefaultTrackableEventHandler/TrackingStatusFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingStatusFilter_tC97280BCF7888F24C882F6B2CC9C30CC4FBFE134, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType
struct  DisplayType_t5138A1FC756E211BEB18A33E11810DF3ECE48C81 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayType_t5138A1FC756E211BEB18A33E11810DF3ECE48C81, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ModelTargetsManager/ModelTargetMode
struct  ModelTargetMode_t5BE2D9E3B55C9321F25AFB65BF1B736536B5D157 
{
public:
	// System.Int32 ModelTargetsManager/ModelTargetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelTargetMode_t5BE2D9E3B55C9321F25AFB65BF1B736536B5D157, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.TrackableBehaviour/Status
struct  Status_tF2BE2623798D864D426B709EEF47FFACC1B60BD9 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_tF2BE2623798D864D426B709EEF47FFACC1B60BD9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.TrackableBehaviour/StatusInfo
struct  StatusInfo_tB058245991A859E0E79AB17E03DC1D1CE01B6A46 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_tB058245991A859E0E79AB17E03DC1D1CE01B6A46, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DimBoxes.maxCamera/<DragObject>d__34
struct  U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515  : public RuntimeObject
{
public:
	// System.Int32 DimBoxes.maxCamera/<DragObject>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DimBoxes.maxCamera/<DragObject>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Vector3 DimBoxes.maxCamera/<DragObject>d__34::startingHit
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startingHit_2;
	// DimBoxes.maxCamera DimBoxes.maxCamera/<DragObject>d__34::<>4__this
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_startingHit_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515, ___startingHit_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startingHit_2() const { return ___startingHit_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startingHit_2() { return &___startingHit_2; }
	inline void set_startingHit_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startingHit_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515, ___U3CU3E4__this_3)); }
	inline maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.BaseEventSystem
struct  BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849  : public BaseService_tECD5AE76F00D74F75592E0FAB96F36E1182F0578
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.BaseEventSystem::eventExecutionDepth
	int32_t ___eventExecutionDepth_6;
	// System.Type Microsoft.MixedReality.Toolkit.BaseEventSystem::eventSystemHandlerType
	Type_t * ___eventSystemHandlerType_7;
	// System.Collections.Generic.List`1<System.Tuple`3<Microsoft.MixedReality.Toolkit.BaseEventSystem/Action,System.Type,UnityEngine.EventSystems.IEventSystemHandler>> Microsoft.MixedReality.Toolkit.BaseEventSystem::postponedActions
	List_1_t6872BA3D1F05A344532B0D53CFA8AF9EB0BA351D * ___postponedActions_8;
	// System.Collections.Generic.List`1<System.Tuple`2<Microsoft.MixedReality.Toolkit.BaseEventSystem/Action,UnityEngine.GameObject>> Microsoft.MixedReality.Toolkit.BaseEventSystem::postponedObjectActions
	List_1_tA938D4A4ACBD0B6F06021A86EA1FBAFE0D8885AB * ___postponedObjectActions_9;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.BaseEventSystem/EventHandlerEntry>> Microsoft.MixedReality.Toolkit.BaseEventSystem::<EventHandlersByType>k__BackingField
	Dictionary_2_t8A32D41C781DF6C3B200705B4DAD08DAA0C1E945 * ___U3CEventHandlersByTypeU3Ek__BackingField_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Microsoft.MixedReality.Toolkit.BaseEventSystem::<EventListeners>k__BackingField
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3CEventListenersU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_eventExecutionDepth_6() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___eventExecutionDepth_6)); }
	inline int32_t get_eventExecutionDepth_6() const { return ___eventExecutionDepth_6; }
	inline int32_t* get_address_of_eventExecutionDepth_6() { return &___eventExecutionDepth_6; }
	inline void set_eventExecutionDepth_6(int32_t value)
	{
		___eventExecutionDepth_6 = value;
	}

	inline static int32_t get_offset_of_eventSystemHandlerType_7() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___eventSystemHandlerType_7)); }
	inline Type_t * get_eventSystemHandlerType_7() const { return ___eventSystemHandlerType_7; }
	inline Type_t ** get_address_of_eventSystemHandlerType_7() { return &___eventSystemHandlerType_7; }
	inline void set_eventSystemHandlerType_7(Type_t * value)
	{
		___eventSystemHandlerType_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventSystemHandlerType_7), (void*)value);
	}

	inline static int32_t get_offset_of_postponedActions_8() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___postponedActions_8)); }
	inline List_1_t6872BA3D1F05A344532B0D53CFA8AF9EB0BA351D * get_postponedActions_8() const { return ___postponedActions_8; }
	inline List_1_t6872BA3D1F05A344532B0D53CFA8AF9EB0BA351D ** get_address_of_postponedActions_8() { return &___postponedActions_8; }
	inline void set_postponedActions_8(List_1_t6872BA3D1F05A344532B0D53CFA8AF9EB0BA351D * value)
	{
		___postponedActions_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___postponedActions_8), (void*)value);
	}

	inline static int32_t get_offset_of_postponedObjectActions_9() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___postponedObjectActions_9)); }
	inline List_1_tA938D4A4ACBD0B6F06021A86EA1FBAFE0D8885AB * get_postponedObjectActions_9() const { return ___postponedObjectActions_9; }
	inline List_1_tA938D4A4ACBD0B6F06021A86EA1FBAFE0D8885AB ** get_address_of_postponedObjectActions_9() { return &___postponedObjectActions_9; }
	inline void set_postponedObjectActions_9(List_1_tA938D4A4ACBD0B6F06021A86EA1FBAFE0D8885AB * value)
	{
		___postponedObjectActions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___postponedObjectActions_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEventHandlersByTypeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___U3CEventHandlersByTypeU3Ek__BackingField_10)); }
	inline Dictionary_2_t8A32D41C781DF6C3B200705B4DAD08DAA0C1E945 * get_U3CEventHandlersByTypeU3Ek__BackingField_10() const { return ___U3CEventHandlersByTypeU3Ek__BackingField_10; }
	inline Dictionary_2_t8A32D41C781DF6C3B200705B4DAD08DAA0C1E945 ** get_address_of_U3CEventHandlersByTypeU3Ek__BackingField_10() { return &___U3CEventHandlersByTypeU3Ek__BackingField_10; }
	inline void set_U3CEventHandlersByTypeU3Ek__BackingField_10(Dictionary_2_t8A32D41C781DF6C3B200705B4DAD08DAA0C1E945 * value)
	{
		___U3CEventHandlersByTypeU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEventHandlersByTypeU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEventListenersU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849, ___U3CEventListenersU3Ek__BackingField_11)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_U3CEventListenersU3Ek__BackingField_11() const { return ___U3CEventListenersU3Ek__BackingField_11; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_U3CEventListenersU3Ek__BackingField_11() { return &___U3CEventListenersU3Ek__BackingField_11; }
	inline void set_U3CEventListenersU3Ek__BackingField_11(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___U3CEventListenersU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEventListenersU3Ek__BackingField_11), (void*)value);
	}
};

struct BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849_StaticFields
{
public:
	// System.Boolean Microsoft.MixedReality.Toolkit.BaseEventSystem::enableDanglingHandlerDiagnostics
	bool ___enableDanglingHandlerDiagnostics_5;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.BaseEventSystem::TraverseEventSystemHandlerHierarchyPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___TraverseEventSystemHandlerHierarchyPerfMarker_12;

public:
	inline static int32_t get_offset_of_enableDanglingHandlerDiagnostics_5() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849_StaticFields, ___enableDanglingHandlerDiagnostics_5)); }
	inline bool get_enableDanglingHandlerDiagnostics_5() const { return ___enableDanglingHandlerDiagnostics_5; }
	inline bool* get_address_of_enableDanglingHandlerDiagnostics_5() { return &___enableDanglingHandlerDiagnostics_5; }
	inline void set_enableDanglingHandlerDiagnostics_5(bool value)
	{
		___enableDanglingHandlerDiagnostics_5 = value;
	}

	inline static int32_t get_offset_of_TraverseEventSystemHandlerHierarchyPerfMarker_12() { return static_cast<int32_t>(offsetof(BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849_StaticFields, ___TraverseEventSystemHandlerHierarchyPerfMarker_12)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_TraverseEventSystemHandlerHierarchyPerfMarker_12() const { return ___TraverseEventSystemHandlerHierarchyPerfMarker_12; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_TraverseEventSystemHandlerHierarchyPerfMarker_12() { return &___TraverseEventSystemHandlerHierarchyPerfMarker_12; }
	inline void set_TraverseEventSystemHandlerHierarchyPerfMarker_12(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___TraverseEventSystemHandlerHierarchyPerfMarker_12 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// Microsoft.MixedReality.Toolkit.BaseCoreSystem
struct  BaseCoreSystem_t6678B90A46A1209B425DA44AFA89C58AB1FFAAB2  : public BaseEventSystem_t0D51FCCD5463776BCD552CFE3E1C2DDBEA604849
{
public:
	// Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar Microsoft.MixedReality.Toolkit.BaseCoreSystem::<Registrar>k__BackingField
	RuntimeObject* ___U3CRegistrarU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CRegistrarU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(BaseCoreSystem_t6678B90A46A1209B425DA44AFA89C58AB1FFAAB2, ___U3CRegistrarU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CRegistrarU3Ek__BackingField_13() const { return ___U3CRegistrarU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CRegistrarU3Ek__BackingField_13() { return &___U3CRegistrarU3Ek__BackingField_13; }
	inline void set_U3CRegistrarU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CRegistrarU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRegistrarU3Ek__BackingField_13), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Assets.Scripts.ClientHandleData/Packet
struct  Packet_t099ACC464898B7230628694003E3136EA3FDA730  : public MulticastDelegate_t
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.BaseDataProviderAccessCoreSystem
struct  BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F  : public BaseCoreSystem_t6678B90A46A1209B425DA44AFA89C58AB1FFAAB2
{
public:
	// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.IMixedRealityDataProvider> Microsoft.MixedReality.Toolkit.BaseDataProviderAccessCoreSystem::dataProviders
	List_1_t2E3A2D87C80FADCE004ABDAD48585B2693D9B3F3 * ___dataProviders_14;

public:
	inline static int32_t get_offset_of_dataProviders_14() { return static_cast<int32_t>(offsetof(BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F, ___dataProviders_14)); }
	inline List_1_t2E3A2D87C80FADCE004ABDAD48585B2693D9B3F3 * get_dataProviders_14() const { return ___dataProviders_14; }
	inline List_1_t2E3A2D87C80FADCE004ABDAD48585B2693D9B3F3 ** get_address_of_dataProviders_14() { return &___dataProviders_14; }
	inline void set_dataProviders_14(List_1_t2E3A2D87C80FADCE004ABDAD48585B2693D9B3F3 * value)
	{
		___dataProviders_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataProviders_14), (void*)value);
	}
};

struct BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.BaseDataProviderAccessCoreSystem::UpdatePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdatePerfMarker_15;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.BaseDataProviderAccessCoreSystem::LateUpdatePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LateUpdatePerfMarker_16;

public:
	inline static int32_t get_offset_of_UpdatePerfMarker_15() { return static_cast<int32_t>(offsetof(BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F_StaticFields, ___UpdatePerfMarker_15)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdatePerfMarker_15() const { return ___UpdatePerfMarker_15; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdatePerfMarker_15() { return &___UpdatePerfMarker_15; }
	inline void set_UpdatePerfMarker_15(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdatePerfMarker_15 = value;
	}

	inline static int32_t get_offset_of_LateUpdatePerfMarker_16() { return static_cast<int32_t>(offsetof(BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F_StaticFields, ___LateUpdatePerfMarker_16)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LateUpdatePerfMarker_16() const { return ___LateUpdatePerfMarker_16; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LateUpdatePerfMarker_16() { return &___LateUpdatePerfMarker_16; }
	inline void set_LateUpdatePerfMarker_16(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LateUpdatePerfMarker_16 = value;
	}
};


// Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem
struct  MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E  : public BaseCoreSystem_t6678B90A46A1209B425DA44AFA89C58AB1FFAAB2
{
public:
	// System.Boolean Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::managerSceneOpInProgress
	bool ___managerSceneOpInProgress_15;
	// System.Single Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::managerSceneOpProgress
	float ___managerSceneOpProgress_16;
	// Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem/SceneContentTracker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::contentTracker
	SceneContentTracker_t9B55B2C7D399F026506AC3039C94B5827E47A67B * ___contentTracker_17;
	// Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem/SceneLightingExecutor Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::lightingExecutor
	SceneLightingExecutor_tDA0C88C144C1A6CEC68C5BA218EFC6AFB3F85C07 * ___lightingExecutor_18;
	// System.String Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_19;
	// System.Action`1<System.Collections.Generic.IEnumerable`1<System.String>> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillLoadContent>k__BackingField
	Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * ___U3COnWillLoadContentU3Ek__BackingField_20;
	// System.Action`1<System.Collections.Generic.IEnumerable`1<System.String>> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnContentLoaded>k__BackingField
	Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * ___U3COnContentLoadedU3Ek__BackingField_21;
	// System.Action`1<System.Collections.Generic.IEnumerable`1<System.String>> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillUnloadContent>k__BackingField
	Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * ___U3COnWillUnloadContentU3Ek__BackingField_22;
	// System.Action`1<System.Collections.Generic.IEnumerable`1<System.String>> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnContentUnloaded>k__BackingField
	Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * ___U3COnContentUnloadedU3Ek__BackingField_23;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillLoadLighting>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnWillLoadLightingU3Ek__BackingField_24;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnLightingLoaded>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnLightingLoadedU3Ek__BackingField_25;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillUnloadLighting>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnWillUnloadLightingU3Ek__BackingField_26;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnLightingUnloaded>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnLightingUnloadedU3Ek__BackingField_27;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillLoadScene>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnWillLoadSceneU3Ek__BackingField_28;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnSceneLoaded>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnSceneLoadedU3Ek__BackingField_29;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnWillUnloadScene>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnWillUnloadSceneU3Ek__BackingField_30;
	// System.Action`1<System.String> Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<OnSceneUnloaded>k__BackingField
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___U3COnSceneUnloadedU3Ek__BackingField_31;
	// System.Boolean Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<SceneOperationInProgress>k__BackingField
	bool ___U3CSceneOperationInProgressU3Ek__BackingField_32;
	// System.Single Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<SceneOperationProgress>k__BackingField
	float ___U3CSceneOperationProgressU3Ek__BackingField_33;
	// System.Boolean Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<LightingOperationInProgress>k__BackingField
	bool ___U3CLightingOperationInProgressU3Ek__BackingField_34;
	// System.Single Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<LightingOperationProgress>k__BackingField
	float ___U3CLightingOperationProgressU3Ek__BackingField_35;
	// System.String Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<ActiveLightingScene>k__BackingField
	String_t* ___U3CActiveLightingSceneU3Ek__BackingField_36;
	// System.Boolean Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<WaitingToProceed>k__BackingField
	bool ___U3CWaitingToProceedU3Ek__BackingField_37;
	// System.UInt32 Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<SourceId>k__BackingField
	uint32_t ___U3CSourceIdU3Ek__BackingField_38;
	// System.String Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::<SourceName>k__BackingField
	String_t* ___U3CSourceNameU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_managerSceneOpInProgress_15() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___managerSceneOpInProgress_15)); }
	inline bool get_managerSceneOpInProgress_15() const { return ___managerSceneOpInProgress_15; }
	inline bool* get_address_of_managerSceneOpInProgress_15() { return &___managerSceneOpInProgress_15; }
	inline void set_managerSceneOpInProgress_15(bool value)
	{
		___managerSceneOpInProgress_15 = value;
	}

	inline static int32_t get_offset_of_managerSceneOpProgress_16() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___managerSceneOpProgress_16)); }
	inline float get_managerSceneOpProgress_16() const { return ___managerSceneOpProgress_16; }
	inline float* get_address_of_managerSceneOpProgress_16() { return &___managerSceneOpProgress_16; }
	inline void set_managerSceneOpProgress_16(float value)
	{
		___managerSceneOpProgress_16 = value;
	}

	inline static int32_t get_offset_of_contentTracker_17() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___contentTracker_17)); }
	inline SceneContentTracker_t9B55B2C7D399F026506AC3039C94B5827E47A67B * get_contentTracker_17() const { return ___contentTracker_17; }
	inline SceneContentTracker_t9B55B2C7D399F026506AC3039C94B5827E47A67B ** get_address_of_contentTracker_17() { return &___contentTracker_17; }
	inline void set_contentTracker_17(SceneContentTracker_t9B55B2C7D399F026506AC3039C94B5827E47A67B * value)
	{
		___contentTracker_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentTracker_17), (void*)value);
	}

	inline static int32_t get_offset_of_lightingExecutor_18() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___lightingExecutor_18)); }
	inline SceneLightingExecutor_tDA0C88C144C1A6CEC68C5BA218EFC6AFB3F85C07 * get_lightingExecutor_18() const { return ___lightingExecutor_18; }
	inline SceneLightingExecutor_tDA0C88C144C1A6CEC68C5BA218EFC6AFB3F85C07 ** get_address_of_lightingExecutor_18() { return &___lightingExecutor_18; }
	inline void set_lightingExecutor_18(SceneLightingExecutor_tDA0C88C144C1A6CEC68C5BA218EFC6AFB3F85C07 * value)
	{
		___lightingExecutor_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lightingExecutor_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CNameU3Ek__BackingField_19)); }
	inline String_t* get_U3CNameU3Ek__BackingField_19() const { return ___U3CNameU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_19() { return &___U3CNameU3Ek__BackingField_19; }
	inline void set_U3CNameU3Ek__BackingField_19(String_t* value)
	{
		___U3CNameU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillLoadContentU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillLoadContentU3Ek__BackingField_20)); }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * get_U3COnWillLoadContentU3Ek__BackingField_20() const { return ___U3COnWillLoadContentU3Ek__BackingField_20; }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 ** get_address_of_U3COnWillLoadContentU3Ek__BackingField_20() { return &___U3COnWillLoadContentU3Ek__BackingField_20; }
	inline void set_U3COnWillLoadContentU3Ek__BackingField_20(Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * value)
	{
		___U3COnWillLoadContentU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillLoadContentU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnContentLoadedU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnContentLoadedU3Ek__BackingField_21)); }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * get_U3COnContentLoadedU3Ek__BackingField_21() const { return ___U3COnContentLoadedU3Ek__BackingField_21; }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 ** get_address_of_U3COnContentLoadedU3Ek__BackingField_21() { return &___U3COnContentLoadedU3Ek__BackingField_21; }
	inline void set_U3COnContentLoadedU3Ek__BackingField_21(Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * value)
	{
		___U3COnContentLoadedU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnContentLoadedU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillUnloadContentU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillUnloadContentU3Ek__BackingField_22)); }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * get_U3COnWillUnloadContentU3Ek__BackingField_22() const { return ___U3COnWillUnloadContentU3Ek__BackingField_22; }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 ** get_address_of_U3COnWillUnloadContentU3Ek__BackingField_22() { return &___U3COnWillUnloadContentU3Ek__BackingField_22; }
	inline void set_U3COnWillUnloadContentU3Ek__BackingField_22(Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * value)
	{
		___U3COnWillUnloadContentU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillUnloadContentU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnContentUnloadedU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnContentUnloadedU3Ek__BackingField_23)); }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * get_U3COnContentUnloadedU3Ek__BackingField_23() const { return ___U3COnContentUnloadedU3Ek__BackingField_23; }
	inline Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 ** get_address_of_U3COnContentUnloadedU3Ek__BackingField_23() { return &___U3COnContentUnloadedU3Ek__BackingField_23; }
	inline void set_U3COnContentUnloadedU3Ek__BackingField_23(Action_1_t8B3FD6155D04E2C2D284B5C21B863CDC18E89439 * value)
	{
		___U3COnContentUnloadedU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnContentUnloadedU3Ek__BackingField_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillLoadLightingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillLoadLightingU3Ek__BackingField_24)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnWillLoadLightingU3Ek__BackingField_24() const { return ___U3COnWillLoadLightingU3Ek__BackingField_24; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnWillLoadLightingU3Ek__BackingField_24() { return &___U3COnWillLoadLightingU3Ek__BackingField_24; }
	inline void set_U3COnWillLoadLightingU3Ek__BackingField_24(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnWillLoadLightingU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillLoadLightingU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnLightingLoadedU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnLightingLoadedU3Ek__BackingField_25)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnLightingLoadedU3Ek__BackingField_25() const { return ___U3COnLightingLoadedU3Ek__BackingField_25; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnLightingLoadedU3Ek__BackingField_25() { return &___U3COnLightingLoadedU3Ek__BackingField_25; }
	inline void set_U3COnLightingLoadedU3Ek__BackingField_25(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnLightingLoadedU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnLightingLoadedU3Ek__BackingField_25), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillUnloadLightingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillUnloadLightingU3Ek__BackingField_26)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnWillUnloadLightingU3Ek__BackingField_26() const { return ___U3COnWillUnloadLightingU3Ek__BackingField_26; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnWillUnloadLightingU3Ek__BackingField_26() { return &___U3COnWillUnloadLightingU3Ek__BackingField_26; }
	inline void set_U3COnWillUnloadLightingU3Ek__BackingField_26(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnWillUnloadLightingU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillUnloadLightingU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnLightingUnloadedU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnLightingUnloadedU3Ek__BackingField_27)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnLightingUnloadedU3Ek__BackingField_27() const { return ___U3COnLightingUnloadedU3Ek__BackingField_27; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnLightingUnloadedU3Ek__BackingField_27() { return &___U3COnLightingUnloadedU3Ek__BackingField_27; }
	inline void set_U3COnLightingUnloadedU3Ek__BackingField_27(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnLightingUnloadedU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnLightingUnloadedU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillLoadSceneU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillLoadSceneU3Ek__BackingField_28)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnWillLoadSceneU3Ek__BackingField_28() const { return ___U3COnWillLoadSceneU3Ek__BackingField_28; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnWillLoadSceneU3Ek__BackingField_28() { return &___U3COnWillLoadSceneU3Ek__BackingField_28; }
	inline void set_U3COnWillLoadSceneU3Ek__BackingField_28(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnWillLoadSceneU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillLoadSceneU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnSceneLoadedU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnSceneLoadedU3Ek__BackingField_29)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnSceneLoadedU3Ek__BackingField_29() const { return ___U3COnSceneLoadedU3Ek__BackingField_29; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnSceneLoadedU3Ek__BackingField_29() { return &___U3COnSceneLoadedU3Ek__BackingField_29; }
	inline void set_U3COnSceneLoadedU3Ek__BackingField_29(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnSceneLoadedU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnSceneLoadedU3Ek__BackingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnWillUnloadSceneU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnWillUnloadSceneU3Ek__BackingField_30)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnWillUnloadSceneU3Ek__BackingField_30() const { return ___U3COnWillUnloadSceneU3Ek__BackingField_30; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnWillUnloadSceneU3Ek__BackingField_30() { return &___U3COnWillUnloadSceneU3Ek__BackingField_30; }
	inline void set_U3COnWillUnloadSceneU3Ek__BackingField_30(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnWillUnloadSceneU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnWillUnloadSceneU3Ek__BackingField_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3COnSceneUnloadedU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3COnSceneUnloadedU3Ek__BackingField_31)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_U3COnSceneUnloadedU3Ek__BackingField_31() const { return ___U3COnSceneUnloadedU3Ek__BackingField_31; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_U3COnSceneUnloadedU3Ek__BackingField_31() { return &___U3COnSceneUnloadedU3Ek__BackingField_31; }
	inline void set_U3COnSceneUnloadedU3Ek__BackingField_31(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___U3COnSceneUnloadedU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COnSceneUnloadedU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSceneOperationInProgressU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CSceneOperationInProgressU3Ek__BackingField_32)); }
	inline bool get_U3CSceneOperationInProgressU3Ek__BackingField_32() const { return ___U3CSceneOperationInProgressU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CSceneOperationInProgressU3Ek__BackingField_32() { return &___U3CSceneOperationInProgressU3Ek__BackingField_32; }
	inline void set_U3CSceneOperationInProgressU3Ek__BackingField_32(bool value)
	{
		___U3CSceneOperationInProgressU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CSceneOperationProgressU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CSceneOperationProgressU3Ek__BackingField_33)); }
	inline float get_U3CSceneOperationProgressU3Ek__BackingField_33() const { return ___U3CSceneOperationProgressU3Ek__BackingField_33; }
	inline float* get_address_of_U3CSceneOperationProgressU3Ek__BackingField_33() { return &___U3CSceneOperationProgressU3Ek__BackingField_33; }
	inline void set_U3CSceneOperationProgressU3Ek__BackingField_33(float value)
	{
		___U3CSceneOperationProgressU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CLightingOperationInProgressU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CLightingOperationInProgressU3Ek__BackingField_34)); }
	inline bool get_U3CLightingOperationInProgressU3Ek__BackingField_34() const { return ___U3CLightingOperationInProgressU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CLightingOperationInProgressU3Ek__BackingField_34() { return &___U3CLightingOperationInProgressU3Ek__BackingField_34; }
	inline void set_U3CLightingOperationInProgressU3Ek__BackingField_34(bool value)
	{
		___U3CLightingOperationInProgressU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CLightingOperationProgressU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CLightingOperationProgressU3Ek__BackingField_35)); }
	inline float get_U3CLightingOperationProgressU3Ek__BackingField_35() const { return ___U3CLightingOperationProgressU3Ek__BackingField_35; }
	inline float* get_address_of_U3CLightingOperationProgressU3Ek__BackingField_35() { return &___U3CLightingOperationProgressU3Ek__BackingField_35; }
	inline void set_U3CLightingOperationProgressU3Ek__BackingField_35(float value)
	{
		___U3CLightingOperationProgressU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CActiveLightingSceneU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CActiveLightingSceneU3Ek__BackingField_36)); }
	inline String_t* get_U3CActiveLightingSceneU3Ek__BackingField_36() const { return ___U3CActiveLightingSceneU3Ek__BackingField_36; }
	inline String_t** get_address_of_U3CActiveLightingSceneU3Ek__BackingField_36() { return &___U3CActiveLightingSceneU3Ek__BackingField_36; }
	inline void set_U3CActiveLightingSceneU3Ek__BackingField_36(String_t* value)
	{
		___U3CActiveLightingSceneU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CActiveLightingSceneU3Ek__BackingField_36), (void*)value);
	}

	inline static int32_t get_offset_of_U3CWaitingToProceedU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CWaitingToProceedU3Ek__BackingField_37)); }
	inline bool get_U3CWaitingToProceedU3Ek__BackingField_37() const { return ___U3CWaitingToProceedU3Ek__BackingField_37; }
	inline bool* get_address_of_U3CWaitingToProceedU3Ek__BackingField_37() { return &___U3CWaitingToProceedU3Ek__BackingField_37; }
	inline void set_U3CWaitingToProceedU3Ek__BackingField_37(bool value)
	{
		___U3CWaitingToProceedU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CSourceIdU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CSourceIdU3Ek__BackingField_38)); }
	inline uint32_t get_U3CSourceIdU3Ek__BackingField_38() const { return ___U3CSourceIdU3Ek__BackingField_38; }
	inline uint32_t* get_address_of_U3CSourceIdU3Ek__BackingField_38() { return &___U3CSourceIdU3Ek__BackingField_38; }
	inline void set_U3CSourceIdU3Ek__BackingField_38(uint32_t value)
	{
		___U3CSourceIdU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CSourceNameU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E, ___U3CSourceNameU3Ek__BackingField_39)); }
	inline String_t* get_U3CSourceNameU3Ek__BackingField_39() const { return ___U3CSourceNameU3Ek__BackingField_39; }
	inline String_t** get_address_of_U3CSourceNameU3Ek__BackingField_39() { return &___U3CSourceNameU3Ek__BackingField_39; }
	inline void set_U3CSourceNameU3Ek__BackingField_39(String_t* value)
	{
		___U3CSourceNameU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSourceNameU3Ek__BackingField_39), (void*)value);
	}
};

struct MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::UpdatePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdatePerfMarker_40;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::LoadNextContentPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LoadNextContentPerfMarker_41;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::LoadPrevContentPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LoadPrevContentPerfMarker_42;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::LoadContentByTagPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LoadContentByTagPerfMarker_43;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::UnloadContentByTagPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UnloadContentByTagPerfMarker_44;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::LoadContentPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LoadContentPerfMarker_45;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::UnloadContentPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UnloadContentPerfMarker_46;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::IsContentLoadedPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___IsContentLoadedPerfMarker_47;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::SetLightingScenePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___SetLightingScenePerfMarker_48;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::SetManagerScenePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___SetManagerScenePerfMarker_49;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::LoadScenesInternalPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___LoadScenesInternalPerfMarker_50;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::UnloadScenesInternalPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UnloadScenesInternalPerfMarker_51;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::InvokeLoadedActionsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___InvokeLoadedActionsPerfMarker_52;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::InvokeWillLoadActionsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___InvokeWillLoadActionsPerfMarker_53;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::InvokeWillUnloadActionsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___InvokeWillUnloadActionsPerfMarker_54;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::InvokeUnloadedActionsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___InvokeUnloadedActionsPerfMarker_55;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::GetScenePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetScenePerfMarker_56;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SceneSystem.MixedRealitySceneSystem::GetLoadedContentScenesPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetLoadedContentScenesPerfMarker_57;

public:
	inline static int32_t get_offset_of_UpdatePerfMarker_40() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___UpdatePerfMarker_40)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdatePerfMarker_40() const { return ___UpdatePerfMarker_40; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdatePerfMarker_40() { return &___UpdatePerfMarker_40; }
	inline void set_UpdatePerfMarker_40(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdatePerfMarker_40 = value;
	}

	inline static int32_t get_offset_of_LoadNextContentPerfMarker_41() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___LoadNextContentPerfMarker_41)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LoadNextContentPerfMarker_41() const { return ___LoadNextContentPerfMarker_41; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LoadNextContentPerfMarker_41() { return &___LoadNextContentPerfMarker_41; }
	inline void set_LoadNextContentPerfMarker_41(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LoadNextContentPerfMarker_41 = value;
	}

	inline static int32_t get_offset_of_LoadPrevContentPerfMarker_42() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___LoadPrevContentPerfMarker_42)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LoadPrevContentPerfMarker_42() const { return ___LoadPrevContentPerfMarker_42; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LoadPrevContentPerfMarker_42() { return &___LoadPrevContentPerfMarker_42; }
	inline void set_LoadPrevContentPerfMarker_42(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LoadPrevContentPerfMarker_42 = value;
	}

	inline static int32_t get_offset_of_LoadContentByTagPerfMarker_43() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___LoadContentByTagPerfMarker_43)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LoadContentByTagPerfMarker_43() const { return ___LoadContentByTagPerfMarker_43; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LoadContentByTagPerfMarker_43() { return &___LoadContentByTagPerfMarker_43; }
	inline void set_LoadContentByTagPerfMarker_43(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LoadContentByTagPerfMarker_43 = value;
	}

	inline static int32_t get_offset_of_UnloadContentByTagPerfMarker_44() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___UnloadContentByTagPerfMarker_44)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UnloadContentByTagPerfMarker_44() const { return ___UnloadContentByTagPerfMarker_44; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UnloadContentByTagPerfMarker_44() { return &___UnloadContentByTagPerfMarker_44; }
	inline void set_UnloadContentByTagPerfMarker_44(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UnloadContentByTagPerfMarker_44 = value;
	}

	inline static int32_t get_offset_of_LoadContentPerfMarker_45() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___LoadContentPerfMarker_45)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LoadContentPerfMarker_45() const { return ___LoadContentPerfMarker_45; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LoadContentPerfMarker_45() { return &___LoadContentPerfMarker_45; }
	inline void set_LoadContentPerfMarker_45(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LoadContentPerfMarker_45 = value;
	}

	inline static int32_t get_offset_of_UnloadContentPerfMarker_46() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___UnloadContentPerfMarker_46)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UnloadContentPerfMarker_46() const { return ___UnloadContentPerfMarker_46; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UnloadContentPerfMarker_46() { return &___UnloadContentPerfMarker_46; }
	inline void set_UnloadContentPerfMarker_46(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UnloadContentPerfMarker_46 = value;
	}

	inline static int32_t get_offset_of_IsContentLoadedPerfMarker_47() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___IsContentLoadedPerfMarker_47)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_IsContentLoadedPerfMarker_47() const { return ___IsContentLoadedPerfMarker_47; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_IsContentLoadedPerfMarker_47() { return &___IsContentLoadedPerfMarker_47; }
	inline void set_IsContentLoadedPerfMarker_47(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___IsContentLoadedPerfMarker_47 = value;
	}

	inline static int32_t get_offset_of_SetLightingScenePerfMarker_48() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___SetLightingScenePerfMarker_48)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_SetLightingScenePerfMarker_48() const { return ___SetLightingScenePerfMarker_48; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_SetLightingScenePerfMarker_48() { return &___SetLightingScenePerfMarker_48; }
	inline void set_SetLightingScenePerfMarker_48(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___SetLightingScenePerfMarker_48 = value;
	}

	inline static int32_t get_offset_of_SetManagerScenePerfMarker_49() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___SetManagerScenePerfMarker_49)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_SetManagerScenePerfMarker_49() const { return ___SetManagerScenePerfMarker_49; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_SetManagerScenePerfMarker_49() { return &___SetManagerScenePerfMarker_49; }
	inline void set_SetManagerScenePerfMarker_49(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___SetManagerScenePerfMarker_49 = value;
	}

	inline static int32_t get_offset_of_LoadScenesInternalPerfMarker_50() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___LoadScenesInternalPerfMarker_50)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_LoadScenesInternalPerfMarker_50() const { return ___LoadScenesInternalPerfMarker_50; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_LoadScenesInternalPerfMarker_50() { return &___LoadScenesInternalPerfMarker_50; }
	inline void set_LoadScenesInternalPerfMarker_50(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___LoadScenesInternalPerfMarker_50 = value;
	}

	inline static int32_t get_offset_of_UnloadScenesInternalPerfMarker_51() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___UnloadScenesInternalPerfMarker_51)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UnloadScenesInternalPerfMarker_51() const { return ___UnloadScenesInternalPerfMarker_51; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UnloadScenesInternalPerfMarker_51() { return &___UnloadScenesInternalPerfMarker_51; }
	inline void set_UnloadScenesInternalPerfMarker_51(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UnloadScenesInternalPerfMarker_51 = value;
	}

	inline static int32_t get_offset_of_InvokeLoadedActionsPerfMarker_52() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___InvokeLoadedActionsPerfMarker_52)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_InvokeLoadedActionsPerfMarker_52() const { return ___InvokeLoadedActionsPerfMarker_52; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_InvokeLoadedActionsPerfMarker_52() { return &___InvokeLoadedActionsPerfMarker_52; }
	inline void set_InvokeLoadedActionsPerfMarker_52(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___InvokeLoadedActionsPerfMarker_52 = value;
	}

	inline static int32_t get_offset_of_InvokeWillLoadActionsPerfMarker_53() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___InvokeWillLoadActionsPerfMarker_53)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_InvokeWillLoadActionsPerfMarker_53() const { return ___InvokeWillLoadActionsPerfMarker_53; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_InvokeWillLoadActionsPerfMarker_53() { return &___InvokeWillLoadActionsPerfMarker_53; }
	inline void set_InvokeWillLoadActionsPerfMarker_53(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___InvokeWillLoadActionsPerfMarker_53 = value;
	}

	inline static int32_t get_offset_of_InvokeWillUnloadActionsPerfMarker_54() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___InvokeWillUnloadActionsPerfMarker_54)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_InvokeWillUnloadActionsPerfMarker_54() const { return ___InvokeWillUnloadActionsPerfMarker_54; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_InvokeWillUnloadActionsPerfMarker_54() { return &___InvokeWillUnloadActionsPerfMarker_54; }
	inline void set_InvokeWillUnloadActionsPerfMarker_54(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___InvokeWillUnloadActionsPerfMarker_54 = value;
	}

	inline static int32_t get_offset_of_InvokeUnloadedActionsPerfMarker_55() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___InvokeUnloadedActionsPerfMarker_55)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_InvokeUnloadedActionsPerfMarker_55() const { return ___InvokeUnloadedActionsPerfMarker_55; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_InvokeUnloadedActionsPerfMarker_55() { return &___InvokeUnloadedActionsPerfMarker_55; }
	inline void set_InvokeUnloadedActionsPerfMarker_55(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___InvokeUnloadedActionsPerfMarker_55 = value;
	}

	inline static int32_t get_offset_of_GetScenePerfMarker_56() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___GetScenePerfMarker_56)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetScenePerfMarker_56() const { return ___GetScenePerfMarker_56; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetScenePerfMarker_56() { return &___GetScenePerfMarker_56; }
	inline void set_GetScenePerfMarker_56(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetScenePerfMarker_56 = value;
	}

	inline static int32_t get_offset_of_GetLoadedContentScenesPerfMarker_57() { return static_cast<int32_t>(offsetof(MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields, ___GetLoadedContentScenesPerfMarker_57)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetLoadedContentScenesPerfMarker_57() const { return ___GetLoadedContentScenesPerfMarker_57; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetLoadedContentScenesPerfMarker_57() { return &___GetLoadedContentScenesPerfMarker_57; }
	inline void set_GetLoadedContentScenesPerfMarker_57(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetLoadedContentScenesPerfMarker_57 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem
struct  MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058  : public BaseCoreSystem_t6678B90A46A1209B425DA44AFA89C58AB1FFAAB2
{
public:
	// Microsoft.MixedReality.Toolkit.Teleport.TeleportEventData Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::teleportEventData
	TeleportEventData_tBBBA556B30350D9370F72039B09B42102FA0681A * ___teleportEventData_14;
	// System.Boolean Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::isTeleporting
	bool ___isTeleporting_15;
	// System.Boolean Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::isProcessingTeleportRequest
	bool ___isProcessingTeleportRequest_16;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::targetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPosition_17;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::targetRotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetRotation_18;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::eventSystemReference
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___eventSystemReference_19;
	// System.String Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_20;
	// System.Single Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::teleportDuration
	float ___teleportDuration_22;

public:
	inline static int32_t get_offset_of_teleportEventData_14() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___teleportEventData_14)); }
	inline TeleportEventData_tBBBA556B30350D9370F72039B09B42102FA0681A * get_teleportEventData_14() const { return ___teleportEventData_14; }
	inline TeleportEventData_tBBBA556B30350D9370F72039B09B42102FA0681A ** get_address_of_teleportEventData_14() { return &___teleportEventData_14; }
	inline void set_teleportEventData_14(TeleportEventData_tBBBA556B30350D9370F72039B09B42102FA0681A * value)
	{
		___teleportEventData_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___teleportEventData_14), (void*)value);
	}

	inline static int32_t get_offset_of_isTeleporting_15() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___isTeleporting_15)); }
	inline bool get_isTeleporting_15() const { return ___isTeleporting_15; }
	inline bool* get_address_of_isTeleporting_15() { return &___isTeleporting_15; }
	inline void set_isTeleporting_15(bool value)
	{
		___isTeleporting_15 = value;
	}

	inline static int32_t get_offset_of_isProcessingTeleportRequest_16() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___isProcessingTeleportRequest_16)); }
	inline bool get_isProcessingTeleportRequest_16() const { return ___isProcessingTeleportRequest_16; }
	inline bool* get_address_of_isProcessingTeleportRequest_16() { return &___isProcessingTeleportRequest_16; }
	inline void set_isProcessingTeleportRequest_16(bool value)
	{
		___isProcessingTeleportRequest_16 = value;
	}

	inline static int32_t get_offset_of_targetPosition_17() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___targetPosition_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPosition_17() const { return ___targetPosition_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPosition_17() { return &___targetPosition_17; }
	inline void set_targetPosition_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPosition_17 = value;
	}

	inline static int32_t get_offset_of_targetRotation_18() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___targetRotation_18)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetRotation_18() const { return ___targetRotation_18; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetRotation_18() { return &___targetRotation_18; }
	inline void set_targetRotation_18(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetRotation_18 = value;
	}

	inline static int32_t get_offset_of_eventSystemReference_19() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___eventSystemReference_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_eventSystemReference_19() const { return ___eventSystemReference_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_eventSystemReference_19() { return &___eventSystemReference_19; }
	inline void set_eventSystemReference_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___eventSystemReference_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventSystemReference_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___U3CNameU3Ek__BackingField_20)); }
	inline String_t* get_U3CNameU3Ek__BackingField_20() const { return ___U3CNameU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_20() { return &___U3CNameU3Ek__BackingField_20; }
	inline void set_U3CNameU3Ek__BackingField_20(String_t* value)
	{
		___U3CNameU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_teleportDuration_22() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058, ___teleportDuration_22)); }
	inline float get_teleportDuration_22() const { return ___teleportDuration_22; }
	inline float* get_address_of_teleportDuration_22() { return &___teleportDuration_22; }
	inline void set_teleportDuration_22(float value)
	{
		___teleportDuration_22 = value;
	}
};

struct MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::HandleEventPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___HandleEventPerfMarker_21;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler> Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::OnTeleportRequestHandler
	EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * ___OnTeleportRequestHandler_23;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportRequestPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RaiseTeleportRequestPerfMarker_24;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler> Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::OnTeleportStartedHandler
	EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * ___OnTeleportStartedHandler_25;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportStartedPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RaiseTeleportStartedPerfMarker_26;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler> Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::OnTeleportCompletedHandler
	EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * ___OnTeleportCompletedHandler_27;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportCompletePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RaiseTeleportCompletePerfMarker_28;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler> Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::OnTeleportCanceledHandler
	EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * ___OnTeleportCanceledHandler_29;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportCanceledPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RaiseTeleportCanceledPerfMarker_30;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::ProcessTeleportationRequestPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ProcessTeleportationRequestPerfMarker_31;

public:
	inline static int32_t get_offset_of_HandleEventPerfMarker_21() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___HandleEventPerfMarker_21)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_HandleEventPerfMarker_21() const { return ___HandleEventPerfMarker_21; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_HandleEventPerfMarker_21() { return &___HandleEventPerfMarker_21; }
	inline void set_HandleEventPerfMarker_21(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___HandleEventPerfMarker_21 = value;
	}

	inline static int32_t get_offset_of_OnTeleportRequestHandler_23() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___OnTeleportRequestHandler_23)); }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * get_OnTeleportRequestHandler_23() const { return ___OnTeleportRequestHandler_23; }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F ** get_address_of_OnTeleportRequestHandler_23() { return &___OnTeleportRequestHandler_23; }
	inline void set_OnTeleportRequestHandler_23(EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * value)
	{
		___OnTeleportRequestHandler_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTeleportRequestHandler_23), (void*)value);
	}

	inline static int32_t get_offset_of_RaiseTeleportRequestPerfMarker_24() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___RaiseTeleportRequestPerfMarker_24)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RaiseTeleportRequestPerfMarker_24() const { return ___RaiseTeleportRequestPerfMarker_24; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RaiseTeleportRequestPerfMarker_24() { return &___RaiseTeleportRequestPerfMarker_24; }
	inline void set_RaiseTeleportRequestPerfMarker_24(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RaiseTeleportRequestPerfMarker_24 = value;
	}

	inline static int32_t get_offset_of_OnTeleportStartedHandler_25() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___OnTeleportStartedHandler_25)); }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * get_OnTeleportStartedHandler_25() const { return ___OnTeleportStartedHandler_25; }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F ** get_address_of_OnTeleportStartedHandler_25() { return &___OnTeleportStartedHandler_25; }
	inline void set_OnTeleportStartedHandler_25(EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * value)
	{
		___OnTeleportStartedHandler_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTeleportStartedHandler_25), (void*)value);
	}

	inline static int32_t get_offset_of_RaiseTeleportStartedPerfMarker_26() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___RaiseTeleportStartedPerfMarker_26)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RaiseTeleportStartedPerfMarker_26() const { return ___RaiseTeleportStartedPerfMarker_26; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RaiseTeleportStartedPerfMarker_26() { return &___RaiseTeleportStartedPerfMarker_26; }
	inline void set_RaiseTeleportStartedPerfMarker_26(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RaiseTeleportStartedPerfMarker_26 = value;
	}

	inline static int32_t get_offset_of_OnTeleportCompletedHandler_27() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___OnTeleportCompletedHandler_27)); }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * get_OnTeleportCompletedHandler_27() const { return ___OnTeleportCompletedHandler_27; }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F ** get_address_of_OnTeleportCompletedHandler_27() { return &___OnTeleportCompletedHandler_27; }
	inline void set_OnTeleportCompletedHandler_27(EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * value)
	{
		___OnTeleportCompletedHandler_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTeleportCompletedHandler_27), (void*)value);
	}

	inline static int32_t get_offset_of_RaiseTeleportCompletePerfMarker_28() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___RaiseTeleportCompletePerfMarker_28)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RaiseTeleportCompletePerfMarker_28() const { return ___RaiseTeleportCompletePerfMarker_28; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RaiseTeleportCompletePerfMarker_28() { return &___RaiseTeleportCompletePerfMarker_28; }
	inline void set_RaiseTeleportCompletePerfMarker_28(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RaiseTeleportCompletePerfMarker_28 = value;
	}

	inline static int32_t get_offset_of_OnTeleportCanceledHandler_29() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___OnTeleportCanceledHandler_29)); }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * get_OnTeleportCanceledHandler_29() const { return ___OnTeleportCanceledHandler_29; }
	inline EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F ** get_address_of_OnTeleportCanceledHandler_29() { return &___OnTeleportCanceledHandler_29; }
	inline void set_OnTeleportCanceledHandler_29(EventFunction_1_tDEFC73DC51F68780C97D331E9A51051D77EE392F * value)
	{
		___OnTeleportCanceledHandler_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTeleportCanceledHandler_29), (void*)value);
	}

	inline static int32_t get_offset_of_RaiseTeleportCanceledPerfMarker_30() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___RaiseTeleportCanceledPerfMarker_30)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RaiseTeleportCanceledPerfMarker_30() const { return ___RaiseTeleportCanceledPerfMarker_30; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RaiseTeleportCanceledPerfMarker_30() { return &___RaiseTeleportCanceledPerfMarker_30; }
	inline void set_RaiseTeleportCanceledPerfMarker_30(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RaiseTeleportCanceledPerfMarker_30 = value;
	}

	inline static int32_t get_offset_of_ProcessTeleportationRequestPerfMarker_31() { return static_cast<int32_t>(offsetof(MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields, ___ProcessTeleportationRequestPerfMarker_31)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ProcessTeleportationRequestPerfMarker_31() const { return ___ProcessTeleportationRequestPerfMarker_31; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ProcessTeleportationRequestPerfMarker_31() { return &___ProcessTeleportationRequestPerfMarker_31; }
	inline void set_ProcessTeleportationRequestPerfMarker_31(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ProcessTeleportationRequestPerfMarker_31 = value;
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// HoloToolkit.Unity.Billboard
struct  Billboard_tBF7123B0F815E7B9BAF817A1D20781DB77AAA4EB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// HoloToolkit.Unity.PivotAxis HoloToolkit.Unity.Billboard::PivotAxis
	int32_t ___PivotAxis_4;
	// UnityEngine.Transform HoloToolkit.Unity.Billboard::TargetTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___TargetTransform_5;

public:
	inline static int32_t get_offset_of_PivotAxis_4() { return static_cast<int32_t>(offsetof(Billboard_tBF7123B0F815E7B9BAF817A1D20781DB77AAA4EB, ___PivotAxis_4)); }
	inline int32_t get_PivotAxis_4() const { return ___PivotAxis_4; }
	inline int32_t* get_address_of_PivotAxis_4() { return &___PivotAxis_4; }
	inline void set_PivotAxis_4(int32_t value)
	{
		___PivotAxis_4 = value;
	}

	inline static int32_t get_offset_of_TargetTransform_5() { return static_cast<int32_t>(offsetof(Billboard_tBF7123B0F815E7B9BAF817A1D20781DB77AAA4EB, ___TargetTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_TargetTransform_5() const { return ___TargetTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_TargetTransform_5() { return &___TargetTransform_5; }
	inline void set_TargetTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___TargetTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetTransform_5), (void*)value);
	}
};


// DimBoxes.BoundBox
struct  BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DimBoxes.BoundBox/BoundSource DimBoxes.BoundBox::boundSource
	int32_t ___boundSource_4;
	// System.Boolean DimBoxes.BoundBox::interactive
	bool ___interactive_5;
	// UnityEngine.Material DimBoxes.BoundBox::lineMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___lineMaterial_6;
	// System.Boolean DimBoxes.BoundBox::wire_renderer
	bool ___wire_renderer_7;
	// UnityEngine.Color DimBoxes.BoundBox::wireColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___wireColor_8;
	// System.Boolean DimBoxes.BoundBox::line_renderer
	bool ___line_renderer_9;
	// UnityEngine.Object DimBoxes.BoundBox::linePrefab
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___linePrefab_10;
	// System.Single DimBoxes.BoundBox::lineWidth
	float ___lineWidth_11;
	// UnityEngine.Color DimBoxes.BoundBox::lineColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___lineColor_12;
	// System.Int32 DimBoxes.BoundBox::numCapVertices
	int32_t ___numCapVertices_13;
	// UnityEngine.Bounds DimBoxes.BoundBox::bound
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___bound_14;
	// UnityEngine.Vector3 DimBoxes.BoundBox::boundOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___boundOffset_15;
	// UnityEngine.Bounds DimBoxes.BoundBox::colliderBound
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___colliderBound_16;
	// UnityEngine.Vector3 DimBoxes.BoundBox::colliderBoundOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___colliderBoundOffset_17;
	// UnityEngine.Bounds DimBoxes.BoundBox::meshBound
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___meshBound_18;
	// UnityEngine.Vector3 DimBoxes.BoundBox::meshBoundOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___meshBoundOffset_19;
	// UnityEngine.Vector3[] DimBoxes.BoundBox::corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___corners_20;
	// UnityEngine.Vector3[0...,0...] DimBoxes.BoundBox::lines
	Vector3U5BU2CU5D_tF16B36EC585DBDA6C179E131AE5794A33695CA71* ___lines_21;
	// UnityEngine.Quaternion DimBoxes.BoundBox::quat
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___quat_22;
	// UnityEngine.LineRenderer[] DimBoxes.BoundBox::lineList
	LineRendererU5BU5D_t65EF16DA3FB3E6D083B247824B04BE10D0B46743* ___lineList_23;
	// UnityEngine.Vector3 DimBoxes.BoundBox::topFrontLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___topFrontLeft_24;
	// UnityEngine.Vector3 DimBoxes.BoundBox::topFrontRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___topFrontRight_25;
	// UnityEngine.Vector3 DimBoxes.BoundBox::topBackLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___topBackLeft_26;
	// UnityEngine.Vector3 DimBoxes.BoundBox::topBackRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___topBackRight_27;
	// UnityEngine.Vector3 DimBoxes.BoundBox::bottomFrontLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bottomFrontLeft_28;
	// UnityEngine.Vector3 DimBoxes.BoundBox::bottomFrontRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bottomFrontRight_29;
	// UnityEngine.Vector3 DimBoxes.BoundBox::bottomBackLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bottomBackLeft_30;
	// UnityEngine.Vector3 DimBoxes.BoundBox::bottomBackRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bottomBackRight_31;
	// UnityEngine.Vector3 DimBoxes.BoundBox::startingScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startingScale_32;
	// UnityEngine.Vector3 DimBoxes.BoundBox::previousScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___previousScale_33;
	// UnityEngine.Vector3 DimBoxes.BoundBox::startingBoundSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startingBoundSize_34;
	// UnityEngine.Vector3 DimBoxes.BoundBox::startingBoundCenterLocal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startingBoundCenterLocal_35;
	// UnityEngine.Vector3 DimBoxes.BoundBox::previousPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___previousPosition_36;
	// UnityEngine.Quaternion DimBoxes.BoundBox::previousRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___previousRotation_37;

public:
	inline static int32_t get_offset_of_boundSource_4() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___boundSource_4)); }
	inline int32_t get_boundSource_4() const { return ___boundSource_4; }
	inline int32_t* get_address_of_boundSource_4() { return &___boundSource_4; }
	inline void set_boundSource_4(int32_t value)
	{
		___boundSource_4 = value;
	}

	inline static int32_t get_offset_of_interactive_5() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___interactive_5)); }
	inline bool get_interactive_5() const { return ___interactive_5; }
	inline bool* get_address_of_interactive_5() { return &___interactive_5; }
	inline void set_interactive_5(bool value)
	{
		___interactive_5 = value;
	}

	inline static int32_t get_offset_of_lineMaterial_6() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___lineMaterial_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_lineMaterial_6() const { return ___lineMaterial_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_lineMaterial_6() { return &___lineMaterial_6; }
	inline void set_lineMaterial_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___lineMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineMaterial_6), (void*)value);
	}

	inline static int32_t get_offset_of_wire_renderer_7() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___wire_renderer_7)); }
	inline bool get_wire_renderer_7() const { return ___wire_renderer_7; }
	inline bool* get_address_of_wire_renderer_7() { return &___wire_renderer_7; }
	inline void set_wire_renderer_7(bool value)
	{
		___wire_renderer_7 = value;
	}

	inline static int32_t get_offset_of_wireColor_8() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___wireColor_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_wireColor_8() const { return ___wireColor_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_wireColor_8() { return &___wireColor_8; }
	inline void set_wireColor_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___wireColor_8 = value;
	}

	inline static int32_t get_offset_of_line_renderer_9() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___line_renderer_9)); }
	inline bool get_line_renderer_9() const { return ___line_renderer_9; }
	inline bool* get_address_of_line_renderer_9() { return &___line_renderer_9; }
	inline void set_line_renderer_9(bool value)
	{
		___line_renderer_9 = value;
	}

	inline static int32_t get_offset_of_linePrefab_10() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___linePrefab_10)); }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * get_linePrefab_10() const { return ___linePrefab_10; }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** get_address_of_linePrefab_10() { return &___linePrefab_10; }
	inline void set_linePrefab_10(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		___linePrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___linePrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_lineWidth_11() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___lineWidth_11)); }
	inline float get_lineWidth_11() const { return ___lineWidth_11; }
	inline float* get_address_of_lineWidth_11() { return &___lineWidth_11; }
	inline void set_lineWidth_11(float value)
	{
		___lineWidth_11 = value;
	}

	inline static int32_t get_offset_of_lineColor_12() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___lineColor_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_lineColor_12() const { return ___lineColor_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_lineColor_12() { return &___lineColor_12; }
	inline void set_lineColor_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___lineColor_12 = value;
	}

	inline static int32_t get_offset_of_numCapVertices_13() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___numCapVertices_13)); }
	inline int32_t get_numCapVertices_13() const { return ___numCapVertices_13; }
	inline int32_t* get_address_of_numCapVertices_13() { return &___numCapVertices_13; }
	inline void set_numCapVertices_13(int32_t value)
	{
		___numCapVertices_13 = value;
	}

	inline static int32_t get_offset_of_bound_14() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___bound_14)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_bound_14() const { return ___bound_14; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_bound_14() { return &___bound_14; }
	inline void set_bound_14(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___bound_14 = value;
	}

	inline static int32_t get_offset_of_boundOffset_15() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___boundOffset_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_boundOffset_15() const { return ___boundOffset_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_boundOffset_15() { return &___boundOffset_15; }
	inline void set_boundOffset_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___boundOffset_15 = value;
	}

	inline static int32_t get_offset_of_colliderBound_16() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___colliderBound_16)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_colliderBound_16() const { return ___colliderBound_16; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_colliderBound_16() { return &___colliderBound_16; }
	inline void set_colliderBound_16(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___colliderBound_16 = value;
	}

	inline static int32_t get_offset_of_colliderBoundOffset_17() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___colliderBoundOffset_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_colliderBoundOffset_17() const { return ___colliderBoundOffset_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_colliderBoundOffset_17() { return &___colliderBoundOffset_17; }
	inline void set_colliderBoundOffset_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___colliderBoundOffset_17 = value;
	}

	inline static int32_t get_offset_of_meshBound_18() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___meshBound_18)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_meshBound_18() const { return ___meshBound_18; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_meshBound_18() { return &___meshBound_18; }
	inline void set_meshBound_18(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___meshBound_18 = value;
	}

	inline static int32_t get_offset_of_meshBoundOffset_19() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___meshBoundOffset_19)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_meshBoundOffset_19() const { return ___meshBoundOffset_19; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_meshBoundOffset_19() { return &___meshBoundOffset_19; }
	inline void set_meshBoundOffset_19(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___meshBoundOffset_19 = value;
	}

	inline static int32_t get_offset_of_corners_20() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___corners_20)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_corners_20() const { return ___corners_20; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_corners_20() { return &___corners_20; }
	inline void set_corners_20(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___corners_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___corners_20), (void*)value);
	}

	inline static int32_t get_offset_of_lines_21() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___lines_21)); }
	inline Vector3U5BU2CU5D_tF16B36EC585DBDA6C179E131AE5794A33695CA71* get_lines_21() const { return ___lines_21; }
	inline Vector3U5BU2CU5D_tF16B36EC585DBDA6C179E131AE5794A33695CA71** get_address_of_lines_21() { return &___lines_21; }
	inline void set_lines_21(Vector3U5BU2CU5D_tF16B36EC585DBDA6C179E131AE5794A33695CA71* value)
	{
		___lines_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lines_21), (void*)value);
	}

	inline static int32_t get_offset_of_quat_22() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___quat_22)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_quat_22() const { return ___quat_22; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_quat_22() { return &___quat_22; }
	inline void set_quat_22(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___quat_22 = value;
	}

	inline static int32_t get_offset_of_lineList_23() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___lineList_23)); }
	inline LineRendererU5BU5D_t65EF16DA3FB3E6D083B247824B04BE10D0B46743* get_lineList_23() const { return ___lineList_23; }
	inline LineRendererU5BU5D_t65EF16DA3FB3E6D083B247824B04BE10D0B46743** get_address_of_lineList_23() { return &___lineList_23; }
	inline void set_lineList_23(LineRendererU5BU5D_t65EF16DA3FB3E6D083B247824B04BE10D0B46743* value)
	{
		___lineList_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineList_23), (void*)value);
	}

	inline static int32_t get_offset_of_topFrontLeft_24() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___topFrontLeft_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_topFrontLeft_24() const { return ___topFrontLeft_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_topFrontLeft_24() { return &___topFrontLeft_24; }
	inline void set_topFrontLeft_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___topFrontLeft_24 = value;
	}

	inline static int32_t get_offset_of_topFrontRight_25() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___topFrontRight_25)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_topFrontRight_25() const { return ___topFrontRight_25; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_topFrontRight_25() { return &___topFrontRight_25; }
	inline void set_topFrontRight_25(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___topFrontRight_25 = value;
	}

	inline static int32_t get_offset_of_topBackLeft_26() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___topBackLeft_26)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_topBackLeft_26() const { return ___topBackLeft_26; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_topBackLeft_26() { return &___topBackLeft_26; }
	inline void set_topBackLeft_26(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___topBackLeft_26 = value;
	}

	inline static int32_t get_offset_of_topBackRight_27() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___topBackRight_27)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_topBackRight_27() const { return ___topBackRight_27; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_topBackRight_27() { return &___topBackRight_27; }
	inline void set_topBackRight_27(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___topBackRight_27 = value;
	}

	inline static int32_t get_offset_of_bottomFrontLeft_28() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___bottomFrontLeft_28)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bottomFrontLeft_28() const { return ___bottomFrontLeft_28; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bottomFrontLeft_28() { return &___bottomFrontLeft_28; }
	inline void set_bottomFrontLeft_28(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bottomFrontLeft_28 = value;
	}

	inline static int32_t get_offset_of_bottomFrontRight_29() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___bottomFrontRight_29)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bottomFrontRight_29() const { return ___bottomFrontRight_29; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bottomFrontRight_29() { return &___bottomFrontRight_29; }
	inline void set_bottomFrontRight_29(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bottomFrontRight_29 = value;
	}

	inline static int32_t get_offset_of_bottomBackLeft_30() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___bottomBackLeft_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bottomBackLeft_30() const { return ___bottomBackLeft_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bottomBackLeft_30() { return &___bottomBackLeft_30; }
	inline void set_bottomBackLeft_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bottomBackLeft_30 = value;
	}

	inline static int32_t get_offset_of_bottomBackRight_31() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___bottomBackRight_31)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bottomBackRight_31() const { return ___bottomBackRight_31; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bottomBackRight_31() { return &___bottomBackRight_31; }
	inline void set_bottomBackRight_31(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bottomBackRight_31 = value;
	}

	inline static int32_t get_offset_of_startingScale_32() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___startingScale_32)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startingScale_32() const { return ___startingScale_32; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startingScale_32() { return &___startingScale_32; }
	inline void set_startingScale_32(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startingScale_32 = value;
	}

	inline static int32_t get_offset_of_previousScale_33() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___previousScale_33)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_previousScale_33() const { return ___previousScale_33; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_previousScale_33() { return &___previousScale_33; }
	inline void set_previousScale_33(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___previousScale_33 = value;
	}

	inline static int32_t get_offset_of_startingBoundSize_34() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___startingBoundSize_34)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startingBoundSize_34() const { return ___startingBoundSize_34; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startingBoundSize_34() { return &___startingBoundSize_34; }
	inline void set_startingBoundSize_34(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startingBoundSize_34 = value;
	}

	inline static int32_t get_offset_of_startingBoundCenterLocal_35() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___startingBoundCenterLocal_35)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startingBoundCenterLocal_35() const { return ___startingBoundCenterLocal_35; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startingBoundCenterLocal_35() { return &___startingBoundCenterLocal_35; }
	inline void set_startingBoundCenterLocal_35(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startingBoundCenterLocal_35 = value;
	}

	inline static int32_t get_offset_of_previousPosition_36() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___previousPosition_36)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_previousPosition_36() const { return ___previousPosition_36; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_previousPosition_36() { return &___previousPosition_36; }
	inline void set_previousPosition_36(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___previousPosition_36 = value;
	}

	inline static int32_t get_offset_of_previousRotation_37() { return static_cast<int32_t>(offsetof(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F, ___previousRotation_37)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_previousRotation_37() const { return ___previousRotation_37; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_previousRotation_37() { return &___previousRotation_37; }
	inline void set_previousRotation_37(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___previousRotation_37 = value;
	}
};


// DimBoxes.BoundBoxExample
struct  BoundBoxExample_t7B9576D542794C5545DF5F9029013C0CA70DFCE6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DimBoxes.BoundBox DimBoxes.BoundBoxExample::boundBox
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F * ___boundBox_4;

public:
	inline static int32_t get_offset_of_boundBox_4() { return static_cast<int32_t>(offsetof(BoundBoxExample_t7B9576D542794C5545DF5F9029013C0CA70DFCE6, ___boundBox_4)); }
	inline BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F * get_boundBox_4() const { return ___boundBox_4; }
	inline BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F ** get_address_of_boundBox_4() { return &___boundBox_4; }
	inline void set_boundBox_4(BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F * value)
	{
		___boundBox_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boundBox_4), (void*)value);
	}
};


// CameraDepthEnabler
struct  CameraDepthEnabler_t07D21C1C19E86EA41B17B5BC5A83A9B3BA30FA02  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ContentPositioner
struct  ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ContentPositioner::contentToAlign
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___contentToAlign_4;
	// System.Single ContentPositioner::defaultDistanceHoloLens1
	float ___defaultDistanceHoloLens1_5;
	// System.Single ContentPositioner::defaultDistanceHoloLens2
	float ___defaultDistanceHoloLens2_6;
	// System.Single ContentPositioner::distanceFromCamera
	float ___distanceFromCamera_7;
	// System.Boolean ContentPositioner::realignContent
	bool ___realignContent_8;
	// UnityEngine.Vector3 ContentPositioner::destinationPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___destinationPosition_9;
	// UnityEngine.Quaternion ContentPositioner::destinationRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___destinationRotation_10;
	// UnityEngine.Camera ContentPositioner::cam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cam_11;

public:
	inline static int32_t get_offset_of_contentToAlign_4() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___contentToAlign_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_contentToAlign_4() const { return ___contentToAlign_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_contentToAlign_4() { return &___contentToAlign_4; }
	inline void set_contentToAlign_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___contentToAlign_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentToAlign_4), (void*)value);
	}

	inline static int32_t get_offset_of_defaultDistanceHoloLens1_5() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___defaultDistanceHoloLens1_5)); }
	inline float get_defaultDistanceHoloLens1_5() const { return ___defaultDistanceHoloLens1_5; }
	inline float* get_address_of_defaultDistanceHoloLens1_5() { return &___defaultDistanceHoloLens1_5; }
	inline void set_defaultDistanceHoloLens1_5(float value)
	{
		___defaultDistanceHoloLens1_5 = value;
	}

	inline static int32_t get_offset_of_defaultDistanceHoloLens2_6() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___defaultDistanceHoloLens2_6)); }
	inline float get_defaultDistanceHoloLens2_6() const { return ___defaultDistanceHoloLens2_6; }
	inline float* get_address_of_defaultDistanceHoloLens2_6() { return &___defaultDistanceHoloLens2_6; }
	inline void set_defaultDistanceHoloLens2_6(float value)
	{
		___defaultDistanceHoloLens2_6 = value;
	}

	inline static int32_t get_offset_of_distanceFromCamera_7() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___distanceFromCamera_7)); }
	inline float get_distanceFromCamera_7() const { return ___distanceFromCamera_7; }
	inline float* get_address_of_distanceFromCamera_7() { return &___distanceFromCamera_7; }
	inline void set_distanceFromCamera_7(float value)
	{
		___distanceFromCamera_7 = value;
	}

	inline static int32_t get_offset_of_realignContent_8() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___realignContent_8)); }
	inline bool get_realignContent_8() const { return ___realignContent_8; }
	inline bool* get_address_of_realignContent_8() { return &___realignContent_8; }
	inline void set_realignContent_8(bool value)
	{
		___realignContent_8 = value;
	}

	inline static int32_t get_offset_of_destinationPosition_9() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___destinationPosition_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_destinationPosition_9() const { return ___destinationPosition_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_destinationPosition_9() { return &___destinationPosition_9; }
	inline void set_destinationPosition_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___destinationPosition_9 = value;
	}

	inline static int32_t get_offset_of_destinationRotation_10() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___destinationRotation_10)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_destinationRotation_10() const { return ___destinationRotation_10; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_destinationRotation_10() { return &___destinationRotation_10; }
	inline void set_destinationRotation_10(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___destinationRotation_10 = value;
	}

	inline static int32_t get_offset_of_cam_11() { return static_cast<int32_t>(offsetof(ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0, ___cam_11)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_cam_11() const { return ___cam_11; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_cam_11() { return &___cam_11; }
	inline void set_cam_11(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___cam_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cam_11), (void*)value);
	}
};


// ContentPositionerClient
struct  ContentPositionerClient_t0A192212343478F3C2F9A7159BB4659068C8ED27  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ContentPositionerClient::distanceHoloLens1
	float ___distanceHoloLens1_4;
	// System.Single ContentPositionerClient::distanceHoloLens2
	float ___distanceHoloLens2_5;

public:
	inline static int32_t get_offset_of_distanceHoloLens1_4() { return static_cast<int32_t>(offsetof(ContentPositionerClient_t0A192212343478F3C2F9A7159BB4659068C8ED27, ___distanceHoloLens1_4)); }
	inline float get_distanceHoloLens1_4() const { return ___distanceHoloLens1_4; }
	inline float* get_address_of_distanceHoloLens1_4() { return &___distanceHoloLens1_4; }
	inline void set_distanceHoloLens1_4(float value)
	{
		___distanceHoloLens1_4 = value;
	}

	inline static int32_t get_offset_of_distanceHoloLens2_5() { return static_cast<int32_t>(offsetof(ContentPositionerClient_t0A192212343478F3C2F9A7159BB4659068C8ED27, ___distanceHoloLens2_5)); }
	inline float get_distanceHoloLens2_5() const { return ___distanceHoloLens2_5; }
	inline float* get_address_of_distanceHoloLens2_5() { return &___distanceHoloLens2_5; }
	inline void set_distanceHoloLens2_5(float value)
	{
		___distanceHoloLens2_5 = value;
	}
};


// CustomTurnOffBehaviour
struct  CustomTurnOffBehaviour_tA4AF26D4FCB81D0E041CB0CF42AF4A895F19B9B3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CustomTurnOffBehaviour/TurnOffRendering CustomTurnOffBehaviour::turnOffRendering
	int32_t ___turnOffRendering_4;

public:
	inline static int32_t get_offset_of_turnOffRendering_4() { return static_cast<int32_t>(offsetof(CustomTurnOffBehaviour_tA4AF26D4FCB81D0E041CB0CF42AF4A895F19B9B3, ___turnOffRendering_4)); }
	inline int32_t get_turnOffRendering_4() const { return ___turnOffRendering_4; }
	inline int32_t* get_address_of_turnOffRendering_4() { return &___turnOffRendering_4; }
	inline void set_turnOffRendering_4(int32_t value)
	{
		___turnOffRendering_4 = value;
	}
};


// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DefaultTrackableEventHandler/TrackingStatusFilter DefaultTrackableEventHandler::StatusFilter
	int32_t ___StatusFilter_4;
	// UnityEngine.Events.UnityEvent DefaultTrackableEventHandler::OnTargetFound
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnTargetFound_5;
	// UnityEngine.Events.UnityEvent DefaultTrackableEventHandler::OnTargetLost
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnTargetLost_6;
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t739912483540FF363C2BC828A6930E0397CAC0A8 * ___mTrackableBehaviour_7;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_8;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_9;
	// Vuforia.TrackableBehaviour/StatusInfo DefaultTrackableEventHandler::m_PreviousStatusInfo
	int32_t ___m_PreviousStatusInfo_10;
	// Vuforia.TrackableBehaviour/StatusInfo DefaultTrackableEventHandler::m_NewStatusInfo
	int32_t ___m_NewStatusInfo_11;
	// System.Boolean DefaultTrackableEventHandler::m_CallbackReceivedOnce
	bool ___m_CallbackReceivedOnce_12;

public:
	inline static int32_t get_offset_of_StatusFilter_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___StatusFilter_4)); }
	inline int32_t get_StatusFilter_4() const { return ___StatusFilter_4; }
	inline int32_t* get_address_of_StatusFilter_4() { return &___StatusFilter_4; }
	inline void set_StatusFilter_4(int32_t value)
	{
		___StatusFilter_4 = value;
	}

	inline static int32_t get_offset_of_OnTargetFound_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___OnTargetFound_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnTargetFound_5() const { return ___OnTargetFound_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnTargetFound_5() { return &___OnTargetFound_5; }
	inline void set_OnTargetFound_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnTargetFound_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTargetFound_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnTargetLost_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___OnTargetLost_6)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnTargetLost_6() const { return ___OnTargetLost_6; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnTargetLost_6() { return &___OnTargetLost_6; }
	inline void set_OnTargetLost_6(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnTargetLost_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTargetLost_6), (void*)value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_7() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___mTrackableBehaviour_7)); }
	inline TrackableBehaviour_t739912483540FF363C2BC828A6930E0397CAC0A8 * get_mTrackableBehaviour_7() const { return ___mTrackableBehaviour_7; }
	inline TrackableBehaviour_t739912483540FF363C2BC828A6930E0397CAC0A8 ** get_address_of_mTrackableBehaviour_7() { return &___mTrackableBehaviour_7; }
	inline void set_mTrackableBehaviour_7(TrackableBehaviour_t739912483540FF363C2BC828A6930E0397CAC0A8 * value)
	{
		___mTrackableBehaviour_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableBehaviour_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_8() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___m_PreviousStatus_8)); }
	inline int32_t get_m_PreviousStatus_8() const { return ___m_PreviousStatus_8; }
	inline int32_t* get_address_of_m_PreviousStatus_8() { return &___m_PreviousStatus_8; }
	inline void set_m_PreviousStatus_8(int32_t value)
	{
		___m_PreviousStatus_8 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_9() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___m_NewStatus_9)); }
	inline int32_t get_m_NewStatus_9() const { return ___m_NewStatus_9; }
	inline int32_t* get_address_of_m_NewStatus_9() { return &___m_NewStatus_9; }
	inline void set_m_NewStatus_9(int32_t value)
	{
		___m_NewStatus_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousStatusInfo_10() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___m_PreviousStatusInfo_10)); }
	inline int32_t get_m_PreviousStatusInfo_10() const { return ___m_PreviousStatusInfo_10; }
	inline int32_t* get_address_of_m_PreviousStatusInfo_10() { return &___m_PreviousStatusInfo_10; }
	inline void set_m_PreviousStatusInfo_10(int32_t value)
	{
		___m_PreviousStatusInfo_10 = value;
	}

	inline static int32_t get_offset_of_m_NewStatusInfo_11() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___m_NewStatusInfo_11)); }
	inline int32_t get_m_NewStatusInfo_11() const { return ___m_NewStatusInfo_11; }
	inline int32_t* get_address_of_m_NewStatusInfo_11() { return &___m_NewStatusInfo_11; }
	inline void set_m_NewStatusInfo_11(int32_t value)
	{
		___m_NewStatusInfo_11 = value;
	}

	inline static int32_t get_offset_of_m_CallbackReceivedOnce_12() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB, ___m_CallbackReceivedOnce_12)); }
	inline bool get_m_CallbackReceivedOnce_12() const { return ___m_CallbackReceivedOnce_12; }
	inline bool* get_address_of_m_CallbackReceivedOnce_12() { return &___m_CallbackReceivedOnce_12; }
	inline void set_m_CallbackReceivedOnce_12(bool value)
	{
		___m_CallbackReceivedOnce_12 = value;
	}
};


// DepthCanvas
struct  DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera DepthCanvas::vuforiaCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___vuforiaCamera_4;
	// UnityEngine.RectTransform DepthCanvas::canvasRectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___canvasRectTransform_5;
	// System.Single DepthCanvas::maxDistanceFromCamera
	float ___maxDistanceFromCamera_6;

public:
	inline static int32_t get_offset_of_vuforiaCamera_4() { return static_cast<int32_t>(offsetof(DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9, ___vuforiaCamera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_vuforiaCamera_4() const { return ___vuforiaCamera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_vuforiaCamera_4() { return &___vuforiaCamera_4; }
	inline void set_vuforiaCamera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___vuforiaCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuforiaCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_canvasRectTransform_5() { return static_cast<int32_t>(offsetof(DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9, ___canvasRectTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_canvasRectTransform_5() const { return ___canvasRectTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_canvasRectTransform_5() { return &___canvasRectTransform_5; }
	inline void set_canvasRectTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___canvasRectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasRectTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_maxDistanceFromCamera_6() { return static_cast<int32_t>(offsetof(DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9, ___maxDistanceFromCamera_6)); }
	inline float get_maxDistanceFromCamera_6() const { return ___maxDistanceFromCamera_6; }
	inline float* get_address_of_maxDistanceFromCamera_6() { return &___maxDistanceFromCamera_6; }
	inline void set_maxDistanceFromCamera_6(float value)
	{
		___maxDistanceFromCamera_6 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult
struct  DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::displayTextMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___displayTextMesh_4;
	// Microsoft.MixedReality.Toolkit.Utilities.AxisType Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::inputType
	int32_t ___inputType_5;
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::axisNumber
	int32_t ___axisNumber_6;
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::buttonNumber
	int32_t ___buttonNumber_7;
	// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::displayType
	int32_t ___displayType_8;

public:
	inline static int32_t get_offset_of_displayTextMesh_4() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___displayTextMesh_4)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_displayTextMesh_4() const { return ___displayTextMesh_4; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_displayTextMesh_4() { return &___displayTextMesh_4; }
	inline void set_displayTextMesh_4(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___displayTextMesh_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayTextMesh_4), (void*)value);
	}

	inline static int32_t get_offset_of_inputType_5() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___inputType_5)); }
	inline int32_t get_inputType_5() const { return ___inputType_5; }
	inline int32_t* get_address_of_inputType_5() { return &___inputType_5; }
	inline void set_inputType_5(int32_t value)
	{
		___inputType_5 = value;
	}

	inline static int32_t get_offset_of_axisNumber_6() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___axisNumber_6)); }
	inline int32_t get_axisNumber_6() const { return ___axisNumber_6; }
	inline int32_t* get_address_of_axisNumber_6() { return &___axisNumber_6; }
	inline void set_axisNumber_6(int32_t value)
	{
		___axisNumber_6 = value;
	}

	inline static int32_t get_offset_of_buttonNumber_7() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___buttonNumber_7)); }
	inline int32_t get_buttonNumber_7() const { return ___buttonNumber_7; }
	inline int32_t* get_address_of_buttonNumber_7() { return &___buttonNumber_7; }
	inline void set_buttonNumber_7(int32_t value)
	{
		___buttonNumber_7 = value;
	}

	inline static int32_t get_offset_of_displayType_8() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___displayType_8)); }
	inline int32_t get_displayType_8() const { return ___displayType_8; }
	inline int32_t* get_address_of_displayType_8() { return &___displayType_8; }
	inline void set_displayType_8(int32_t value)
	{
		___displayType_8 = value;
	}
};


// DimBoxes.DrawLines
struct  DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Material DimBoxes.DrawLines::lineMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___lineMaterial_4;
	// UnityEngine.Color DimBoxes.DrawLines::lColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___lColor_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[0...,0...]> DimBoxes.DrawLines::outlines
	List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * ___outlines_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[][]> DimBoxes.DrawLines::triangles
	List_1_t22506B3452F881404307E66213B0A382BA5C1DA3 * ___triangles_7;
	// System.Collections.Generic.List`1<UnityEngine.Color> DimBoxes.DrawLines::colors
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___colors_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[0...,0...]> DimBoxes.DrawLines::screenOutlines
	List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * ___screenOutlines_9;
	// System.Collections.Generic.List`1<UnityEngine.Color> DimBoxes.DrawLines::screenColors
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___screenColors_10;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___lineMaterial_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineMaterial_4), (void*)value);
	}

	inline static int32_t get_offset_of_lColor_5() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___lColor_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_lColor_5() const { return ___lColor_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_lColor_5() { return &___lColor_5; }
	inline void set_lColor_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___lColor_5 = value;
	}

	inline static int32_t get_offset_of_outlines_6() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___outlines_6)); }
	inline List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * get_outlines_6() const { return ___outlines_6; }
	inline List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 ** get_address_of_outlines_6() { return &___outlines_6; }
	inline void set_outlines_6(List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * value)
	{
		___outlines_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outlines_6), (void*)value);
	}

	inline static int32_t get_offset_of_triangles_7() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___triangles_7)); }
	inline List_1_t22506B3452F881404307E66213B0A382BA5C1DA3 * get_triangles_7() const { return ___triangles_7; }
	inline List_1_t22506B3452F881404307E66213B0A382BA5C1DA3 ** get_address_of_triangles_7() { return &___triangles_7; }
	inline void set_triangles_7(List_1_t22506B3452F881404307E66213B0A382BA5C1DA3 * value)
	{
		___triangles_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangles_7), (void*)value);
	}

	inline static int32_t get_offset_of_colors_8() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___colors_8)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_colors_8() const { return ___colors_8; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_colors_8() { return &___colors_8; }
	inline void set_colors_8(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___colors_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_8), (void*)value);
	}

	inline static int32_t get_offset_of_screenOutlines_9() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___screenOutlines_9)); }
	inline List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * get_screenOutlines_9() const { return ___screenOutlines_9; }
	inline List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 ** get_address_of_screenOutlines_9() { return &___screenOutlines_9; }
	inline void set_screenOutlines_9(List_1_tC4363427E5DEB59A2B318280C537AE31E449D1E8 * value)
	{
		___screenOutlines_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___screenOutlines_9), (void*)value);
	}

	inline static int32_t get_offset_of_screenColors_10() { return static_cast<int32_t>(offsetof(DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6, ___screenColors_10)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_screenColors_10() const { return ___screenColors_10; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_screenColors_10() { return &___screenColors_10; }
	inline void set_screenColors_10(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___screenColors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___screenColors_10), (void*)value);
	}
};


// FixPositionTarget
struct  FixPositionTarget_tEC41273C21FADD2DE3E80E7A01C5B828E48B91E1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject FixPositionTarget::hologram
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hologram_4;

public:
	inline static int32_t get_offset_of_hologram_4() { return static_cast<int32_t>(offsetof(FixPositionTarget_tEC41273C21FADD2DE3E80E7A01C5B828E48B91E1, ___hologram_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hologram_4() const { return ___hologram_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hologram_4() { return &___hologram_4; }
	inline void set_hologram_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hologram_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hologram_4), (void*)value);
	}
};


// HoloToolkit.Unity.FixedAngularSize
struct  FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single HoloToolkit.Unity.FixedAngularSize::SizeRatio
	float ___SizeRatio_4;
	// System.Single HoloToolkit.Unity.FixedAngularSize::startingDistance
	float ___startingDistance_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.FixedAngularSize::startingScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startingScale_6;

public:
	inline static int32_t get_offset_of_SizeRatio_4() { return static_cast<int32_t>(offsetof(FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4, ___SizeRatio_4)); }
	inline float get_SizeRatio_4() const { return ___SizeRatio_4; }
	inline float* get_address_of_SizeRatio_4() { return &___SizeRatio_4; }
	inline void set_SizeRatio_4(float value)
	{
		___SizeRatio_4 = value;
	}

	inline static int32_t get_offset_of_startingDistance_5() { return static_cast<int32_t>(offsetof(FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4, ___startingDistance_5)); }
	inline float get_startingDistance_5() const { return ___startingDistance_5; }
	inline float* get_address_of_startingDistance_5() { return &___startingDistance_5; }
	inline void set_startingDistance_5(float value)
	{
		___startingDistance_5 = value;
	}

	inline static int32_t get_offset_of_startingScale_6() { return static_cast<int32_t>(offsetof(FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4, ___startingScale_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startingScale_6() const { return ___startingScale_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startingScale_6() { return &___startingScale_6; }
	inline void set_startingScale_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startingScale_6 = value;
	}
};


// HandleControls
struct  HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject HandleControls::hologram
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hologram_4;

public:
	inline static int32_t get_offset_of_hologram_4() { return static_cast<int32_t>(offsetof(HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984, ___hologram_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hologram_4() const { return ___hologram_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hologram_4() { return &___hologram_4; }
	inline void set_hologram_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hologram_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hologram_4), (void*)value);
	}
};


// HideDevConsole
struct  HideDevConsole_t4BCC4C35BEAF39F3528F56A843E04C57A389C723  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// HoloToolkit.Unity.Interpolator
struct  Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean HoloToolkit.Unity.Interpolator::UseUnscaledTime
	bool ___UseUnscaledTime_4;
	// System.Single HoloToolkit.Unity.Interpolator::PositionPerSecond
	float ___PositionPerSecond_6;
	// System.Single HoloToolkit.Unity.Interpolator::RotationDegreesPerSecond
	float ___RotationDegreesPerSecond_7;
	// System.Single HoloToolkit.Unity.Interpolator::RotationSpeedScaler
	float ___RotationSpeedScaler_8;
	// System.Single HoloToolkit.Unity.Interpolator::ScalePerSecond
	float ___ScalePerSecond_9;
	// System.Boolean HoloToolkit.Unity.Interpolator::SmoothLerpToTarget
	bool ___SmoothLerpToTarget_10;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothPositionLerpRatio
	float ___SmoothPositionLerpRatio_11;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothRotationLerpRatio
	float ___SmoothRotationLerpRatio_12;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothScaleLerpRatio
	float ___SmoothScaleLerpRatio_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPosition_14;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingPosition>k__BackingField
	bool ___U3CAnimatingPositionU3Ek__BackingField_15;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___targetRotation_16;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingRotation>k__BackingField
	bool ___U3CAnimatingRotationU3Ek__BackingField_17;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetLocalRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___targetLocalRotation_18;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalRotation>k__BackingField
	bool ___U3CAnimatingLocalRotationU3Ek__BackingField_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetLocalScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetLocalScale_20;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalScale>k__BackingField
	bool ___U3CAnimatingLocalScaleU3Ek__BackingField_21;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationStarted
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___InterpolationStarted_22;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationDone
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___InterpolationDone_23;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::<PositionVelocity>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionVelocityU3Ek__BackingField_24;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::oldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oldPosition_25;

public:
	inline static int32_t get_offset_of_UseUnscaledTime_4() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___UseUnscaledTime_4)); }
	inline bool get_UseUnscaledTime_4() const { return ___UseUnscaledTime_4; }
	inline bool* get_address_of_UseUnscaledTime_4() { return &___UseUnscaledTime_4; }
	inline void set_UseUnscaledTime_4(bool value)
	{
		___UseUnscaledTime_4 = value;
	}

	inline static int32_t get_offset_of_PositionPerSecond_6() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___PositionPerSecond_6)); }
	inline float get_PositionPerSecond_6() const { return ___PositionPerSecond_6; }
	inline float* get_address_of_PositionPerSecond_6() { return &___PositionPerSecond_6; }
	inline void set_PositionPerSecond_6(float value)
	{
		___PositionPerSecond_6 = value;
	}

	inline static int32_t get_offset_of_RotationDegreesPerSecond_7() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___RotationDegreesPerSecond_7)); }
	inline float get_RotationDegreesPerSecond_7() const { return ___RotationDegreesPerSecond_7; }
	inline float* get_address_of_RotationDegreesPerSecond_7() { return &___RotationDegreesPerSecond_7; }
	inline void set_RotationDegreesPerSecond_7(float value)
	{
		___RotationDegreesPerSecond_7 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedScaler_8() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___RotationSpeedScaler_8)); }
	inline float get_RotationSpeedScaler_8() const { return ___RotationSpeedScaler_8; }
	inline float* get_address_of_RotationSpeedScaler_8() { return &___RotationSpeedScaler_8; }
	inline void set_RotationSpeedScaler_8(float value)
	{
		___RotationSpeedScaler_8 = value;
	}

	inline static int32_t get_offset_of_ScalePerSecond_9() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___ScalePerSecond_9)); }
	inline float get_ScalePerSecond_9() const { return ___ScalePerSecond_9; }
	inline float* get_address_of_ScalePerSecond_9() { return &___ScalePerSecond_9; }
	inline void set_ScalePerSecond_9(float value)
	{
		___ScalePerSecond_9 = value;
	}

	inline static int32_t get_offset_of_SmoothLerpToTarget_10() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___SmoothLerpToTarget_10)); }
	inline bool get_SmoothLerpToTarget_10() const { return ___SmoothLerpToTarget_10; }
	inline bool* get_address_of_SmoothLerpToTarget_10() { return &___SmoothLerpToTarget_10; }
	inline void set_SmoothLerpToTarget_10(bool value)
	{
		___SmoothLerpToTarget_10 = value;
	}

	inline static int32_t get_offset_of_SmoothPositionLerpRatio_11() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___SmoothPositionLerpRatio_11)); }
	inline float get_SmoothPositionLerpRatio_11() const { return ___SmoothPositionLerpRatio_11; }
	inline float* get_address_of_SmoothPositionLerpRatio_11() { return &___SmoothPositionLerpRatio_11; }
	inline void set_SmoothPositionLerpRatio_11(float value)
	{
		___SmoothPositionLerpRatio_11 = value;
	}

	inline static int32_t get_offset_of_SmoothRotationLerpRatio_12() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___SmoothRotationLerpRatio_12)); }
	inline float get_SmoothRotationLerpRatio_12() const { return ___SmoothRotationLerpRatio_12; }
	inline float* get_address_of_SmoothRotationLerpRatio_12() { return &___SmoothRotationLerpRatio_12; }
	inline void set_SmoothRotationLerpRatio_12(float value)
	{
		___SmoothRotationLerpRatio_12 = value;
	}

	inline static int32_t get_offset_of_SmoothScaleLerpRatio_13() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___SmoothScaleLerpRatio_13)); }
	inline float get_SmoothScaleLerpRatio_13() const { return ___SmoothScaleLerpRatio_13; }
	inline float* get_address_of_SmoothScaleLerpRatio_13() { return &___SmoothScaleLerpRatio_13; }
	inline void set_SmoothScaleLerpRatio_13(float value)
	{
		___SmoothScaleLerpRatio_13 = value;
	}

	inline static int32_t get_offset_of_targetPosition_14() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___targetPosition_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPosition_14() const { return ___targetPosition_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPosition_14() { return &___targetPosition_14; }
	inline void set_targetPosition_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPosition_14 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___U3CAnimatingPositionU3Ek__BackingField_15)); }
	inline bool get_U3CAnimatingPositionU3Ek__BackingField_15() const { return ___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CAnimatingPositionU3Ek__BackingField_15() { return &___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline void set_U3CAnimatingPositionU3Ek__BackingField_15(bool value)
	{
		___U3CAnimatingPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___targetRotation_16)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_targetRotation_16() const { return ___targetRotation_16; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___targetRotation_16 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___U3CAnimatingRotationU3Ek__BackingField_17)); }
	inline bool get_U3CAnimatingRotationU3Ek__BackingField_17() const { return ___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CAnimatingRotationU3Ek__BackingField_17() { return &___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline void set_U3CAnimatingRotationU3Ek__BackingField_17(bool value)
	{
		___U3CAnimatingRotationU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_targetLocalRotation_18() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___targetLocalRotation_18)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_targetLocalRotation_18() const { return ___targetLocalRotation_18; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_targetLocalRotation_18() { return &___targetLocalRotation_18; }
	inline void set_targetLocalRotation_18(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___targetLocalRotation_18 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___U3CAnimatingLocalRotationU3Ek__BackingField_19)); }
	inline bool get_U3CAnimatingLocalRotationU3Ek__BackingField_19() const { return ___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return &___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline void set_U3CAnimatingLocalRotationU3Ek__BackingField_19(bool value)
	{
		___U3CAnimatingLocalRotationU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_targetLocalScale_20() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___targetLocalScale_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetLocalScale_20() const { return ___targetLocalScale_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetLocalScale_20() { return &___targetLocalScale_20; }
	inline void set_targetLocalScale_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetLocalScale_20 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___U3CAnimatingLocalScaleU3Ek__BackingField_21)); }
	inline bool get_U3CAnimatingLocalScaleU3Ek__BackingField_21() const { return ___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return &___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline void set_U3CAnimatingLocalScaleU3Ek__BackingField_21(bool value)
	{
		___U3CAnimatingLocalScaleU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_InterpolationStarted_22() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___InterpolationStarted_22)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_InterpolationStarted_22() const { return ___InterpolationStarted_22; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_InterpolationStarted_22() { return &___InterpolationStarted_22; }
	inline void set_InterpolationStarted_22(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___InterpolationStarted_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InterpolationStarted_22), (void*)value);
	}

	inline static int32_t get_offset_of_InterpolationDone_23() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___InterpolationDone_23)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_InterpolationDone_23() const { return ___InterpolationDone_23; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_InterpolationDone_23() { return &___InterpolationDone_23; }
	inline void set_InterpolationDone_23(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___InterpolationDone_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InterpolationDone_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPositionVelocityU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___U3CPositionVelocityU3Ek__BackingField_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CPositionVelocityU3Ek__BackingField_24() const { return ___U3CPositionVelocityU3Ek__BackingField_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CPositionVelocityU3Ek__BackingField_24() { return &___U3CPositionVelocityU3Ek__BackingField_24; }
	inline void set_U3CPositionVelocityU3Ek__BackingField_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CPositionVelocityU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_oldPosition_25() { return static_cast<int32_t>(offsetof(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9, ___oldPosition_25)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oldPosition_25() const { return ___oldPosition_25; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oldPosition_25() { return &___oldPosition_25; }
	inline void set_oldPosition_25(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oldPosition_25 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages
struct  ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::listInputDevicesTextMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___listInputDevicesTextMesh_4;
	// UnityEngine.TextMesh[] Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::displayFeatureUsagesTextMeshes
	TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* ___displayFeatureUsagesTextMeshes_5;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::controllerInputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___controllerInputDevices_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::handInputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___handInputDevices_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::featureUsages
	List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * ___featureUsages_8;

public:
	inline static int32_t get_offset_of_listInputDevicesTextMesh_4() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___listInputDevicesTextMesh_4)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_listInputDevicesTextMesh_4() const { return ___listInputDevicesTextMesh_4; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_listInputDevicesTextMesh_4() { return &___listInputDevicesTextMesh_4; }
	inline void set_listInputDevicesTextMesh_4(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___listInputDevicesTextMesh_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listInputDevicesTextMesh_4), (void*)value);
	}

	inline static int32_t get_offset_of_displayFeatureUsagesTextMeshes_5() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___displayFeatureUsagesTextMeshes_5)); }
	inline TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* get_displayFeatureUsagesTextMeshes_5() const { return ___displayFeatureUsagesTextMeshes_5; }
	inline TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981** get_address_of_displayFeatureUsagesTextMeshes_5() { return &___displayFeatureUsagesTextMeshes_5; }
	inline void set_displayFeatureUsagesTextMeshes_5(TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* value)
	{
		___displayFeatureUsagesTextMeshes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayFeatureUsagesTextMeshes_5), (void*)value);
	}

	inline static int32_t get_offset_of_controllerInputDevices_6() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___controllerInputDevices_6)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_controllerInputDevices_6() const { return ___controllerInputDevices_6; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_controllerInputDevices_6() { return &___controllerInputDevices_6; }
	inline void set_controllerInputDevices_6(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___controllerInputDevices_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controllerInputDevices_6), (void*)value);
	}

	inline static int32_t get_offset_of_handInputDevices_7() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___handInputDevices_7)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_handInputDevices_7() const { return ___handInputDevices_7; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_handInputDevices_7() { return &___handInputDevices_7; }
	inline void set_handInputDevices_7(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___handInputDevices_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handInputDevices_7), (void*)value);
	}

	inline static int32_t get_offset_of_featureUsages_8() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___featureUsages_8)); }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * get_featureUsages_8() const { return ___featureUsages_8; }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E ** get_address_of_featureUsages_8() { return &___featureUsages_8; }
	inline void set_featureUsages_8(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * value)
	{
		___featureUsages_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___featureUsages_8), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem
struct  MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9  : public BaseDataProviderAccessCoreSystem_t1AD705612035CBBB501452E58E8B07BA8F62C04F
{
public:
	// System.String Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_17;
	// Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::meshEventData
	MixedRealitySpatialAwarenessEventData_1_t00EC2B520F876AA2904782A9299DCBEB60209B8B * ___meshEventData_18;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::spatialAwarenessObjectParent
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___spatialAwarenessObjectParent_19;
	// System.UInt32 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::nextSourceId
	uint32_t ___nextSourceId_20;
	// Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::spatialAwarenessSystemProfile
	MixedRealitySpatialAwarenessSystemProfile_tC7E99BA5DC82D06919DE73FF542CAE4929C41D4B * ___spatialAwarenessSystemProfile_21;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9, ___U3CNameU3Ek__BackingField_17)); }
	inline String_t* get_U3CNameU3Ek__BackingField_17() const { return ___U3CNameU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_17() { return &___U3CNameU3Ek__BackingField_17; }
	inline void set_U3CNameU3Ek__BackingField_17(String_t* value)
	{
		___U3CNameU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_meshEventData_18() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9, ___meshEventData_18)); }
	inline MixedRealitySpatialAwarenessEventData_1_t00EC2B520F876AA2904782A9299DCBEB60209B8B * get_meshEventData_18() const { return ___meshEventData_18; }
	inline MixedRealitySpatialAwarenessEventData_1_t00EC2B520F876AA2904782A9299DCBEB60209B8B ** get_address_of_meshEventData_18() { return &___meshEventData_18; }
	inline void set_meshEventData_18(MixedRealitySpatialAwarenessEventData_1_t00EC2B520F876AA2904782A9299DCBEB60209B8B * value)
	{
		___meshEventData_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshEventData_18), (void*)value);
	}

	inline static int32_t get_offset_of_spatialAwarenessObjectParent_19() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9, ___spatialAwarenessObjectParent_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_spatialAwarenessObjectParent_19() const { return ___spatialAwarenessObjectParent_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_spatialAwarenessObjectParent_19() { return &___spatialAwarenessObjectParent_19; }
	inline void set_spatialAwarenessObjectParent_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___spatialAwarenessObjectParent_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spatialAwarenessObjectParent_19), (void*)value);
	}

	inline static int32_t get_offset_of_nextSourceId_20() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9, ___nextSourceId_20)); }
	inline uint32_t get_nextSourceId_20() const { return ___nextSourceId_20; }
	inline uint32_t* get_address_of_nextSourceId_20() { return &___nextSourceId_20; }
	inline void set_nextSourceId_20(uint32_t value)
	{
		___nextSourceId_20 = value;
	}

	inline static int32_t get_offset_of_spatialAwarenessSystemProfile_21() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9, ___spatialAwarenessSystemProfile_21)); }
	inline MixedRealitySpatialAwarenessSystemProfile_tC7E99BA5DC82D06919DE73FF542CAE4929C41D4B * get_spatialAwarenessSystemProfile_21() const { return ___spatialAwarenessSystemProfile_21; }
	inline MixedRealitySpatialAwarenessSystemProfile_tC7E99BA5DC82D06919DE73FF542CAE4929C41D4B ** get_address_of_spatialAwarenessSystemProfile_21() { return &___spatialAwarenessSystemProfile_21; }
	inline void set_spatialAwarenessSystemProfile_21(MixedRealitySpatialAwarenessSystemProfile_tC7E99BA5DC82D06919DE73FF542CAE4929C41D4B * value)
	{
		___spatialAwarenessSystemProfile_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spatialAwarenessSystemProfile_21), (void*)value);
	}
};

struct MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserversPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetObserversPerfMarker_22;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserversTPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetObserversTPerfMarker_23;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserverPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetObserverPerfMarker_24;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserverTPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetObserverTPerfMarker_25;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProvidersPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetDataProvidersPerfMarker_26;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProviderPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetDataProviderPerfMarker_27;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserversPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ResumeObserversPerfMarker_28;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserversTPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ResumeObserversTPerfMarker_29;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserverPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ResumeObserverPerfMarker_30;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserversPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___SuspendObserversPerfMarker_31;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserversTPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___SuspendObserversTPerfMarker_32;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserverPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___SuspendObserverPerfMarker_33;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservationsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ClearObservationsPerfMarker_34;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservationsTPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___ClearObservationsTPerfMarker_35;

public:
	inline static int32_t get_offset_of_GetObserversPerfMarker_22() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetObserversPerfMarker_22)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetObserversPerfMarker_22() const { return ___GetObserversPerfMarker_22; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetObserversPerfMarker_22() { return &___GetObserversPerfMarker_22; }
	inline void set_GetObserversPerfMarker_22(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetObserversPerfMarker_22 = value;
	}

	inline static int32_t get_offset_of_GetObserversTPerfMarker_23() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetObserversTPerfMarker_23)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetObserversTPerfMarker_23() const { return ___GetObserversTPerfMarker_23; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetObserversTPerfMarker_23() { return &___GetObserversTPerfMarker_23; }
	inline void set_GetObserversTPerfMarker_23(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetObserversTPerfMarker_23 = value;
	}

	inline static int32_t get_offset_of_GetObserverPerfMarker_24() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetObserverPerfMarker_24)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetObserverPerfMarker_24() const { return ___GetObserverPerfMarker_24; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetObserverPerfMarker_24() { return &___GetObserverPerfMarker_24; }
	inline void set_GetObserverPerfMarker_24(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetObserverPerfMarker_24 = value;
	}

	inline static int32_t get_offset_of_GetObserverTPerfMarker_25() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetObserverTPerfMarker_25)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetObserverTPerfMarker_25() const { return ___GetObserverTPerfMarker_25; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetObserverTPerfMarker_25() { return &___GetObserverTPerfMarker_25; }
	inline void set_GetObserverTPerfMarker_25(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetObserverTPerfMarker_25 = value;
	}

	inline static int32_t get_offset_of_GetDataProvidersPerfMarker_26() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetDataProvidersPerfMarker_26)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetDataProvidersPerfMarker_26() const { return ___GetDataProvidersPerfMarker_26; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetDataProvidersPerfMarker_26() { return &___GetDataProvidersPerfMarker_26; }
	inline void set_GetDataProvidersPerfMarker_26(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetDataProvidersPerfMarker_26 = value;
	}

	inline static int32_t get_offset_of_GetDataProviderPerfMarker_27() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___GetDataProviderPerfMarker_27)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetDataProviderPerfMarker_27() const { return ___GetDataProviderPerfMarker_27; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetDataProviderPerfMarker_27() { return &___GetDataProviderPerfMarker_27; }
	inline void set_GetDataProviderPerfMarker_27(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetDataProviderPerfMarker_27 = value;
	}

	inline static int32_t get_offset_of_ResumeObserversPerfMarker_28() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___ResumeObserversPerfMarker_28)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ResumeObserversPerfMarker_28() const { return ___ResumeObserversPerfMarker_28; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ResumeObserversPerfMarker_28() { return &___ResumeObserversPerfMarker_28; }
	inline void set_ResumeObserversPerfMarker_28(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ResumeObserversPerfMarker_28 = value;
	}

	inline static int32_t get_offset_of_ResumeObserversTPerfMarker_29() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___ResumeObserversTPerfMarker_29)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ResumeObserversTPerfMarker_29() const { return ___ResumeObserversTPerfMarker_29; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ResumeObserversTPerfMarker_29() { return &___ResumeObserversTPerfMarker_29; }
	inline void set_ResumeObserversTPerfMarker_29(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ResumeObserversTPerfMarker_29 = value;
	}

	inline static int32_t get_offset_of_ResumeObserverPerfMarker_30() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___ResumeObserverPerfMarker_30)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ResumeObserverPerfMarker_30() const { return ___ResumeObserverPerfMarker_30; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ResumeObserverPerfMarker_30() { return &___ResumeObserverPerfMarker_30; }
	inline void set_ResumeObserverPerfMarker_30(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ResumeObserverPerfMarker_30 = value;
	}

	inline static int32_t get_offset_of_SuspendObserversPerfMarker_31() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___SuspendObserversPerfMarker_31)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_SuspendObserversPerfMarker_31() const { return ___SuspendObserversPerfMarker_31; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_SuspendObserversPerfMarker_31() { return &___SuspendObserversPerfMarker_31; }
	inline void set_SuspendObserversPerfMarker_31(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___SuspendObserversPerfMarker_31 = value;
	}

	inline static int32_t get_offset_of_SuspendObserversTPerfMarker_32() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___SuspendObserversTPerfMarker_32)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_SuspendObserversTPerfMarker_32() const { return ___SuspendObserversTPerfMarker_32; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_SuspendObserversTPerfMarker_32() { return &___SuspendObserversTPerfMarker_32; }
	inline void set_SuspendObserversTPerfMarker_32(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___SuspendObserversTPerfMarker_32 = value;
	}

	inline static int32_t get_offset_of_SuspendObserverPerfMarker_33() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___SuspendObserverPerfMarker_33)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_SuspendObserverPerfMarker_33() const { return ___SuspendObserverPerfMarker_33; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_SuspendObserverPerfMarker_33() { return &___SuspendObserverPerfMarker_33; }
	inline void set_SuspendObserverPerfMarker_33(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___SuspendObserverPerfMarker_33 = value;
	}

	inline static int32_t get_offset_of_ClearObservationsPerfMarker_34() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___ClearObservationsPerfMarker_34)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ClearObservationsPerfMarker_34() const { return ___ClearObservationsPerfMarker_34; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ClearObservationsPerfMarker_34() { return &___ClearObservationsPerfMarker_34; }
	inline void set_ClearObservationsPerfMarker_34(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ClearObservationsPerfMarker_34 = value;
	}

	inline static int32_t get_offset_of_ClearObservationsTPerfMarker_35() { return static_cast<int32_t>(offsetof(MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields, ___ClearObservationsTPerfMarker_35)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_ClearObservationsTPerfMarker_35() const { return ___ClearObservationsTPerfMarker_35; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_ClearObservationsTPerfMarker_35() { return &___ClearObservationsTPerfMarker_35; }
	inline void set_ClearObservationsTPerfMarker_35(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___ClearObservationsTPerfMarker_35 = value;
	}
};


// ModelTargetsManager
struct  ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// ModelTargetsManager/ModelTargetMode ModelTargetsManager::modelTargetMode
	int32_t ___modelTargetMode_4;
	// System.Boolean ModelTargetsManager::autoActivate
	bool ___autoActivate_5;
	// UnityEngine.GameObject ModelTargetsManager::augmentation
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___augmentation_6;
	// Vuforia.ModelTargetBehaviour ModelTargetsManager::modelStandard
	ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * ___modelStandard_7;
	// Vuforia.ModelTargetBehaviour ModelTargetsManager::modelAdvanced
	ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * ___modelAdvanced_8;
	// System.String ModelTargetsManager::DataSetStandard
	String_t* ___DataSetStandard_9;
	// System.String ModelTargetsManager::DataSetAdvanced
	String_t* ___DataSetAdvanced_10;
	// UnityEngine.UI.Button ModelTargetsManager::guideViewsButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___guideViewsButton_11;
	// UnityEngine.CanvasGroup ModelTargetsManager::datasetsMenu
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___datasetsMenu_12;
	// Vuforia.ObjectTracker ModelTargetsManager::objectTracker
	ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * ___objectTracker_13;
	// Vuforia.StateManager ModelTargetsManager::stateManager
	StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * ___stateManager_14;
	// ModelTargetsUIManager ModelTargetsManager::modelTargetsUIManager
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * ___modelTargetsUIManager_15;
	// System.Collections.Generic.List`1<Vuforia.ModelTargetBehaviour> ModelTargetsManager::modelTargetBehaviours
	List_1_t40D79F96F2804A6662D8C123BA490604CF2D60E9 * ___modelTargetBehaviours_16;
	// System.String ModelTargetsManager::currentActiveDataSet
	String_t* ___currentActiveDataSet_17;

public:
	inline static int32_t get_offset_of_modelTargetMode_4() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___modelTargetMode_4)); }
	inline int32_t get_modelTargetMode_4() const { return ___modelTargetMode_4; }
	inline int32_t* get_address_of_modelTargetMode_4() { return &___modelTargetMode_4; }
	inline void set_modelTargetMode_4(int32_t value)
	{
		___modelTargetMode_4 = value;
	}

	inline static int32_t get_offset_of_autoActivate_5() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___autoActivate_5)); }
	inline bool get_autoActivate_5() const { return ___autoActivate_5; }
	inline bool* get_address_of_autoActivate_5() { return &___autoActivate_5; }
	inline void set_autoActivate_5(bool value)
	{
		___autoActivate_5 = value;
	}

	inline static int32_t get_offset_of_augmentation_6() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___augmentation_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_augmentation_6() const { return ___augmentation_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_augmentation_6() { return &___augmentation_6; }
	inline void set_augmentation_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___augmentation_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___augmentation_6), (void*)value);
	}

	inline static int32_t get_offset_of_modelStandard_7() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___modelStandard_7)); }
	inline ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * get_modelStandard_7() const { return ___modelStandard_7; }
	inline ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A ** get_address_of_modelStandard_7() { return &___modelStandard_7; }
	inline void set_modelStandard_7(ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * value)
	{
		___modelStandard_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelStandard_7), (void*)value);
	}

	inline static int32_t get_offset_of_modelAdvanced_8() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___modelAdvanced_8)); }
	inline ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * get_modelAdvanced_8() const { return ___modelAdvanced_8; }
	inline ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A ** get_address_of_modelAdvanced_8() { return &___modelAdvanced_8; }
	inline void set_modelAdvanced_8(ModelTargetBehaviour_tA8F75217DF4D79A9B9F8252678024ACED3C65C0A * value)
	{
		___modelAdvanced_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelAdvanced_8), (void*)value);
	}

	inline static int32_t get_offset_of_DataSetStandard_9() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___DataSetStandard_9)); }
	inline String_t* get_DataSetStandard_9() const { return ___DataSetStandard_9; }
	inline String_t** get_address_of_DataSetStandard_9() { return &___DataSetStandard_9; }
	inline void set_DataSetStandard_9(String_t* value)
	{
		___DataSetStandard_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DataSetStandard_9), (void*)value);
	}

	inline static int32_t get_offset_of_DataSetAdvanced_10() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___DataSetAdvanced_10)); }
	inline String_t* get_DataSetAdvanced_10() const { return ___DataSetAdvanced_10; }
	inline String_t** get_address_of_DataSetAdvanced_10() { return &___DataSetAdvanced_10; }
	inline void set_DataSetAdvanced_10(String_t* value)
	{
		___DataSetAdvanced_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DataSetAdvanced_10), (void*)value);
	}

	inline static int32_t get_offset_of_guideViewsButton_11() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___guideViewsButton_11)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_guideViewsButton_11() const { return ___guideViewsButton_11; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_guideViewsButton_11() { return &___guideViewsButton_11; }
	inline void set_guideViewsButton_11(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___guideViewsButton_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___guideViewsButton_11), (void*)value);
	}

	inline static int32_t get_offset_of_datasetsMenu_12() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___datasetsMenu_12)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_datasetsMenu_12() const { return ___datasetsMenu_12; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_datasetsMenu_12() { return &___datasetsMenu_12; }
	inline void set_datasetsMenu_12(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___datasetsMenu_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___datasetsMenu_12), (void*)value);
	}

	inline static int32_t get_offset_of_objectTracker_13() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___objectTracker_13)); }
	inline ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * get_objectTracker_13() const { return ___objectTracker_13; }
	inline ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 ** get_address_of_objectTracker_13() { return &___objectTracker_13; }
	inline void set_objectTracker_13(ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * value)
	{
		___objectTracker_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectTracker_13), (void*)value);
	}

	inline static int32_t get_offset_of_stateManager_14() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___stateManager_14)); }
	inline StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * get_stateManager_14() const { return ___stateManager_14; }
	inline StateManager_t1452558828440FBD9B4570C03085876DD02337E8 ** get_address_of_stateManager_14() { return &___stateManager_14; }
	inline void set_stateManager_14(StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * value)
	{
		___stateManager_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateManager_14), (void*)value);
	}

	inline static int32_t get_offset_of_modelTargetsUIManager_15() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___modelTargetsUIManager_15)); }
	inline ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * get_modelTargetsUIManager_15() const { return ___modelTargetsUIManager_15; }
	inline ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 ** get_address_of_modelTargetsUIManager_15() { return &___modelTargetsUIManager_15; }
	inline void set_modelTargetsUIManager_15(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169 * value)
	{
		___modelTargetsUIManager_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelTargetsUIManager_15), (void*)value);
	}

	inline static int32_t get_offset_of_modelTargetBehaviours_16() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___modelTargetBehaviours_16)); }
	inline List_1_t40D79F96F2804A6662D8C123BA490604CF2D60E9 * get_modelTargetBehaviours_16() const { return ___modelTargetBehaviours_16; }
	inline List_1_t40D79F96F2804A6662D8C123BA490604CF2D60E9 ** get_address_of_modelTargetBehaviours_16() { return &___modelTargetBehaviours_16; }
	inline void set_modelTargetBehaviours_16(List_1_t40D79F96F2804A6662D8C123BA490604CF2D60E9 * value)
	{
		___modelTargetBehaviours_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelTargetBehaviours_16), (void*)value);
	}

	inline static int32_t get_offset_of_currentActiveDataSet_17() { return static_cast<int32_t>(offsetof(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D, ___currentActiveDataSet_17)); }
	inline String_t* get_currentActiveDataSet_17() const { return ___currentActiveDataSet_17; }
	inline String_t** get_address_of_currentActiveDataSet_17() { return &___currentActiveDataSet_17; }
	inline void set_currentActiveDataSet_17(String_t* value)
	{
		___currentActiveDataSet_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentActiveDataSet_17), (void*)value);
	}
};


// ModelTargetsUIManager
struct  ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.CanvasGroup ModelTargetsUIManager::canvasGroupAdvanced
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___canvasGroupAdvanced_4;
	// System.Boolean ModelTargetsUIManager::cycleMultipleIcons
	bool ___cycleMultipleIcons_5;
	// UnityEngine.GameObject ModelTargetsUIManager::spatialMapping
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___spatialMapping_6;
	// UnityEngine.Color ModelTargetsUIManager::whiteTransparent
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___whiteTransparent_7;
	// UnityEngine.UI.Image[] ModelTargetsUIManager::imageSequence
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___imageSequence_8;
	// UnityEngine.UI.Image[] ModelTargetsUIManager::imagesAdvanced
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___imagesAdvanced_9;
	// System.Boolean ModelTargetsUIManager::uiEnabled
	bool ___uiEnabled_10;
	// System.Boolean ModelTargetsUIManager::imageSequencePaused
	bool ___imageSequencePaused_11;
	// System.Int32 ModelTargetsUIManager::imageSequenceIndex
	int32_t ___imageSequenceIndex_12;
	// System.Single ModelTargetsUIManager::clock
	float ___clock_13;
	// System.Single ModelTargetsUIManager::fadeMeter
	float ___fadeMeter_14;

public:
	inline static int32_t get_offset_of_canvasGroupAdvanced_4() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___canvasGroupAdvanced_4)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_canvasGroupAdvanced_4() const { return ___canvasGroupAdvanced_4; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_canvasGroupAdvanced_4() { return &___canvasGroupAdvanced_4; }
	inline void set_canvasGroupAdvanced_4(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___canvasGroupAdvanced_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasGroupAdvanced_4), (void*)value);
	}

	inline static int32_t get_offset_of_cycleMultipleIcons_5() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___cycleMultipleIcons_5)); }
	inline bool get_cycleMultipleIcons_5() const { return ___cycleMultipleIcons_5; }
	inline bool* get_address_of_cycleMultipleIcons_5() { return &___cycleMultipleIcons_5; }
	inline void set_cycleMultipleIcons_5(bool value)
	{
		___cycleMultipleIcons_5 = value;
	}

	inline static int32_t get_offset_of_spatialMapping_6() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___spatialMapping_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_spatialMapping_6() const { return ___spatialMapping_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_spatialMapping_6() { return &___spatialMapping_6; }
	inline void set_spatialMapping_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___spatialMapping_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spatialMapping_6), (void*)value);
	}

	inline static int32_t get_offset_of_whiteTransparent_7() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___whiteTransparent_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_whiteTransparent_7() const { return ___whiteTransparent_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_whiteTransparent_7() { return &___whiteTransparent_7; }
	inline void set_whiteTransparent_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___whiteTransparent_7 = value;
	}

	inline static int32_t get_offset_of_imageSequence_8() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___imageSequence_8)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_imageSequence_8() const { return ___imageSequence_8; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_imageSequence_8() { return &___imageSequence_8; }
	inline void set_imageSequence_8(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___imageSequence_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imageSequence_8), (void*)value);
	}

	inline static int32_t get_offset_of_imagesAdvanced_9() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___imagesAdvanced_9)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_imagesAdvanced_9() const { return ___imagesAdvanced_9; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_imagesAdvanced_9() { return &___imagesAdvanced_9; }
	inline void set_imagesAdvanced_9(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___imagesAdvanced_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imagesAdvanced_9), (void*)value);
	}

	inline static int32_t get_offset_of_uiEnabled_10() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___uiEnabled_10)); }
	inline bool get_uiEnabled_10() const { return ___uiEnabled_10; }
	inline bool* get_address_of_uiEnabled_10() { return &___uiEnabled_10; }
	inline void set_uiEnabled_10(bool value)
	{
		___uiEnabled_10 = value;
	}

	inline static int32_t get_offset_of_imageSequencePaused_11() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___imageSequencePaused_11)); }
	inline bool get_imageSequencePaused_11() const { return ___imageSequencePaused_11; }
	inline bool* get_address_of_imageSequencePaused_11() { return &___imageSequencePaused_11; }
	inline void set_imageSequencePaused_11(bool value)
	{
		___imageSequencePaused_11 = value;
	}

	inline static int32_t get_offset_of_imageSequenceIndex_12() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___imageSequenceIndex_12)); }
	inline int32_t get_imageSequenceIndex_12() const { return ___imageSequenceIndex_12; }
	inline int32_t* get_address_of_imageSequenceIndex_12() { return &___imageSequenceIndex_12; }
	inline void set_imageSequenceIndex_12(int32_t value)
	{
		___imageSequenceIndex_12 = value;
	}

	inline static int32_t get_offset_of_clock_13() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___clock_13)); }
	inline float get_clock_13() const { return ___clock_13; }
	inline float* get_address_of_clock_13() { return &___clock_13; }
	inline void set_clock_13(float value)
	{
		___clock_13 = value;
	}

	inline static int32_t get_offset_of_fadeMeter_14() { return static_cast<int32_t>(offsetof(ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169, ___fadeMeter_14)); }
	inline float get_fadeMeter_14() const { return ___fadeMeter_14; }
	inline float* get_address_of_fadeMeter_14() { return &___fadeMeter_14; }
	inline void set_fadeMeter_14(float value)
	{
		___fadeMeter_14 = value;
	}
};


// NetworkManager
struct  NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields
{
public:
	// NetworkManager NetworkManager::instance
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields, ___instance_4)); }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * get_instance_4() const { return ___instance_4; }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// ReadInput
struct  ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String ReadInput::input
	String_t* ___input_4;
	// UnityEngine.GameObject ReadInput::textInputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___textInputField_5;
	// UnityEngine.GameObject ReadInput::debugLog
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___debugLog_6;
	// UnityEngine.GameObject ReadInput::button
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___button_7;
	// NetworkManager ReadInput::networkManager
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * ___networkManager_8;
	// UnityEngine.GameObject ReadInput::Canvas
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Canvas_9;
	// UnityEngine.GameObject ReadInput::hologram
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hologram_10;

public:
	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___input_4)); }
	inline String_t* get_input_4() const { return ___input_4; }
	inline String_t** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(String_t* value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_4), (void*)value);
	}

	inline static int32_t get_offset_of_textInputField_5() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___textInputField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_textInputField_5() const { return ___textInputField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_textInputField_5() { return &___textInputField_5; }
	inline void set_textInputField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___textInputField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInputField_5), (void*)value);
	}

	inline static int32_t get_offset_of_debugLog_6() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___debugLog_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_debugLog_6() const { return ___debugLog_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_debugLog_6() { return &___debugLog_6; }
	inline void set_debugLog_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___debugLog_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugLog_6), (void*)value);
	}

	inline static int32_t get_offset_of_button_7() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___button_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_button_7() const { return ___button_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_button_7() { return &___button_7; }
	inline void set_button_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___button_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button_7), (void*)value);
	}

	inline static int32_t get_offset_of_networkManager_8() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___networkManager_8)); }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * get_networkManager_8() const { return ___networkManager_8; }
	inline NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 ** get_address_of_networkManager_8() { return &___networkManager_8; }
	inline void set_networkManager_8(NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606 * value)
	{
		___networkManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___networkManager_8), (void*)value);
	}

	inline static int32_t get_offset_of_Canvas_9() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___Canvas_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Canvas_9() const { return ___Canvas_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Canvas_9() { return &___Canvas_9; }
	inline void set_Canvas_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Canvas_9), (void*)value);
	}

	inline static int32_t get_offset_of_hologram_10() { return static_cast<int32_t>(offsetof(ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48, ___hologram_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hologram_10() const { return ___hologram_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hologram_10() { return &___hologram_10; }
	inline void set_hologram_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hologram_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hologram_10), (void*)value);
	}
};


// SampleUtil
struct  SampleUtil_tFD1A43414112EF85C38DAF0D3F96CE0F66ECD1AA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// SceneLoader
struct  SceneLoader_t2995AD1491223D32053837F27878AFE973D40D1F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 SceneLoader::SceneToLoad
	int32_t ___SceneToLoad_4;

public:
	inline static int32_t get_offset_of_SceneToLoad_4() { return static_cast<int32_t>(offsetof(SceneLoader_t2995AD1491223D32053837F27878AFE973D40D1F, ___SceneToLoad_4)); }
	inline int32_t get_SceneToLoad_4() const { return ___SceneToLoad_4; }
	inline int32_t* get_address_of_SceneToLoad_4() { return &___SceneToLoad_4; }
	inline void set_SceneToLoad_4(int32_t value)
	{
		___SceneToLoad_4 = value;
	}
};


// SetDrivenKey
struct  SetDrivenKey_t6EAB070660121AAD1B7245A25A08E95353094D0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform SetDrivenKey::watchSizeChanges
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___watchSizeChanges_4;
	// UnityEngine.RectTransform SetDrivenKey::readChangedSize
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___readChangedSize_5;

public:
	inline static int32_t get_offset_of_watchSizeChanges_4() { return static_cast<int32_t>(offsetof(SetDrivenKey_t6EAB070660121AAD1B7245A25A08E95353094D0E, ___watchSizeChanges_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_watchSizeChanges_4() const { return ___watchSizeChanges_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_watchSizeChanges_4() { return &___watchSizeChanges_4; }
	inline void set_watchSizeChanges_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___watchSizeChanges_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___watchSizeChanges_4), (void*)value);
	}

	inline static int32_t get_offset_of_readChangedSize_5() { return static_cast<int32_t>(offsetof(SetDrivenKey_t6EAB070660121AAD1B7245A25A08E95353094D0E, ___readChangedSize_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_readChangedSize_5() const { return ___readChangedSize_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_readChangedSize_5() { return &___readChangedSize_5; }
	inline void set_readChangedSize_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___readChangedSize_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___readChangedSize_5), (void*)value);
	}
};


// HoloToolkit.Unity.SimpleTagalong
struct  SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single HoloToolkit.Unity.SimpleTagalong::TagalongDistance
	float ___TagalongDistance_4;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::EnforceDistance
	bool ___EnforceDistance_5;
	// System.Single HoloToolkit.Unity.SimpleTagalong::PositionUpdateSpeed
	float ___PositionUpdateSpeed_6;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::SmoothMotion
	bool ___SmoothMotion_7;
	// System.Single HoloToolkit.Unity.SimpleTagalong::SmoothingFactor
	float ___SmoothingFactor_8;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SimpleTagalong::tagalongCollider
	BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * ___tagalongCollider_9;
	// HoloToolkit.Unity.Interpolator HoloToolkit.Unity.SimpleTagalong::interpolator
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9 * ___interpolator_10;
	// UnityEngine.Plane[] HoloToolkit.Unity.SimpleTagalong::frustumPlanes
	PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* ___frustumPlanes_11;

public:
	inline static int32_t get_offset_of_TagalongDistance_4() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___TagalongDistance_4)); }
	inline float get_TagalongDistance_4() const { return ___TagalongDistance_4; }
	inline float* get_address_of_TagalongDistance_4() { return &___TagalongDistance_4; }
	inline void set_TagalongDistance_4(float value)
	{
		___TagalongDistance_4 = value;
	}

	inline static int32_t get_offset_of_EnforceDistance_5() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___EnforceDistance_5)); }
	inline bool get_EnforceDistance_5() const { return ___EnforceDistance_5; }
	inline bool* get_address_of_EnforceDistance_5() { return &___EnforceDistance_5; }
	inline void set_EnforceDistance_5(bool value)
	{
		___EnforceDistance_5 = value;
	}

	inline static int32_t get_offset_of_PositionUpdateSpeed_6() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___PositionUpdateSpeed_6)); }
	inline float get_PositionUpdateSpeed_6() const { return ___PositionUpdateSpeed_6; }
	inline float* get_address_of_PositionUpdateSpeed_6() { return &___PositionUpdateSpeed_6; }
	inline void set_PositionUpdateSpeed_6(float value)
	{
		___PositionUpdateSpeed_6 = value;
	}

	inline static int32_t get_offset_of_SmoothMotion_7() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___SmoothMotion_7)); }
	inline bool get_SmoothMotion_7() const { return ___SmoothMotion_7; }
	inline bool* get_address_of_SmoothMotion_7() { return &___SmoothMotion_7; }
	inline void set_SmoothMotion_7(bool value)
	{
		___SmoothMotion_7 = value;
	}

	inline static int32_t get_offset_of_SmoothingFactor_8() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___SmoothingFactor_8)); }
	inline float get_SmoothingFactor_8() const { return ___SmoothingFactor_8; }
	inline float* get_address_of_SmoothingFactor_8() { return &___SmoothingFactor_8; }
	inline void set_SmoothingFactor_8(float value)
	{
		___SmoothingFactor_8 = value;
	}

	inline static int32_t get_offset_of_tagalongCollider_9() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___tagalongCollider_9)); }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * get_tagalongCollider_9() const { return ___tagalongCollider_9; }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 ** get_address_of_tagalongCollider_9() { return &___tagalongCollider_9; }
	inline void set_tagalongCollider_9(BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * value)
	{
		___tagalongCollider_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tagalongCollider_9), (void*)value);
	}

	inline static int32_t get_offset_of_interpolator_10() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___interpolator_10)); }
	inline Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9 * get_interpolator_10() const { return ___interpolator_10; }
	inline Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9 ** get_address_of_interpolator_10() { return &___interpolator_10; }
	inline void set_interpolator_10(Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9 * value)
	{
		___interpolator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_10), (void*)value);
	}

	inline static int32_t get_offset_of_frustumPlanes_11() { return static_cast<int32_t>(offsetof(SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C, ___frustumPlanes_11)); }
	inline PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* get_frustumPlanes_11() const { return ___frustumPlanes_11; }
	inline PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD** get_address_of_frustumPlanes_11() { return &___frustumPlanes_11; }
	inline void set_frustumPlanes_11(PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* value)
	{
		___frustumPlanes_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frustumPlanes_11), (void*)value);
	}
};


// DimBoxes.TestDynamicBehaviour
struct  TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 DimBoxes.TestDynamicBehaviour::dynScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dynScale_4;
	// System.Single DimBoxes.TestDynamicBehaviour::t
	float ___t_5;
	// UnityEngine.Vector3 DimBoxes.TestDynamicBehaviour::angles
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___angles_6;
	// UnityEngine.Vector3 DimBoxes.TestDynamicBehaviour::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_7;
	// System.Boolean DimBoxes.TestDynamicBehaviour::run
	bool ___run_8;
	// System.Single DimBoxes.TestDynamicBehaviour::speed
	float ___speed_9;

public:
	inline static int32_t get_offset_of_dynScale_4() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___dynScale_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_dynScale_4() const { return ___dynScale_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_dynScale_4() { return &___dynScale_4; }
	inline void set_dynScale_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___dynScale_4 = value;
	}

	inline static int32_t get_offset_of_t_5() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___t_5)); }
	inline float get_t_5() const { return ___t_5; }
	inline float* get_address_of_t_5() { return &___t_5; }
	inline void set_t_5(float value)
	{
		___t_5 = value;
	}

	inline static int32_t get_offset_of_angles_6() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___angles_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_angles_6() const { return ___angles_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_angles_6() { return &___angles_6; }
	inline void set_angles_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___angles_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___scale_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_7() const { return ___scale_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_run_8() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___run_8)); }
	inline bool get_run_8() const { return ___run_8; }
	inline bool* get_address_of_run_8() { return &___run_8; }
	inline void set_run_8(bool value)
	{
		___run_8 = value;
	}

	inline static int32_t get_offset_of_speed_9() { return static_cast<int32_t>(offsetof(TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309, ___speed_9)); }
	inline float get_speed_9() const { return ___speed_9; }
	inline float* get_address_of_speed_9() { return &___speed_9; }
	inline void set_speed_9(float value)
	{
		___speed_9 = value;
	}
};


// UnityThread
struct  UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionCopiedQueueUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionCopiedQueueUpdateFunc_6;
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionCopiedQueueLateUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionCopiedQueueLateUpdateFunc_9;
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionCopiedQueueFixedUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionCopiedQueueFixedUpdateFunc_12;

public:
	inline static int32_t get_offset_of_actionCopiedQueueUpdateFunc_6() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A, ___actionCopiedQueueUpdateFunc_6)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionCopiedQueueUpdateFunc_6() const { return ___actionCopiedQueueUpdateFunc_6; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionCopiedQueueUpdateFunc_6() { return &___actionCopiedQueueUpdateFunc_6; }
	inline void set_actionCopiedQueueUpdateFunc_6(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionCopiedQueueUpdateFunc_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionCopiedQueueUpdateFunc_6), (void*)value);
	}

	inline static int32_t get_offset_of_actionCopiedQueueLateUpdateFunc_9() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A, ___actionCopiedQueueLateUpdateFunc_9)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionCopiedQueueLateUpdateFunc_9() const { return ___actionCopiedQueueLateUpdateFunc_9; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionCopiedQueueLateUpdateFunc_9() { return &___actionCopiedQueueLateUpdateFunc_9; }
	inline void set_actionCopiedQueueLateUpdateFunc_9(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionCopiedQueueLateUpdateFunc_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionCopiedQueueLateUpdateFunc_9), (void*)value);
	}

	inline static int32_t get_offset_of_actionCopiedQueueFixedUpdateFunc_12() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A, ___actionCopiedQueueFixedUpdateFunc_12)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionCopiedQueueFixedUpdateFunc_12() const { return ___actionCopiedQueueFixedUpdateFunc_12; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionCopiedQueueFixedUpdateFunc_12() { return &___actionCopiedQueueFixedUpdateFunc_12; }
	inline void set_actionCopiedQueueFixedUpdateFunc_12(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionCopiedQueueFixedUpdateFunc_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionCopiedQueueFixedUpdateFunc_12), (void*)value);
	}
};

struct UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields
{
public:
	// UnityThread UnityThread::instance
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A * ___instance_4;
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionQueuesUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionQueuesUpdateFunc_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityThread::noActionQueueToExecuteUpdateFunc
	bool ___noActionQueueToExecuteUpdateFunc_7;
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionQueuesLateUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionQueuesLateUpdateFunc_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityThread::noActionQueueToExecuteLateUpdateFunc
	bool ___noActionQueueToExecuteLateUpdateFunc_10;
	// System.Collections.Generic.List`1<System.Action> UnityThread::actionQueuesFixedUpdateFunc
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___actionQueuesFixedUpdateFunc_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityThread::noActionQueueToExecuteFixedUpdateFunc
	bool ___noActionQueueToExecuteFixedUpdateFunc_13;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___instance_4)); }
	inline UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A * get_instance_4() const { return ___instance_4; }
	inline UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_actionQueuesUpdateFunc_5() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___actionQueuesUpdateFunc_5)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionQueuesUpdateFunc_5() const { return ___actionQueuesUpdateFunc_5; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionQueuesUpdateFunc_5() { return &___actionQueuesUpdateFunc_5; }
	inline void set_actionQueuesUpdateFunc_5(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionQueuesUpdateFunc_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionQueuesUpdateFunc_5), (void*)value);
	}

	inline static int32_t get_offset_of_noActionQueueToExecuteUpdateFunc_7() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___noActionQueueToExecuteUpdateFunc_7)); }
	inline bool get_noActionQueueToExecuteUpdateFunc_7() const { return ___noActionQueueToExecuteUpdateFunc_7; }
	inline bool* get_address_of_noActionQueueToExecuteUpdateFunc_7() { return &___noActionQueueToExecuteUpdateFunc_7; }
	inline void set_noActionQueueToExecuteUpdateFunc_7(bool value)
	{
		___noActionQueueToExecuteUpdateFunc_7 = value;
	}

	inline static int32_t get_offset_of_actionQueuesLateUpdateFunc_8() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___actionQueuesLateUpdateFunc_8)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionQueuesLateUpdateFunc_8() const { return ___actionQueuesLateUpdateFunc_8; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionQueuesLateUpdateFunc_8() { return &___actionQueuesLateUpdateFunc_8; }
	inline void set_actionQueuesLateUpdateFunc_8(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionQueuesLateUpdateFunc_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionQueuesLateUpdateFunc_8), (void*)value);
	}

	inline static int32_t get_offset_of_noActionQueueToExecuteLateUpdateFunc_10() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___noActionQueueToExecuteLateUpdateFunc_10)); }
	inline bool get_noActionQueueToExecuteLateUpdateFunc_10() const { return ___noActionQueueToExecuteLateUpdateFunc_10; }
	inline bool* get_address_of_noActionQueueToExecuteLateUpdateFunc_10() { return &___noActionQueueToExecuteLateUpdateFunc_10; }
	inline void set_noActionQueueToExecuteLateUpdateFunc_10(bool value)
	{
		___noActionQueueToExecuteLateUpdateFunc_10 = value;
	}

	inline static int32_t get_offset_of_actionQueuesFixedUpdateFunc_11() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___actionQueuesFixedUpdateFunc_11)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_actionQueuesFixedUpdateFunc_11() const { return ___actionQueuesFixedUpdateFunc_11; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_actionQueuesFixedUpdateFunc_11() { return &___actionQueuesFixedUpdateFunc_11; }
	inline void set_actionQueuesFixedUpdateFunc_11(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___actionQueuesFixedUpdateFunc_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionQueuesFixedUpdateFunc_11), (void*)value);
	}

	inline static int32_t get_offset_of_noActionQueueToExecuteFixedUpdateFunc_13() { return static_cast<int32_t>(offsetof(UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields, ___noActionQueueToExecuteFixedUpdateFunc_13)); }
	inline bool get_noActionQueueToExecuteFixedUpdateFunc_13() const { return ___noActionQueueToExecuteFixedUpdateFunc_13; }
	inline bool* get_address_of_noActionQueueToExecuteFixedUpdateFunc_13() { return &___noActionQueueToExecuteFixedUpdateFunc_13; }
	inline void set_noActionQueueToExecuteFixedUpdateFunc_13(bool value)
	{
		___noActionQueueToExecuteFixedUpdateFunc_13 = value;
	}
};


// UpdateObject
struct  UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject UpdateObject::hologram
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hologram_4;
	// System.Single UpdateObject::timeUpdate
	float ___timeUpdate_5;
	// Manipulation UpdateObject::currentState
	int32_t ___currentState_6;
	// UnityEngine.Color UpdateObject::myColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___myColor_7;

public:
	inline static int32_t get_offset_of_hologram_4() { return static_cast<int32_t>(offsetof(UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B, ___hologram_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hologram_4() const { return ___hologram_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hologram_4() { return &___hologram_4; }
	inline void set_hologram_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hologram_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hologram_4), (void*)value);
	}

	inline static int32_t get_offset_of_timeUpdate_5() { return static_cast<int32_t>(offsetof(UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B, ___timeUpdate_5)); }
	inline float get_timeUpdate_5() const { return ___timeUpdate_5; }
	inline float* get_address_of_timeUpdate_5() { return &___timeUpdate_5; }
	inline void set_timeUpdate_5(float value)
	{
		___timeUpdate_5 = value;
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B, ___currentState_6)); }
	inline int32_t get_currentState_6() const { return ___currentState_6; }
	inline int32_t* get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(int32_t value)
	{
		___currentState_6 = value;
	}

	inline static int32_t get_offset_of_myColor_7() { return static_cast<int32_t>(offsetof(UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B, ___myColor_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_myColor_7() const { return ___myColor_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_myColor_7() { return &___myColor_7; }
	inline void set_myColor_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___myColor_7 = value;
	}
};


// Versions
struct  Versions_t30D650E74E89F9A948D971C05EEA8FB39E3D5B61  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// VoiceCommands
struct  VoiceCommands_tB74343302DA5B4B2BC141581E320F6542E1B7723  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// VuMarkHandler
struct  VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Vuforia.VuMarkManager VuMarkHandler::vumarkManager
	VuMarkManager_t3E90D7E4ABBD0C9AAC4022C35D5FAB2F78770B72 * ___vumarkManager_5;
	// UnityEngine.LineRenderer VuMarkHandler::lineRenderer
	LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * ___lineRenderer_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> VuMarkHandler::vumarkInstanceTextures
	Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * ___vumarkInstanceTextures_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> VuMarkHandler::vumarkAugmentationObjects
	Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * ___vumarkAugmentationObjects_8;
	// VuMarkTrackableStatusUI VuMarkHandler::nearestVuMarkScreenPanel
	VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615 * ___nearestVuMarkScreenPanel_9;
	// Vuforia.VuMarkTarget VuMarkHandler::closestVuMark
	RuntimeObject* ___closestVuMark_10;
	// Vuforia.VuMarkTarget VuMarkHandler::currentVuMark
	RuntimeObject* ___currentVuMark_11;
	// UnityEngine.Camera VuMarkHandler::vuforiaCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___vuforiaCamera_12;
	// VuMarkHandler/AugmentationObject[] VuMarkHandler::augmentationObjects
	AugmentationObjectU5BU5D_t1BC7E9AF1E891CACD8C3D092105858257FDE482D* ___augmentationObjects_13;

public:
	inline static int32_t get_offset_of_vumarkManager_5() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___vumarkManager_5)); }
	inline VuMarkManager_t3E90D7E4ABBD0C9AAC4022C35D5FAB2F78770B72 * get_vumarkManager_5() const { return ___vumarkManager_5; }
	inline VuMarkManager_t3E90D7E4ABBD0C9AAC4022C35D5FAB2F78770B72 ** get_address_of_vumarkManager_5() { return &___vumarkManager_5; }
	inline void set_vumarkManager_5(VuMarkManager_t3E90D7E4ABBD0C9AAC4022C35D5FAB2F78770B72 * value)
	{
		___vumarkManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkManager_5), (void*)value);
	}

	inline static int32_t get_offset_of_lineRenderer_6() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___lineRenderer_6)); }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * get_lineRenderer_6() const { return ___lineRenderer_6; }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 ** get_address_of_lineRenderer_6() { return &___lineRenderer_6; }
	inline void set_lineRenderer_6(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * value)
	{
		___lineRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of_vumarkInstanceTextures_7() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___vumarkInstanceTextures_7)); }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * get_vumarkInstanceTextures_7() const { return ___vumarkInstanceTextures_7; }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C ** get_address_of_vumarkInstanceTextures_7() { return &___vumarkInstanceTextures_7; }
	inline void set_vumarkInstanceTextures_7(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * value)
	{
		___vumarkInstanceTextures_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkInstanceTextures_7), (void*)value);
	}

	inline static int32_t get_offset_of_vumarkAugmentationObjects_8() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___vumarkAugmentationObjects_8)); }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * get_vumarkAugmentationObjects_8() const { return ___vumarkAugmentationObjects_8; }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C ** get_address_of_vumarkAugmentationObjects_8() { return &___vumarkAugmentationObjects_8; }
	inline void set_vumarkAugmentationObjects_8(Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * value)
	{
		___vumarkAugmentationObjects_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkAugmentationObjects_8), (void*)value);
	}

	inline static int32_t get_offset_of_nearestVuMarkScreenPanel_9() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___nearestVuMarkScreenPanel_9)); }
	inline VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615 * get_nearestVuMarkScreenPanel_9() const { return ___nearestVuMarkScreenPanel_9; }
	inline VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615 ** get_address_of_nearestVuMarkScreenPanel_9() { return &___nearestVuMarkScreenPanel_9; }
	inline void set_nearestVuMarkScreenPanel_9(VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615 * value)
	{
		___nearestVuMarkScreenPanel_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nearestVuMarkScreenPanel_9), (void*)value);
	}

	inline static int32_t get_offset_of_closestVuMark_10() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___closestVuMark_10)); }
	inline RuntimeObject* get_closestVuMark_10() const { return ___closestVuMark_10; }
	inline RuntimeObject** get_address_of_closestVuMark_10() { return &___closestVuMark_10; }
	inline void set_closestVuMark_10(RuntimeObject* value)
	{
		___closestVuMark_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___closestVuMark_10), (void*)value);
	}

	inline static int32_t get_offset_of_currentVuMark_11() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___currentVuMark_11)); }
	inline RuntimeObject* get_currentVuMark_11() const { return ___currentVuMark_11; }
	inline RuntimeObject** get_address_of_currentVuMark_11() { return &___currentVuMark_11; }
	inline void set_currentVuMark_11(RuntimeObject* value)
	{
		___currentVuMark_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentVuMark_11), (void*)value);
	}

	inline static int32_t get_offset_of_vuforiaCamera_12() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___vuforiaCamera_12)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_vuforiaCamera_12() const { return ___vuforiaCamera_12; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_vuforiaCamera_12() { return &___vuforiaCamera_12; }
	inline void set_vuforiaCamera_12(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___vuforiaCamera_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vuforiaCamera_12), (void*)value);
	}

	inline static int32_t get_offset_of_augmentationObjects_13() { return static_cast<int32_t>(offsetof(VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1, ___augmentationObjects_13)); }
	inline AugmentationObjectU5BU5D_t1BC7E9AF1E891CACD8C3D092105858257FDE482D* get_augmentationObjects_13() const { return ___augmentationObjects_13; }
	inline AugmentationObjectU5BU5D_t1BC7E9AF1E891CACD8C3D092105858257FDE482D** get_address_of_augmentationObjects_13() { return &___augmentationObjects_13; }
	inline void set_augmentationObjects_13(AugmentationObjectU5BU5D_t1BC7E9AF1E891CACD8C3D092105858257FDE482D* value)
	{
		___augmentationObjects_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___augmentationObjects_13), (void*)value);
	}
};


// VuMarkTrackableStatusUI
struct  VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image VuMarkTrackableStatusUI::m_Image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_Image_4;
	// UnityEngine.UI.Text VuMarkTrackableStatusUI::m_Info
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Info_5;

public:
	inline static int32_t get_offset_of_m_Image_4() { return static_cast<int32_t>(offsetof(VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615, ___m_Image_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_Image_4() const { return ___m_Image_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_Image_4() { return &___m_Image_4; }
	inline void set_m_Image_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_Image_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Image_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Info_5() { return static_cast<int32_t>(offsetof(VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615, ___m_Info_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Info_5() const { return ___m_Info_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Info_5() { return &___m_Info_5; }
	inline void set_m_Info_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Info_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_5), (void*)value);
	}
};


// VuforiaAdditiveSceneLoader
struct  VuforiaAdditiveSceneLoader_t3ECB26104E71BD79DDBF3521D1B2FA5BC4585CB2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String[] VuforiaAdditiveSceneLoader::DataSetsToActivate
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___DataSetsToActivate_4;
	// System.Action VuforiaAdditiveSceneLoader::mOnVuforiaStarted
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___mOnVuforiaStarted_5;

public:
	inline static int32_t get_offset_of_DataSetsToActivate_4() { return static_cast<int32_t>(offsetof(VuforiaAdditiveSceneLoader_t3ECB26104E71BD79DDBF3521D1B2FA5BC4585CB2, ___DataSetsToActivate_4)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_DataSetsToActivate_4() const { return ___DataSetsToActivate_4; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_DataSetsToActivate_4() { return &___DataSetsToActivate_4; }
	inline void set_DataSetsToActivate_4(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___DataSetsToActivate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DataSetsToActivate_4), (void*)value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_5() { return static_cast<int32_t>(offsetof(VuforiaAdditiveSceneLoader_t3ECB26104E71BD79DDBF3521D1B2FA5BC4585CB2, ___mOnVuforiaStarted_5)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_mOnVuforiaStarted_5() const { return ___mOnVuforiaStarted_5; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_mOnVuforiaStarted_5() { return &___mOnVuforiaStarted_5; }
	inline void set_mOnVuforiaStarted_5(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___mOnVuforiaStarted_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mOnVuforiaStarted_5), (void*)value);
	}
};


// VuforiaStateInfo
struct  VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject VuforiaStateInfo::textObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___textObject_4;
	// System.String VuforiaStateInfo::activeDataSetsTitle
	String_t* ___activeDataSetsTitle_5;
	// System.String VuforiaStateInfo::activeDataSetsInfo
	String_t* ___activeDataSetsInfo_6;
	// System.String VuforiaStateInfo::trackableStateInfo
	String_t* ___trackableStateInfo_7;
	// System.String VuforiaStateInfo::vumarkTrackableStateInfo
	String_t* ___vumarkTrackableStateInfo_8;
	// Vuforia.ObjectTracker VuforiaStateInfo::objectTracker
	ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * ___objectTracker_9;
	// Vuforia.StateManager VuforiaStateInfo::stateManager
	StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * ___stateManager_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> VuforiaStateInfo::trackablesDictionary
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___trackablesDictionary_11;

public:
	inline static int32_t get_offset_of_textObject_4() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___textObject_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_textObject_4() const { return ___textObject_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_textObject_4() { return &___textObject_4; }
	inline void set_textObject_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___textObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_activeDataSetsTitle_5() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___activeDataSetsTitle_5)); }
	inline String_t* get_activeDataSetsTitle_5() const { return ___activeDataSetsTitle_5; }
	inline String_t** get_address_of_activeDataSetsTitle_5() { return &___activeDataSetsTitle_5; }
	inline void set_activeDataSetsTitle_5(String_t* value)
	{
		___activeDataSetsTitle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeDataSetsTitle_5), (void*)value);
	}

	inline static int32_t get_offset_of_activeDataSetsInfo_6() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___activeDataSetsInfo_6)); }
	inline String_t* get_activeDataSetsInfo_6() const { return ___activeDataSetsInfo_6; }
	inline String_t** get_address_of_activeDataSetsInfo_6() { return &___activeDataSetsInfo_6; }
	inline void set_activeDataSetsInfo_6(String_t* value)
	{
		___activeDataSetsInfo_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeDataSetsInfo_6), (void*)value);
	}

	inline static int32_t get_offset_of_trackableStateInfo_7() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___trackableStateInfo_7)); }
	inline String_t* get_trackableStateInfo_7() const { return ___trackableStateInfo_7; }
	inline String_t** get_address_of_trackableStateInfo_7() { return &___trackableStateInfo_7; }
	inline void set_trackableStateInfo_7(String_t* value)
	{
		___trackableStateInfo_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackableStateInfo_7), (void*)value);
	}

	inline static int32_t get_offset_of_vumarkTrackableStateInfo_8() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___vumarkTrackableStateInfo_8)); }
	inline String_t* get_vumarkTrackableStateInfo_8() const { return ___vumarkTrackableStateInfo_8; }
	inline String_t** get_address_of_vumarkTrackableStateInfo_8() { return &___vumarkTrackableStateInfo_8; }
	inline void set_vumarkTrackableStateInfo_8(String_t* value)
	{
		___vumarkTrackableStateInfo_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkTrackableStateInfo_8), (void*)value);
	}

	inline static int32_t get_offset_of_objectTracker_9() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___objectTracker_9)); }
	inline ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * get_objectTracker_9() const { return ___objectTracker_9; }
	inline ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 ** get_address_of_objectTracker_9() { return &___objectTracker_9; }
	inline void set_objectTracker_9(ObjectTracker_tE63FB2F6FB1501BB0F6AE09AD0F5F2BD1E8DBD92 * value)
	{
		___objectTracker_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectTracker_9), (void*)value);
	}

	inline static int32_t get_offset_of_stateManager_10() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___stateManager_10)); }
	inline StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * get_stateManager_10() const { return ___stateManager_10; }
	inline StateManager_t1452558828440FBD9B4570C03085876DD02337E8 ** get_address_of_stateManager_10() { return &___stateManager_10; }
	inline void set_stateManager_10(StateManager_t1452558828440FBD9B4570C03085876DD02337E8 * value)
	{
		___stateManager_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateManager_10), (void*)value);
	}

	inline static int32_t get_offset_of_trackablesDictionary_11() { return static_cast<int32_t>(offsetof(VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC, ___trackablesDictionary_11)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_trackablesDictionary_11() const { return ___trackablesDictionary_11; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_trackablesDictionary_11() { return &___trackablesDictionary_11; }
	inline void set_trackablesDictionary_11(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___trackablesDictionary_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackablesDictionary_11), (void*)value);
	}
};


// DimBoxes.maxCamera
struct  maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform DimBoxes.maxCamera::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_4;
	// UnityEngine.GameObject DimBoxes.maxCamera::terrainMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___terrainMesh_5;
	// UnityEngine.Vector3 DimBoxes.maxCamera::targetOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetOffset_6;
	// System.Single DimBoxes.maxCamera::distance
	float ___distance_7;
	// System.Single DimBoxes.maxCamera::maxDistance
	float ___maxDistance_8;
	// System.Single DimBoxes.maxCamera::minDistance
	float ___minDistance_9;
	// System.Single DimBoxes.maxCamera::xSpeed
	float ___xSpeed_10;
	// System.Single DimBoxes.maxCamera::ySpeed
	float ___ySpeed_11;
	// System.Single DimBoxes.maxCamera::aboveYmin
	float ___aboveYmin_12;
	// System.Single DimBoxes.maxCamera::yMaxLimit
	float ___yMaxLimit_13;
	// System.Single DimBoxes.maxCamera::zoomRate
	float ___zoomRate_14;
	// System.Single DimBoxes.maxCamera::panSpeed
	float ___panSpeed_15;
	// System.Single DimBoxes.maxCamera::zoomDampening
	float ___zoomDampening_16;
	// System.Single DimBoxes.maxCamera::xDeg
	float ___xDeg_17;
	// System.Single DimBoxes.maxCamera::yDeg
	float ___yDeg_18;
	// System.Single DimBoxes.maxCamera::currentDistance
	float ___currentDistance_19;
	// System.Single DimBoxes.maxCamera::desiredDistance
	float ___desiredDistance_20;
	// UnityEngine.Quaternion DimBoxes.maxCamera::currentRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___currentRotation_21;
	// UnityEngine.Quaternion DimBoxes.maxCamera::desiredRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___desiredRotation_22;
	// UnityEngine.Quaternion DimBoxes.maxCamera::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_23;
	// UnityEngine.Vector3 DimBoxes.maxCamera::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_24;
	// System.Single DimBoxes.maxCamera::yMinLimit
	float ___yMinLimit_25;
	// System.Single DimBoxes.maxCamera::viewerYmin
	float ___viewerYmin_26;
	// UnityEngine.Vector2 DimBoxes.maxCamera::desiredInputPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___desiredInputPosition_27;
	// UnityEngine.Vector2 DimBoxes.maxCamera::currentInputPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___currentInputPosition_28;
	// UnityEngine.Vector3 DimBoxes.maxCamera::currentTargetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentTargetPosition_29;
	// UnityEngine.Vector3 DimBoxes.maxCamera::hitPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___hitPoint_30;
	// System.Boolean DimBoxes.maxCamera::dragging
	bool ___dragging_31;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___target_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_4() const { return ___target_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_terrainMesh_5() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___terrainMesh_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_terrainMesh_5() const { return ___terrainMesh_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_terrainMesh_5() { return &___terrainMesh_5; }
	inline void set_terrainMesh_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___terrainMesh_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___terrainMesh_5), (void*)value);
	}

	inline static int32_t get_offset_of_targetOffset_6() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___targetOffset_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetOffset_6() const { return ___targetOffset_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetOffset_6() { return &___targetOffset_6; }
	inline void set_targetOffset_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetOffset_6 = value;
	}

	inline static int32_t get_offset_of_distance_7() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___distance_7)); }
	inline float get_distance_7() const { return ___distance_7; }
	inline float* get_address_of_distance_7() { return &___distance_7; }
	inline void set_distance_7(float value)
	{
		___distance_7 = value;
	}

	inline static int32_t get_offset_of_maxDistance_8() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___maxDistance_8)); }
	inline float get_maxDistance_8() const { return ___maxDistance_8; }
	inline float* get_address_of_maxDistance_8() { return &___maxDistance_8; }
	inline void set_maxDistance_8(float value)
	{
		___maxDistance_8 = value;
	}

	inline static int32_t get_offset_of_minDistance_9() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___minDistance_9)); }
	inline float get_minDistance_9() const { return ___minDistance_9; }
	inline float* get_address_of_minDistance_9() { return &___minDistance_9; }
	inline void set_minDistance_9(float value)
	{
		___minDistance_9 = value;
	}

	inline static int32_t get_offset_of_xSpeed_10() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___xSpeed_10)); }
	inline float get_xSpeed_10() const { return ___xSpeed_10; }
	inline float* get_address_of_xSpeed_10() { return &___xSpeed_10; }
	inline void set_xSpeed_10(float value)
	{
		___xSpeed_10 = value;
	}

	inline static int32_t get_offset_of_ySpeed_11() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___ySpeed_11)); }
	inline float get_ySpeed_11() const { return ___ySpeed_11; }
	inline float* get_address_of_ySpeed_11() { return &___ySpeed_11; }
	inline void set_ySpeed_11(float value)
	{
		___ySpeed_11 = value;
	}

	inline static int32_t get_offset_of_aboveYmin_12() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___aboveYmin_12)); }
	inline float get_aboveYmin_12() const { return ___aboveYmin_12; }
	inline float* get_address_of_aboveYmin_12() { return &___aboveYmin_12; }
	inline void set_aboveYmin_12(float value)
	{
		___aboveYmin_12 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_13() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___yMaxLimit_13)); }
	inline float get_yMaxLimit_13() const { return ___yMaxLimit_13; }
	inline float* get_address_of_yMaxLimit_13() { return &___yMaxLimit_13; }
	inline void set_yMaxLimit_13(float value)
	{
		___yMaxLimit_13 = value;
	}

	inline static int32_t get_offset_of_zoomRate_14() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___zoomRate_14)); }
	inline float get_zoomRate_14() const { return ___zoomRate_14; }
	inline float* get_address_of_zoomRate_14() { return &___zoomRate_14; }
	inline void set_zoomRate_14(float value)
	{
		___zoomRate_14 = value;
	}

	inline static int32_t get_offset_of_panSpeed_15() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___panSpeed_15)); }
	inline float get_panSpeed_15() const { return ___panSpeed_15; }
	inline float* get_address_of_panSpeed_15() { return &___panSpeed_15; }
	inline void set_panSpeed_15(float value)
	{
		___panSpeed_15 = value;
	}

	inline static int32_t get_offset_of_zoomDampening_16() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___zoomDampening_16)); }
	inline float get_zoomDampening_16() const { return ___zoomDampening_16; }
	inline float* get_address_of_zoomDampening_16() { return &___zoomDampening_16; }
	inline void set_zoomDampening_16(float value)
	{
		___zoomDampening_16 = value;
	}

	inline static int32_t get_offset_of_xDeg_17() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___xDeg_17)); }
	inline float get_xDeg_17() const { return ___xDeg_17; }
	inline float* get_address_of_xDeg_17() { return &___xDeg_17; }
	inline void set_xDeg_17(float value)
	{
		___xDeg_17 = value;
	}

	inline static int32_t get_offset_of_yDeg_18() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___yDeg_18)); }
	inline float get_yDeg_18() const { return ___yDeg_18; }
	inline float* get_address_of_yDeg_18() { return &___yDeg_18; }
	inline void set_yDeg_18(float value)
	{
		___yDeg_18 = value;
	}

	inline static int32_t get_offset_of_currentDistance_19() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___currentDistance_19)); }
	inline float get_currentDistance_19() const { return ___currentDistance_19; }
	inline float* get_address_of_currentDistance_19() { return &___currentDistance_19; }
	inline void set_currentDistance_19(float value)
	{
		___currentDistance_19 = value;
	}

	inline static int32_t get_offset_of_desiredDistance_20() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___desiredDistance_20)); }
	inline float get_desiredDistance_20() const { return ___desiredDistance_20; }
	inline float* get_address_of_desiredDistance_20() { return &___desiredDistance_20; }
	inline void set_desiredDistance_20(float value)
	{
		___desiredDistance_20 = value;
	}

	inline static int32_t get_offset_of_currentRotation_21() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___currentRotation_21)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_currentRotation_21() const { return ___currentRotation_21; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_currentRotation_21() { return &___currentRotation_21; }
	inline void set_currentRotation_21(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___currentRotation_21 = value;
	}

	inline static int32_t get_offset_of_desiredRotation_22() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___desiredRotation_22)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_desiredRotation_22() const { return ___desiredRotation_22; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_desiredRotation_22() { return &___desiredRotation_22; }
	inline void set_desiredRotation_22(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___desiredRotation_22 = value;
	}

	inline static int32_t get_offset_of_rotation_23() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___rotation_23)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_23() const { return ___rotation_23; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_23() { return &___rotation_23; }
	inline void set_rotation_23(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_23 = value;
	}

	inline static int32_t get_offset_of_position_24() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___position_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_24() const { return ___position_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_24() { return &___position_24; }
	inline void set_position_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_24 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_25() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___yMinLimit_25)); }
	inline float get_yMinLimit_25() const { return ___yMinLimit_25; }
	inline float* get_address_of_yMinLimit_25() { return &___yMinLimit_25; }
	inline void set_yMinLimit_25(float value)
	{
		___yMinLimit_25 = value;
	}

	inline static int32_t get_offset_of_viewerYmin_26() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___viewerYmin_26)); }
	inline float get_viewerYmin_26() const { return ___viewerYmin_26; }
	inline float* get_address_of_viewerYmin_26() { return &___viewerYmin_26; }
	inline void set_viewerYmin_26(float value)
	{
		___viewerYmin_26 = value;
	}

	inline static int32_t get_offset_of_desiredInputPosition_27() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___desiredInputPosition_27)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_desiredInputPosition_27() const { return ___desiredInputPosition_27; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_desiredInputPosition_27() { return &___desiredInputPosition_27; }
	inline void set_desiredInputPosition_27(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___desiredInputPosition_27 = value;
	}

	inline static int32_t get_offset_of_currentInputPosition_28() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___currentInputPosition_28)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_currentInputPosition_28() const { return ___currentInputPosition_28; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_currentInputPosition_28() { return &___currentInputPosition_28; }
	inline void set_currentInputPosition_28(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___currentInputPosition_28 = value;
	}

	inline static int32_t get_offset_of_currentTargetPosition_29() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___currentTargetPosition_29)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentTargetPosition_29() const { return ___currentTargetPosition_29; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentTargetPosition_29() { return &___currentTargetPosition_29; }
	inline void set_currentTargetPosition_29(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentTargetPosition_29 = value;
	}

	inline static int32_t get_offset_of_hitPoint_30() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___hitPoint_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_hitPoint_30() const { return ___hitPoint_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_hitPoint_30() { return &___hitPoint_30; }
	inline void set_hitPoint_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___hitPoint_30 = value;
	}

	inline static int32_t get_offset_of_dragging_31() { return static_cast<int32_t>(offsetof(maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7, ___dragging_31)); }
	inline bool get_dragging_31() const { return ___dragging_31; }
	inline bool* get_address_of_dragging_31() { return &___dragging_31; }
	inline void set_dragging_31(bool value)
	{
		___dragging_31 = value;
	}
};


// HoloToolkit.Unity.Tagalong
struct  Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9  : public SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C
{
public:
	// System.Single HoloToolkit.Unity.Tagalong::MinimumHorizontalOverlap
	float ___MinimumHorizontalOverlap_16;
	// System.Single HoloToolkit.Unity.Tagalong::TargetHorizontalOverlap
	float ___TargetHorizontalOverlap_17;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumVerticalOverlap
	float ___MinimumVerticalOverlap_18;
	// System.Single HoloToolkit.Unity.Tagalong::TargetVerticalOverlap
	float ___TargetVerticalOverlap_19;
	// System.Int32 HoloToolkit.Unity.Tagalong::HorizontalRayCount
	int32_t ___HorizontalRayCount_20;
	// System.Int32 HoloToolkit.Unity.Tagalong::VerticalRayCount
	int32_t ___VerticalRayCount_21;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumTagalongDistance
	float ___MinimumTagalongDistance_22;
	// System.Boolean HoloToolkit.Unity.Tagalong::MaintainFixedSize
	bool ___MaintainFixedSize_23;
	// System.Single HoloToolkit.Unity.Tagalong::DepthUpdateSpeed
	float ___DepthUpdateSpeed_24;
	// System.Single HoloToolkit.Unity.Tagalong::defaultTagalongDistance
	float ___defaultTagalongDistance_25;
	// System.Boolean HoloToolkit.Unity.Tagalong::DebugDrawLines
	bool ___DebugDrawLines_26;
	// UnityEngine.Light HoloToolkit.Unity.Tagalong::DebugPointLight
	Light_tA2F349FE839781469A0344CF6039B51512394275 * ___DebugPointLight_27;

public:
	inline static int32_t get_offset_of_MinimumHorizontalOverlap_16() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___MinimumHorizontalOverlap_16)); }
	inline float get_MinimumHorizontalOverlap_16() const { return ___MinimumHorizontalOverlap_16; }
	inline float* get_address_of_MinimumHorizontalOverlap_16() { return &___MinimumHorizontalOverlap_16; }
	inline void set_MinimumHorizontalOverlap_16(float value)
	{
		___MinimumHorizontalOverlap_16 = value;
	}

	inline static int32_t get_offset_of_TargetHorizontalOverlap_17() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___TargetHorizontalOverlap_17)); }
	inline float get_TargetHorizontalOverlap_17() const { return ___TargetHorizontalOverlap_17; }
	inline float* get_address_of_TargetHorizontalOverlap_17() { return &___TargetHorizontalOverlap_17; }
	inline void set_TargetHorizontalOverlap_17(float value)
	{
		___TargetHorizontalOverlap_17 = value;
	}

	inline static int32_t get_offset_of_MinimumVerticalOverlap_18() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___MinimumVerticalOverlap_18)); }
	inline float get_MinimumVerticalOverlap_18() const { return ___MinimumVerticalOverlap_18; }
	inline float* get_address_of_MinimumVerticalOverlap_18() { return &___MinimumVerticalOverlap_18; }
	inline void set_MinimumVerticalOverlap_18(float value)
	{
		___MinimumVerticalOverlap_18 = value;
	}

	inline static int32_t get_offset_of_TargetVerticalOverlap_19() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___TargetVerticalOverlap_19)); }
	inline float get_TargetVerticalOverlap_19() const { return ___TargetVerticalOverlap_19; }
	inline float* get_address_of_TargetVerticalOverlap_19() { return &___TargetVerticalOverlap_19; }
	inline void set_TargetVerticalOverlap_19(float value)
	{
		___TargetVerticalOverlap_19 = value;
	}

	inline static int32_t get_offset_of_HorizontalRayCount_20() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___HorizontalRayCount_20)); }
	inline int32_t get_HorizontalRayCount_20() const { return ___HorizontalRayCount_20; }
	inline int32_t* get_address_of_HorizontalRayCount_20() { return &___HorizontalRayCount_20; }
	inline void set_HorizontalRayCount_20(int32_t value)
	{
		___HorizontalRayCount_20 = value;
	}

	inline static int32_t get_offset_of_VerticalRayCount_21() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___VerticalRayCount_21)); }
	inline int32_t get_VerticalRayCount_21() const { return ___VerticalRayCount_21; }
	inline int32_t* get_address_of_VerticalRayCount_21() { return &___VerticalRayCount_21; }
	inline void set_VerticalRayCount_21(int32_t value)
	{
		___VerticalRayCount_21 = value;
	}

	inline static int32_t get_offset_of_MinimumTagalongDistance_22() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___MinimumTagalongDistance_22)); }
	inline float get_MinimumTagalongDistance_22() const { return ___MinimumTagalongDistance_22; }
	inline float* get_address_of_MinimumTagalongDistance_22() { return &___MinimumTagalongDistance_22; }
	inline void set_MinimumTagalongDistance_22(float value)
	{
		___MinimumTagalongDistance_22 = value;
	}

	inline static int32_t get_offset_of_MaintainFixedSize_23() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___MaintainFixedSize_23)); }
	inline bool get_MaintainFixedSize_23() const { return ___MaintainFixedSize_23; }
	inline bool* get_address_of_MaintainFixedSize_23() { return &___MaintainFixedSize_23; }
	inline void set_MaintainFixedSize_23(bool value)
	{
		___MaintainFixedSize_23 = value;
	}

	inline static int32_t get_offset_of_DepthUpdateSpeed_24() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___DepthUpdateSpeed_24)); }
	inline float get_DepthUpdateSpeed_24() const { return ___DepthUpdateSpeed_24; }
	inline float* get_address_of_DepthUpdateSpeed_24() { return &___DepthUpdateSpeed_24; }
	inline void set_DepthUpdateSpeed_24(float value)
	{
		___DepthUpdateSpeed_24 = value;
	}

	inline static int32_t get_offset_of_defaultTagalongDistance_25() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___defaultTagalongDistance_25)); }
	inline float get_defaultTagalongDistance_25() const { return ___defaultTagalongDistance_25; }
	inline float* get_address_of_defaultTagalongDistance_25() { return &___defaultTagalongDistance_25; }
	inline void set_defaultTagalongDistance_25(float value)
	{
		___defaultTagalongDistance_25 = value;
	}

	inline static int32_t get_offset_of_DebugDrawLines_26() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___DebugDrawLines_26)); }
	inline bool get_DebugDrawLines_26() const { return ___DebugDrawLines_26; }
	inline bool* get_address_of_DebugDrawLines_26() { return &___DebugDrawLines_26; }
	inline void set_DebugDrawLines_26(bool value)
	{
		___DebugDrawLines_26 = value;
	}

	inline static int32_t get_offset_of_DebugPointLight_27() { return static_cast<int32_t>(offsetof(Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9, ___DebugPointLight_27)); }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 * get_DebugPointLight_27() const { return ___DebugPointLight_27; }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 ** get_address_of_DebugPointLight_27() { return &___DebugPointLight_27; }
	inline void set_DebugPointLight_27(Light_tA2F349FE839781469A0344CF6039B51512394275 * value)
	{
		___DebugPointLight_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DebugPointLight_27), (void*)value);
	}
};


// TrackableStatusEventHandler
struct  TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2  : public DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB
{
public:
	// Vuforia.TrackableBehaviour/StatusInfo TrackableStatusEventHandler::m_PreviousStatusInfo
	int32_t ___m_PreviousStatusInfo_13;
	// Vuforia.TrackableBehaviour/StatusInfo TrackableStatusEventHandler::m_NewStatusInfo
	int32_t ___m_NewStatusInfo_14;

public:
	inline static int32_t get_offset_of_m_PreviousStatusInfo_13() { return static_cast<int32_t>(offsetof(TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2, ___m_PreviousStatusInfo_13)); }
	inline int32_t get_m_PreviousStatusInfo_13() const { return ___m_PreviousStatusInfo_13; }
	inline int32_t* get_address_of_m_PreviousStatusInfo_13() { return &___m_PreviousStatusInfo_13; }
	inline void set_m_PreviousStatusInfo_13(int32_t value)
	{
		___m_PreviousStatusInfo_13 = value;
	}

	inline static int32_t get_offset_of_m_NewStatusInfo_14() { return static_cast<int32_t>(offsetof(TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2, ___m_NewStatusInfo_14)); }
	inline int32_t get_m_NewStatusInfo_14() const { return ___m_NewStatusInfo_14; }
	inline int32_t* get_address_of_m_NewStatusInfo_14() { return &___m_NewStatusInfo_14; }
	inline void set_m_NewStatusInfo_14(int32_t value)
	{
		___m_NewStatusInfo_14 = value;
	}
};


// VuMarkTrackableEventHandler
struct  VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46  : public DefaultTrackableEventHandler_t2FC08D1CD10F50D03BD1EDAE6D81CA64BCF6AFAB
{
public:
	// Vuforia.VuMarkBehaviour VuMarkTrackableEventHandler::vumarkBehaviour
	VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * ___vumarkBehaviour_15;
	// UnityEngine.LineRenderer VuMarkTrackableEventHandler::lineRenderer
	LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * ___lineRenderer_16;
	// UnityEngine.CanvasGroup VuMarkTrackableEventHandler::canvasGroup
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___canvasGroup_17;
	// UnityEngine.Vector2 VuMarkTrackableEventHandler::fadeRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___fadeRange_18;
	// UnityEngine.Transform VuMarkTrackableEventHandler::centralAnchorPoint
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___centralAnchorPoint_19;

public:
	inline static int32_t get_offset_of_vumarkBehaviour_15() { return static_cast<int32_t>(offsetof(VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46, ___vumarkBehaviour_15)); }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * get_vumarkBehaviour_15() const { return ___vumarkBehaviour_15; }
	inline VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 ** get_address_of_vumarkBehaviour_15() { return &___vumarkBehaviour_15; }
	inline void set_vumarkBehaviour_15(VuMarkBehaviour_tD3D58E64EEEFB90772C1066D6AD3D36FD1C88D42 * value)
	{
		___vumarkBehaviour_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vumarkBehaviour_15), (void*)value);
	}

	inline static int32_t get_offset_of_lineRenderer_16() { return static_cast<int32_t>(offsetof(VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46, ___lineRenderer_16)); }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * get_lineRenderer_16() const { return ___lineRenderer_16; }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 ** get_address_of_lineRenderer_16() { return &___lineRenderer_16; }
	inline void set_lineRenderer_16(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * value)
	{
		___lineRenderer_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineRenderer_16), (void*)value);
	}

	inline static int32_t get_offset_of_canvasGroup_17() { return static_cast<int32_t>(offsetof(VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46, ___canvasGroup_17)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_canvasGroup_17() const { return ___canvasGroup_17; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_canvasGroup_17() { return &___canvasGroup_17; }
	inline void set_canvasGroup_17(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___canvasGroup_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasGroup_17), (void*)value);
	}

	inline static int32_t get_offset_of_fadeRange_18() { return static_cast<int32_t>(offsetof(VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46, ___fadeRange_18)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_fadeRange_18() const { return ___fadeRange_18; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_fadeRange_18() { return &___fadeRange_18; }
	inline void set_fadeRange_18(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___fadeRange_18 = value;
	}

	inline static int32_t get_offset_of_centralAnchorPoint_19() { return static_cast<int32_t>(offsetof(VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46, ___centralAnchorPoint_19)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_centralAnchorPoint_19() const { return ___centralAnchorPoint_19; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_centralAnchorPoint_19() { return &___centralAnchorPoint_19; }
	inline void set_centralAnchorPoint_19(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___centralAnchorPoint_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___centralAnchorPoint_19), (void*)value);
	}
};


// MTExtendedTrackableEventHandler
struct  MTExtendedTrackableEventHandler_t79E9857CE50A2F7DE46697AB54E83A0A536393F0  : public TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2
{
public:
	// UnityEngine.MeshRenderer MTExtendedTrackableEventHandler::boundingBox
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ___boundingBox_15;

public:
	inline static int32_t get_offset_of_boundingBox_15() { return static_cast<int32_t>(offsetof(MTExtendedTrackableEventHandler_t79E9857CE50A2F7DE46697AB54E83A0A536393F0, ___boundingBox_15)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get_boundingBox_15() const { return ___boundingBox_15; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of_boundingBox_15() { return &___boundingBox_15; }
	inline void set_boundingBox_15(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		___boundingBox_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boundingBox_15), (void*)value);
	}
};


// MTAdvancedTrackableEventHandler
struct  MTAdvancedTrackableEventHandler_t0CC9E3A921447CB8CF6F65E02FAD38C6E27D0F50  : public MTExtendedTrackableEventHandler_t79E9857CE50A2F7DE46697AB54E83A0A536393F0
{
public:
	// ModelTargetsManager MTAdvancedTrackableEventHandler::modelTargetsManager
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D * ___modelTargetsManager_16;
	// System.Boolean MTAdvancedTrackableEventHandler::isModelTargetReferenceStored
	bool ___isModelTargetReferenceStored_17;

public:
	inline static int32_t get_offset_of_modelTargetsManager_16() { return static_cast<int32_t>(offsetof(MTAdvancedTrackableEventHandler_t0CC9E3A921447CB8CF6F65E02FAD38C6E27D0F50, ___modelTargetsManager_16)); }
	inline ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D * get_modelTargetsManager_16() const { return ___modelTargetsManager_16; }
	inline ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D ** get_address_of_modelTargetsManager_16() { return &___modelTargetsManager_16; }
	inline void set_modelTargetsManager_16(ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D * value)
	{
		___modelTargetsManager_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelTargetsManager_16), (void*)value);
	}

	inline static int32_t get_offset_of_isModelTargetReferenceStored_17() { return static_cast<int32_t>(offsetof(MTAdvancedTrackableEventHandler_t0CC9E3A921447CB8CF6F65E02FAD38C6E27D0F50, ___isModelTargetReferenceStored_17)); }
	inline bool get_isModelTargetReferenceStored_17() const { return ___isModelTargetReferenceStored_17; }
	inline bool* get_address_of_isModelTargetReferenceStored_17() { return &___isModelTargetReferenceStored_17; }
	inline void set_isModelTargetReferenceStored_17(bool value)
	{
		___isModelTargetReferenceStored_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6344[44] = 
{
	0,
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_managerSceneOpInProgress_15(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_managerSceneOpProgress_16(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_contentTracker_17(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_lightingExecutor_18(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CNameU3Ek__BackingField_19(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillLoadContentU3Ek__BackingField_20(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnContentLoadedU3Ek__BackingField_21(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillUnloadContentU3Ek__BackingField_22(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnContentUnloadedU3Ek__BackingField_23(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillLoadLightingU3Ek__BackingField_24(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnLightingLoadedU3Ek__BackingField_25(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillUnloadLightingU3Ek__BackingField_26(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnLightingUnloadedU3Ek__BackingField_27(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillLoadSceneU3Ek__BackingField_28(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnSceneLoadedU3Ek__BackingField_29(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnWillUnloadSceneU3Ek__BackingField_30(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3COnSceneUnloadedU3Ek__BackingField_31(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CSceneOperationInProgressU3Ek__BackingField_32(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CSceneOperationProgressU3Ek__BackingField_33(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CLightingOperationInProgressU3Ek__BackingField_34(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CLightingOperationProgressU3Ek__BackingField_35(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CActiveLightingSceneU3Ek__BackingField_36(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CWaitingToProceedU3Ek__BackingField_37(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CSourceIdU3Ek__BackingField_38(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E::get_offset_of_U3CSourceNameU3Ek__BackingField_39(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_UpdatePerfMarker_40(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_LoadNextContentPerfMarker_41(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_LoadPrevContentPerfMarker_42(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_LoadContentByTagPerfMarker_43(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_UnloadContentByTagPerfMarker_44(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_LoadContentPerfMarker_45(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_UnloadContentPerfMarker_46(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_IsContentLoadedPerfMarker_47(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_SetLightingScenePerfMarker_48(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_SetManagerScenePerfMarker_49(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_LoadScenesInternalPerfMarker_50(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_UnloadScenesInternalPerfMarker_51(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_InvokeLoadedActionsPerfMarker_52(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_InvokeWillLoadActionsPerfMarker_53(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_InvokeWillUnloadActionsPerfMarker_54(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_InvokeUnloadedActionsPerfMarker_55(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_GetScenePerfMarker_56(),
	MixedRealitySceneSystem_tC9A4F8300FFB9BEAAD90481F6E2069854F4B896E_StaticFields::get_offset_of_GetLoadedContentScenesPerfMarker_57(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6346[19] = 
{
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9::get_offset_of_U3CNameU3Ek__BackingField_17(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9::get_offset_of_meshEventData_18(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9::get_offset_of_spatialAwarenessObjectParent_19(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9::get_offset_of_nextSourceId_20(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9::get_offset_of_spatialAwarenessSystemProfile_21(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetObserversPerfMarker_22(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetObserversTPerfMarker_23(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetObserverPerfMarker_24(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetObserverTPerfMarker_25(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetDataProvidersPerfMarker_26(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_GetDataProviderPerfMarker_27(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_ResumeObserversPerfMarker_28(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_ResumeObserversTPerfMarker_29(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_ResumeObserverPerfMarker_30(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_SuspendObserversPerfMarker_31(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_SuspendObserversTPerfMarker_32(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_SuspendObserverPerfMarker_33(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_ClearObservationsPerfMarker_34(),
	MixedRealitySpatialAwarenessSystem_tB4BF493560F6CA93729762897D63BF3871D9E5E9_StaticFields::get_offset_of_ClearObservationsTPerfMarker_35(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6348[1] = 
{
	U3CU3Ec_tE5A871F4005FA8E3E3DD1E0298E699F84FC294BE_StaticFields::get_offset_of_U3CU3E9_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6349[18] = 
{
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_teleportEventData_14(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_isTeleporting_15(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_isProcessingTeleportRequest_16(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_targetPosition_17(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_targetRotation_18(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_eventSystemReference_19(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_U3CNameU3Ek__BackingField_20(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_HandleEventPerfMarker_21(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058::get_offset_of_teleportDuration_22(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_OnTeleportRequestHandler_23(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_RaiseTeleportRequestPerfMarker_24(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_OnTeleportStartedHandler_25(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_RaiseTeleportStartedPerfMarker_26(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_OnTeleportCompletedHandler_27(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_RaiseTeleportCompletePerfMarker_28(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_OnTeleportCanceledHandler_29(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_RaiseTeleportCanceledPerfMarker_30(),
	MixedRealityTeleportSystem_t5843C0EBE3A4143CBD93B46F1935371B3BF2B058_StaticFields::get_offset_of_ProcessTeleportationRequestPerfMarker_31(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6351[3] = 
{
	DisplayType_t5138A1FC756E211BEB18A33E11810DF3ECE48C81::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6352[7] = 
{
	DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC::get_offset_of_displayTextMesh_4(),
	DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC::get_offset_of_inputType_5(),
	DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC::get_offset_of_axisNumber_6(),
	DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC::get_offset_of_buttonNumber_7(),
	DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC::get_offset_of_displayType_8(),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6353[5] = 
{
	ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E::get_offset_of_listInputDevicesTextMesh_4(),
	ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E::get_offset_of_displayFeatureUsagesTextMeshes_5(),
	ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E::get_offset_of_controllerInputDevices_6(),
	ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E::get_offset_of_handInputDevices_7(),
	ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E::get_offset_of_featureUsages_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6356[3] = 
{
	DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9::get_offset_of_vuforiaCamera_4(),
	DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9::get_offset_of_canvasRectTransform_5(),
	DepthCanvas_tD1409C89CDBC8F2497C75AEBCBB22F15C3682FE9::get_offset_of_maxDistanceFromCamera_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6357[2] = 
{
	MTAdvancedTrackableEventHandler_t0CC9E3A921447CB8CF6F65E02FAD38C6E27D0F50::get_offset_of_modelTargetsManager_16(),
	MTAdvancedTrackableEventHandler_t0CC9E3A921447CB8CF6F65E02FAD38C6E27D0F50::get_offset_of_isModelTargetReferenceStored_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6358[1] = 
{
	MTExtendedTrackableEventHandler_t79E9857CE50A2F7DE46697AB54E83A0A536393F0::get_offset_of_boundingBox_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6359[3] = 
{
	ModelTargetMode_t5BE2D9E3B55C9321F25AFB65BF1B736536B5D157::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6360[14] = 
{
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_modelTargetMode_4(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_autoActivate_5(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_augmentation_6(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_modelStandard_7(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_modelAdvanced_8(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_DataSetStandard_9(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_DataSetAdvanced_10(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_guideViewsButton_11(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_datasetsMenu_12(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_objectTracker_13(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_stateManager_14(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_modelTargetsUIManager_15(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_modelTargetBehaviours_16(),
	ModelTargetsManager_t6B67787463228219D68EA18684AC43F7B0C45F6D::get_offset_of_currentActiveDataSet_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6361[3] = 
{
	U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3::get_offset_of_U3CU3E1__state_0(),
	U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3::get_offset_of_U3CU3E2__current_1(),
	U3CClearImageSequencePauseU3Ed__21_t6F9C9E4C2EB2C3D0FD55CB37E7867296013BF8E3::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6362[12] = 
{
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_canvasGroupAdvanced_4(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_cycleMultipleIcons_5(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_spatialMapping_6(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_whiteTransparent_7(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_imageSequence_8(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_imagesAdvanced_9(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_uiEnabled_10(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_imageSequencePaused_11(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_imageSequenceIndex_12(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_clock_13(),
	ModelTargetsUIManager_t39B950D3A411864B03602D83AE96BF87ED663169::get_offset_of_fadeMeter_14(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6363[2] = 
{
	AugmentationObject_t591E71F0CFBF761AF0B1F2D96031031B1BBD9C58::get_offset_of_vumarkID_0(),
	AugmentationObject_t591E71F0CFBF761AF0B1F2D96031031B1BBD9C58::get_offset_of_augmentation_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6364[1] = 
{
	U3CU3Ec__DisplayClass18_0_tF9F235AF92649B41506D0DA0EAFBD250C5D4917C::get_offset_of_vumarkBehaviour_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6365[5] = 
{
	U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005::get_offset_of_U3CU3E1__state_0(),
	U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005::get_offset_of_U3CU3E2__current_1(),
	U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005::get_offset_of_vumarkBehaviour_2(),
	U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005::get_offset_of_U3CU3E4__this_3(),
	U3COnVuMarkTargetAvailableU3Ed__18_tE632CAD28C9BFB5EBB7205FA6FB21C108FBD0005::get_offset_of_U3CU3E8__1_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6366[8] = 
{
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_U3CU3E1__state_0(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_U3CU3E2__current_1(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_seconds_2(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_U3CU3E4__this_3(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_vuMarkId_4(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_vuMarkDataType_5(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_vuMarkDesc_6(),
	U3CShowPanelAfterU3Ed__35_t066A2FB0C530C6D5803D8704015131FE6E83BD52::get_offset_of_vuMarkImage_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6367[10] = 
{
	0,
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_vumarkManager_5(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_lineRenderer_6(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_vumarkInstanceTextures_7(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_vumarkAugmentationObjects_8(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_nearestVuMarkScreenPanel_9(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_closestVuMark_10(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_currentVuMark_11(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_vuforiaCamera_12(),
	VuMarkHandler_t1B3DF8989CB4C4C57DB5C0BD9DF962C990741CA1::get_offset_of_augmentationObjects_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6368[7] = 
{
	0,
	0,
	VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46::get_offset_of_vumarkBehaviour_15(),
	VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46::get_offset_of_lineRenderer_16(),
	VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46::get_offset_of_canvasGroup_17(),
	VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46::get_offset_of_fadeRange_18(),
	VuMarkTrackableEventHandler_t487A8EFA804F1D8D1E3EFD205B4A3B026A56BB46::get_offset_of_centralAnchorPoint_19(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6369[2] = 
{
	VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615::get_offset_of_m_Image_4(),
	VuMarkTrackableStatusUI_t6C622FB3C74A06196EE0AFB1C31B0E1DE60D9615::get_offset_of_m_Info_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6370[8] = 
{
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_contentToAlign_4(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_defaultDistanceHoloLens1_5(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_defaultDistanceHoloLens2_6(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_distanceFromCamera_7(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_realignContent_8(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_destinationPosition_9(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_destinationRotation_10(),
	ContentPositioner_tB40EE61E2759818A81F59CF6AE11323B424570A0::get_offset_of_cam_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6371[2] = 
{
	ContentPositionerClient_t0A192212343478F3C2F9A7159BB4659068C8ED27::get_offset_of_distanceHoloLens1_4(),
	ContentPositionerClient_t0A192212343478F3C2F9A7159BB4659068C8ED27::get_offset_of_distanceHoloLens2_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6372[4] = 
{
	TurnOffRendering_t00AD57AC5E1944F728909CA8B78D5298B0D6F20B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6373[1] = 
{
	CustomTurnOffBehaviour_tA4AF26D4FCB81D0E041CB0CF42AF4A895F19B9B3::get_offset_of_turnOffRendering_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6376[1] = 
{
	SceneLoader_t2995AD1491223D32053837F27878AFE973D40D1F::get_offset_of_SceneToLoad_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6377[2] = 
{
	SetDrivenKey_t6EAB070660121AAD1B7245A25A08E95353094D0E::get_offset_of_watchSizeChanges_4(),
	SetDrivenKey_t6EAB070660121AAD1B7245A25A08E95353094D0E::get_offset_of_readChangedSize_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6378[2] = 
{
	TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2::get_offset_of_m_PreviousStatusInfo_13(),
	TrackableStatusEventHandler_t2BF04637554910862ED4F0B88AAC4C55BCF266B2::get_offset_of_m_NewStatusInfo_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6381[3] = 
{
	U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t564B9D0492668C6BBF4877B63ABD860499788124_StaticFields::get_offset_of_U3CU3E9__4_1_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6382[2] = 
{
	VuforiaAdditiveSceneLoader_t3ECB26104E71BD79DDBF3521D1B2FA5BC4585CB2::get_offset_of_DataSetsToActivate_4(),
	VuforiaAdditiveSceneLoader_t3ECB26104E71BD79DDBF3521D1B2FA5BC4585CB2::get_offset_of_mOnVuforiaStarted_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6383[8] = 
{
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_textObject_4(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_activeDataSetsTitle_5(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_activeDataSetsInfo_6(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_trackableStateInfo_7(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_vumarkTrackableStateInfo_8(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_objectTracker_9(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_stateManager_10(),
	VuforiaStateInfo_t2291A35344647CBB28F55D4DA2159B0F34ECFEEC::get_offset_of_trackablesDictionary_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6385[1] = 
{
	U3CU3Ec__DisplayClass12_0_t942BA12B202E70079D8B79ED42A1984B1961BA63::get_offset_of_action_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6386[10] = 
{
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_instance_4(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_actionQueuesUpdateFunc_5(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A::get_offset_of_actionCopiedQueueUpdateFunc_6(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_noActionQueueToExecuteUpdateFunc_7(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_actionQueuesLateUpdateFunc_8(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A::get_offset_of_actionCopiedQueueLateUpdateFunc_9(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_noActionQueueToExecuteLateUpdateFunc_10(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_actionQueuesFixedUpdateFunc_11(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A::get_offset_of_actionCopiedQueueFixedUpdateFunc_12(),
	UnityThread_t1E9372D9058727DF149C46B4FC8D637F3BE3322A_StaticFields::get_offset_of_noActionQueueToExecuteFixedUpdateFunc_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6387[1] = 
{
	FixPositionTarget_tEC41273C21FADD2DE3E80E7A01C5B828E48B91E1::get_offset_of_hologram_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6388[1] = 
{
	HandleControls_t2DEC08A714CAE98B3E0839DFE57C08656F020984::get_offset_of_hologram_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6389[1] = 
{
	NetworkManager_tDA8A4BECD3B0245AF4351E9ABC199B1B185BF606_StaticFields::get_offset_of_instance_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6390[7] = 
{
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_input_4(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_textInputField_5(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_debugLog_6(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_button_7(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_networkManager_8(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_Canvas_9(),
	ReadInput_t86A08B0FC81F81E0A6FB7FB46F1B497DF1B7CB48::get_offset_of_hologram_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6391[5] = 
{
	Manipulation_t7DD93A854132277D32A7980DEEA84AB82904213E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6392[5] = 
{
	U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3::get_offset_of_U3CU3E1__state_0(),
	U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3::get_offset_of_U3CU3E2__current_1(),
	U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3::get_offset_of_time_3(),
	U3CDelayTimeU3Ed__14_tF56F928BF758D5ED38881BB4658E46881691A0A3::get_offset_of_U3CU3E3__time_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6393[4] = 
{
	UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B::get_offset_of_hologram_4(),
	UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B::get_offset_of_timeUpdate_5(),
	UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B::get_offset_of_currentState_6(),
	UpdateObject_t9BC869D764898F82145B3C3235242E974AB89F6B::get_offset_of_myColor_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6394[3] = 
{
	BoundSource_t6A08E1F647A907F6EB5116F9AEE56F24C24957AF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6395[34] = 
{
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_boundSource_4(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_interactive_5(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_lineMaterial_6(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_wire_renderer_7(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_wireColor_8(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_line_renderer_9(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_linePrefab_10(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_lineWidth_11(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_lineColor_12(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_numCapVertices_13(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_bound_14(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_boundOffset_15(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_colliderBound_16(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_colliderBoundOffset_17(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_meshBound_18(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_meshBoundOffset_19(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_corners_20(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_lines_21(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_quat_22(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_lineList_23(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_topFrontLeft_24(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_topFrontRight_25(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_topBackLeft_26(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_topBackRight_27(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_bottomFrontLeft_28(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_bottomFrontRight_29(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_bottomBackLeft_30(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_bottomBackRight_31(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_startingScale_32(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_previousScale_33(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_startingBoundSize_34(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_startingBoundCenterLocal_35(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_previousPosition_36(),
	BoundBox_t51E0B9AEF0EB04DB761A753D318968624E98D07F::get_offset_of_previousRotation_37(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6396[1] = 
{
	BoundBoxExample_t7B9576D542794C5545DF5F9029013C0CA70DFCE6::get_offset_of_boundBox_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6397[7] = 
{
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_lineMaterial_4(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_lColor_5(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_outlines_6(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_triangles_7(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_colors_8(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_screenOutlines_9(),
	DrawLines_tE215F8A33B9733AF071A06077903983F1165D5D6::get_offset_of_screenColors_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6398[6] = 
{
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_dynScale_4(),
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_t_5(),
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_angles_6(),
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_scale_7(),
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_run_8(),
	TestDynamicBehaviour_t5168DA008088F1E32B5EC23969E2B6C7A9F49309::get_offset_of_speed_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6399[4] = 
{
	U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515::get_offset_of_U3CU3E1__state_0(),
	U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515::get_offset_of_U3CU3E2__current_1(),
	U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515::get_offset_of_startingHit_2(),
	U3CDragObjectU3Ed__34_t9943EE4632706E198C6D4D2E917D9438D7FFA515::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6400[28] = 
{
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_target_4(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_terrainMesh_5(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_targetOffset_6(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_distance_7(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_maxDistance_8(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_minDistance_9(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_xSpeed_10(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_ySpeed_11(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_aboveYmin_12(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_yMaxLimit_13(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_zoomRate_14(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_panSpeed_15(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_zoomDampening_16(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_xDeg_17(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_yDeg_18(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_currentDistance_19(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_desiredDistance_20(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_currentRotation_21(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_desiredRotation_22(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_rotation_23(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_position_24(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_yMinLimit_25(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_viewerYmin_26(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_desiredInputPosition_27(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_currentInputPosition_28(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_currentTargetPosition_29(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_hitPoint_30(),
	maxCamera_tF3C3C4B434F4D84EC692551119083A3EC0FA77B7::get_offset_of_dragging_31(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6401[1] = 
{
	CameraCache_t8FE25B8A2D81F52E694B70EF48DE821618DFC033_StaticFields::get_offset_of_cachedCamera_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6402[22] = 
{
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_UseUnscaledTime_4(),
	0,
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_PositionPerSecond_6(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_RotationDegreesPerSecond_7(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_RotationSpeedScaler_8(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_ScalePerSecond_9(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_SmoothLerpToTarget_10(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_SmoothPositionLerpRatio_11(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_SmoothRotationLerpRatio_12(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_SmoothScaleLerpRatio_13(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_targetPosition_14(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_targetRotation_16(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_targetLocalRotation_18(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_targetLocalScale_20(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_InterpolationStarted_22(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_InterpolationDone_23(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_U3CPositionVelocityU3Ek__BackingField_24(),
	Interpolator_tE40D73AA1A84B003812A8165959456CACA1CC9E9::get_offset_of_oldPosition_25(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6403[3] = 
{
	PivotAxis_t566603A3C8D29430881F470E06AC82E308AF2D9B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6404[2] = 
{
	Billboard_tBF7123B0F815E7B9BAF817A1D20781DB77AAA4EB::get_offset_of_PivotAxis_4(),
	Billboard_tBF7123B0F815E7B9BAF817A1D20781DB77AAA4EB::get_offset_of_TargetTransform_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6405[3] = 
{
	FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4::get_offset_of_SizeRatio_4(),
	FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4::get_offset_of_startingDistance_5(),
	FixedAngularSize_t3EA4E587FE61929441884B17C204B311A670EAB4::get_offset_of_startingScale_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6406[12] = 
{
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_TagalongDistance_4(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_EnforceDistance_5(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_PositionUpdateSpeed_6(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_SmoothMotion_7(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_SmoothingFactor_8(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_tagalongCollider_9(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_interpolator_10(),
	SimpleTagalong_tFBBA74D08A5664F3B5F3173D2E2C71CEF5508F9C::get_offset_of_frustumPlanes_11(),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6407[12] = 
{
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_MinimumHorizontalOverlap_16(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_TargetHorizontalOverlap_17(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_MinimumVerticalOverlap_18(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_TargetVerticalOverlap_19(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_HorizontalRayCount_20(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_VerticalRayCount_21(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_MinimumTagalongDistance_22(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_MaintainFixedSize_23(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_DepthUpdateSpeed_24(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_defaultTagalongDistance_25(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_DebugDrawLines_26(),
	Tagalong_t2049C203BED1E44116ED2BD6C6C714AD20267EF9::get_offset_of_DebugPointLight_27(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6409[2] = 
{
	ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921_StaticFields::get_offset_of_packets_0(),
	ClientHandleData_tC1024E2BF108BCDCD8703E349FE3A7A883829921_StaticFields::get_offset_of_playerBuffer_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6410[1] = 
{
	U3CU3Ec__DisplayClass9_0_t9712E44A4DA708FBA21BD30CC4EC3AF748B55EE0::get_offset_of_newBytes_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6411[4] = 
{
	ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields::get_offset_of_clientSocket_0(),
	ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields::get_offset_of_stream_1(),
	ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields::get_offset_of_recBuffer_2(),
	ClientTCP_tA171F10CF728CB607534DD52D8C0CD13D7E94A7A_StaticFields::get_offset_of_U3CServerAddressU3Ek__BackingField_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6412[8] = 
{
	ServerPackets_tFEABB95C6CCC979FDA138BE5CF3B43FA1F4678AD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6413[3] = 
{
	Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185::get_offset_of_U3CpositionU3Ek__BackingField_0(),
	Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185::get_offset_of_U3CrotationU3Ek__BackingField_1(),
	Transform_tFCAD6B5B2AA90675B0E03593F83A857B1BA60185::get_offset_of_U3CscaleU3Ek__BackingField_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6414[2] = 
{
	DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E_StaticFields::get_offset_of_handleControls_0(),
	DataReceiver_tA7B63AD08B0C462F6F744017A83D3E693DD0340E_StaticFields::get_offset_of_updateObject_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6415[9] = 
{
	ClientPackets_tBB32DF50AC986BFFFCD804B6D01A3FEA857939D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable6417[5] = 
{
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623::get_offset_of_buff_0(),
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623::get_offset_of_readBuff_1(),
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623::get_offset_of_readPos_2(),
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623::get_offset_of_buffUpdated_3(),
	ByteBuffer_tF47004BA136B2C776E8F93AFA35E9FA4DCC13623::get_offset_of_dispose_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
