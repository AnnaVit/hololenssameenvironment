﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar)
extern void MixedRealityTeleportSystem__ctor_m1700C07363B3425FC7980502E993D50E1D34D3AA (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.ctor()
extern void MixedRealityTeleportSystem__ctor_mE1A6960CE1385B52E0DD4332FA53DC69956C7457 (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_Name()
extern void MixedRealityTeleportSystem_get_Name_mC741D40F9C0AAD3CEA0DD7B5D95DE2A00FFE9F2F (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::set_Name(System.String)
extern void MixedRealityTeleportSystem_set_Name_m0E0F20FA42FA29245B934539EAEF4FC048308A5D (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Initialize()
extern void MixedRealityTeleportSystem_Initialize_mAA674E7E9F84FF447842600338F8FDB9880A386E (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::InitializeInternal()
extern void MixedRealityTeleportSystem_InitializeInternal_mF07DDC9BB8E1A6727DE053013529AA972864CC2C (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Destroy()
extern void MixedRealityTeleportSystem_Destroy_m51C0DE5C53C2CD01A167D049BA62278D876A7AF5 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::HandleEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Register(UnityEngine.GameObject)
extern void MixedRealityTeleportSystem_Register_m40E231C084B67E255526D808355E4988BFB15AB1 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Unregister(UnityEngine.GameObject)
extern void MixedRealityTeleportSystem_Unregister_mB57652330CC0EA741A15F109B01E974720E58664 (void);
// 0x0000000B System.Boolean Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_IsInputSystemEnabled()
extern void MixedRealityTeleportSystem_get_IsInputSystemEnabled_m04BB2016DA792092F023CBB2723EDA2B94601708 (void);
// 0x0000000C System.Single Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_TeleportDuration()
extern void MixedRealityTeleportSystem_get_TeleportDuration_m5EE6C7634BA5501B3CF4E9356AE222544F9B57F1 (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::set_TeleportDuration(System.Single)
extern void MixedRealityTeleportSystem_set_TeleportDuration_m2726C77706ADDFD54304283800784C7D35CD0A84 (void);
// 0x0000000E System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportRequest(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotSpot)
extern void MixedRealityTeleportSystem_RaiseTeleportRequest_m739C8D77D38FFF69EFFC1EBB6FE9B67B6F2F9995 (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotSpot)
extern void MixedRealityTeleportSystem_RaiseTeleportStarted_m55BB66597FDA207EB109A43E81A14729DBDC12AB (void);
// 0x00000010 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportComplete(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotSpot)
extern void MixedRealityTeleportSystem_RaiseTeleportComplete_mC42A41B26E168777DB9EC5C1FD9C37EEDAE56828 (void);
// 0x00000011 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotSpot)
extern void MixedRealityTeleportSystem_RaiseTeleportCanceled_mFE069D9B8EAA8C10E4C9B05D6023F7378B8ED549 (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::ProcessTeleportationRequest(Microsoft.MixedReality.Toolkit.Teleport.TeleportEventData)
extern void MixedRealityTeleportSystem_ProcessTeleportationRequest_mA50B79E1038497CD4E39FD105EE07E655DDACB27 (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.cctor()
extern void MixedRealityTeleportSystem__cctor_m126619C833876D3D146D4D87FB0ED8AB5128B2D9 (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::.cctor()
extern void U3CU3Ec__cctor_mEA97223DD057C02ED8FD652A77AF8E12576520AF (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::.ctor()
extern void U3CU3Ec__ctor_m20C0392CA02A937CED7A01CAC76B1DFA315E6ED1 (void);
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_0(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_0_m8F237EA0BD72A2BFBAC11E77B642AE9C5331566C (void);
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_1(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_1_m89FC8CC85EF014683C116FFE46AD64037ADCC815 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_2(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_2_m91FE686DB3D6647776040522B2B1AAC72D2D1A7E (void);
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_3(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_3_mC41663BD8031C445AB4F4DDA6843C5EB6280DE83 (void);
static Il2CppMethodPointer s_methodPointers[25] = 
{
	MixedRealityTeleportSystem__ctor_m1700C07363B3425FC7980502E993D50E1D34D3AA,
	MixedRealityTeleportSystem__ctor_mE1A6960CE1385B52E0DD4332FA53DC69956C7457,
	MixedRealityTeleportSystem_get_Name_mC741D40F9C0AAD3CEA0DD7B5D95DE2A00FFE9F2F,
	MixedRealityTeleportSystem_set_Name_m0E0F20FA42FA29245B934539EAEF4FC048308A5D,
	MixedRealityTeleportSystem_Initialize_mAA674E7E9F84FF447842600338F8FDB9880A386E,
	MixedRealityTeleportSystem_InitializeInternal_mF07DDC9BB8E1A6727DE053013529AA972864CC2C,
	MixedRealityTeleportSystem_Destroy_m51C0DE5C53C2CD01A167D049BA62278D876A7AF5,
	NULL,
	MixedRealityTeleportSystem_Register_m40E231C084B67E255526D808355E4988BFB15AB1,
	MixedRealityTeleportSystem_Unregister_mB57652330CC0EA741A15F109B01E974720E58664,
	MixedRealityTeleportSystem_get_IsInputSystemEnabled_m04BB2016DA792092F023CBB2723EDA2B94601708,
	MixedRealityTeleportSystem_get_TeleportDuration_m5EE6C7634BA5501B3CF4E9356AE222544F9B57F1,
	MixedRealityTeleportSystem_set_TeleportDuration_m2726C77706ADDFD54304283800784C7D35CD0A84,
	MixedRealityTeleportSystem_RaiseTeleportRequest_m739C8D77D38FFF69EFFC1EBB6FE9B67B6F2F9995,
	MixedRealityTeleportSystem_RaiseTeleportStarted_m55BB66597FDA207EB109A43E81A14729DBDC12AB,
	MixedRealityTeleportSystem_RaiseTeleportComplete_mC42A41B26E168777DB9EC5C1FD9C37EEDAE56828,
	MixedRealityTeleportSystem_RaiseTeleportCanceled_mFE069D9B8EAA8C10E4C9B05D6023F7378B8ED549,
	MixedRealityTeleportSystem_ProcessTeleportationRequest_mA50B79E1038497CD4E39FD105EE07E655DDACB27,
	MixedRealityTeleportSystem__cctor_m126619C833876D3D146D4D87FB0ED8AB5128B2D9,
	U3CU3Ec__cctor_mEA97223DD057C02ED8FD652A77AF8E12576520AF,
	U3CU3Ec__ctor_m20C0392CA02A937CED7A01CAC76B1DFA315E6ED1,
	U3CU3Ec_U3C_cctorU3Eb__39_0_m8F237EA0BD72A2BFBAC11E77B642AE9C5331566C,
	U3CU3Ec_U3C_cctorU3Eb__39_1_m89FC8CC85EF014683C116FFE46AD64037ADCC815,
	U3CU3Ec_U3C_cctorU3Eb__39_2_m91FE686DB3D6647776040522B2B1AAC72D2D1A7E,
	U3CU3Ec_U3C_cctorU3Eb__39_3_mC41663BD8031C445AB4F4DDA6843C5EB6280DE83,
};
static const int32_t s_InvokerIndices[25] = 
{
	3635,
	4406,
	4326,
	3635,
	4406,
	4406,
	4406,
	-1,
	3635,
	3635,
	4364,
	4371,
	3676,
	2172,
	2172,
	2172,
	2172,
	3635,
	6303,
	6303,
	4406,
	2172,
	2172,
	2172,
	2172,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000008, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, 48895 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_TeleportSystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_TeleportSystem_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_TeleportSystem_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.TeleportSystem.dll",
	25,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Services_TeleportSystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
