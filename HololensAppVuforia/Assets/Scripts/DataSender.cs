﻿using Assets.Externals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum ClientPackets
    {
        CHelloServer = 1,

        CPressButton = 2,

        CRotation = 3,

        CScale = 4,

        CPosition = 5,

        CRequestControl = 6,

        CReleseControl = 7,

        CEulerAngle = 8,
    }

    static class DataSender
    {
        public static void SendHelloServer()
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CHelloServer);
            buffer.WriteString("Client connected");
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void SendPressButton()
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CPressButton);
            buffer.WriteString("Press button!");
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void SendRotation(string rotation)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CRotation);
            buffer.WriteString(rotation);
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void SendScale(string scale)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CScale);
            buffer.WriteString(scale);
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void SendPosition(string position)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CPosition);
            buffer.WriteString(position);
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void SendEulerAngle(string eulerAngle)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CEulerAngle);
            buffer.WriteString(eulerAngle);
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void RequestControl(string color)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CRequestControl);
            buffer.WriteString(color);
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }

        public static void ReleseControl()
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ClientPackets.CReleseControl);
            buffer.WriteString("Relese control!");
            ClientTCP.SendData(buffer.ToArray());
            buffer.Dispose();
        }
    }
}
