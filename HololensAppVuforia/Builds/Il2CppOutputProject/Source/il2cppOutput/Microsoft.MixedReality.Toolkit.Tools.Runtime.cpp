﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.IEnumerable`1<UnityEngine.XR.InputDevice>
struct IEnumerable_1_t37A11B97B7FA1B094D2A1ADAA8B3A5F4B2E3502A;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F;
// System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>
struct List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem>
struct List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.XR.InputDevice[]
struct InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE;
// UnityEngine.XR.InputFeatureUsage[]
struct InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult
struct DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC;
// Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages
struct ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.String
struct String_t;
// UnityEngine.TextMesh
struct TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral0C2FD7F14008DC5A9704A71B5926F425273F6DFC;
IL2CPP_EXTERN_C String_t* _stringLiteral10F4DE454A7A7EDC63FFA330FCC744864D606F50;
IL2CPP_EXTERN_C String_t* _stringLiteral1653F9A5516D70F1640148CADCC97C572A62936F;
IL2CPP_EXTERN_C String_t* _stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE;
IL2CPP_EXTERN_C String_t* _stringLiteral2FB1273DCF301695744F96DE3A31C5DBDDB6DBB0;
IL2CPP_EXTERN_C String_t* _stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B;
IL2CPP_EXTERN_C String_t* _stringLiteral80E8E33CFC68B6519B4050770677835060BEB5E8;
IL2CPP_EXTERN_C String_t* _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
IL2CPP_EXTERN_C String_t* _stringLiteral87E261F2D88CAAEDCD99354542725097ED590502;
IL2CPP_EXTERN_C String_t* _stringLiteralB69CD88CA44BBA6464A30CAFD46811605F0109DF;
IL2CPP_EXTERN_C String_t* _stringLiteralBB47B58026352318D44CEC93932BDFB273FEAF1C;
IL2CPP_EXTERN_C String_t* _stringLiteralCA604168AB048809605F0545853F2D2C30960F1F;
IL2CPP_EXTERN_C String_t* _stringLiteralCB6CE6B89F14D219960135E95D3D3A31031318C1;
IL2CPP_EXTERN_C String_t* _stringLiteralD098894B7802A8D5E690D40161CCBD3A3B74AB08;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDAF2BD03270A51BF9C135F3694DCAD2D3E66F465;
IL2CPP_EXTERN_C String_t* _stringLiteralF3193468E3E53BE1E41883B70A00F57ABFA77BFA;
IL2CPP_EXTERN_C const RuntimeMethod* Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_m44982DBFBF71598D4ADEE6632D78D390CEFA42D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var;

struct InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tE9A3975296CDF88EEC2B5A711AA5BC6FD4B0E156 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct  List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F, ____items_1)); }
	inline InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* get__items_1() const { return ____items_1; }
	inline InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_StaticFields, ____emptyArray_5)); }
	inline InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>
struct  List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E, ____items_1)); }
	inline InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* get__items_1() const { return ____items_1; }
	inline InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E_StaticFields, ____emptyArray_5)); }
	inline InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InputFeatureUsageU5BU5D_tD0C2F52A32C5B649AB7127926A9A852ACC2A08D1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>
struct  InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>
struct  InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>
struct  InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<System.Single>
struct  InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<System.UInt32>
struct  InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<System.UInt32Enum>
struct  InputFeatureUsage_1_t6914D487EFC663AE00B3D7725DD123A5814507D6 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t6914D487EFC663AE00B3D7725DD123A5814507D6, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>
struct  InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>
struct  InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.XR.InputDevice
struct  InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E 
{
public:
	// System.UInt64 UnityEngine.XR.InputDevice::m_DeviceId
	uint64_t ___m_DeviceId_1;
	// System.Boolean UnityEngine.XR.InputDevice::m_Initialized
	bool ___m_Initialized_2;

public:
	inline static int32_t get_offset_of_m_DeviceId_1() { return static_cast<int32_t>(offsetof(InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E, ___m_DeviceId_1)); }
	inline uint64_t get_m_DeviceId_1() const { return ___m_DeviceId_1; }
	inline uint64_t* get_address_of_m_DeviceId_1() { return &___m_DeviceId_1; }
	inline void set_m_DeviceId_1(uint64_t value)
	{
		___m_DeviceId_1 = value;
	}

	inline static int32_t get_offset_of_m_Initialized_2() { return static_cast<int32_t>(offsetof(InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E, ___m_Initialized_2)); }
	inline bool get_m_Initialized_2() const { return ___m_Initialized_2; }
	inline bool* get_address_of_m_Initialized_2() { return &___m_Initialized_2; }
	inline void set_m_Initialized_2(bool value)
	{
		___m_Initialized_2 = value;
	}
};

struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.InputDevice::s_InputSubsystemCache
	List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * ___s_InputSubsystemCache_0;

public:
	inline static int32_t get_offset_of_s_InputSubsystemCache_0() { return static_cast<int32_t>(offsetof(InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_StaticFields, ___s_InputSubsystemCache_0)); }
	inline List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * get_s_InputSubsystemCache_0() const { return ___s_InputSubsystemCache_0; }
	inline List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 ** get_address_of_s_InputSubsystemCache_0() { return &___s_InputSubsystemCache_0; }
	inline void set_s_InputSubsystemCache_0(List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * value)
	{
		___s_InputSubsystemCache_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InputSubsystemCache_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_marshaled_pinvoke
{
	uint64_t ___m_DeviceId_1;
	int32_t ___m_Initialized_2;
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_marshaled_com
{
	uint64_t ___m_DeviceId_1;
	int32_t ___m_Initialized_2;
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Microsoft.MixedReality.Toolkit.Utilities.AxisType
struct  AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.InputDeviceCharacteristics
struct  InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64 
{
public:
	// System.UInt32 UnityEngine.XR.InputDeviceCharacteristics::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.InputFeatureType
struct  InputFeatureType_t3581EE01C178BF1CC9BAFE6443BEF6B0C0B2609C 
{
public:
	// System.UInt32 UnityEngine.XR.InputFeatureType::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputFeatureType_t3581EE01C178BF1CC9BAFE6443BEF6B0C0B2609C, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.InputTrackingState
struct  InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9 
{
public:
	// System.UInt32 UnityEngine.XR.InputTrackingState::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// System.Int32Enum
struct  Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType
struct  DisplayType_t5138A1FC756E211BEB18A33E11810DF3ECE48C81 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayType_t5138A1FC756E211BEB18A33E11810DF3ECE48C81, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.XR.InputFeatureUsage
struct  InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage::m_Name
	String_t* ___m_Name_0;
	// UnityEngine.XR.InputFeatureType UnityEngine.XR.InputFeatureUsage::m_InternalType
	uint32_t ___m_InternalType_1;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_InternalType_1() { return static_cast<int32_t>(offsetof(InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE, ___m_InternalType_1)); }
	inline uint32_t get_m_InternalType_1() const { return ___m_InternalType_1; }
	inline uint32_t* get_address_of_m_InternalType_1() { return &___m_InternalType_1; }
	inline void set_m_InternalType_1(uint32_t value)
	{
		___m_InternalType_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE_marshaled_pinvoke
{
	char* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>
struct  Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36, ___list_0)); }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * get_list_0() const { return ___list_0; }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36, ___current_3)); }
	inline InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  get_current_3() const { return ___current_3; }
	inline InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Name_0), (void*)NULL);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.TextMesh
struct  TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult
struct  DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::displayTextMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___displayTextMesh_4;
	// Microsoft.MixedReality.Toolkit.Utilities.AxisType Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::inputType
	int32_t ___inputType_5;
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::axisNumber
	int32_t ___axisNumber_6;
	// System.Int32 Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::buttonNumber
	int32_t ___buttonNumber_7;
	// Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult/DisplayType Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::displayType
	int32_t ___displayType_8;

public:
	inline static int32_t get_offset_of_displayTextMesh_4() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___displayTextMesh_4)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_displayTextMesh_4() const { return ___displayTextMesh_4; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_displayTextMesh_4() { return &___displayTextMesh_4; }
	inline void set_displayTextMesh_4(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___displayTextMesh_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayTextMesh_4), (void*)value);
	}

	inline static int32_t get_offset_of_inputType_5() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___inputType_5)); }
	inline int32_t get_inputType_5() const { return ___inputType_5; }
	inline int32_t* get_address_of_inputType_5() { return &___inputType_5; }
	inline void set_inputType_5(int32_t value)
	{
		___inputType_5 = value;
	}

	inline static int32_t get_offset_of_axisNumber_6() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___axisNumber_6)); }
	inline int32_t get_axisNumber_6() const { return ___axisNumber_6; }
	inline int32_t* get_address_of_axisNumber_6() { return &___axisNumber_6; }
	inline void set_axisNumber_6(int32_t value)
	{
		___axisNumber_6 = value;
	}

	inline static int32_t get_offset_of_buttonNumber_7() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___buttonNumber_7)); }
	inline int32_t get_buttonNumber_7() const { return ___buttonNumber_7; }
	inline int32_t* get_address_of_buttonNumber_7() { return &___buttonNumber_7; }
	inline void set_buttonNumber_7(int32_t value)
	{
		___buttonNumber_7 = value;
	}

	inline static int32_t get_offset_of_displayType_8() { return static_cast<int32_t>(offsetof(DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC, ___displayType_8)); }
	inline int32_t get_displayType_8() const { return ___displayType_8; }
	inline int32_t* get_address_of_displayType_8() { return &___displayType_8; }
	inline void set_displayType_8(int32_t value)
	{
		___displayType_8 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages
struct  ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::listInputDevicesTextMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___listInputDevicesTextMesh_4;
	// UnityEngine.TextMesh[] Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::displayFeatureUsagesTextMeshes
	TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* ___displayFeatureUsagesTextMeshes_5;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::controllerInputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___controllerInputDevices_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::handInputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___handInputDevices_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage> Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::featureUsages
	List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * ___featureUsages_8;

public:
	inline static int32_t get_offset_of_listInputDevicesTextMesh_4() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___listInputDevicesTextMesh_4)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_listInputDevicesTextMesh_4() const { return ___listInputDevicesTextMesh_4; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_listInputDevicesTextMesh_4() { return &___listInputDevicesTextMesh_4; }
	inline void set_listInputDevicesTextMesh_4(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___listInputDevicesTextMesh_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listInputDevicesTextMesh_4), (void*)value);
	}

	inline static int32_t get_offset_of_displayFeatureUsagesTextMeshes_5() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___displayFeatureUsagesTextMeshes_5)); }
	inline TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* get_displayFeatureUsagesTextMeshes_5() const { return ___displayFeatureUsagesTextMeshes_5; }
	inline TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981** get_address_of_displayFeatureUsagesTextMeshes_5() { return &___displayFeatureUsagesTextMeshes_5; }
	inline void set_displayFeatureUsagesTextMeshes_5(TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* value)
	{
		___displayFeatureUsagesTextMeshes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayFeatureUsagesTextMeshes_5), (void*)value);
	}

	inline static int32_t get_offset_of_controllerInputDevices_6() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___controllerInputDevices_6)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_controllerInputDevices_6() const { return ___controllerInputDevices_6; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_controllerInputDevices_6() { return &___controllerInputDevices_6; }
	inline void set_controllerInputDevices_6(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___controllerInputDevices_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controllerInputDevices_6), (void*)value);
	}

	inline static int32_t get_offset_of_handInputDevices_7() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___handInputDevices_7)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_handInputDevices_7() const { return ___handInputDevices_7; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_handInputDevices_7() { return &___handInputDevices_7; }
	inline void set_handInputDevices_7(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___handInputDevices_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handInputDevices_7), (void*)value);
	}

	inline static int32_t get_offset_of_featureUsages_8() { return static_cast<int32_t>(offsetof(ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E, ___featureUsages_8)); }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * get_featureUsages_8() const { return ___featureUsages_8; }
	inline List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E ** get_address_of_featureUsages_8() { return &___featureUsages_8; }
	inline void set_featureUsages_8(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * value)
	{
		___featureUsages_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___featureUsages_8), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * m_Items[1];

public:
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.XR.InputDevice[]
struct InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  m_Items[1];

public:
	inline InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  value)
	{
		m_Items[index] = value;
	}
};


// System.Boolean System.Enum::TryParse<System.Int32Enum>(System.String,!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enum_TryParse_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_m41610E72E4BC8727099F8EF765A76BBE6841ADAC_gshared (String_t* ___value0, int32_t* ___result1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Union<UnityEngine.XR.InputDevice>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915_gshared (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.XR.InputDevice>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_gshared_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_gshared_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36  List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD_gshared (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_gshared_inline (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.Boolean>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B_gshared (InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.UInt32>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E_gshared (InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.Single>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693_gshared (InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C_gshared (InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.UInt32Enum>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_m434C9140634FE21814DB3C9E6DED22B456EE4E42_gshared (InputFeatureUsage_1_t6914D487EFC663AE00B3D7725DD123A5814507D6 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B_gshared (InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465_gshared (InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49 * __this, String_t* ___usageName0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5_gshared (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1_gshared (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_gshared (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B_gshared (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * __this, const RuntimeMethod* method);

// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04 (TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.Enum::TryParse<UnityEngine.KeyCode>(System.String,!!0&)
inline bool Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2 (String_t* ___value0, int32_t* ___result1, const RuntimeMethod* method)
{
	return ((  bool (*) (String_t*, int32_t*, const RuntimeMethod*))Enum_TryParse_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_m41610E72E4BC8727099F8EF765A76BBE6841ADAC_gshared)(___value0, ___result1, method);
}
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429 (int32_t ___key0, const RuntimeMethod* method);
// System.String UnityEngine.TextMesh::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B (TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.String[] UnityEngine.Input::GetJoystickNames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Input_GetJoystickNames_mBD6976AEAFB4B2930FA2AB3215C0E8ED27A7938B (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputDevices::GetDevicesWithCharacteristics(UnityEngine.XR.InputDeviceCharacteristics,System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputDevices_GetDevicesWithCharacteristics_m8AED20982C7D099E74435E41F8705FAE70ADBDEC (uint32_t ___desiredCharacteristics0, List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___inputDevices1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Union<UnityEngine.XR.InputDevice>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
inline RuntimeObject* Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915 (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*))Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915_gshared)(___first0, ___second1, method);
}
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.XR.InputDevice>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB_gshared)(___source0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::get_Count()
inline int32_t List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F *, const RuntimeMethod*))List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_gshared_inline)(__this, method);
}
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::get_Item(System.Int32)
inline InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  (*) (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F *, int32_t, const RuntimeMethod*))List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_gshared_inline)(__this, ___index0, method);
}
// System.String UnityEngine.XR.InputDevice::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InputDevice_get_name_m5FA211BC67629D36A37753E4D2E524586B66CA89 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureUsages(System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureUsages_mAFFD78196DA1E75A265B427D9C70212382A3A465 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * ___featureUsages0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>::GetEnumerator()
inline Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36  List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36  (*) (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E *, const RuntimeMethod*))List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::get_Current()
inline InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_inline (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method)
{
	return ((  InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  (*) (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *, const RuntimeMethod*))Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_gshared_inline)(__this, method);
}
// System.String UnityEngine.XR.InputFeatureUsage::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988 (InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE * __this, const RuntimeMethod* method);
// System.Type UnityEngine.XR.InputFeatureUsage::get_type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957 (InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.Boolean>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B (InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<System.Boolean>,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_mF70AB4F4BC703E9242A757D0A6D84A4DA48BBBBE (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___usage0, bool* ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.UInt32>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E (InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<System.UInt32>,System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_mD813F6D528B7C8574AC1DF351C99A60FCCB89576 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23  ___usage0, uint32_t* ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<System.Single>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693 (InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<System.Single>,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_m531D0670B4FBDA6CD5B8E899627B70F3DB840E69 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1  ___usage0, float* ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C (InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>,UnityEngine.Vector2&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_m18C3DB44FFE8E7ACFF29F2F83E54A2F057AFF1C5 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625  ___usage0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_m44982DBFBF71598D4ADEE6632D78D390CEFA42D5 (InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_m434C9140634FE21814DB3C9E6DED22B456EE4E42_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>,UnityEngine.XR.InputTrackingState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_m64CA9CE33B12A117D903F11373D5A4B3BC479497 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65  ___usage0, uint32_t* ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B (InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_m9B40B8FAD5FED79E840DA7064A7A48FD9AA61C1A (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709  ___usage0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>::.ctor(System.String)
inline void InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465 (InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49 * __this, String_t* ___usageName0, const RuntimeMethod* method)
{
	((  void (*) (InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49 *, String_t*, const RuntimeMethod*))InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465_gshared)(__this, ___usageName0, method);
}
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>,UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_mD5DCBE5046B0EAF63A26EB4DF5AE46030C18F7A0 (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49  ___usage0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::MoveNext()
inline bool Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5 (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *, const RuntimeMethod*))Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.InputFeatureUsage>::Dispose()
inline void Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1 (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *, const RuntimeMethod*))Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>::.ctor()
inline void List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1 (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F *, const RuntimeMethod*))List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.InputFeatureUsage>::.ctor()
inline void List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E *, const RuntimeMethod*))List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B_gshared)(__this, method);
}
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::OnValidate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayInputResult_OnValidate_mE7EDD50BC3E850DD16163BE181F321D1AD2C2E11 (DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1653F9A5516D70F1640148CADCC97C572A62936F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3193468E3E53BE1E41883B70A00F57ABFA77BFA);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (displayType == DisplayType.InputAxes)
		int32_t L_0 = __this->get_displayType_8();
		if (L_0)
		{
			goto IL_0080;
		}
	}
	{
		// switch (inputType)
		int32_t L_1 = __this->get_inputType_5();
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0074;
			}
			case 1:
			{
				goto IL_0094;
			}
			case 2:
			{
				goto IL_004d;
			}
			case 3:
			{
				goto IL_0026;
			}
		}
	}
	{
		return;
	}

IL_0026:
	{
		// name = $"{inputType}{axisNumber}";
		int32_t L_3 = __this->get_inputType_5();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9_il2cpp_TypeInfo_var, &L_4);
		int32_t L_6 = __this->get_axisNumber_6();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE, L_5, L_8, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(__this, L_9, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_004d:
	{
		// name = $"{inputType}{buttonNumber}";
		int32_t L_10 = __this->get_inputType_5();
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(AxisType_tF1AA7514F988F041D5C2D43E641FA67B9BF7C6E9_il2cpp_TypeInfo_var, &L_11);
		int32_t L_13 = __this->get_buttonNumber_7();
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_14);
		String_t* L_16;
		L_16 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE, L_12, L_15, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(__this, L_16, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0074:
	{
		// name = "AllActiveAxes";
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(__this, _stringLiteralF3193468E3E53BE1E41883B70A00F57ABFA77BFA, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0080:
	{
		// else if (displayType == DisplayType.JoystickNames)
		int32_t L_17 = __this->get_displayType_8();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_0094;
		}
	}
	{
		// name = "JoystickNames";
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(__this, _stringLiteral1653F9A5516D70F1640148CADCC97C572A62936F, /*hidden argument*/NULL);
	}

IL_0094:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayInputResult_Update_m8D4CEDF6D2D78D917B732BB689BBC5F5AFCA819F (DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0C2FD7F14008DC5A9704A71B5926F425273F6DFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral10F4DE454A7A7EDC63FFA330FCC744864D606F50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB1273DCF301695744F96DE3A31C5DBDDB6DBB0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80E8E33CFC68B6519B4050770677835060BEB5E8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral87E261F2D88CAAEDCD99354542725097ED590502);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB69CD88CA44BBA6464A30CAFD46811605F0109DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB6CE6B89F14D219960135E95D3D3A31031318C1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD098894B7802A8D5E690D40161CCBD3A3B74AB08);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDAF2BD03270A51BF9C135F3694DCAD2D3E66F465);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	bool V_6 = false;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_7 = NULL;
	int32_t V_8 = 0;
	RuntimeObject * G_B22_0 = NULL;
	String_t* G_B22_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B22_2 = NULL;
	RuntimeObject * G_B21_0 = NULL;
	String_t* G_B21_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B21_2 = NULL;
	String_t* G_B23_0 = NULL;
	RuntimeObject * G_B23_1 = NULL;
	String_t* G_B23_2 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B23_3 = NULL;
	{
		// if (displayTextMesh == null)
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_0 = __this->get_displayTextMesh_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// if (displayType == DisplayType.InputAxes)
		int32_t L_2 = __this->get_displayType_8();
		if (L_2)
		{
			goto IL_0198;
		}
	}
	{
		// switch (inputType)
		int32_t L_3 = __this->get_inputType_5();
		V_1 = L_3;
		int32_t L_4 = V_1;
		switch (L_4)
		{
			case 0:
			{
				goto IL_00c5;
			}
			case 1:
			{
				goto IL_0203;
			}
			case 2:
			{
				goto IL_0078;
			}
			case 3:
			{
				goto IL_0038;
			}
		}
	}
	{
		return;
	}

IL_0038:
	{
		// displayTextMesh.text = $"Axis {axisNumber}: {UnityEngine.Input.GetAxis($"AXIS_{axisNumber}")}";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_5 = __this->get_displayTextMesh_4();
		int32_t L_6 = __this->get_axisNumber_6();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = __this->get_axisNumber_6();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_10);
		String_t* L_12;
		L_12 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral10F4DE454A7A7EDC63FFA330FCC744864D606F50, L_11, /*hidden argument*/NULL);
		float L_13;
		L_13 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_12, /*hidden argument*/NULL);
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		String_t* L_16;
		L_16 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralD098894B7802A8D5E690D40161CCBD3A3B74AB08, L_8, L_15, /*hidden argument*/NULL);
		NullCheck(L_5);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_5, L_16, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0078:
	{
		// if (Enum.TryParse($"JoystickButton{buttonNumber}", out KeyCode keyCode))
		int32_t L_17 = __this->get_buttonNumber_7();
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20;
		L_20 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralCB6CE6B89F14D219960135E95D3D3A31031318C1, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		bool L_21;
		L_21 = Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2(L_20, (int32_t*)(&V_0), /*hidden argument*/Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2_RuntimeMethod_var);
		if (!L_21)
		{
			goto IL_0203;
		}
	}
	{
		// displayTextMesh.text = $"Button {buttonNumber}: {UnityEngine.Input.GetKey(keyCode)}";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_22 = __this->get_displayTextMesh_4();
		int32_t L_23 = __this->get_buttonNumber_7();
		int32_t L_24 = L_23;
		RuntimeObject * L_25 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_24);
		int32_t L_26 = V_0;
		bool L_27;
		L_27 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_26, /*hidden argument*/NULL);
		bool L_28 = L_27;
		RuntimeObject * L_29 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_28);
		String_t* L_30;
		L_30 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralB69CD88CA44BBA6464A30CAFD46811605F0109DF, L_25, L_29, /*hidden argument*/NULL);
		NullCheck(L_22);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_22, L_30, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00c5:
	{
		// displayTextMesh.text = "All active:\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_31 = __this->get_displayTextMesh_4();
		NullCheck(L_31);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_31, _stringLiteral2FB1273DCF301695744F96DE3A31C5DBDDB6DBB0, /*hidden argument*/NULL);
		// for (int i = 1; i <= UnityInputAxisCount; i++)
		V_2 = 1;
		goto IL_012c;
	}

IL_00d9:
	{
		// float reading = UnityEngine.Input.GetAxis($"AXIS_{i}");
		int32_t L_32 = V_2;
		int32_t L_33 = L_32;
		RuntimeObject * L_34 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_33);
		String_t* L_35;
		L_35 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral10F4DE454A7A7EDC63FFA330FCC744864D606F50, L_34, /*hidden argument*/NULL);
		float L_36;
		L_36 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		// if (reading != 0.0)
		float L_37 = V_3;
		if ((((double)((double)((double)L_37))) == ((double)(0.0))))
		{
			goto IL_0128;
		}
	}
	{
		// displayTextMesh.text += $"Axis {i}: {reading}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_38 = __this->get_displayTextMesh_4();
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_39 = L_38;
		NullCheck(L_39);
		String_t* L_40;
		L_40 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_39, /*hidden argument*/NULL);
		int32_t L_41 = V_2;
		int32_t L_42 = L_41;
		RuntimeObject * L_43 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_42);
		float L_44 = V_3;
		float L_45 = L_44;
		RuntimeObject * L_46 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_45);
		String_t* L_47;
		L_47 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral87E261F2D88CAAEDCD99354542725097ED590502, L_43, L_46, /*hidden argument*/NULL);
		String_t* L_48;
		L_48 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_40, L_47, /*hidden argument*/NULL);
		NullCheck(L_39);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_39, L_48, /*hidden argument*/NULL);
	}

IL_0128:
	{
		// for (int i = 1; i <= UnityInputAxisCount; i++)
		int32_t L_49 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_012c:
	{
		// for (int i = 1; i <= UnityInputAxisCount; i++)
		int32_t L_50 = V_2;
		if ((((int32_t)L_50) <= ((int32_t)((int32_t)28))))
		{
			goto IL_00d9;
		}
	}
	{
		// for (int i = 0; i < UnityInputButtonCount; i++)
		V_4 = 0;
		goto IL_0191;
	}

IL_0136:
	{
		// if (Enum.TryParse($"JoystickButton{i}", out KeyCode buttonCode))
		int32_t L_51 = V_4;
		int32_t L_52 = L_51;
		RuntimeObject * L_53 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_52);
		String_t* L_54;
		L_54 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralCB6CE6B89F14D219960135E95D3D3A31031318C1, L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		bool L_55;
		L_55 = Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2(L_54, (int32_t*)(&V_5), /*hidden argument*/Enum_TryParse_TisKeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4_mE9447AD6FFC174131093B1E729A78A0891B3B3A2_RuntimeMethod_var);
		if (!L_55)
		{
			goto IL_018b;
		}
	}
	{
		// bool isPressed = UnityEngine.Input.GetKey(buttonCode);
		int32_t L_56 = V_5;
		bool L_57;
		L_57 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_56, /*hidden argument*/NULL);
		V_6 = L_57;
		// if (isPressed)
		bool L_58 = V_6;
		if (!L_58)
		{
			goto IL_018b;
		}
	}
	{
		// displayTextMesh.text += $"Button {i}: {isPressed}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_59 = __this->get_displayTextMesh_4();
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_60 = L_59;
		NullCheck(L_60);
		String_t* L_61;
		L_61 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_60, /*hidden argument*/NULL);
		int32_t L_62 = V_4;
		int32_t L_63 = L_62;
		RuntimeObject * L_64 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_63);
		bool L_65 = V_6;
		bool L_66 = L_65;
		RuntimeObject * L_67 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_66);
		String_t* L_68;
		L_68 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral80E8E33CFC68B6519B4050770677835060BEB5E8, L_64, L_67, /*hidden argument*/NULL);
		String_t* L_69;
		L_69 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_61, L_68, /*hidden argument*/NULL);
		NullCheck(L_60);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_60, L_69, /*hidden argument*/NULL);
	}

IL_018b:
	{
		// for (int i = 0; i < UnityInputButtonCount; i++)
		int32_t L_70 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_70, (int32_t)1));
	}

IL_0191:
	{
		// for (int i = 0; i < UnityInputButtonCount; i++)
		int32_t L_71 = V_4;
		if ((((int32_t)L_71) < ((int32_t)((int32_t)20))))
		{
			goto IL_0136;
		}
	}
	{
		// break;
		return;
	}

IL_0198:
	{
		// string[] joystickNames = UnityEngine.Input.GetJoystickNames();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_72;
		L_72 = Input_GetJoystickNames_mBD6976AEAFB4B2930FA2AB3215C0E8ED27A7938B(/*hidden argument*/NULL);
		V_7 = L_72;
		// displayTextMesh.text = $"Detected {joystickNames.Length} controller{(joystickNames.Length != 1 ? "s" : "")}:\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_73 = __this->get_displayTextMesh_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_74 = V_7;
		NullCheck(L_74);
		int32_t L_75 = ((int32_t)((int32_t)(((RuntimeArray*)L_74)->max_length)));
		RuntimeObject * L_76 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_75);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_77 = V_7;
		NullCheck(L_77);
		G_B21_0 = L_76;
		G_B21_1 = _stringLiteral0C2FD7F14008DC5A9704A71B5926F425273F6DFC;
		G_B21_2 = L_73;
		if ((!(((uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_77)->max_length)))) == ((uint32_t)1))))
		{
			G_B22_0 = L_76;
			G_B22_1 = _stringLiteral0C2FD7F14008DC5A9704A71B5926F425273F6DFC;
			G_B22_2 = L_73;
			goto IL_01c1;
		}
	}
	{
		G_B23_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		goto IL_01c6;
	}

IL_01c1:
	{
		G_B23_0 = _stringLiteralDAF2BD03270A51BF9C135F3694DCAD2D3E66F465;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
	}

IL_01c6:
	{
		String_t* L_78;
		L_78 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(G_B23_2, G_B23_1, G_B23_0, /*hidden argument*/NULL);
		NullCheck(G_B23_3);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B23_3, L_78, /*hidden argument*/NULL);
		// for (int i = 0; i < joystickNames.Length; i++)
		V_8 = 0;
		goto IL_01fb;
	}

IL_01d5:
	{
		// displayTextMesh.text += $"{joystickNames[i]}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_79 = __this->get_displayTextMesh_4();
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_80 = L_79;
		NullCheck(L_80);
		String_t* L_81;
		L_81 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_80, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_82 = V_7;
		int32_t L_83 = V_8;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		String_t* L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		String_t* L_86;
		L_86 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_81, L_85, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		NullCheck(L_80);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_80, L_86, /*hidden argument*/NULL);
		// for (int i = 0; i < joystickNames.Length; i++)
		int32_t L_87 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_87, (int32_t)1));
	}

IL_01fb:
	{
		// for (int i = 0; i < joystickNames.Length; i++)
		int32_t L_88 = V_8;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_89 = V_7;
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_89)->max_length))))))
		{
			goto IL_01d5;
		}
	}

IL_0203:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.DisplayInputResult::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayInputResult__ctor_m12D77C1F4B5939FA7DE5213BA396F09365ACC9D3 (DisplayInputResult_t028EA98F394CC83E4F98550128E488B041B38DFC * __this, const RuntimeMethod* method)
{
	{
		// private int axisNumber = 1;
		__this->set_axisNumber_6(1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListInputFeatureUsages_Update_mC0E120575E1F9C4316408558050A6E9FDFA4964E (ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_m44982DBFBF71598D4ADEE6632D78D390CEFA42D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBB47B58026352318D44CEC93932BDFB273FEAF1C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCA604168AB048809605F0545853F2D2C30960F1F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDAF2BD03270A51BF9C135F3694DCAD2D3E66F465);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * V_0 = NULL;
	int32_t V_1 = 0;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * V_2 = NULL;
	InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36  V_4;
	memset((&V_4), 0, sizeof(V_4));
	InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  V_5;
	memset((&V_5), 0, sizeof(V_5));
	bool V_6 = false;
	uint32_t V_7 = 0;
	float V_8 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_9;
	memset((&V_9), 0, sizeof(V_9));
	uint32_t V_10 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_11;
	memset((&V_11), 0, sizeof(V_11));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_12;
	memset((&V_12), 0, sizeof(V_12));
	int32_t V_13 = 0;
	InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  V_14;
	memset((&V_14), 0, sizeof(V_14));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	RuntimeObject * G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B7_2 = NULL;
	RuntimeObject * G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B4_2 = NULL;
	RuntimeObject * G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B6_2 = NULL;
	RuntimeObject * G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B5_2 = NULL;
	String_t* G_B8_0 = NULL;
	RuntimeObject * G_B8_1 = NULL;
	String_t* G_B8_2 = NULL;
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * G_B8_3 = NULL;
	{
		// if (listInputDevicesTextMesh == null || displayFeatureUsagesTextMeshes.Length == 0)
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_0 = __this->get_listInputDevicesTextMesh_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* L_2 = __this->get_displayFeatureUsagesTextMeshes_5();
		NullCheck(L_2);
		if ((((RuntimeArray*)L_2)->max_length))
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		// return;
		return;
	}

IL_0018:
	{
		// InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Controller, controllerInputDevices);
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_3 = __this->get_controllerInputDevices_6();
		InputDevices_GetDevicesWithCharacteristics_m8AED20982C7D099E74435E41F8705FAE70ADBDEC(((int32_t)64), L_3, /*hidden argument*/NULL);
		// InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.HandTracking, handInputDevices);
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_4 = __this->get_handInputDevices_7();
		InputDevices_GetDevicesWithCharacteristics_m8AED20982C7D099E74435E41F8705FAE70ADBDEC(8, L_4, /*hidden argument*/NULL);
		// List<InputDevice> inputDevices = controllerInputDevices.Union(handInputDevices).ToList();
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_5 = __this->get_controllerInputDevices_6();
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_6 = __this->get_handInputDevices_7();
		RuntimeObject* L_7;
		L_7 = Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915(L_5, L_6, /*hidden argument*/Enumerable_Union_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mA505D15244DE52B42806B481557FD68E4B336915_RuntimeMethod_var);
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_8;
		L_8 = Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB(L_7, /*hidden argument*/Enumerable_ToList_TisInputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_mEB4311C4CA17AFD04A0422C729036CC177D411EB_RuntimeMethod_var);
		V_0 = L_8;
		// listInputDevicesTextMesh.text = $"Detected {inputDevices.Count} input source{(inputDevices.Count > 1 ? "s:" : inputDevices.Count != 0 ? ":" : "s")}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_9 = __this->get_listInputDevicesTextMesh_4();
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline(L_10, /*hidden argument*/List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_12);
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline(L_14, /*hidden argument*/List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		G_B4_0 = L_13;
		G_B4_1 = _stringLiteralBB47B58026352318D44CEC93932BDFB273FEAF1C;
		G_B4_2 = L_9;
		if ((((int32_t)L_15) > ((int32_t)1)))
		{
			G_B7_0 = L_13;
			G_B7_1 = _stringLiteralBB47B58026352318D44CEC93932BDFB273FEAF1C;
			G_B7_2 = L_9;
			goto IL_007d;
		}
	}
	{
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17;
		L_17 = List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline(L_16, /*hidden argument*/List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = G_B4_2;
		if (L_17)
		{
			G_B6_0 = G_B4_0;
			G_B6_1 = G_B4_1;
			G_B6_2 = G_B4_2;
			goto IL_0076;
		}
	}
	{
		G_B8_0 = _stringLiteralDAF2BD03270A51BF9C135F3694DCAD2D3E66F465;
		G_B8_1 = G_B5_0;
		G_B8_2 = G_B5_1;
		G_B8_3 = G_B5_2;
		goto IL_0082;
	}

IL_0076:
	{
		G_B8_0 = _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_0082;
	}

IL_007d:
	{
		G_B8_0 = _stringLiteralCA604168AB048809605F0545853F2D2C30960F1F;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_0082:
	{
		String_t* L_18;
		L_18 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		NullCheck(G_B8_3);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(G_B8_3, L_18, /*hidden argument*/NULL);
		// for (int i = 0; i < displayFeatureUsagesTextMeshes.Length; i++)
		V_1 = 0;
		goto IL_03f5;
	}

IL_0093:
	{
		// TextMesh textMesh = displayFeatureUsagesTextMeshes[i];
		TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* L_19 = __this->get_displayFeatureUsagesTextMeshes_5();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_2 = L_22;
		// if (textMesh == null)
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_24;
		L_24 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_23, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_03f1;
		}
	}
	{
		// if (i >= inputDevices.Count && textMesh.text != string.Empty)
		int32_t L_25 = V_1;
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline(L_26, /*hidden argument*/List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_00d3;
		}
	}
	{
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_28 = V_2;
		NullCheck(L_28);
		String_t* L_29;
		L_29 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_28, /*hidden argument*/NULL);
		String_t* L_30 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		bool L_31;
		L_31 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_29, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00d3;
		}
	}
	{
		// textMesh.text = string.Empty;
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_32 = V_2;
		String_t* L_33 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_32);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_32, L_33, /*hidden argument*/NULL);
		// continue;
		goto IL_03f1;
	}

IL_00d3:
	{
		// InputDevice inputDevice = inputDevices[i];
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_34 = V_0;
		int32_t L_35 = V_1;
		NullCheck(L_34);
		InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  L_36;
		L_36 = List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_inline(L_34, L_35, /*hidden argument*/List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_RuntimeMethod_var);
		V_3 = L_36;
		// listInputDevicesTextMesh.text += $"{inputDevice.name}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_37 = __this->get_listInputDevicesTextMesh_4();
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_38 = L_37;
		NullCheck(L_38);
		String_t* L_39;
		L_39 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_38, /*hidden argument*/NULL);
		String_t* L_40;
		L_40 = InputDevice_get_name_m5FA211BC67629D36A37753E4D2E524586B66CA89((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), /*hidden argument*/NULL);
		String_t* L_41;
		L_41 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_39, L_40, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		NullCheck(L_38);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_38, L_41, /*hidden argument*/NULL);
		// textMesh.text = $"{inputDevice.name}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_42 = V_2;
		String_t* L_43;
		L_43 = InputDevice_get_name_m5FA211BC67629D36A37753E4D2E524586B66CA89((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), /*hidden argument*/NULL);
		String_t* L_44;
		L_44 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_43, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		NullCheck(L_42);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_42, L_44, /*hidden argument*/NULL);
		// if (inputDevice.TryGetFeatureUsages(featureUsages))
		List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * L_45 = __this->get_featureUsages_8();
		bool L_46;
		L_46 = InputDevice_TryGetFeatureUsages_mAFFD78196DA1E75A265B427D9C70212382A3A465((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_03f1;
		}
	}
	{
		// foreach (InputFeatureUsage inputFeatureUsage in featureUsages)
		List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * L_47 = __this->get_featureUsages_8();
		NullCheck(L_47);
		Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36  L_48;
		L_48 = List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD(L_47, /*hidden argument*/List_1_GetEnumerator_mFA4CE48B4A8D1A96EA78E971806AEB85765756BD_RuntimeMethod_var);
		V_4 = L_48;
	}

IL_0133:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03d5;
		}

IL_0138:
		{
			// foreach (InputFeatureUsage inputFeatureUsage in featureUsages)
			InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  L_49;
			L_49 = Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_inline((Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *)(&V_4), /*hidden argument*/Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_RuntimeMethod_var);
			V_5 = L_49;
			// textMesh.text += $"{inputFeatureUsage.name}";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_50 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_51 = L_50;
			NullCheck(L_51);
			String_t* L_52;
			L_52 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_51, /*hidden argument*/NULL);
			String_t* L_53;
			L_53 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			String_t* L_54;
			L_54 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_52, L_53, /*hidden argument*/NULL);
			NullCheck(L_51);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_51, L_54, /*hidden argument*/NULL);
			// if (inputFeatureUsage.type.Equals(typeof(bool)))
			Type_t * L_55;
			L_55 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_56 = { reinterpret_cast<intptr_t> (Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_57;
			L_57 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_56, /*hidden argument*/NULL);
			NullCheck(L_55);
			bool L_58;
			L_58 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_55, L_57);
			if (!L_58)
			{
				goto IL_01b2;
			}
		}

IL_0171:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<bool>(inputFeatureUsage.name), out bool data))
			String_t* L_59;
			L_59 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  L_60;
			memset((&L_60), 0, sizeof(L_60));
			InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B((&L_60), L_59, /*hidden argument*/InputFeatureUsage_1__ctor_mEE947872BF40C181B3612C9FDC0F63F61E05266B_RuntimeMethod_var);
			bool L_61;
			L_61 = InputDevice_TryGetFeatureValue_mF70AB4F4BC703E9242A757D0A6D84A4DA48BBBBE((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_60, (bool*)(&V_6), /*hidden argument*/NULL);
			if (!L_61)
			{
				goto IL_03d5;
			}
		}

IL_018b:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_62 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_63 = L_62;
			NullCheck(L_63);
			String_t* L_64;
			L_64 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_63, /*hidden argument*/NULL);
			bool L_65 = V_6;
			bool L_66 = L_65;
			RuntimeObject * L_67 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_66);
			String_t* L_68;
			L_68 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_67, /*hidden argument*/NULL);
			String_t* L_69;
			L_69 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_64, L_68, /*hidden argument*/NULL);
			NullCheck(L_63);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_63, L_69, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_01b2:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(uint)))
			Type_t * L_70;
			L_70 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_71 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_72;
			L_72 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_71, /*hidden argument*/NULL);
			NullCheck(L_70);
			bool L_73;
			L_73 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_70, L_72);
			if (!L_73)
			{
				goto IL_020b;
			}
		}

IL_01ca:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<uint>(inputFeatureUsage.name), out uint data))
			String_t* L_74;
			L_74 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_tD6146B581B25DEF11C50FB255ECE1F1F76067F23  L_75;
			memset((&L_75), 0, sizeof(L_75));
			InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E((&L_75), L_74, /*hidden argument*/InputFeatureUsage_1__ctor_mFA556ED010B117E44F21659A13788CB2F9F1198E_RuntimeMethod_var);
			bool L_76;
			L_76 = InputDevice_TryGetFeatureValue_mD813F6D528B7C8574AC1DF351C99A60FCCB89576((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_75, (uint32_t*)(&V_7), /*hidden argument*/NULL);
			if (!L_76)
			{
				goto IL_03d5;
			}
		}

IL_01e4:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_77 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_78 = L_77;
			NullCheck(L_78);
			String_t* L_79;
			L_79 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_78, /*hidden argument*/NULL);
			uint32_t L_80 = V_7;
			uint32_t L_81 = L_80;
			RuntimeObject * L_82 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_81);
			String_t* L_83;
			L_83 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_82, /*hidden argument*/NULL);
			String_t* L_84;
			L_84 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_79, L_83, /*hidden argument*/NULL);
			NullCheck(L_78);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_78, L_84, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_020b:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(float)))
			Type_t * L_85;
			L_85 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_86 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_87;
			L_87 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_86, /*hidden argument*/NULL);
			NullCheck(L_85);
			bool L_88;
			L_88 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_85, L_87);
			if (!L_88)
			{
				goto IL_0264;
			}
		}

IL_0223:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<float>(inputFeatureUsage.name), out float data))
			String_t* L_89;
			L_89 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t9525982C3C73085CB36503407750B9DE0E598BE1  L_90;
			memset((&L_90), 0, sizeof(L_90));
			InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693((&L_90), L_89, /*hidden argument*/InputFeatureUsage_1__ctor_mC5DB990E503E023D4A66AF4C4B2AAC9040D00693_RuntimeMethod_var);
			bool L_91;
			L_91 = InputDevice_TryGetFeatureValue_m531D0670B4FBDA6CD5B8E899627B70F3DB840E69((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_90, (float*)(&V_8), /*hidden argument*/NULL);
			if (!L_91)
			{
				goto IL_03d5;
			}
		}

IL_023d:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_92 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_93 = L_92;
			NullCheck(L_93);
			String_t* L_94;
			L_94 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_93, /*hidden argument*/NULL);
			float L_95 = V_8;
			float L_96 = L_95;
			RuntimeObject * L_97 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_96);
			String_t* L_98;
			L_98 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_97, /*hidden argument*/NULL);
			String_t* L_99;
			L_99 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_94, L_98, /*hidden argument*/NULL);
			NullCheck(L_93);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_93, L_99, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_0264:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(Vector2)))
			Type_t * L_100;
			L_100 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_101 = { reinterpret_cast<intptr_t> (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_102;
			L_102 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_101, /*hidden argument*/NULL);
			NullCheck(L_100);
			bool L_103;
			L_103 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_100, L_102);
			if (!L_103)
			{
				goto IL_02bd;
			}
		}

IL_027c:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<Vector2>(inputFeatureUsage.name), out Vector2 data))
			String_t* L_104;
			L_104 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t8BAF53459FF79264F0E3F7F9716191756AFAC625  L_105;
			memset((&L_105), 0, sizeof(L_105));
			InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C((&L_105), L_104, /*hidden argument*/InputFeatureUsage_1__ctor_m61F3D0F63039EBF07EF45EAF6ADD15EE734CC94C_RuntimeMethod_var);
			bool L_106;
			L_106 = InputDevice_TryGetFeatureValue_m18C3DB44FFE8E7ACFF29F2F83E54A2F057AFF1C5((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_105, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_9), /*hidden argument*/NULL);
			if (!L_106)
			{
				goto IL_03d5;
			}
		}

IL_0296:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_107 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_108 = L_107;
			NullCheck(L_108);
			String_t* L_109;
			L_109 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_108, /*hidden argument*/NULL);
			Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_110 = V_9;
			Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_111 = L_110;
			RuntimeObject * L_112 = Box(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var, &L_111);
			String_t* L_113;
			L_113 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_112, /*hidden argument*/NULL);
			String_t* L_114;
			L_114 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_109, L_113, /*hidden argument*/NULL);
			NullCheck(L_108);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_108, L_114, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_02bd:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(InputTrackingState)))
			Type_t * L_115;
			L_115 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_116 = { reinterpret_cast<intptr_t> (InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_117;
			L_117 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_116, /*hidden argument*/NULL);
			NullCheck(L_115);
			bool L_118;
			L_118 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_115, L_117);
			if (!L_118)
			{
				goto IL_0316;
			}
		}

IL_02d5:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<InputTrackingState>(inputFeatureUsage.name), out InputTrackingState data))
			String_t* L_119;
			L_119 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t6C373EE0FA4FD8646D31410FB0FD222C5C1E9E65  L_120;
			memset((&L_120), 0, sizeof(L_120));
			InputFeatureUsage_1__ctor_m44982DBFBF71598D4ADEE6632D78D390CEFA42D5((&L_120), L_119, /*hidden argument*/InputFeatureUsage_1__ctor_m44982DBFBF71598D4ADEE6632D78D390CEFA42D5_RuntimeMethod_var);
			bool L_121;
			L_121 = InputDevice_TryGetFeatureValue_m64CA9CE33B12A117D903F11373D5A4B3BC479497((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_120, (uint32_t*)(&V_10), /*hidden argument*/NULL);
			if (!L_121)
			{
				goto IL_03d5;
			}
		}

IL_02ef:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_122 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_123 = L_122;
			NullCheck(L_123);
			String_t* L_124;
			L_124 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_123, /*hidden argument*/NULL);
			uint32_t L_125 = V_10;
			uint32_t L_126 = L_125;
			RuntimeObject * L_127 = Box(InputTrackingState_t787D19F40F78D57D589D01C27945FD614A426DA9_il2cpp_TypeInfo_var, &L_126);
			String_t* L_128;
			L_128 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_127, /*hidden argument*/NULL);
			String_t* L_129;
			L_129 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_124, L_128, /*hidden argument*/NULL);
			NullCheck(L_123);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_123, L_129, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_0316:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(Vector3)))
			Type_t * L_130;
			L_130 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_131 = { reinterpret_cast<intptr_t> (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_132;
			L_132 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_131, /*hidden argument*/NULL);
			NullCheck(L_130);
			bool L_133;
			L_133 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_130, L_132);
			if (!L_133)
			{
				goto IL_036c;
			}
		}

IL_032e:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<Vector3>(inputFeatureUsage.name), out Vector3 data))
			String_t* L_134;
			L_134 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t2E7E3FD2C721D53BE7A1B809921F9476185C8709  L_135;
			memset((&L_135), 0, sizeof(L_135));
			InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B((&L_135), L_134, /*hidden argument*/InputFeatureUsage_1__ctor_mB7291B69529D3D20070F308210E0AEC1AC3FA77B_RuntimeMethod_var);
			bool L_136;
			L_136 = InputDevice_TryGetFeatureValue_m9B40B8FAD5FED79E840DA7064A7A48FD9AA61C1A((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_135, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
			if (!L_136)
			{
				goto IL_03d5;
			}
		}

IL_0348:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_137 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_138 = L_137;
			NullCheck(L_138);
			String_t* L_139;
			L_139 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_138, /*hidden argument*/NULL);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_140 = V_11;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_141 = L_140;
			RuntimeObject * L_142 = Box(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var, &L_141);
			String_t* L_143;
			L_143 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_142, /*hidden argument*/NULL);
			String_t* L_144;
			L_144 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_139, L_143, /*hidden argument*/NULL);
			NullCheck(L_138);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_138, L_144, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_036c:
		{
			// else if (inputFeatureUsage.type.Equals(typeof(Quaternion)))
			Type_t * L_145;
			L_145 = InputFeatureUsage_get_type_m5F338C5941174F8FCB6B9A494C0F109D2EB8D957((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_146 = { reinterpret_cast<intptr_t> (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_147;
			L_147 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_146, /*hidden argument*/NULL);
			NullCheck(L_145);
			bool L_148;
			L_148 = VirtFuncInvoker1< bool, Type_t * >::Invoke(118 /* System.Boolean System.Type::Equals(System.Type) */, L_145, L_147);
			if (!L_148)
			{
				goto IL_03bf;
			}
		}

IL_0384:
		{
			// if (inputDevice.TryGetFeatureValue(new InputFeatureUsage<Quaternion>(inputFeatureUsage.name), out Quaternion data))
			String_t* L_149;
			L_149 = InputFeatureUsage_get_name_m08FB0101027B503D080EE38F4273BB61C59E3988((InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE *)(&V_5), /*hidden argument*/NULL);
			InputFeatureUsage_1_t152DE78832E6E5157647309AA0BF7CFC75F44A49  L_150;
			memset((&L_150), 0, sizeof(L_150));
			InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465((&L_150), L_149, /*hidden argument*/InputFeatureUsage_1__ctor_mFA680A87265CD87295096C040D21180ABD15E465_RuntimeMethod_var);
			bool L_151;
			L_151 = InputDevice_TryGetFeatureValue_mD5DCBE5046B0EAF63A26EB4DF5AE46030C18F7A0((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_3), L_150, (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_12), /*hidden argument*/NULL);
			if (!L_151)
			{
				goto IL_03d5;
			}
		}

IL_039b:
		{
			// textMesh.text += $": {data}\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_152 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_153 = L_152;
			NullCheck(L_153);
			String_t* L_154;
			L_154 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_153, /*hidden argument*/NULL);
			Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_155 = V_12;
			Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_156 = L_155;
			RuntimeObject * L_157 = Box(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var, &L_156);
			String_t* L_158;
			L_158 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral483E1DEC34B9D5362E7384C9F67FEBDC4970498B, L_157, /*hidden argument*/NULL);
			String_t* L_159;
			L_159 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_154, L_158, /*hidden argument*/NULL);
			NullCheck(L_153);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_153, L_159, /*hidden argument*/NULL);
			// }
			goto IL_03d5;
		}

IL_03bf:
		{
			// textMesh.text += $"\n";
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_160 = V_2;
			TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_161 = L_160;
			NullCheck(L_161);
			String_t* L_162;
			L_162 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_161, /*hidden argument*/NULL);
			String_t* L_163;
			L_163 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_162, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
			NullCheck(L_161);
			TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_161, L_163, /*hidden argument*/NULL);
		}

IL_03d5:
		{
			// foreach (InputFeatureUsage inputFeatureUsage in featureUsages)
			bool L_164;
			L_164 = Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5((Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_mA882A1C259A1174649DF9F78391F8036732949C5_RuntimeMethod_var);
			if (L_164)
			{
				goto IL_0138;
			}
		}

IL_03e1:
		{
			IL2CPP_LEAVE(0x3F1, FINALLY_03e3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_03e3;
	}

FINALLY_03e3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1((Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 *)(&V_4), /*hidden argument*/Enumerator_Dispose_mCD2C1136622529F7FA6ABA47BB92A436E1E896B1_RuntimeMethod_var);
		IL2CPP_END_FINALLY(995)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(995)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3F1, IL_03f1)
	}

IL_03f1:
	{
		// for (int i = 0; i < displayFeatureUsagesTextMeshes.Length; i++)
		int32_t L_165 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_165, (int32_t)1));
	}

IL_03f5:
	{
		// for (int i = 0; i < displayFeatureUsagesTextMeshes.Length; i++)
		int32_t L_166 = V_1;
		TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* L_167 = __this->get_displayFeatureUsagesTextMeshes_5();
		NullCheck(L_167);
		if ((((int32_t)L_166) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_167)->max_length))))))
		{
			goto IL_0093;
		}
	}
	{
		// for (int i = displayFeatureUsagesTextMeshes.Length; i < inputDevices.Count; i++)
		TextMeshU5BU5D_tBC85515B64AEF3A0DDB6BCE7C3686F6FB86A1981* L_168 = __this->get_displayFeatureUsagesTextMeshes_5();
		NullCheck(L_168);
		V_13 = ((int32_t)((int32_t)(((RuntimeArray*)L_168)->max_length)));
		goto IL_0441;
	}

IL_040f:
	{
		// listInputDevicesTextMesh.text += $"{inputDevices[i].name}\n";
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_169 = __this->get_listInputDevicesTextMesh_4();
		TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * L_170 = L_169;
		NullCheck(L_170);
		String_t* L_171;
		L_171 = TextMesh_get_text_m5F0AFB132AB91B91B04386F5769F2A2F04C2A13B(L_170, /*hidden argument*/NULL);
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_172 = V_0;
		int32_t L_173 = V_13;
		NullCheck(L_172);
		InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  L_174;
		L_174 = List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_inline(L_172, L_173, /*hidden argument*/List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_RuntimeMethod_var);
		V_14 = L_174;
		String_t* L_175;
		L_175 = InputDevice_get_name_m5FA211BC67629D36A37753E4D2E524586B66CA89((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&V_14), /*hidden argument*/NULL);
		String_t* L_176;
		L_176 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_171, L_175, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		NullCheck(L_170);
		TextMesh_set_text_m5879B13F5C9E4A1D05155839B89CCDB85BE28A04(L_170, L_176, /*hidden argument*/NULL);
		// for (int i = displayFeatureUsagesTextMeshes.Length; i < inputDevices.Count; i++)
		int32_t L_177 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_177, (int32_t)1));
	}

IL_0441:
	{
		// for (int i = displayFeatureUsagesTextMeshes.Length; i < inputDevices.Count; i++)
		int32_t L_178 = V_13;
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_179 = V_0;
		NullCheck(L_179);
		int32_t L_180;
		L_180 = List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_inline(L_179, /*hidden argument*/List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_RuntimeMethod_var);
		if ((((int32_t)L_178) < ((int32_t)L_180)))
		{
			goto IL_040f;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.Tools.Runtime.ListInputFeatureUsages::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListInputFeatureUsages__ctor_mCE9C6B5EFA9966396CF688348915A88F115AF50E (ListInputFeatureUsages_t9634FFEFBDF8001B839AF14CB43D461390B50D6E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private readonly List<InputDevice> controllerInputDevices = new List<InputDevice>();
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_0 = (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F *)il2cpp_codegen_object_new(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_il2cpp_TypeInfo_var);
		List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1(L_0, /*hidden argument*/List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_RuntimeMethod_var);
		__this->set_controllerInputDevices_6(L_0);
		// private readonly List<InputDevice> handInputDevices = new List<InputDevice>();
		List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * L_1 = (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F *)il2cpp_codegen_object_new(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F_il2cpp_TypeInfo_var);
		List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1(L_1, /*hidden argument*/List_1__ctor_m301E51804C88F4CDA1665B126A03BC15123C2BB1_RuntimeMethod_var);
		__this->set_handInputDevices_7(L_1);
		// private readonly List<InputFeatureUsage> featureUsages = new List<InputFeatureUsage>();
		List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E * L_2 = (List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E *)il2cpp_codegen_object_new(List_1_tB4FF4F53752C11CCDC20B9E54FB3B807278F134E_il2cpp_TypeInfo_var);
		List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B(L_2, /*hidden argument*/List_1__ctor_mB0147DAC8F87F25B260B66D4E1DF950682CADC3B_RuntimeMethod_var);
		__this->set_featureUsages_8(L_2);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m946FE168DA67B05C55F5B0D2654FE2B69BA3A242_gshared_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  List_1_get_Item_m69B586C86E7A415FDE5732E94E28549EAAE35DD7_gshared_inline (List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE* L_2 = (InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((InputDeviceU5BU5D_t1D4B7FC58B0B757E4DFEC85259EF3FDE297ECACE*)L_2, (int32_t)L_3);
		return (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  Enumerator_get_Current_mC62382A263C7901E5A2D8803FAA3FFCAE647167D_gshared_inline (Enumerator_tAA33D3F894D1F8A9565DC8DE9A34115723C2BD36 * __this, const RuntimeMethod* method)
{
	{
		InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE  L_0 = (InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE )__this->get_current_3();
		return (InputFeatureUsage_tB971D811B38B1DA549F529BB15E60672940FB0EE )L_0;
	}
}
