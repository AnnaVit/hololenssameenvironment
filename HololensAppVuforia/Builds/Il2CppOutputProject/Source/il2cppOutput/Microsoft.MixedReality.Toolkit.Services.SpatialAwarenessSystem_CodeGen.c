﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile)
extern void MixedRealitySpatialAwarenessSystem__ctor_m20A195B5DE22C4D28D7C2FD886EDF0AE7ABED408 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.ctor(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile)
extern void MixedRealitySpatialAwarenessSystem__ctor_mEA51434A97AC57F724C99A38F054E6318E611FA1 (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_Name()
extern void MixedRealitySpatialAwarenessSystem_get_Name_m6408D6FA45864E3FBCA4BE8A258AD5A4D310371B (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::set_Name(System.String)
extern void MixedRealitySpatialAwarenessSystem_set_Name_mCF69EE323D1F0283F2367E971DD82CE03CC0B619 (void);
// 0x00000005 System.Boolean Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void MixedRealitySpatialAwarenessSystem_CheckCapability_m6A354C0F41EF88B043DB441FFE645C5014864E50 (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Initialize()
extern void MixedRealitySpatialAwarenessSystem_Initialize_m1B31890BF418C45D7C0547A5D0BBA107CEC8F558 (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::InitializeInternal()
extern void MixedRealitySpatialAwarenessSystem_InitializeInternal_m4141CCF85BCDAA7B9DB26CD1336FB0592177A680 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Disable()
extern void MixedRealitySpatialAwarenessSystem_Disable_m76CF0E2F8B8A0185BD81217D763C52945758217A (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Enable()
extern void MixedRealitySpatialAwarenessSystem_Enable_mC9667F8E42734E91BBCF902591C4BC33FF386402 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Reset()
extern void MixedRealitySpatialAwarenessSystem_Reset_mE775F75CE941964A1B56097323B06DE42118AF56 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Destroy()
extern void MixedRealitySpatialAwarenessSystem_Destroy_m9CFF7ECE7BF07DED8F48A01502FFC36FF5997BC1 (void);
// 0x0000000C UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessObjectParent()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m1AB1E30961DD0D548C29DDB7722CDE6F4A7841B8 (void);
// 0x0000000D UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_CreateSpatialAwarenessObjectParent()
extern void MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessObjectParent_m7E4C431AB01CDAF859C4CB3962651DBC19D38401 (void);
// 0x0000000E UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::CreateSpatialAwarenessObservationParent(System.String)
extern void MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObservationParent_m2382DBA61D49D67181D5F521F21B87F4D8242B16 (void);
// 0x0000000F System.UInt32 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GenerateNewSourceId()
extern void MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_m79D425705E04A8A56144926D1D13545E5805A997 (void);
// 0x00000010 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessSystemProfile()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m4A6EB6E88F08301DD0B56F29BBB062CDE81A3153 (void);
// 0x00000011 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
extern void MixedRealitySpatialAwarenessSystem_GetObservers_m49489ACFA818C2EEFCD5EC578D23DD43ED76D62E (void);
// 0x00000012 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
// 0x00000013 Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
extern void MixedRealitySpatialAwarenessSystem_GetObserver_m6AC6ADC03BC1829C3D48A0629CB3D6F2BC58A6FC (void);
// 0x00000014 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
// 0x00000015 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProviders()
// 0x00000016 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProvider(System.String)
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
extern void MixedRealitySpatialAwarenessSystem_ResumeObservers_m8AC9FF9D20B47F68A13F7E9216DD52B10AB5C573 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserver(System.String)
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
extern void MixedRealitySpatialAwarenessSystem_SuspendObservers_m5B14090BCF7C77A73A021E8744742FAB4E469192 (void);
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserver(System.String)
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservations()
extern void MixedRealitySpatialAwarenessSystem_ClearObservations_mF46C745D65B9E5E58439A75B613ABE79B66C6D20 (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservations(System.String)
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.cctor()
extern void MixedRealitySpatialAwarenessSystem__cctor_m2DB3C30445F55BB1D6F1C5334FA9E04EA738C829 (void);
static Il2CppMethodPointer s_methodPointers[31] = 
{
	MixedRealitySpatialAwarenessSystem__ctor_m20A195B5DE22C4D28D7C2FD886EDF0AE7ABED408,
	MixedRealitySpatialAwarenessSystem__ctor_mEA51434A97AC57F724C99A38F054E6318E611FA1,
	MixedRealitySpatialAwarenessSystem_get_Name_m6408D6FA45864E3FBCA4BE8A258AD5A4D310371B,
	MixedRealitySpatialAwarenessSystem_set_Name_mCF69EE323D1F0283F2367E971DD82CE03CC0B619,
	MixedRealitySpatialAwarenessSystem_CheckCapability_m6A354C0F41EF88B043DB441FFE645C5014864E50,
	MixedRealitySpatialAwarenessSystem_Initialize_m1B31890BF418C45D7C0547A5D0BBA107CEC8F558,
	MixedRealitySpatialAwarenessSystem_InitializeInternal_m4141CCF85BCDAA7B9DB26CD1336FB0592177A680,
	MixedRealitySpatialAwarenessSystem_Disable_m76CF0E2F8B8A0185BD81217D763C52945758217A,
	MixedRealitySpatialAwarenessSystem_Enable_mC9667F8E42734E91BBCF902591C4BC33FF386402,
	MixedRealitySpatialAwarenessSystem_Reset_mE775F75CE941964A1B56097323B06DE42118AF56,
	MixedRealitySpatialAwarenessSystem_Destroy_m9CFF7ECE7BF07DED8F48A01502FFC36FF5997BC1,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m1AB1E30961DD0D548C29DDB7722CDE6F4A7841B8,
	MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessObjectParent_m7E4C431AB01CDAF859C4CB3962651DBC19D38401,
	MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObservationParent_m2382DBA61D49D67181D5F521F21B87F4D8242B16,
	MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_m79D425705E04A8A56144926D1D13545E5805A997,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m4A6EB6E88F08301DD0B56F29BBB062CDE81A3153,
	MixedRealitySpatialAwarenessSystem_GetObservers_m49489ACFA818C2EEFCD5EC578D23DD43ED76D62E,
	NULL,
	MixedRealitySpatialAwarenessSystem_GetObserver_m6AC6ADC03BC1829C3D48A0629CB3D6F2BC58A6FC,
	NULL,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_ResumeObservers_m8AC9FF9D20B47F68A13F7E9216DD52B10AB5C573,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_SuspendObservers_m5B14090BCF7C77A73A021E8744742FAB4E469192,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_ClearObservations_mF46C745D65B9E5E58439A75B613ABE79B66C6D20,
	NULL,
	MixedRealitySpatialAwarenessSystem__cctor_m2DB3C30445F55BB1D6F1C5334FA9E04EA738C829,
};
static const int32_t s_InvokerIndices[31] = 
{
	2172,
	3635,
	4326,
	3635,
	3144,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4326,
	4326,
	2846,
	4295,
	4326,
	4326,
	-1,
	2846,
	-1,
	-1,
	-1,
	4406,
	-1,
	-1,
	4406,
	-1,
	-1,
	4406,
	-1,
	6303,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x06000012, { 0, 1 } },
	{ 0x06000014, { 1, 1 } },
	{ 0x06000015, { 2, 2 } },
	{ 0x06000016, { 4, 2 } },
	{ 0x06000018, { 6, 1 } },
	{ 0x06000019, { 7, 1 } },
	{ 0x0600001B, { 8, 1 } },
	{ 0x0600001C, { 9, 1 } },
	{ 0x0600001E, { 10, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[12] = 
{
	{ (Il2CppRGCTXDataType)3, 48870 },
	{ (Il2CppRGCTXDataType)3, 48865 },
	{ (Il2CppRGCTXDataType)1, 331 },
	{ (Il2CppRGCTXDataType)3, 48869 },
	{ (Il2CppRGCTXDataType)1, 338 },
	{ (Il2CppRGCTXDataType)3, 48864 },
	{ (Il2CppRGCTXDataType)2, 335 },
	{ (Il2CppRGCTXDataType)2, 334 },
	{ (Il2CppRGCTXDataType)2, 337 },
	{ (Il2CppRGCTXDataType)2, 336 },
	{ (Il2CppRGCTXDataType)3, 50277 },
	{ (Il2CppRGCTXDataType)2, 333 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystem_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystem_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.SpatialAwarenessSystem.dll",
	31,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	12,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
