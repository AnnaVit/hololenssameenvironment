﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class FixPositionTarget : MonoBehaviour
{
    [SerializeField]
    private GameObject hologram;
    // Update is called once per frame
    void Update()
    {
        hologram.transform.localPosition = this.transform.parent.position;

        if (this.transform.parent.GetComponent<TrackableBehaviour>().CurrentStatus != TrackableBehaviour.Status.TRACKED)
        {
            hologram.SetActive(false);
        }
       else if (this.transform.parent.GetComponent<TrackableBehaviour>().CurrentStatus == TrackableBehaviour.Status.TRACKED)
        {
            hologram.SetActive(true);
        }
    }
}
