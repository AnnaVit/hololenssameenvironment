﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread::GetAwaiter()
extern void WaitForBackgroundThread_GetAwaiter_m1426512C58FF671DC2A1A2944EFC6A8B12A29F30 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread::.ctor()
extern void WaitForBackgroundThread__ctor_mFDCA225DD275058EA0166A1E49B13D32BFFE2C7C (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::.cctor()
extern void U3CU3Ec__cctor_m2EAF89ECC182B39F674957325455514D2B68AE9A (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::.ctor()
extern void U3CU3Ec__ctor_m50A6D3CC00C48AA989DD82DAAA28F2733DF090E6 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::<GetAwaiter>b__0_0()
extern void U3CU3Ec_U3CGetAwaiterU3Eb__0_0_m0676A57D95CFCE31AFAE2B4644159C99573A70AC (void);
// 0x00000006 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate::get_keepWaiting()
extern void WaitForUpdate_get_keepWaiting_m187BDF1528B1D934777E51A8EA49D38D3D3B7C8B (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate::.ctor()
extern void WaitForUpdate__ctor_m56726EC61884A9C5F6A71CE6696A324089315ECE (void);
// 0x00000008 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForSeconds)
extern void AwaiterExtensions_GetAwaiter_m512F9096DDFF479C59DAC8A1511D36264905D0AF (void);
// 0x00000009 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate)
extern void AwaiterExtensions_GetAwaiter_m0142AF0B04F8A74FD491B425CD09CB9212968234 (void);
// 0x0000000A Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForEndOfFrame)
extern void AwaiterExtensions_GetAwaiter_m8C5F4FB79A7484D3BA83243D7FC481B3F02B7DE9 (void);
// 0x0000000B Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForFixedUpdate)
extern void AwaiterExtensions_GetAwaiter_m9DDB4B63B036094F2D75508AB4A009EE23B9357F (void);
// 0x0000000C Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForSecondsRealtime)
extern void AwaiterExtensions_GetAwaiter_m33994726D0B802DAC589974E38BBECF6D0EDD60F (void);
// 0x0000000D Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitUntil)
extern void AwaiterExtensions_GetAwaiter_m4406C7241628C4BE0138CC9A4919876AAC7378E0 (void);
// 0x0000000E Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitWhile)
extern void AwaiterExtensions_GetAwaiter_mF7265561C482417D743CA6E279F8EED4C907C1C2 (void);
// 0x0000000F Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AsyncOperation> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AsyncOperation)
extern void AwaiterExtensions_GetAwaiter_m9A2BBDD2550F6EB54E20DA74275DBF7F97F87B47 (void);
// 0x00000010 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.ResourceRequest)
extern void AwaiterExtensions_GetAwaiter_mC18898EAA13A71D8BFDC992B59B58EB8648FB56D (void);
// 0x00000011 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AssetBundleCreateRequest)
extern void AwaiterExtensions_GetAwaiter_m265357C8F5D41F54F5BF90E700F8450BE91DA3DA (void);
// 0x00000012 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AssetBundleRequest)
extern void AwaiterExtensions_GetAwaiter_m1B8FF1ECF199945C1FDBF420D49BAEE97996F305 (void);
// 0x00000013 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(System.Collections.Generic.IEnumerator`1<T>)
// 0x00000014 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<System.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(System.Collections.IEnumerator)
extern void AwaiterExtensions_GetAwaiter_m9F8A57146B37CBEED99DDB67918DEC32373342A8 (void);
// 0x00000015 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiterReturnVoid(System.Object)
extern void AwaiterExtensions_GetAwaiterReturnVoid_m89DEA4576B6008008CD61243B5483A76898F70CE (void);
// 0x00000016 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiterReturnSelf(T)
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::RunOnUnityScheduler(System.Action)
extern void AwaiterExtensions_RunOnUnityScheduler_m399A3338345E2E9F80BB9C329096CDDFA022ACAF (void);
// 0x00000018 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::get_IsCompleted()
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::set_IsCompleted(System.Boolean)
// 0x0000001A T Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::GetResult()
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::Complete(T,System.Exception)
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::.ctor()
// 0x0000001E System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::get_IsCompleted()
extern void SimpleCoroutineAwaiter_get_IsCompleted_m23F52130D905F5F3C9B35E7E454C90C278E248F0 (void);
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::set_IsCompleted(System.Boolean)
extern void SimpleCoroutineAwaiter_set_IsCompleted_m9C4C0FC64913B415CB4DBD1921FBB941D94CA29D (void);
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::GetResult()
extern void SimpleCoroutineAwaiter_GetResult_mC52BCE2DF0EF8C5552604A5E6C22BEDD1CB5AAFA (void);
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::Complete(System.Exception)
extern void SimpleCoroutineAwaiter_Complete_m3DC6F24EFC07E1783CD167C9AEF4CBC747149AF9 (void);
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
extern void SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_m571BFC4F21C92C0087A2CF3D92F03906AD8BD677 (void);
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::.ctor()
extern void SimpleCoroutineAwaiter__ctor_mBB2FF5FA9AE5A9BF6C0EEB3D342AE00C366E2A56 (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::.ctor(System.Collections.IEnumerator,Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T>)
// 0x00000025 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::Run()
// 0x00000026 System.String Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::GenerateObjectTraceMessage(System.Collections.Generic.List`1<System.Type>)
// 0x00000027 System.Collections.Generic.List`1<System.Type> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::GenerateObjectTrace(System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerator>)
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::.ctor(System.Int32)
// 0x00000029 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.IDisposable.Dispose()
// 0x0000002A System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::MoveNext()
// 0x0000002B System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x0000002C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.IEnumerator.Reset()
// 0x0000002D System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.IEnumerator.get_Current()
// 0x0000002E System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ReturnVoid(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter,System.Object)
extern void InstructionWrappers_ReturnVoid_mD022779C07C51FDC74018F5B660873966AC47480 (void);
// 0x0000002F System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::AssetBundleCreateRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle>,UnityEngine.AssetBundleCreateRequest)
extern void InstructionWrappers_AssetBundleCreateRequest_mA0E069DFF0C300ACEBAC6EA3232F3CA55679EF0A (void);
// 0x00000030 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ReturnSelf(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T>,T)
// 0x00000031 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::AssetBundleRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.AssetBundleRequest)
extern void InstructionWrappers_AssetBundleRequest_mA18579E5D6C4DCC87C7F07708E66A8CFAD8D99DA (void);
// 0x00000032 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ResourceRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.ResourceRequest)
extern void InstructionWrappers_ResourceRequest_mCE171462253C4E540E4402CBB1823D5AA72CDFA4 (void);
// 0x00000033 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::.ctor(System.Int32)
extern void U3CReturnVoidU3Ed__0__ctor_m6FEAAA49A381416674CA9B85F4E55EF115E7BF77 (void);
// 0x00000034 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.IDisposable.Dispose()
extern void U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m27CAB859E8D06AAEC599A7202319B64148665A45 (void);
// 0x00000035 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::MoveNext()
extern void U3CReturnVoidU3Ed__0_MoveNext_m060EECC4B14C6A86805F45E7258EFA509A43F680 (void);
// 0x00000036 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1B69654B4C1CA7D1B2D9ABFF487CFBD7F849DB (void);
// 0x00000037 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.IEnumerator.Reset()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mA2F833C1707C9E403E13986B7D0A8A7DDE55CB9E (void);
// 0x00000038 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m724B5FD67B9479605BFF0BFCF343B4B643841C2A (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::.ctor(System.Int32)
extern void U3CAssetBundleCreateRequestU3Ed__1__ctor_m8DBD580C5433983031595374937DE8576FF08D6E (void);
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.IDisposable.Dispose()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m94E54716364CEBAFE63E70C9EA25613D6D99FC93 (void);
// 0x0000003B System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::MoveNext()
extern void U3CAssetBundleCreateRequestU3Ed__1_MoveNext_m2DACFA7EC5B789F2CA35A946EC5031C270F569F9 (void);
// 0x0000003C System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8127002C1248A845A231700D2B8478E8B7BB2D57 (void);
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mCF94D1D05CA888D1A87E756341AC2C3730894F1E (void);
// 0x0000003E System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m28D0284032AB784807EEFD0A385005667BA185E0 (void);
// 0x0000003F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::.ctor(System.Int32)
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.IDisposable.Dispose()
// 0x00000041 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::MoveNext()
// 0x00000042 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000044 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000045 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::.ctor(System.Int32)
extern void U3CAssetBundleRequestU3Ed__3__ctor_m5BF5419E18224E3BB05C8822D69C9DEE9F604637 (void);
// 0x00000046 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.IDisposable.Dispose()
extern void U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_mFF55C5A68DEADCD98AB648D732B1924E9E72F621 (void);
// 0x00000047 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::MoveNext()
extern void U3CAssetBundleRequestU3Ed__3_MoveNext_m54186CC684C06AB3EE0F3578F55C5D74187555DA (void);
// 0x00000048 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D2CDEE5A6F7DC7AF28623546CA6E40B03C82DD8 (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_mDB8C815A5A1A4A3C398522C110FE3BB83A261DD0 (void);
// 0x0000004A System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m44C9F7B1CA14F65178A60EE1FF9C992F1BB12877 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::.ctor(System.Int32)
extern void U3CResourceRequestU3Ed__4__ctor_m8AB4E0721573E50A5E0A4209C8D4A70C5E9A577D (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.IDisposable.Dispose()
extern void U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m04064ABC86F37919780BB4F73BF1F591C26FD441 (void);
// 0x0000004D System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::MoveNext()
extern void U3CResourceRequestU3Ed__4_MoveNext_mFCA9791516285B3A088BB3A6ECB811A266177BEA (void);
// 0x0000004E System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF18EBCDA5C34ABC9425A00AC2064EC6745978A01 (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.IEnumerator.Reset()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mCE1DC9C2BCAC62574898B1F75F0C1FA9BFA552FD (void);
// 0x00000050 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m7593290530819EB9E05A09E991AB99D57F123F30 (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m5C0A8D3F144EAD939339C172E6E44B833AA427F3 (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass8_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_mE4DB4279B79DA3F91784949DF82575C673D3AB25 (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mE81F1DF717B8CFDE850A74846AEE0F7CB774AA9A (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass9_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CGetAwaiterU3Eb__0_m9751325D77D01F512A30CD059A004C4530E47D94 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mDAC2DB1AC5E754EF744DA24E73A53AB266A3F80B (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass10_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_mA0D562BFFEB6FB0C5D4F52AB0C4A761903397C27 (void);
// 0x00000057 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass11_0`1::.ctor()
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass11_0`1::<GetAwaiter>b__0()
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m1EEDBC14ADE3D75A6C8198218F46EC0E976953CF (void);
// 0x0000005A System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass12_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CGetAwaiterU3Eb__0_m7D62A216ADDBEDA46EB2EA7CF7E807D2A97C7885 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m34718D0B7AA248CC62F042224C09073557D0ECC3 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass13_0::<GetAwaiterReturnVoid>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CGetAwaiterReturnVoidU3Eb__0_m93827BAE9F674067CF93FBDEC484557C2E1E6C51 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass14_0`1::.ctor()
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass14_0`1::<GetAwaiterReturnSelf>b__0()
// 0x0000005F Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::get_Instance()
extern void AsyncCoroutineRunner_get_Instance_m24D7F2EA3C32FBC70A248AAF35B28C1F603FD6F1 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::Post(System.Action)
extern void AsyncCoroutineRunner_Post_m62827D9DDB353E733ACFEE62A8E3C18BC69A60D0 (void);
// 0x00000061 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::Update()
extern void AsyncCoroutineRunner_Update_m406654D4FD8D6A173B9F76FDC64A032CC917A924 (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::.ctor()
extern void AsyncCoroutineRunner__ctor_m2FD2633BA608347900167D31152B67011D3123A2 (void);
// 0x00000063 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::.cctor()
extern void AsyncCoroutineRunner__cctor_mEE0414B654DDF0C6F3BEC665627A09CEEB383A0F (void);
// 0x00000064 System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::Initialize()
extern void SyncContextUtility_Initialize_mC185FB5956026D2EC84F569046DFB577CBE56AE1 (void);
// 0x00000065 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_UnityThreadId()
extern void SyncContextUtility_get_UnityThreadId_m18C75B3EB16752A8D4ECC98D7330F1191FE2D805 (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::set_UnityThreadId(System.Int32)
extern void SyncContextUtility_set_UnityThreadId_mB4DC95D9BAA29112A27E221617E9902A4F3DF22A (void);
// 0x00000067 System.Threading.SynchronizationContext Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_UnitySynchronizationContext()
extern void SyncContextUtility_get_UnitySynchronizationContext_mE335E621F442423DEB6978469B66135D979C5599 (void);
// 0x00000068 System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::set_UnitySynchronizationContext(System.Threading.SynchronizationContext)
extern void SyncContextUtility_set_UnitySynchronizationContext_mACECEE32A8BB9054DE2228E798A22ED2BDAFFA9B (void);
// 0x00000069 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_IsMainThread()
extern void SyncContextUtility_get_IsMainThread_mACFE687879309BACBA35CF93880CB5231B3EE477 (void);
static Il2CppMethodPointer s_methodPointers[105] = 
{
	WaitForBackgroundThread_GetAwaiter_m1426512C58FF671DC2A1A2944EFC6A8B12A29F30,
	WaitForBackgroundThread__ctor_mFDCA225DD275058EA0166A1E49B13D32BFFE2C7C,
	U3CU3Ec__cctor_m2EAF89ECC182B39F674957325455514D2B68AE9A,
	U3CU3Ec__ctor_m50A6D3CC00C48AA989DD82DAAA28F2733DF090E6,
	U3CU3Ec_U3CGetAwaiterU3Eb__0_0_m0676A57D95CFCE31AFAE2B4644159C99573A70AC,
	WaitForUpdate_get_keepWaiting_m187BDF1528B1D934777E51A8EA49D38D3D3B7C8B,
	WaitForUpdate__ctor_m56726EC61884A9C5F6A71CE6696A324089315ECE,
	AwaiterExtensions_GetAwaiter_m512F9096DDFF479C59DAC8A1511D36264905D0AF,
	AwaiterExtensions_GetAwaiter_m0142AF0B04F8A74FD491B425CD09CB9212968234,
	AwaiterExtensions_GetAwaiter_m8C5F4FB79A7484D3BA83243D7FC481B3F02B7DE9,
	AwaiterExtensions_GetAwaiter_m9DDB4B63B036094F2D75508AB4A009EE23B9357F,
	AwaiterExtensions_GetAwaiter_m33994726D0B802DAC589974E38BBECF6D0EDD60F,
	AwaiterExtensions_GetAwaiter_m4406C7241628C4BE0138CC9A4919876AAC7378E0,
	AwaiterExtensions_GetAwaiter_mF7265561C482417D743CA6E279F8EED4C907C1C2,
	AwaiterExtensions_GetAwaiter_m9A2BBDD2550F6EB54E20DA74275DBF7F97F87B47,
	AwaiterExtensions_GetAwaiter_mC18898EAA13A71D8BFDC992B59B58EB8648FB56D,
	AwaiterExtensions_GetAwaiter_m265357C8F5D41F54F5BF90E700F8450BE91DA3DA,
	AwaiterExtensions_GetAwaiter_m1B8FF1ECF199945C1FDBF420D49BAEE97996F305,
	NULL,
	AwaiterExtensions_GetAwaiter_m9F8A57146B37CBEED99DDB67918DEC32373342A8,
	AwaiterExtensions_GetAwaiterReturnVoid_m89DEA4576B6008008CD61243B5483A76898F70CE,
	NULL,
	AwaiterExtensions_RunOnUnityScheduler_m399A3338345E2E9F80BB9C329096CDDFA022ACAF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SimpleCoroutineAwaiter_get_IsCompleted_m23F52130D905F5F3C9B35E7E454C90C278E248F0,
	SimpleCoroutineAwaiter_set_IsCompleted_m9C4C0FC64913B415CB4DBD1921FBB941D94CA29D,
	SimpleCoroutineAwaiter_GetResult_mC52BCE2DF0EF8C5552604A5E6C22BEDD1CB5AAFA,
	SimpleCoroutineAwaiter_Complete_m3DC6F24EFC07E1783CD167C9AEF4CBC747149AF9,
	SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_m571BFC4F21C92C0087A2CF3D92F03906AD8BD677,
	SimpleCoroutineAwaiter__ctor_mBB2FF5FA9AE5A9BF6C0EEB3D342AE00C366E2A56,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InstructionWrappers_ReturnVoid_mD022779C07C51FDC74018F5B660873966AC47480,
	InstructionWrappers_AssetBundleCreateRequest_mA0E069DFF0C300ACEBAC6EA3232F3CA55679EF0A,
	NULL,
	InstructionWrappers_AssetBundleRequest_mA18579E5D6C4DCC87C7F07708E66A8CFAD8D99DA,
	InstructionWrappers_ResourceRequest_mCE171462253C4E540E4402CBB1823D5AA72CDFA4,
	U3CReturnVoidU3Ed__0__ctor_m6FEAAA49A381416674CA9B85F4E55EF115E7BF77,
	U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m27CAB859E8D06AAEC599A7202319B64148665A45,
	U3CReturnVoidU3Ed__0_MoveNext_m060EECC4B14C6A86805F45E7258EFA509A43F680,
	U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1B69654B4C1CA7D1B2D9ABFF487CFBD7F849DB,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mA2F833C1707C9E403E13986B7D0A8A7DDE55CB9E,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m724B5FD67B9479605BFF0BFCF343B4B643841C2A,
	U3CAssetBundleCreateRequestU3Ed__1__ctor_m8DBD580C5433983031595374937DE8576FF08D6E,
	U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m94E54716364CEBAFE63E70C9EA25613D6D99FC93,
	U3CAssetBundleCreateRequestU3Ed__1_MoveNext_m2DACFA7EC5B789F2CA35A946EC5031C270F569F9,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8127002C1248A845A231700D2B8478E8B7BB2D57,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mCF94D1D05CA888D1A87E756341AC2C3730894F1E,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m28D0284032AB784807EEFD0A385005667BA185E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CAssetBundleRequestU3Ed__3__ctor_m5BF5419E18224E3BB05C8822D69C9DEE9F604637,
	U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_mFF55C5A68DEADCD98AB648D732B1924E9E72F621,
	U3CAssetBundleRequestU3Ed__3_MoveNext_m54186CC684C06AB3EE0F3578F55C5D74187555DA,
	U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D2CDEE5A6F7DC7AF28623546CA6E40B03C82DD8,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_mDB8C815A5A1A4A3C398522C110FE3BB83A261DD0,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m44C9F7B1CA14F65178A60EE1FF9C992F1BB12877,
	U3CResourceRequestU3Ed__4__ctor_m8AB4E0721573E50A5E0A4209C8D4A70C5E9A577D,
	U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m04064ABC86F37919780BB4F73BF1F591C26FD441,
	U3CResourceRequestU3Ed__4_MoveNext_mFCA9791516285B3A088BB3A6ECB811A266177BEA,
	U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF18EBCDA5C34ABC9425A00AC2064EC6745978A01,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mCE1DC9C2BCAC62574898B1F75F0C1FA9BFA552FD,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m7593290530819EB9E05A09E991AB99D57F123F30,
	U3CU3Ec__DisplayClass8_0__ctor_m5C0A8D3F144EAD939339C172E6E44B833AA427F3,
	U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_mE4DB4279B79DA3F91784949DF82575C673D3AB25,
	U3CU3Ec__DisplayClass9_0__ctor_mE81F1DF717B8CFDE850A74846AEE0F7CB774AA9A,
	U3CU3Ec__DisplayClass9_0_U3CGetAwaiterU3Eb__0_m9751325D77D01F512A30CD059A004C4530E47D94,
	U3CU3Ec__DisplayClass10_0__ctor_mDAC2DB1AC5E754EF744DA24E73A53AB266A3F80B,
	U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_mA0D562BFFEB6FB0C5D4F52AB0C4A761903397C27,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass12_0__ctor_m1EEDBC14ADE3D75A6C8198218F46EC0E976953CF,
	U3CU3Ec__DisplayClass12_0_U3CGetAwaiterU3Eb__0_m7D62A216ADDBEDA46EB2EA7CF7E807D2A97C7885,
	U3CU3Ec__DisplayClass13_0__ctor_m34718D0B7AA248CC62F042224C09073557D0ECC3,
	U3CU3Ec__DisplayClass13_0_U3CGetAwaiterReturnVoidU3Eb__0_m93827BAE9F674067CF93FBDEC484557C2E1E6C51,
	NULL,
	NULL,
	AsyncCoroutineRunner_get_Instance_m24D7F2EA3C32FBC70A248AAF35B28C1F603FD6F1,
	AsyncCoroutineRunner_Post_m62827D9DDB353E733ACFEE62A8E3C18BC69A60D0,
	AsyncCoroutineRunner_Update_m406654D4FD8D6A173B9F76FDC64A032CC917A924,
	AsyncCoroutineRunner__ctor_m2FD2633BA608347900167D31152B67011D3123A2,
	AsyncCoroutineRunner__cctor_mEE0414B654DDF0C6F3BEC665627A09CEEB383A0F,
	SyncContextUtility_Initialize_mC185FB5956026D2EC84F569046DFB577CBE56AE1,
	SyncContextUtility_get_UnityThreadId_m18C75B3EB16752A8D4ECC98D7330F1191FE2D805,
	SyncContextUtility_set_UnityThreadId_mB4DC95D9BAA29112A27E221617E9902A4F3DF22A,
	SyncContextUtility_get_UnitySynchronizationContext_mE335E621F442423DEB6978469B66135D979C5599,
	SyncContextUtility_set_UnitySynchronizationContext_mACECEE32A8BB9054DE2228E798A22ED2BDAFFA9B,
	SyncContextUtility_get_IsMainThread_mACFE687879309BACBA35CF93880CB5231B3EE477,
};
static const int32_t s_InvokerIndices[105] = 
{
	4429,
	4406,
	6303,
	4406,
	4406,
	4364,
	4406,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	-1,
	6106,
	6106,
	-1,
	6218,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4364,
	3670,
	4406,
	3635,
	3635,
	4406,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5647,
	5647,
	-1,
	5647,
	5647,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	-1,
	-1,
	4406,
	4406,
	4406,
	4406,
	-1,
	-1,
	6279,
	6218,
	4406,
	4406,
	6303,
	6303,
	6268,
	6213,
	6279,
	6218,
	6290,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x02000006, { 10, 1 } },
	{ 0x02000008, { 11, 2 } },
	{ 0x02000009, { 13, 5 } },
	{ 0x0200000D, { 20, 2 } },
	{ 0x02000013, { 22, 3 } },
	{ 0x02000016, { 25, 1 } },
	{ 0x06000013, { 0, 5 } },
	{ 0x06000016, { 5, 5 } },
	{ 0x06000030, { 18, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)2, 1475 },
	{ (Il2CppRGCTXDataType)3, 90 },
	{ (Il2CppRGCTXDataType)2, 11229 },
	{ (Il2CppRGCTXDataType)3, 41231 },
	{ (Il2CppRGCTXDataType)3, 91 },
	{ (Il2CppRGCTXDataType)2, 1476 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)2, 11230 },
	{ (Il2CppRGCTXDataType)3, 41232 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)3, 41233 },
	{ (Il2CppRGCTXDataType)2, 1567 },
	{ (Il2CppRGCTXDataType)3, 477 },
	{ (Il2CppRGCTXDataType)3, 5897 },
	{ (Il2CppRGCTXDataType)2, 2246 },
	{ (Il2CppRGCTXDataType)3, 5898 },
	{ (Il2CppRGCTXDataType)3, 41234 },
	{ (Il2CppRGCTXDataType)2, 954 },
	{ (Il2CppRGCTXDataType)2, 1565 },
	{ (Il2CppRGCTXDataType)3, 470 },
	{ (Il2CppRGCTXDataType)2, 955 },
	{ (Il2CppRGCTXDataType)3, 41235 },
	{ (Il2CppRGCTXDataType)2, 2245 },
	{ (Il2CppRGCTXDataType)3, 5895 },
	{ (Il2CppRGCTXDataType)3, 5896 },
	{ (Il2CppRGCTXDataType)3, 50995 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Async_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Async_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Async_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Async.dll",
	105,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Async_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
