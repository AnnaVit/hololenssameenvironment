﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Microsoft.MixedReality.Toolkit.Input.TouchableEventType Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::get_EventsToReceive()
extern void BaseNearInteractionTouchable_get_EventsToReceive_m0505A1FB06A460224393FC173D1827843693EA54 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::set_EventsToReceive(Microsoft.MixedReality.Toolkit.Input.TouchableEventType)
extern void BaseNearInteractionTouchable_set_EventsToReceive_m492A4370E83CC973F72584FEED2827E200C7E41C (void);
// 0x00000003 System.Single Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::get_DebounceThreshold()
extern void BaseNearInteractionTouchable_get_DebounceThreshold_m3C15CEA64527EF19D8FDDCD912DC47785D96E846 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::set_DebounceThreshold(System.Single)
extern void BaseNearInteractionTouchable_set_DebounceThreshold_m2D4DDD7968E31CD94F328999AECA1091DD2A5602 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::OnValidate()
extern void BaseNearInteractionTouchable_OnValidate_m8A1C58AC88AE6551B70DFD8475064E1C62D34006 (void);
// 0x00000006 System.Single Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::.ctor()
extern void BaseNearInteractionTouchable__ctor_m93F939290D114E005ADB131ACDBF0A6358090AE4 (void);
// 0x00000008 System.Boolean Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::get_ColliderEnabled()
extern void ColliderNearInteractionTouchable_get_ColliderEnabled_mB9EB8D5247221CBA1FB77838806D0DEED24D223F (void);
// 0x00000009 UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::get_TouchableCollider()
extern void ColliderNearInteractionTouchable_get_TouchableCollider_mB59C8E7EC64DD918D22C6E21D75104A26EF1843D (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::OnValidate()
extern void ColliderNearInteractionTouchable_OnValidate_m9FDFA4A931C62CC1F16D13BC6218B2EE16102100 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::.ctor()
extern void ColliderNearInteractionTouchable__ctor_mF70E4D84C8724EA39FE2226205EEBD4CBC47BC94 (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void DefaultRaycastProvider__ctor_m6C9BFDF95ED7D2A3637CFADB18E944021FAD5502 (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void DefaultRaycastProvider__ctor_mF71BBF984AD9EFFD209653769BFDAD36E618319A (void);
// 0x0000000E System.String Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::get_Name()
extern void DefaultRaycastProvider_get_Name_mB538072A2AA39C1F0D4E94CAD47753B13883B819 (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::set_Name(System.String)
extern void DefaultRaycastProvider_set_Name_mC07E922AC5547DC7329A72AC0D40E3B22D2D3368 (void);
// 0x00000010 System.Boolean Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::Raycast(Microsoft.MixedReality.Toolkit.Physics.RayStep,UnityEngine.LayerMask[],System.Boolean,Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit&)
extern void DefaultRaycastProvider_Raycast_mD9A4598CB3D81F166A9CF0D80AFC7966E27F46EF (void);
// 0x00000011 System.Boolean Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::SphereCast(Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Single,UnityEngine.LayerMask[],System.Boolean,Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit&)
extern void DefaultRaycastProvider_SphereCast_mD7D22ACA928F29279E80D7D8E4C4DDE466F4002F (void);
// 0x00000012 UnityEngine.EventSystems.RaycastResult Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::GraphicsRaycast(UnityEngine.EventSystems.EventSystem,UnityEngine.EventSystems.PointerEventData,UnityEngine.LayerMask[])
extern void DefaultRaycastProvider_GraphicsRaycast_mCFAA6CB42CBB950284C1ABA88655998E7B6A036D (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.cctor()
extern void DefaultRaycastProvider__cctor_m555D9B394EFDD3F3CBC25922D13997EBFD4F053A (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void FocusProvider__ctor_m8D0E8A29D807C00C1D81BB5A466615370FC4A8DF (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void FocusProvider__ctor_m175FA798215CF56A3E9B94033E3AAF32EED75581 (void);
// 0x00000016 System.Collections.Generic.IReadOnlyDictionary`2<System.UInt32,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerMediator> Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_PointerMediators()
extern void FocusProvider_get_PointerMediators_m0FD9722F816E4BA09114CF863A7C3B9CE94483D4 (void);
// 0x00000017 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_NumNearPointersActive()
extern void FocusProvider_get_NumNearPointersActive_mD274E9C0A892E817F66A743C56DEF37E67ABF072 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_NumNearPointersActive(System.Int32)
extern void FocusProvider_set_NumNearPointersActive_m9613789E92FCBB8522F58F2DA6A078661EE12387 (void);
// 0x00000019 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_NumFarPointersActive()
extern void FocusProvider_get_NumFarPointersActive_mC0F81CB40E3972A1117970B2DD5B0D726855EC4F (void);
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_NumFarPointersActive(System.Int32)
extern void FocusProvider_set_NumFarPointersActive_m13DC9A8A3E637896C6ACA296C6990AF2CA2ACCD4 (void);
// 0x0000001B Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_PrimaryPointer()
extern void FocusProvider_get_PrimaryPointer_m67A6C329DE4381982D42BAFAD8C9500444E6C258 (void);
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_PrimaryPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_set_PrimaryPointer_mDF5C85A4CD191F11E32C62E3A61C99609D0EBDE1 (void);
// 0x0000001D System.String Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_Name()
extern void FocusProvider_get_Name_m720C4506B2E808BF28B2B3023E7CE2EC657C493A (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_Name(System.String)
extern void FocusProvider_set_Name_m503BE058D12180B3711D681E4EED3A2279D4E90A (void);
// 0x0000001F System.UInt32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_Priority()
extern void FocusProvider_get_Priority_m47F0D5EB72E8D7C0214FE6FAD2DA655F5E331B75 (void);
// 0x00000020 System.Single Microsoft.MixedReality.Toolkit.Input.FocusProvider::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusProvider.get_GlobalPointingExtent()
extern void FocusProvider_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusProvider_get_GlobalPointingExtent_mEF53A5D73E8CA23DBF51B2C130170FD6AD79C38F (void);
// 0x00000021 UnityEngine.LayerMask[] Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_FocusLayerMasks()
extern void FocusProvider_get_FocusLayerMasks_mE55BF03590DBDB993743AA30CE343F09E92C206D (void);
// 0x00000022 UnityEngine.Camera Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_UIRaycastCamera()
extern void FocusProvider_get_UIRaycastCamera_m59F704468E3415AE7EE64F7BDFDF28F797AD006E (void);
// 0x00000023 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_IsSetupValid()
extern void FocusProvider_get_IsSetupValid_mFEF4AB0FA78DC2C8BF9AF27EB959CE39768596EA (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::add_PrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_add_PrimaryPointerChanged_m0B2C3345940B23C2293660EEBAA5496D4A050E07 (void);
// 0x00000025 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::remove_PrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_remove_PrimaryPointerChanged_mF9C04B5518DD91BBB33D102BBF7E59ECB3EA935B (void);
// 0x00000026 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Initialize()
extern void FocusProvider_Initialize_mB70ECA1601A08AAFF74972D95CBBEF90D9AA257D (void);
// 0x00000027 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Destroy()
extern void FocusProvider_Destroy_m45682B8433D684F7248520BD6C1B71900C536C4B (void);
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Update()
extern void FocusProvider_Update_mBF08DE91B8EEDFB370C78DD3FD0DA9D05ABA3D55 (void);
// 0x00000029 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdateGazeProvider()
extern void FocusProvider_UpdateGazeProvider_m6BD1C1703E153BDC313209FCE766BE19338A2D17 (void);
// 0x0000002A UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetFocusedObject(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_GetFocusedObject_m34363B1D194B56795919F9381EA833FDB2157925 (void);
// 0x0000002B System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryGetFocusDetails(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Physics.FocusDetails&)
extern void FocusProvider_TryGetFocusDetails_m2C1393C1080F79A66DF9DBA8653C41A0CFA76619 (void);
// 0x0000002C System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryOverrideFocusDetails(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Physics.FocusDetails)
extern void FocusProvider_TryOverrideFocusDetails_mDC281510A6B4F17AE8C573B842D8BB2F743BD8AC (void);
// 0x0000002D System.UInt32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::GenerateNewPointerId()
extern void FocusProvider_GenerateNewPointerId_m5DC46AE0652DF3EB0D0B1E6D2D408BB93C310CAA (void);
// 0x0000002E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::FindOrCreateUiRaycastCamera()
extern void FocusProvider_FindOrCreateUiRaycastCamera_mB8C5D0954084D4899015CDE6159D5FF7560BE6FE (void);
// 0x0000002F System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::CleanUpUiRaycastCamera()
extern void FocusProvider_CleanUpUiRaycastCamera_mCDF009AB414796C9064D1AEFCD188DA21CBA9835 (void);
// 0x00000030 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::IsPointerRegistered(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_IsPointerRegistered_m15FDADAB2BA0405C6F6CFEE2308B9C6570EB67FE (void);
// 0x00000031 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::RegisterPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_RegisterPointer_m02CA192C760BED0E4C027F6526DA07711219119C (void);
// 0x00000032 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::RegisterPointers(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void FocusProvider_RegisterPointers_m752EE4215B7AE482CB0216E572D6D919CE0EDC65 (void);
// 0x00000033 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::UnregisterPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_UnregisterPointer_m747A9A0471F267FD427372B2399E9CA5BE558D71 (void);
// 0x00000034 System.Collections.Generic.IEnumerable`1<T> Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointers()
// 0x00000035 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::SubscribeToPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler,System.Boolean)
extern void FocusProvider_SubscribeToPrimaryPointerChanged_mE8A065E6F9983E4ED6B7ACB5E7BA1DFF25894090 (void);
// 0x00000036 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UnsubscribeFromPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_UnsubscribeFromPrimaryPointerChanged_mC84B891347B1D9C63FDE141AF6D861E7758CC8DB (void);
// 0x00000037 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryGetPointerData(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData&)
extern void FocusProvider_TryGetPointerData_m84B662BFAC034D8A969BBC8A21E9C04E129EBA0E (void);
// 0x00000038 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdatePointers()
extern void FocusProvider_UpdatePointers_m5A9E9E0AE3F5BDF5030097904F1A425D1C9A7C2C (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdatePointer(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData)
extern void FocusProvider_UpdatePointer_m0917842AEE2470F2EA51746333E8B64754EDAF7B (void);
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::TruncatePointerRayToHit(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void FocusProvider_TruncatePointerRayToHit_m8EDA73CAFC4F986408A18943046ED9A485697EC1 (void);
// 0x0000003B Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPrioritizedHitResult(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,UnityEngine.LayerMask[])
extern void FocusProvider_GetPrioritizedHitResult_mB1F0218429B02A4D22F8EE2E9DB8653DF8C8DA8C (void);
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::ReconcilePointers()
extern void FocusProvider_ReconcilePointers_mD40D1C6375E5732BA810959C9794CE09DB680C9D (void);
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::QueryScene(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.IMixedRealityRaycastProvider,UnityEngine.LayerMask[],Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,System.Int32,System.Boolean)
extern void FocusProvider_QueryScene_m0BA963B07BEBF925BC31C1599E4DE831AE0B065A (void);
// 0x0000003E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::RaycastGraphics(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.EventSystems.PointerEventData,UnityEngine.LayerMask[],Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void FocusProvider_RaycastGraphics_mD6EA004478F55D085BE84BAF431B9D95591512B4 (void);
// 0x0000003F System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::RaycastGraphicsStep(UnityEngine.EventSystems.PointerEventData,Microsoft.MixedReality.Toolkit.Physics.RayStep,UnityEngine.LayerMask[],UnityEngine.EventSystems.RaycastResult&)
extern void FocusProvider_RaycastGraphicsStep_mB6D1F1E3AF1A7D275579C6F72B07AEE09FE585CC (void);
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdateFocusedObjects()
extern void FocusProvider_UpdateFocusedObjects_m2FABD19BDF0775BAEFC163E2A9BAD6ADC32A2E43 (void);
// 0x00000041 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void FocusProvider_OnSourceDetected_mE6B127303131DD549E60D59088FDE645155BB4F8 (void);
// 0x00000042 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void FocusProvider_OnSourceLost_m87A12A91144EEFB30F9E9B51BC386334B025856B (void);
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void FocusProvider_OnSpeechKeywordRecognized_m0F1914D8FBD7D1B62F28F98A28D1C7CF97567516 (void);
// 0x00000044 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_GetPointerBehavior_m3BDEE6F27053B53C231E5C72170D5F82AACAC71E (void);
// 0x00000045 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
// 0x00000046 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(System.Type,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void FocusProvider_GetPointerBehavior_m42A082B9B97A48823BB76B76DB8FB6C32517BE65 (void);
// 0x00000047 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_GazePointerBehavior()
extern void FocusProvider_get_GazePointerBehavior_mDF1B6115D96E6277DF7CAC5B1C388C0405521E3D (void);
// 0x00000048 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_GazePointerBehavior(Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
extern void FocusProvider_set_GazePointerBehavior_m0E22B435FBCB479CF07FA4A56648B217640878D4 (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::SetPointerBehavior(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType,Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.cctor()
extern void FocusProvider__cctor_m90281887F81FD5E807A69AB0B3F2D338458688A5 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Clear()
extern void PointerHitResult_Clear_m37A1D48BB64830FF92CFF2BF321BB231AFA7BCAB (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector4,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single)
extern void PointerHitResult_Set_m5CCE1F892E8D535427EB09BE35899925007331FD (void);
// 0x0000004D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single,System.Boolean)
extern void PointerHitResult_Set_m9390771108296A6E273D641433FE4C35E3C66D97 (void);
// 0x0000004E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(UnityEngine.EventSystems.RaycastResult,UnityEngine.Vector3,UnityEngine.Vector4,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single)
extern void PointerHitResult_Set_m813BC86177D934696A741FAF6F684D3A3DAD9105 (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::.ctor()
extern void PointerHitResult__ctor_mBEC90BEDD5F2FFF6C9AB32443F0E71D5D7F08461 (void);
// 0x00000050 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_StartPoint()
extern void PointerData_get_StartPoint_m422E8F053289CEC020F5C70D77B659F27C293704 (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_StartPoint(UnityEngine.Vector3)
extern void PointerData_set_StartPoint_mE7A9832F845512E22F776B7771A6FE382D19F650 (void);
// 0x00000052 Microsoft.MixedReality.Toolkit.Physics.FocusDetails Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_Details()
extern void PointerData_get_Details_m449621AE07F092995FC8D1D7779FC4A929BC25E8 (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_Details(Microsoft.MixedReality.Toolkit.Physics.FocusDetails)
extern void PointerData_set_Details_m5608FB0B6C7812A601B624D10F08CAD477968972 (void);
// 0x00000054 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_CurrentPointerTarget()
extern void PointerData_get_CurrentPointerTarget_m696E8BAF4EAC0B660C3B898B91DC7C25155DAF32 (void);
// 0x00000055 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_PreviousPointerTarget()
extern void PointerData_get_PreviousPointerTarget_mE4A4F7D8FF4B703D0CA41233C46681F0700C4AD6 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_PreviousPointerTarget(UnityEngine.GameObject)
extern void PointerData_set_PreviousPointerTarget_mAB6B24961FF961038C0BA412674131767E2AE158 (void);
// 0x00000057 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_RayStepIndex()
extern void PointerData_get_RayStepIndex_mBEFE82827761AD963E494D0AD4E4AF6B910244BB (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_RayStepIndex(System.Int32)
extern void PointerData_set_RayStepIndex_m1F1B0B9D2359CCD008CD57135A7E643D533DFDB0 (void);
// 0x00000059 UnityEngine.EventSystems.PointerEventData Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_GraphicEventData()
extern void PointerData_get_GraphicEventData_mD3DBC4A4F2CCEC60815AAE1D93CB8D7A70853E52 (void);
// 0x0000005A System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_IsCurrentPointerTargetInvalid()
extern void PointerData_get_IsCurrentPointerTargetInvalid_m641BF8EDEC1187908E2689D320628B6026C98FC8 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void PointerData__ctor_m77FC628173F230336DCEFB1F78EB91FE0D888F97 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::UpdateHit(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void PointerData_UpdateHit_mE9B2F582288E913E769B8B075B64DA6269D05362 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::UpdateFocusLockedHit()
extern void PointerData_UpdateFocusLockedHit_mEB1F4C03A34EE69E2180AE355E5E2D5318C5226E (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::ResetFocusedObjects(System.Boolean)
extern void PointerData_ResetFocusedObjects_m63EAC7BEAE7B82AA5A22C25F2B288BEB852C4DFC (void);
// 0x0000005F System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::Equals(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData)
extern void PointerData_Equals_mA12226B69AF91E0379719F5055947F9F9068E0A8 (void);
// 0x00000060 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::Equals(System.Object)
extern void PointerData_Equals_m45D094AC03D5303BB9628B946976B28883D84562 (void);
// 0x00000061 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::GetHashCode()
extern void PointerData_GetHashCode_m856A80E826A7475D3EDDD9762DF44E8308DF3855 (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::.cctor()
extern void PointerData__cctor_m39CE37A04540D315935104AFFCE1FD41052D3379 (void);
// 0x00000063 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::Matches(System.Type,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void PointerPreferences_Matches_m2CC26081615B3A98AF390150B7EE6E930C684968 (void);
// 0x00000064 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::Matches(System.Type)
extern void PointerPreferences_Matches_mD9B2D82C9CB272C9DD307574CD31F4602C790497 (void);
// 0x00000065 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::GetBehaviorForHandedness(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void PointerPreferences_GetBehaviorForHandedness_m86E34BC76D17378233458D330C554969F4F5DE11 (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::SetBehaviorForHandedness(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
extern void PointerPreferences_SetBehaviorForHandedness_m75D751BBD751DC20FE5E36A409AA28DABCF5215B (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::.ctor(System.Type,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void PointerPreferences__ctor_m159AF82C116638A657FFA5DA997C1496DF89F8F9 (void);
// 0x00000068 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::get_IsGazePointerActive()
extern void GazePointerVisibilityStateMachine_get_IsGazePointerActive_m86CE3233AB1763B852185A0B288200020C3000B5 (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::UpdateState(System.Int32,System.Int32,System.Int32,System.Boolean)
extern void GazePointerVisibilityStateMachine_UpdateState_m69AACD2DA02B7EE0AC56248D7ABB821070E1139E (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void GazePointerVisibilityStateMachine_OnSpeechKeywordRecognized_m4D5233497AD43570A301E4EC30C97A8CB66686DD (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::.ctor()
extern void GazePointerVisibilityStateMachine__ctor_mCAE946FE1BB775115E1CAE30873574444D956092 (void);
// 0x0000006C System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_Enabled()
extern void GazeProvider_get_Enabled_m368C901012FDE826E6FE567F1073EBF335F2CD41 (void);
// 0x0000006D System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_Enabled(System.Boolean)
extern void GazeProvider_set_Enabled_mF1033CC00741D9CFF1DF41055C8F4774345C5F1E (void);
// 0x0000006E Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeInputSource()
extern void GazeProvider_get_GazeInputSource_m77C5F3D23E07ED72FAE042EEDCA8237DCE40BA42 (void);
// 0x0000006F Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazePointer()
extern void GazeProvider_get_GazePointer_mA23A36BBF88BD62DAFA0B01BDB755A3558184368 (void);
// 0x00000070 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeCursorPrefab()
extern void GazeProvider_get_GazeCursorPrefab_m40CCB21A3FFC04008C68CFF6F968A7DB50A7A4C1 (void);
// 0x00000071 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_GazeCursorPrefab(UnityEngine.GameObject)
extern void GazeProvider_set_GazeCursorPrefab_m607EAA00B5292F16A9A8AE99089386A7D31AA285 (void);
// 0x00000072 Microsoft.MixedReality.Toolkit.Input.IMixedRealityCursor Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeCursor()
extern void GazeProvider_get_GazeCursor_m989AC4F8F64E3075BFFABBA52DD1EF390E67B5C1 (void);
// 0x00000073 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeTarget()
extern void GazeProvider_get_GazeTarget_m5D370EFB4AF8118F049EDCCC6808CA9E0AA38C33 (void);
// 0x00000074 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_GazeTarget(UnityEngine.GameObject)
extern void GazeProvider_set_GazeTarget_m50110F0812B93A399CEA008C211161A55D751671 (void);
// 0x00000075 Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitInfo()
extern void GazeProvider_get_HitInfo_mC6C64774B811FEAC54FA3CD8F373224611DC5997 (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitInfo(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit)
extern void GazeProvider_set_HitInfo_m0DB7F9C9E348535033363E78240E78F5ED9C700B (void);
// 0x00000077 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitPosition()
extern void GazeProvider_get_HitPosition_m0CF3DB2AF31C873E2EEFE0BEE43925E2C3530343 (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitPosition(UnityEngine.Vector3)
extern void GazeProvider_set_HitPosition_m66A66BE5409C4802135CE6895E0328A06E5C57AE (void);
// 0x00000079 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitNormal()
extern void GazeProvider_get_HitNormal_m7886F528908F93E27934F25198FDAD458FC07BE8 (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitNormal(UnityEngine.Vector3)
extern void GazeProvider_set_HitNormal_m309231C3EEEADBBE0604FFB3135E49172DE8D8AB (void);
// 0x0000007B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeOrigin()
extern void GazeProvider_get_GazeOrigin_m21B179E75578442F26A28448815F401E50056C39 (void);
// 0x0000007C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeDirection()
extern void GazeProvider_get_GazeDirection_mAF8C5F55A0F4F364BF9696E3B083D8E053E0A7A0 (void);
// 0x0000007D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HeadVelocity()
extern void GazeProvider_get_HeadVelocity_mEB0602B2E2A76D9D56FB1347990EC2686B06111B (void);
// 0x0000007E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HeadVelocity(UnityEngine.Vector3)
extern void GazeProvider_set_HeadVelocity_m702CC2A261F592379393E8364DA12A629B16700A (void);
// 0x0000007F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HeadMovementDirection()
extern void GazeProvider_get_HeadMovementDirection_m31DFC53340F1BDF5D981D2D6D561FE9585AA4C26 (void);
// 0x00000080 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HeadMovementDirection(UnityEngine.Vector3)
extern void GazeProvider_set_HeadMovementDirection_m259DD40DD5B118FF39AB473691124281F44AB982 (void);
// 0x00000081 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GameObjectReference()
extern void GazeProvider_get_GameObjectReference_m46ECE327BE1EE346BDDC72CEE246386F0D299181 (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnValidate()
extern void GazeProvider_OnValidate_m56821F9DF00A1EFAE75DEBAA6FECE428CD1AECE2 (void);
// 0x00000083 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnEnable()
extern void GazeProvider_OnEnable_m9D300CB8A0B977C9CB9E26DBAA22FB1F9CD05083 (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::Start()
extern void GazeProvider_Start_m6EAAFA7051415C3DFF106BCAB7906E0CD22A5A55 (void);
// 0x00000085 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::Update()
extern void GazeProvider_Update_m474C9F183E25ECF7A61BCEF80B75CED169132012 (void);
// 0x00000086 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::LateUpdate()
extern void GazeProvider_LateUpdate_m80D4AC4CAFACE4C3ADE651C644CE97F86F713ED4 (void);
// 0x00000087 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnDisable()
extern void GazeProvider_OnDisable_mF61B8B274C85BB9E150BE6AFF67D4F939270BFAB (void);
// 0x00000088 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnDestroy()
extern void GazeProvider_OnDestroy_m451C99952E39EA3EC995052F6B0F9A2B9D44854B (void);
// 0x00000089 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::RegisterHandlers()
extern void GazeProvider_RegisterHandlers_m41853DBA329FAA7DD19846DBF81E58A38A9352B9 (void);
// 0x0000008A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UnregisterHandlers()
extern void GazeProvider_UnregisterHandlers_mA84EDE3F8C861856CBF3D4F187AFAAA6584F4000 (void);
// 0x0000008B System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GazeProvider_OnInputUp_m1DF81468D5F9D9D9D1348EA95ABC07B4ED5B3A6E (void);
// 0x0000008C System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GazeProvider_OnInputDown_m1B6A7CA01295B6DEA76F8C6F50B9AF11DC564D5E (void);
// 0x0000008D Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.GazeProvider::InitializeGazePointer()
extern void GazeProvider_InitializeGazePointer_m818F5D9505AA98B1416FDDC626B005B5A66A172F (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::RaiseSourceDetected()
extern void GazeProvider_RaiseSourceDetected_mA1848997E168EC3DF06ED7ADBB21DA3577E099D8 (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateGazeInfoFromHit(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit)
extern void GazeProvider_UpdateGazeInfoFromHit_m4C079B3BAC36B206BAA10525FEFAF553639BB8E3 (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::SetGazeCursor(UnityEngine.GameObject)
extern void GazeProvider_SetGazeCursor_m01E4DCAA39698054AFFAAE1EE02778146E79F81F (void);
// 0x00000091 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingEnabledAndValid()
extern void GazeProvider_get_IsEyeTrackingEnabledAndValid_m00430FEF418064AC463019D2CCF07F820EC1EE8A (void);
// 0x00000092 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingDataValid()
extern void GazeProvider_get_IsEyeTrackingDataValid_m67B147F43A4BA6083A0A97875F6D5B53561B47E5 (void);
// 0x00000093 System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeCalibrationValid()
extern void GazeProvider_get_IsEyeCalibrationValid_m3C0C60B8B60763627E4084F0F629ECD0BA4AC646 (void);
// 0x00000094 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_IsEyeCalibrationValid(System.Nullable`1<System.Boolean>)
extern void GazeProvider_set_IsEyeCalibrationValid_m47D339440A612BCBFEF444DF9C902E94CF4E1437 (void);
// 0x00000095 UnityEngine.Ray Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_LatestEyeGaze()
extern void GazeProvider_get_LatestEyeGaze_m8C2F9236CC1EFF9DE37BDD092188E0341DAC2BB7 (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_LatestEyeGaze(UnityEngine.Ray)
extern void GazeProvider_set_LatestEyeGaze_mC2D1E9684AEEA1D31255F4A2B6DE238379850B0E (void);
// 0x00000097 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingEnabled()
extern void GazeProvider_get_IsEyeTrackingEnabled_mCC73A14376428F67C50D3C5BC2EFB87E37F64258 (void);
// 0x00000098 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_IsEyeTrackingEnabled(System.Boolean)
extern void GazeProvider_set_IsEyeTrackingEnabled_mDD8948C0BAE92D06415061E4692DA004877AE53A (void);
// 0x00000099 System.DateTime Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_Timestamp()
extern void GazeProvider_get_Timestamp_m860D6B2FDA518AB94D9C7EB2A2F6DEEACB3485AB (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_Timestamp(System.DateTime)
extern void GazeProvider_set_Timestamp_m653309C70F41768C302BADBDC28F268D9201C358 (void);
// 0x0000009B System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateEyeGaze(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider,UnityEngine.Ray,System.DateTime)
extern void GazeProvider_UpdateEyeGaze_m55A35E7069354DB56C836FD6815FE351F37B4939 (void);
// 0x0000009C System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateEyeTrackingStatus(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider,System.Boolean)
extern void GazeProvider_UpdateEyeTrackingStatus_m935F690DA106715ED089930A7D2F287D3600D8F7 (void);
// 0x0000009D System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_UseHeadGazeOverride()
extern void GazeProvider_get_UseHeadGazeOverride_mDD01AC1180B147624A905121A4D1E86D1A87C6BB (void);
// 0x0000009E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_UseHeadGazeOverride(System.Boolean)
extern void GazeProvider_set_UseHeadGazeOverride_m2AEF31DA36CEE59029EA6C52BB1B95F4D97CB9EA (void);
// 0x0000009F System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OverrideHeadGaze(UnityEngine.Vector3,UnityEngine.Vector3)
extern void GazeProvider_OverrideHeadGaze_mB431591F8176DA4586E5EE1888E1410FE4CFC56F (void);
// 0x000000A0 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::.ctor()
extern void GazeProvider__ctor_m79E43BF32B094720136E42A1AB34F57000A5AB11 (void);
// 0x000000A1 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::.cctor()
extern void GazeProvider__cctor_m8D9D0DCDDCCD5D347BB21D2A7396E995F614B38C (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::<>n__0()
extern void GazeProvider_U3CU3En__0_m81634D8D8E8FE66D0AE962D62DEFBF63BC93B1C3 (void);
// 0x000000A3 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::.ctor(Microsoft.MixedReality.Toolkit.Input.GazeProvider,System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,UnityEngine.LayerMask[],System.Single,UnityEngine.Transform,Microsoft.MixedReality.Toolkit.Physics.BaseRayStabilizer)
extern void InternalGazePointer__ctor_m807892FD633E185C8BA93459E411F558B1B7C063 (void);
// 0x000000A4 Microsoft.MixedReality.Toolkit.Input.IMixedRealityController Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Controller()
extern void InternalGazePointer_get_Controller_m23B01EFF6925D47A30E55F16ADB694469A44591B (void);
// 0x000000A5 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_Controller(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void InternalGazePointer_set_Controller_m75F90DF47C5A592700751CF5C81EBC321F7D4F49 (void);
// 0x000000A6 Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_InputSourceParent()
extern void InternalGazePointer_get_InputSourceParent_m1E2AF81A1856861FA0AE5A073AF0D12764D76A30 (void);
// 0x000000A7 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_InputSourceParent(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_set_InputSourceParent_m48D88841B17601D3E949CCB3C8ECCEDE8765FF85 (void);
// 0x000000A8 System.Single Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_PointerExtent()
extern void InternalGazePointer_get_PointerExtent_m514958ECDB821AF2A953027C29C0C2A310C32203 (void);
// 0x000000A9 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_PointerExtent(System.Single)
extern void InternalGazePointer_set_PointerExtent_m3021B1E0DFF6112C44DC9D239BAE89100A5592B5 (void);
// 0x000000AA System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::SetGazeInputSourceParent(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_SetGazeInputSourceParent_mD9A387CE28F0916D61E0D246820FC8D5AA8D91E9 (void);
// 0x000000AB System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPreSceneQuery()
extern void InternalGazePointer_OnPreSceneQuery_mC5757C5C53EE0FDB57D92CE58FC4D1026E61764F (void);
// 0x000000AC System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPostSceneQuery()
extern void InternalGazePointer_OnPostSceneQuery_m50E9702E704503FCA59503F934B824F2B00A6DE2 (void);
// 0x000000AD System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPreCurrentPointerTargetChange()
extern void InternalGazePointer_OnPreCurrentPointerTargetChange_m3B1D5B047C205B5A54BE784C4363098739B388EF (void);
// 0x000000AE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Position()
extern void InternalGazePointer_get_Position_mE73B899C274A19CDE119AAB8AE3C9F805053C4B7 (void);
// 0x000000AF UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Rotation()
extern void InternalGazePointer_get_Rotation_m93AFC889420D1C8F76F55C0B469C7686D8F8A15C (void);
// 0x000000B0 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::Reset()
extern void InternalGazePointer_Reset_m37DB374B49DCDEDBC57C6674287F6486120A8ED6 (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::RaisePointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_RaisePointerDown_mA46F01E9CF07B7134962FEA67AC4C80FC729501C (void);
// 0x000000B2 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::RaisePointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_RaisePointerUp_m14760F977169DA125B9393E3E06F0030B4AA68DF (void);
// 0x000000B3 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::.cctor()
extern void InternalGazePointer__cctor_m8E03AD788B610A5370585EEC3463F7C75AD05D10 (void);
// 0x000000B4 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<Start>d__63::MoveNext()
extern void U3CStartU3Ed__63_MoveNext_mE7A9ED52606E6BAED7043E305EC7F70044C149F0 (void);
// 0x000000B5 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<Start>d__63::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92 (void);
// 0x000000B6 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<RaiseSourceDetected>d__77::MoveNext()
extern void U3CRaiseSourceDetectedU3Ed__77_MoveNext_mCD2B2EAAEC3539A9CFD61476E4A1394BA6BDEF76 (void);
// 0x000000B7 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<RaiseSourceDetected>d__77::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6 (void);
// 0x000000B8 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::OnEnable()
extern void InputSystemGlobalHandlerListener_OnEnable_mE51B9E57A822DF166BA80C7C1F3EF0E27E9A6334 (void);
// 0x000000B9 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::Start()
extern void InputSystemGlobalHandlerListener_Start_m646082223199D214B966CDA947646D59B74E7A21 (void);
// 0x000000BA System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::OnDisable()
extern void InputSystemGlobalHandlerListener_OnDisable_m4E7232E02F2B9AC058DF26E5B17DDDCD8B7E5A00 (void);
// 0x000000BB System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::EnsureInputSystemValid()
extern void InputSystemGlobalHandlerListener_EnsureInputSystemValid_m515C9327DB925F3BA2811BA579F20852F506C450 (void);
// 0x000000BC System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::RegisterHandlers()
// 0x000000BD System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::UnregisterHandlers()
// 0x000000BE System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::.ctor()
extern void InputSystemGlobalHandlerListener__ctor_m86BCCE3D91A93E6A3B28D998831596D975D79B5A (void);
// 0x000000BF System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_mFC68DF4C9CA99DD620FEFA5D6D24F9D2895BB564 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<Start>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B (void);
// 0x000000C1 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::.cctor()
extern void U3CU3Ec__cctor_mC8A90C96E1D88CE2F6A778D21FBB0E77E9466C57 (void);
// 0x000000C2 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::.ctor()
extern void U3CU3Ec__ctor_mD66F4EA2895C5D26EB655BC5261BD65B4996E653 (void);
// 0x000000C3 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::<EnsureInputSystemValid>b__4_0()
extern void U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m89844A843D3D8E4C9D4B3F87BB82236884B0E5B1 (void);
// 0x000000C4 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<EnsureInputSystemValid>d__4::MoveNext()
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_mAB5FACD80C03C56E8C4A200A03A98225C7E77AAE (void);
// 0x000000C5 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<EnsureInputSystemValid>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A (void);
// 0x000000C6 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::OnEnable()
extern void InputSystemGlobalListener_OnEnable_m7A9F927B1CB1BE009B3DD85CC6FDFEAA3DBC65BB (void);
// 0x000000C7 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::Start()
extern void InputSystemGlobalListener_Start_mD5DBB464B691AFCDE571E7F843CE88F39431E245 (void);
// 0x000000C8 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::OnDisable()
extern void InputSystemGlobalListener_OnDisable_m53F9F208C27CBF12AEC21EFD7ECFB3D2B1256499 (void);
// 0x000000C9 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::EnsureInputSystemValid()
extern void InputSystemGlobalListener_EnsureInputSystemValid_m847891045193EBB4FEC9F815149AF2A0AA9A4232 (void);
// 0x000000CA System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::.ctor()
extern void InputSystemGlobalListener__ctor_mD610BF668F3CA0691C5A83E4C3D3B18BB01377C7 (void);
// 0x000000CB System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m4C323A0DD7F833C4C47D0256CCE1CE07BD473CF2 (void);
// 0x000000CC System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<Start>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D (void);
// 0x000000CD System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::.cctor()
extern void U3CU3Ec__cctor_mC32486E47F6CCFE4DDDB3C32656C4C825AC90A97 (void);
// 0x000000CE System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::.ctor()
extern void U3CU3Ec__ctor_m745EE9B0C5157FF3E75852036F2320DBD85FAF02 (void);
// 0x000000CF System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::<EnsureInputSystemValid>b__4_0()
extern void U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m0929EA834E63ED45C72F7E646CFCB56E0F8ED5B9 (void);
// 0x000000D0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<EnsureInputSystemValid>d__4::MoveNext()
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m78B851E87D4DA8335500019F0FA8824730AA31AA (void);
// 0x000000D1 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<EnsureInputSystemValid>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750 (void);
// 0x000000D2 UnityEngine.Camera Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_RaycastCamera()
extern void MixedRealityInputModule_get_RaycastCamera_m5EBBAC2918E34E483E3E4E0A4E0E03A358A75CB6 (void);
// 0x000000D3 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::set_RaycastCamera(UnityEngine.Camera)
extern void MixedRealityInputModule_set_RaycastCamera_mC37A01ED874AEB9B3A305D5718A6F0E35EF0B29B (void);
// 0x000000D4 System.Collections.Generic.IEnumerable`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_ActiveMixedRealityPointers()
extern void MixedRealityInputModule_get_ActiveMixedRealityPointers_mD404D46EFCE9678AC4FEB250D1E14D44A7D80A12 (void);
// 0x000000D5 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ActivateModule()
extern void MixedRealityInputModule_ActivateModule_mC403E32504AE6251C56A4CC5DCE887A696E710D2 (void);
// 0x000000D6 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::DeactivateModule()
extern void MixedRealityInputModule_DeactivateModule_mC87411CC8D1AFBE82E61D1EAEBEFAC4CC15917DC (void);
// 0x000000D7 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Process()
extern void MixedRealityInputModule_Process_m455B4DD3F3664E1EA6A9A5A07C4229AE276F9306 (void);
// 0x000000D8 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ProcessMrtkPointerLost(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_ProcessMrtkPointerLost_mEB37964C8FCE6002FDEF7C3A19FC2B40EDA57706 (void);
// 0x000000D9 UnityEngine.EventSystems.PointerInputModule/MouseState Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::GetMousePointerEventData(System.Int32)
extern void MixedRealityInputModule_GetMousePointerEventData_m4DFAC7C578DDF14F8AF72979AB7728F7B5360851 (void);
// 0x000000DA System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::UpdateMousePointerEventData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_UpdateMousePointerEventData_mD256364C9C2B06D270FA21A3A7727E10E156BA17 (void);
// 0x000000DB System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ResetMousePointerEventData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_ResetMousePointerEventData_mC3EB2C14D43E5B9E92A0B9990A0E89E9869B3A78 (void);
// 0x000000DC UnityEngine.EventSystems.PointerEventData/FramePressState Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::StateForPointer(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_StateForPointer_mAB0D597A85B65620B373DC8C0FDD48AA53B0A3E9 (void);
// 0x000000DD System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mA537E625C1156979476A7ABB1043D76EC8EA3E68 (void);
// 0x000000DE System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m47DFF009BB7E154CD639B79720D4182D1D483CDF (void);
// 0x000000DF System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m628A777D1269E31730726E1FA12D0E098A5E76FF (void);
// 0x000000E0 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mC7FE04F80A7B9F2136B2748570C6143D547885DB (void);
// 0x000000E1 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::IsPointerIdInRemovedList(System.Int32)
extern void MixedRealityInputModule_IsPointerIdInRemovedList_m051D545523F3C2E9AC4152E1CA4A8C8B5757D9BF (void);
// 0x000000E2 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m5DB2995E215213532257F5F884E02CEF04D9F89A (void);
// 0x000000E3 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputModule_OnSourceDetected_m33241EA549BDE569491A5F18B3C330EE483A3E92 (void);
// 0x000000E4 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m44DDB4269D52B630E885F22BCD6341D1637E1277 (void);
// 0x000000E5 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::.ctor()
extern void MixedRealityInputModule__ctor_mE59CA71278433B3B67E2DE1C929A65924E264E6A (void);
// 0x000000E6 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::.cctor()
extern void MixedRealityInputModule__cctor_mB68480138E7A85BBC2C77576C59F689701A465CF (void);
// 0x000000E7 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.EventSystems.EventSystem)
extern void PointerData__ctor_m2F3DCA621335A4FE0F41C346AFC25BD05E877CE4 (void);
// 0x000000E8 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::.ctor(System.Int32)
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8__ctor_mF3A98BD0D0D2C7C8C37FF46BDD27381E108BA7D6 (void);
// 0x000000E9 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.IDisposable.Dispose()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_IDisposable_Dispose_m81F86CD2815658EFE836955595D3850A73135051 (void);
// 0x000000EA System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::MoveNext()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_MoveNext_mD8E1A16F6D711D801549FC470AEF4E949A80D6AC (void);
// 0x000000EB System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::<>m__Finally1()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_U3CU3Em__Finally1_m9B0FCB95A174A826CC1F9773236D3EA24A1085F3 (void);
// 0x000000EC Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.Collections.Generic.IEnumerator<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer>.get_Current()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m9568F1068F08E2264198020333FF2F6767945E4E (void);
// 0x000000ED System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.Collections.IEnumerator.Reset()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_Reset_mEE3D9D014BC562911BB721DCDCA35E34844E192D (void);
// 0x000000EE System.Object Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_get_Current_mB1FF6B129B0CD975E4FCC900F7FCE02D92B8ED90 (void);
// 0x000000EF System.Collections.Generic.IEnumerator`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.Collections.Generic.IEnumerable<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer>.GetEnumerator()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m96D61CBBA557F0BE30D5907F60C15828000F9CE8 (void);
// 0x000000F0 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__8::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerable_GetEnumerator_m8FAD01E0882328EB6A6490CFDF93F19424A8BC64 (void);
// 0x000000F1 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void MixedRealityInputSystem__ctor_mCBFAAC863AC579F9FF4BC901CA89C88AD2306C4F (void);
// 0x000000F2 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void MixedRealityInputSystem__ctor_m49CBA9C3F6527102B8F34968831319DD4196A3BC (void);
// 0x000000F3 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_Name()
extern void MixedRealityInputSystem_get_Name_mC32F1847F9B7F24AEF47BF87DCA70CD57EE4B5C2 (void);
// 0x000000F4 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_Name(System.String)
extern void MixedRealityInputSystem_set_Name_mB2D49E0C4C68FAA1CF4DC10EAF03BABD10A92640 (void);
// 0x000000F5 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::add_InputEnabled(System.Action)
extern void MixedRealityInputSystem_add_InputEnabled_m1CD6DD9E8CB62896E260E26A0DEFD3C5C7C669BF (void);
// 0x000000F6 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::remove_InputEnabled(System.Action)
extern void MixedRealityInputSystem_remove_InputEnabled_m6D3FAF296444A1C3CBAE2657E9FA58FEBC4614DB (void);
// 0x000000F7 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::add_InputDisabled(System.Action)
extern void MixedRealityInputSystem_add_InputDisabled_mA592CDD3F94DBE095D8B1345A24BD5296961F4FC (void);
// 0x000000F8 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::remove_InputDisabled(System.Action)
extern void MixedRealityInputSystem_remove_InputDisabled_mA1FA59DAFA1A68AEF5F5D1C398C093043623E967 (void);
// 0x000000F9 System.Collections.Generic.HashSet`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_DetectedInputSources()
extern void MixedRealityInputSystem_get_DetectedInputSources_m134E5FCEF6DD92AC33A99E81314E060BC08A723D (void);
// 0x000000FA System.Collections.Generic.HashSet`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityController> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_DetectedControllers()
extern void MixedRealityInputSystem_get_DetectedControllers_mA3036DA07CF5C3A6784CEE28B672B0077DD36792 (void);
// 0x000000FB Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_InputSystemProfile()
extern void MixedRealityInputSystem_get_InputSystemProfile_m602782FCBE0D5ECD45C04CD5196968B827C244AB (void);
// 0x000000FC Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_FocusProvider()
extern void MixedRealityInputSystem_get_FocusProvider_mF60DEAE82915EE180314A8EF05FCFE6CF560FC3E (void);
// 0x000000FD Microsoft.MixedReality.Toolkit.Input.IMixedRealityRaycastProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_RaycastProvider()
extern void MixedRealityInputSystem_get_RaycastProvider_m6042DCEFD2951DDE3F2E7469499714D51D02BA92 (void);
// 0x000000FE Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_GazeProvider()
extern void MixedRealityInputSystem_get_GazeProvider_mDBC37FA9CF1D0BBD3CF11F3D80FE8F511AC0F5CD (void);
// 0x000000FF System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_GazeProvider(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProvider)
extern void MixedRealityInputSystem_set_GazeProvider_mD0124EAFA6FA864EDA6A7B78152E79F301E521CC (void);
// 0x00000100 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_EyeGazeProvider()
extern void MixedRealityInputSystem_get_EyeGazeProvider_mEFB38A18AF9BD544D816002C7A6804C410E5F981 (void);
// 0x00000101 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_EyeGazeProvider(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider)
extern void MixedRealityInputSystem_set_EyeGazeProvider_m339E998955220B401FBED8BCFFA6145C811818EE (void);
// 0x00000102 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_IsInputEnabled()
extern void MixedRealityInputSystem_get_IsInputEnabled_m05585FA46FDAC92E2CC6585F0524756B9AAA4927 (void);
// 0x00000103 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_CurrentInputActionRulesProfile()
extern void MixedRealityInputSystem_get_CurrentInputActionRulesProfile_mBC867FA63A1B9CBF5DE129EEEC79D8F1061CB8A2 (void);
// 0x00000104 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_CurrentInputActionRulesProfile(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile)
extern void MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mFAB94A5E10A743D706D962E0444E04B1FB7665D4 (void);
// 0x00000105 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void MixedRealityInputSystem_CheckCapability_mE31FB54CA00A4CD5F5F4220B0A582F2149D00335 (void);
// 0x00000106 System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_Priority()
extern void MixedRealityInputSystem_get_Priority_m86DE4B6F57AE4A87CD0F8B9610466F7AD56C36CD (void);
// 0x00000107 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Initialize()
extern void MixedRealityInputSystem_Initialize_mECAD8C11A5D5B76A06FF29312B4C762D56C40D86 (void);
// 0x00000108 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Enable()
extern void MixedRealityInputSystem_Enable_mF5FF8DCA76AF52862844B83CB7D89F62C8898D66 (void);
// 0x00000109 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::CreateDataProviders()
extern void MixedRealityInputSystem_CreateDataProviders_m9DE879B9B3ECFB99115467F0D0293E0D8E5B9BCF (void);
// 0x0000010A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::InstantiateGazeProvider(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerProfile)
extern void MixedRealityInputSystem_InstantiateGazeProvider_m464FC79A26459EFF33D994860C4D1E7297FA36E9 (void);
// 0x0000010B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Reset()
extern void MixedRealityInputSystem_Reset_m77CE467C1BF6268AD3660A359E6C126C3AF5EA02 (void);
// 0x0000010C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Disable()
extern void MixedRealityInputSystem_Disable_mC5E356D7DA2F9EF1549219A9766E7E0DF1FE12F7 (void);
// 0x0000010D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Destroy()
extern void MixedRealityInputSystem_Destroy_m27262A390DA05A5FC2475F94EE150CE78BDF384F (void);
// 0x0000010E System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GetDataProviders()
// 0x0000010F T Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GetDataProvider(System.String)
// 0x00000110 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000111 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleFocusChangedEvents(Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler>)
extern void MixedRealityInputSystem_HandleFocusChangedEvents_m962F0FE91411C4872018954A5A1C81799BAE19E9 (void);
// 0x00000112 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleFocusEvent(UnityEngine.GameObject,Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler>)
extern void MixedRealityInputSystem_HandleFocusEvent_mAE4D22E916AEBE7F7698DDEBDACC1077DE2E068C (void);
// 0x00000113 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandlePointerEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000114 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToGlobalListeners(Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000115 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToGlobalListeners(Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000116 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToFallbackHandlers(Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000117 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToObjectFocusedByPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,System.Boolean,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000118 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Register(UnityEngine.GameObject)
extern void MixedRealityInputSystem_Register_m4519B1B2550DC1F3929AE0B3260EB5AEC6D42FF3 (void);
// 0x00000119 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Unregister(UnityEngine.GameObject)
extern void MixedRealityInputSystem_Unregister_mB90776F1DD00B1E8BB1DD1D87DC198B8D0AE02F7 (void);
// 0x0000011A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushInputDisable()
extern void MixedRealityInputSystem_PushInputDisable_m5FF7A5CE7CBCCFF1B8F820D619F183BE17950499 (void);
// 0x0000011B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopInputDisable()
extern void MixedRealityInputSystem_PopInputDisable_m121939D21BC45185DD97B59B6F11311E8D65F816 (void);
// 0x0000011C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearInputDisableStack()
extern void MixedRealityInputSystem_ClearInputDisableStack_m3E24CABEA9F7A2275BD4B0C677F9F286B4AEFAAF (void);
// 0x0000011D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushModalInputHandler(UnityEngine.GameObject)
extern void MixedRealityInputSystem_PushModalInputHandler_m87CC890E182ACAB61F9E4681AAE0C5EDBE11F2ED (void);
// 0x0000011E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopModalInputHandler()
extern void MixedRealityInputSystem_PopModalInputHandler_mB60637C72F670A2B69B46F8F095EB1A65A28674D (void);
// 0x0000011F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearModalInputStack()
extern void MixedRealityInputSystem_ClearModalInputStack_mFEB18A7B20832D73E62DA438705E812AAA1FF64D (void);
// 0x00000120 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushFallbackInputHandler(UnityEngine.GameObject)
extern void MixedRealityInputSystem_PushFallbackInputHandler_m539812EE9E6EAB3BC44B1FD50CE72DCC304F5209 (void);
// 0x00000121 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopFallbackInputHandler()
extern void MixedRealityInputSystem_PopFallbackInputHandler_m413FAA64BDACFB976465F58C07741808F282CABE (void);
// 0x00000122 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearFallbackInputStack()
extern void MixedRealityInputSystem_ClearFallbackInputStack_m2A306DC4FD6966F9EE7A9EB79D5C40CFD598F9DF (void);
// 0x00000123 System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GenerateNewSourceId()
extern void MixedRealityInputSystem_GenerateNewSourceId_mC59C6F312F70F9FA4CEC2FDE916792C5CA7A7414 (void);
// 0x00000124 Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RequestNewGenericInputSource(System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[],Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void MixedRealityInputSystem_RequestNewGenericInputSource_m475CF373AE055DEBD313B94343226A52E53B35A6 (void);
// 0x00000125 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceDetected(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void MixedRealityInputSystem_RaiseSourceDetected_m5830B595259BF976516B64C18BA0202215708E0B (void);
// 0x00000126 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceLost(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void MixedRealityInputSystem_RaiseSourceLost_mB4C5595A95C96E4F73BCE4EE65D4D0648F75526F (void);
// 0x00000127 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceTrackingStateChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.TrackingState)
extern void MixedRealityInputSystem_RaiseSourceTrackingStateChanged_m6B99FAFBCAC1430AC98797B28415B2111D0FE2B6 (void);
// 0x00000128 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePositionChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseSourcePositionChanged_mF6585E9AE3F97F6B6A1401C27E52E87567D9A193 (void);
// 0x00000129 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePositionChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseSourcePositionChanged_m546DF514895131D86A7BCF568CD7359E66A76921 (void);
// 0x0000012A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceRotationChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseSourceRotationChanged_mAA9196198D6FB43C86AF62B90128263C91E11187 (void);
// 0x0000012B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePoseChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseSourcePoseChanged_m6FE3FBE061655CAAE206614361E288DB74E7CA99 (void);
// 0x0000012C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePreFocusChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaisePreFocusChanged_m0284B94A742D7BF44ED60AA10DC3FD196AF9D5C8 (void);
// 0x0000012D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusChanged_m93AF6AB04EF1291225C67A1D52F8C39C366B6832 (void);
// 0x0000012E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusEnter(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusEnter_mBFD2B7D78BF1B160253DF93329A49805052121A5 (void);
// 0x0000012F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusExit(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusExit_mF022A2A8E8D35A71269AE70F61216073C4DE693F (void);
// 0x00000130 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerDown_m71F07B376C01C99B23D70AFBC458886C218AC562 (void);
// 0x00000131 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerDragged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerDragged_m9F6A1B8030C23820F2795C99C5F0B383DFD99438 (void);
// 0x00000132 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerClicked(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerClicked_m7E0408EC3123BE2CE869A50A4DB0176F35F0FFF3 (void);
// 0x00000133 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleClick()
extern void MixedRealityInputSystem_HandleClick_m30481C516558937AC76A9658B9024964BCA07AEB (void);
// 0x00000134 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerUp_mCFEE40027BF38D2073403417DF147C99AE577B44 (void);
// 0x00000135 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnInputDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseOnInputDown_mA08160D1A553B61A34AEF2688106C36913285B2B (void);
// 0x00000136 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnInputUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseOnInputUp_mF2D73D1D0EFD430E2D7423AAA20EFE48DC5EC7E2 (void);
// 0x00000137 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFloatInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Single)
extern void MixedRealityInputSystem_RaiseFloatInputChanged_m416B75B47BBEA6386CBC3E3189870BF07C5FB3D3 (void);
// 0x00000138 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePositionInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaisePositionInputChanged_mE87013DAEAF6EAFD42B6E31EE28E575C13EF2E12 (void);
// 0x00000139 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePositionInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaisePositionInputChanged_m18CBF4C3A2932CC78499F37E42A7B94ECB428072 (void);
// 0x0000013A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseRotationInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseRotationInputChanged_m02A1D1F20495C56789D9A35E73C523F5540D241A (void);
// 0x0000013B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePoseInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaisePoseInputChanged_m7328018B541BED20B6F205B2E2BBC59DDCB1764D (void);
// 0x0000013C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureStarted_mDB77FF55FA1E1B4A79F6B8C834436305A7FDBEED (void);
// 0x0000013D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m3636951588FE28749B81334AE099F57725CB5FB7 (void);
// 0x0000013E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseGestureUpdated_mDF8C4A1DF8B2AF73B80CF6114FF0C74EF1B20E0C (void);
// 0x0000013F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseGestureUpdated_mD3161870F9738275179EF41382C41F0FE2D6A834 (void);
// 0x00000140 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m8D9A3F7C2D2CB74134EF3394C4DB27BF98CDBFF1 (void);
// 0x00000141 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m4D59E6D520D56E3773DCC86E0FB1ADA5BC3A3BE2 (void);
// 0x00000142 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m747A07338F6A0509D389BC95952A3AC791F7889D (void);
// 0x00000143 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m4E797F9C4FE6390F6A24941B9A4E055EE0C39E97 (void);
// 0x00000144 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m069CAEC88EF60FD1EDA98CE45FEE30206A0D0B26 (void);
// 0x00000145 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m1BAA275BD345A04119232A5CB451D24CEB687F35 (void);
// 0x00000146 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m6DFFB3C58D3D735BA2919710104C071D9DD100D1 (void);
// 0x00000147 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureCanceled_m42AA55427125B489E2CAFF7848456318A4CB1306 (void);
// 0x00000148 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSpeechCommandRecognized(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.RecognitionConfidenceLevel,System.TimeSpan,System.DateTime,Microsoft.MixedReality.Toolkit.Input.SpeechCommands)
extern void MixedRealityInputSystem_RaiseSpeechCommandRecognized_mD4CBE4E016FE6B3BA19CEA5170765324927BCB79 (void);
// 0x00000149 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationHypothesis(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationHypothesis_m40E7B3103F4FBB159E427C913F3DB51FDF1EB9C4 (void);
// 0x0000014A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationResult(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationResult_mEA704DDADEB3B0E306FD82036A33BB0DCD14A673 (void);
// 0x0000014B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationComplete(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationComplete_mE6E3C38764DF405C0EA7C732D5F986ECA090D57D (void);
// 0x0000014C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationError(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationError_m4342EAEDFD44249D510934306F66430E5491000D (void);
// 0x0000014D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>)
extern void MixedRealityInputSystem_RaiseHandJointsUpdated_mC96ABEEFA1E46BF29AAA5628139D39EF43732D53 (void);
// 0x0000014E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseHandMeshUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.HandMeshInfo)
extern void MixedRealityInputSystem_RaiseHandMeshUpdated_mFC72CEE2AA6EE231AE2904091B4E8EC9A8EEA339 (void);
// 0x0000014F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchStarted_m5BAE8EC21AEF923696B22F88DF3C2013AAACBCA8 (void);
// 0x00000150 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchCompleted_m0DE444233AF7F66F08A16DFDEE371AD2D3B77B14 (void);
// 0x00000151 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchUpdated_mB399F63DC294EF2D801020316E72FB707CBD3233 (void);
// 0x00000152 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules_Internal(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,T1[],T2)
// 0x00000153 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Boolean)
extern void MixedRealityInputSystem_ProcessRules_m1C8B4AF9E3120D2C339D7AD89AC1F1C2E3A8A598 (void);
// 0x00000154 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Single)
extern void MixedRealityInputSystem_ProcessRules_mA96EE551DB90A6AE71D582A3E1FC7EA78E61A92A (void);
// 0x00000155 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_ProcessRules_m8778EA7C8AF81CBA034126F7EC0641BB54DD6981 (void);
// 0x00000156 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_ProcessRules_m1BAFE9CFFE57E01C4DE6FC9ADC1F4FA1B560A92B (void);
// 0x00000157 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_ProcessRules_m3D1B0AA50F91F222421BCB9A4244BC193B5DC461 (void);
// 0x00000158 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_ProcessRules_m42794788CE56EB3DD2720C40DDAF457860A98A0E (void);
// 0x00000159 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.cctor()
extern void MixedRealityInputSystem__cctor_m71B547E800DA41F59CC78E17800A0921507B02A0 (void);
// 0x0000015A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::.cctor()
extern void U3CU3Ec__cctor_mEB624057F0A51A303F0D70C8ACE8C6B58EE5F1A4 (void);
// 0x0000015B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::.ctor()
extern void U3CU3Ec__ctor_m9D389BA8DAD63B3748236B62EAB68C7891EB8549 (void);
// 0x0000015C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_0(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_0_m525234F28C3C3C8EB7403A6A6BC6C43EBA3D93D9 (void);
// 0x0000015D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_1(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_1_m876FBFEF02A8D22AFA879B7DF10741E046883A79 (void);
// 0x0000015E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_2(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_2_mC86D31C235E922ED1C3D8742C90081B3752DC7A9 (void);
// 0x0000015F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_3(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_3_m21689171154D1D1DE85E413C58B633F809018A9F (void);
// 0x00000160 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_4(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_4_mF865B3FA559AFF909DE84026E3A1B3303FB56481 (void);
// 0x00000161 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_5(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_5_mB756317FC4EB7BEC91EE447C7D22CB3BAF78A52D (void);
// 0x00000162 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_6(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_6_m9C5AC2C24E7AB6C91AF08E9EF8B663438EB0AA22 (void);
// 0x00000163 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_7(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_7_m28856F0840DF3D8D5DDBCDFC5C6F13713015E496 (void);
// 0x00000164 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_8(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_8_mC4B8E2D3438D7D5C5C37B41E127E6B9A2D510DD8 (void);
// 0x00000165 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_9(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_9_m90753BC90BDBB2294CB80AD55B922BFD7A7E99BA (void);
// 0x00000166 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_10(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_10_mA33C5B092E43F6319B946EFAE8B540CC9A4642A7 (void);
// 0x00000167 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_11(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_11_m04A424D53BA479C70DEB8DBE50F1ED72D9F62023 (void);
// 0x00000168 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_12(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_12_m57D4D9F69F782251435E06589442E8B4C4196C4A (void);
// 0x00000169 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_13(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_13_mEB72B2EF158F9552D5C502CD1DA34AE76AFFAD53 (void);
// 0x0000016A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_14(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_14_m8AA79C3DB1C38D844774BE23AB3D8C83ADDBA922 (void);
// 0x0000016B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_15(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_15_mF0A3A28E7503A3161598B734F8BBB88AE1A6FB84 (void);
// 0x0000016C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_16(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_16_m2705537F14E66ABF1B2F73F2BD52622A75C65880 (void);
// 0x0000016D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_17(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_17_m4E8342574CE106396911C0D2FF5EBEBC81694DD0 (void);
// 0x0000016E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_18(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_18_m45431E95849ADB6CBA124A9ACBE80AE507E361CB (void);
// 0x0000016F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_19(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<System.Single>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_19_m3FAC2ADBF04FD4F6F4D6ADE10112A4EB53DFAE44 (void);
// 0x00000170 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_20(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_20_m68030209C501F202D588A88A9BDAC92D1AF06272 (void);
// 0x00000171 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_21(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_21_mEC04976F0D889C964D8EDA2CA596FA5D169F3518 (void);
// 0x00000172 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_22(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_22_m8F8919BD4B0D9A838E5EECB67CCB3CBA91120905 (void);
// 0x00000173 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_23(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_23_mE711847519123254395174DD39F1041409DB4F89 (void);
// 0x00000174 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_24(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_24_m41D0469969E6DEE9935DEC00BFF0C90043B6CE54 (void);
// 0x00000175 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_25(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_25_m2F8F283A0B89D18C9051B943E94706AE0F07E1A3 (void);
// 0x00000176 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_26(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_26_m644C8DE660F872590333CCA9264437AA37DBF7E2 (void);
// 0x00000177 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_27(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_27_mF2CC7FEBE3AFC5AC601884A22DDE69A58C22F357 (void);
// 0x00000178 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_28(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_28_mE6864DC22E4CD59BFEE3AC0D6C0703A6B1DAB2C2 (void);
// 0x00000179 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_29(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_29_m60D432C49EAE743751639D7EDE1109739D70C02A (void);
// 0x0000017A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_30(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_30_mF938DD3B8724E0D0F73996353581A79A0F88A895 (void);
// 0x0000017B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_31(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_31_mE88E9A6AF80200CFE01145C9BE9B0703A1DA6712 (void);
// 0x0000017C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_32(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_32_m5DDA0AD1462A430AECCE96B5DB689CF957791CBA (void);
// 0x0000017D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_33(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_33_m877A6A5CD849A5A026B12D9F52C9C80FD9B1D9C4 (void);
// 0x0000017E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_34(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_34_m37E35576C51863E9ABD17CEABF6C5A2790FF09F6 (void);
// 0x0000017F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_35(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_35_m076DBB3D503C733AA8E96B9F0639C55CD6BFDB20 (void);
// 0x00000180 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_36(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_36_mBD0C16B0993BCC57518755B4E3C45034122EECD1 (void);
// 0x00000181 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_37(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_37_mAC759A8D9E8FD38211408BC9C0FEFD2E6E881BD4 (void);
// 0x00000182 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_38(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_38_m32E06893EA30D8FEFED2748D5C658FC02BA2B627 (void);
// 0x00000183 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_39(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_39_mC070F0473B1CBB520E2A68B8059C5E3683C474F8 (void);
// 0x00000184 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_40(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_40_mF80EA22470EB25C36006541902A640CC27DE1B42 (void);
// 0x00000185 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_41(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_41_mE363DCF662C5B406DB95AC9F3025CE16A695AA6E (void);
// 0x00000186 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_42(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_42_mF642F722136A72FAAF87875BBC320FBAFE771393 (void);
// 0x00000187 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_43(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_43_mB75F095F226C6E11DB43C1970E51FF06D94E8F88 (void);
// 0x00000188 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_44(Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_44_mD6D0D27098C69E3BF89DACED9587BAB09BC4BC0C (void);
// 0x00000189 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_45(Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandMeshHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_45_m092D12124EFAD480E7475D9DA7C679F9724C2511 (void);
// 0x0000018A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_46(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_46_m39A85C3EB1C552A0EF54989FB94DAD8A2B800526 (void);
// 0x0000018B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_47(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_47_mFFD605711F52E238FFB0B751CF6E878016EE1D29 (void);
// 0x0000018C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__240_48(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__240_48_mB539E7F9519B8A142E87E84B338ABD09806C33AA (void);
// 0x0000018D System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionGrabbable::OnEnable()
extern void NearInteractionGrabbable_OnEnable_m7DEBC2467E5C6061A5A85ADC0BC494BE2BBCECBA (void);
// 0x0000018E System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionGrabbable::.ctor()
extern void NearInteractionGrabbable__ctor_m241DE7D67105307D3B49B95D310602C5BC5F5E34 (void);
// 0x0000018F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalForward()
extern void NearInteractionTouchable_get_LocalForward_mF25D6950ED0AD34E52E7064F5D92646BB7379062 (void);
// 0x00000190 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalUp()
extern void NearInteractionTouchable_get_LocalUp_m32F1523D8D7190B34DECB5A51F963406327A076A (void);
// 0x00000191 System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_AreLocalVectorsOrthogonal()
extern void NearInteractionTouchable_get_AreLocalVectorsOrthogonal_mA01A7AC5185521468558395A19BC7A2B7F0F2658 (void);
// 0x00000192 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalCenter()
extern void NearInteractionTouchable_get_LocalCenter_mE97E72A423E98217E09C8CE621665835CE012533 (void);
// 0x00000193 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalRight()
extern void NearInteractionTouchable_get_LocalRight_m2E9197705A3B4E3B07940D6B565C93F336361004 (void);
// 0x00000194 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_Forward()
extern void NearInteractionTouchable_get_Forward_mF986BCD574CFC2309B57E80533D2070587A157A5 (void);
// 0x00000195 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalPressDirection()
extern void NearInteractionTouchable_get_LocalPressDirection_m77CFF9CE630F8BA7004700FB7266A7201ACB0770 (void);
// 0x00000196 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_Bounds()
extern void NearInteractionTouchable_get_Bounds_mD81866F6ECADCA44EAE054543D824C826B140FB5 (void);
// 0x00000197 System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_ColliderEnabled()
extern void NearInteractionTouchable_get_ColliderEnabled_m3E873E8CC43C4D22124909E8A24BD8DB5C555F39 (void);
// 0x00000198 UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_TouchableCollider()
extern void NearInteractionTouchable_get_TouchableCollider_m610B62724339F0F8C9571F026A3176E2852FF8D9 (void);
// 0x00000199 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::OnValidate()
extern void NearInteractionTouchable_OnValidate_mCB421C48D4F62A93D182DAF012D8A1C2471E9A0B (void);
// 0x0000019A System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::OnEnable()
extern void NearInteractionTouchable_OnEnable_m047011120D7FED1B25D460F1311EFBB1D32E5F0D (void);
// 0x0000019B System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalForward(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalForward_m65968DD0EBE51683F1DA87BF1411D2826F4CC25C (void);
// 0x0000019C System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalUp(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalUp_mDB5C11FC8A422FC76709CA8EC094067152D57FA4 (void);
// 0x0000019D System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalCenter(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalCenter_mDCC21B38F1D5045B59ECB340BB89A795C4D945D2 (void);
// 0x0000019E System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetBounds(UnityEngine.Vector2)
extern void NearInteractionTouchable_SetBounds_mDAF144A75C3BF3517D8F77744CCF4D84894EDDC6 (void);
// 0x0000019F System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetTouchableCollider(UnityEngine.BoxCollider)
extern void NearInteractionTouchable_SetTouchableCollider_m47C6561F66AD76B6FA99D04B694CB85FA4533273 (void);
// 0x000001A0 System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchable_DistanceToTouchable_mF089274125BEFA2E6B3A67044520E0D28E93147F (void);
// 0x000001A1 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::.ctor()
extern void NearInteractionTouchable__ctor_mA211A6C387CCEEE332C32DE5D6224F8BEF66CD17 (void);
// 0x000001A2 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::.cctor()
extern void U3CU3Ec__cctor_mD1B1E18C29182562FC99414E1442BA02F9D44D92 (void);
// 0x000001A3 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::.ctor()
extern void U3CU3Ec__ctor_m4844B4656AC70CA4F32DA94E483C1B15968D284A (void);
// 0x000001A4 System.String Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::<OnValidate>b__25_0(System.String,UnityEngine.Transform)
extern void U3CU3Ec_U3COnValidateU3Eb__25_0_m772CE3B48534FD5D71BD04057A2361180548FFD9 (void);
// 0x000001A5 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_LocalCenter()
// 0x000001A6 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_LocalPressDirection()
// 0x000001A7 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_Bounds()
// 0x000001A8 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::.ctor()
extern void NearInteractionTouchableSurface__ctor_m6DBA5832EB7A7FBBCE489EC0E2854C2DA6CEDCA5 (void);
// 0x000001A9 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI> Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_Instances()
extern void NearInteractionTouchableUnityUI_get_Instances_mDCA0E86351579A84DAF70659F7D0F4A3789E8B9D (void);
// 0x000001AA UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_LocalCenter()
extern void NearInteractionTouchableUnityUI_get_LocalCenter_m0954A95FA0D61F634B6E14026FCAE9BA6589F53B (void);
// 0x000001AB UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_LocalPressDirection()
extern void NearInteractionTouchableUnityUI_get_LocalPressDirection_m471D0AFA72DACEEACF04FEFF28E6C2F7B5F042B9 (void);
// 0x000001AC UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_Bounds()
extern void NearInteractionTouchableUnityUI_get_Bounds_m1BBEC715835F2ED44418B2B0198FD5056B2FB2FE (void);
// 0x000001AD System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::.ctor()
extern void NearInteractionTouchableUnityUI__ctor_mA3B66DEF6CEED77F379689CC3B9595D5E2006111 (void);
// 0x000001AE System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchableUnityUI_DistanceToTouchable_m851652FACA38220BE297C1B296B7D9EF619825F2 (void);
// 0x000001AF System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::OnEnable()
extern void NearInteractionTouchableUnityUI_OnEnable_m619029642940BD5EFA6E9ABB19365ABAE35F8422 (void);
// 0x000001B0 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::OnDisable()
extern void NearInteractionTouchableUnityUI_OnDisable_m020E63F1BDB03FA29A2D85FB92B72ED664EE52F4 (void);
// 0x000001B1 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::.cctor()
extern void NearInteractionTouchableUnityUI__cctor_m7A1CC65E3B1E873CEDE6ACA5BDD92CD998C4A136 (void);
// 0x000001B2 System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::get_ColliderEnabled()
extern void NearInteractionTouchableVolume_get_ColliderEnabled_m5FDAFA7735CD6DA3306ADACEE12591A4FB411E04 (void);
// 0x000001B3 UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::get_TouchableCollider()
extern void NearInteractionTouchableVolume_get_TouchableCollider_m4B05AF553637BA8B7B7E0328BF48C4C12623C10A (void);
// 0x000001B4 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::OnValidate()
extern void NearInteractionTouchableVolume_OnValidate_m4951B3E4E020FC5F0446EB338AE1DB02D6DD6349 (void);
// 0x000001B5 System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchableVolume_DistanceToTouchable_mC4B7F8A11DB40419E1DE595C08334BAB2D09B24D (void);
// 0x000001B6 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::.ctor()
extern void NearInteractionTouchableVolume__ctor_mB4B2D40ACE279F38AC0E664BAF827FBB8C16117E (void);
// 0x000001B7 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerClicked_mF18E07625E7EB838D4B293A328002949CBBA187E (void);
// 0x000001B8 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerDown_m273A518A59E5662F7FEB6373B0C29E483A4C78D3 (void);
// 0x000001B9 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerDragged_m2FB19C5600F07AACFA96977D8498CF03415AB2A8 (void);
// 0x000001BA System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerUp_m058194B41BDA2B38C3CFA83E7931BFA59CE7F714 (void);
// 0x000001BB System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::Start()
extern void CanvasUtility_Start_m5FD2A7B77D4CAD8FA7D017E0447388D1A7F2D074 (void);
// 0x000001BC System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::.ctor()
extern void CanvasUtility__ctor_mA44F4DC903F30E94859A2EE453AE3AB2C3D1953D (void);
// 0x000001BD System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::Awake()
extern void ScaleMeshEffect_Awake_mD3DECB9CC81A3832D5196401404D3BAF54921AB8 (void);
// 0x000001BE System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper)
extern void ScaleMeshEffect_ModifyMesh_m3AA4BF3C24E81269AF1765BA1CCA7C91F00CBA3C (void);
// 0x000001BF System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::.ctor()
extern void ScaleMeshEffect__ctor_mE96C5C9DA997BF9438B902AD1E2A2F39C0CD0FBA (void);
static Il2CppMethodPointer s_methodPointers[447] = 
{
	BaseNearInteractionTouchable_get_EventsToReceive_m0505A1FB06A460224393FC173D1827843693EA54,
	BaseNearInteractionTouchable_set_EventsToReceive_m492A4370E83CC973F72584FEED2827E200C7E41C,
	BaseNearInteractionTouchable_get_DebounceThreshold_m3C15CEA64527EF19D8FDDCD912DC47785D96E846,
	BaseNearInteractionTouchable_set_DebounceThreshold_m2D4DDD7968E31CD94F328999AECA1091DD2A5602,
	BaseNearInteractionTouchable_OnValidate_m8A1C58AC88AE6551B70DFD8475064E1C62D34006,
	NULL,
	BaseNearInteractionTouchable__ctor_m93F939290D114E005ADB131ACDBF0A6358090AE4,
	ColliderNearInteractionTouchable_get_ColliderEnabled_mB9EB8D5247221CBA1FB77838806D0DEED24D223F,
	ColliderNearInteractionTouchable_get_TouchableCollider_mB59C8E7EC64DD918D22C6E21D75104A26EF1843D,
	ColliderNearInteractionTouchable_OnValidate_m9FDFA4A931C62CC1F16D13BC6218B2EE16102100,
	ColliderNearInteractionTouchable__ctor_mF70E4D84C8724EA39FE2226205EEBD4CBC47BC94,
	DefaultRaycastProvider__ctor_m6C9BFDF95ED7D2A3637CFADB18E944021FAD5502,
	DefaultRaycastProvider__ctor_mF71BBF984AD9EFFD209653769BFDAD36E618319A,
	DefaultRaycastProvider_get_Name_mB538072A2AA39C1F0D4E94CAD47753B13883B819,
	DefaultRaycastProvider_set_Name_mC07E922AC5547DC7329A72AC0D40E3B22D2D3368,
	DefaultRaycastProvider_Raycast_mD9A4598CB3D81F166A9CF0D80AFC7966E27F46EF,
	DefaultRaycastProvider_SphereCast_mD7D22ACA928F29279E80D7D8E4C4DDE466F4002F,
	DefaultRaycastProvider_GraphicsRaycast_mCFAA6CB42CBB950284C1ABA88655998E7B6A036D,
	DefaultRaycastProvider__cctor_m555D9B394EFDD3F3CBC25922D13997EBFD4F053A,
	FocusProvider__ctor_m8D0E8A29D807C00C1D81BB5A466615370FC4A8DF,
	FocusProvider__ctor_m175FA798215CF56A3E9B94033E3AAF32EED75581,
	FocusProvider_get_PointerMediators_m0FD9722F816E4BA09114CF863A7C3B9CE94483D4,
	FocusProvider_get_NumNearPointersActive_mD274E9C0A892E817F66A743C56DEF37E67ABF072,
	FocusProvider_set_NumNearPointersActive_m9613789E92FCBB8522F58F2DA6A078661EE12387,
	FocusProvider_get_NumFarPointersActive_mC0F81CB40E3972A1117970B2DD5B0D726855EC4F,
	FocusProvider_set_NumFarPointersActive_m13DC9A8A3E637896C6ACA296C6990AF2CA2ACCD4,
	FocusProvider_get_PrimaryPointer_m67A6C329DE4381982D42BAFAD8C9500444E6C258,
	FocusProvider_set_PrimaryPointer_mDF5C85A4CD191F11E32C62E3A61C99609D0EBDE1,
	FocusProvider_get_Name_m720C4506B2E808BF28B2B3023E7CE2EC657C493A,
	FocusProvider_set_Name_m503BE058D12180B3711D681E4EED3A2279D4E90A,
	FocusProvider_get_Priority_m47F0D5EB72E8D7C0214FE6FAD2DA655F5E331B75,
	FocusProvider_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusProvider_get_GlobalPointingExtent_mEF53A5D73E8CA23DBF51B2C130170FD6AD79C38F,
	FocusProvider_get_FocusLayerMasks_mE55BF03590DBDB993743AA30CE343F09E92C206D,
	FocusProvider_get_UIRaycastCamera_m59F704468E3415AE7EE64F7BDFDF28F797AD006E,
	FocusProvider_get_IsSetupValid_mFEF4AB0FA78DC2C8BF9AF27EB959CE39768596EA,
	FocusProvider_add_PrimaryPointerChanged_m0B2C3345940B23C2293660EEBAA5496D4A050E07,
	FocusProvider_remove_PrimaryPointerChanged_mF9C04B5518DD91BBB33D102BBF7E59ECB3EA935B,
	FocusProvider_Initialize_mB70ECA1601A08AAFF74972D95CBBEF90D9AA257D,
	FocusProvider_Destroy_m45682B8433D684F7248520BD6C1B71900C536C4B,
	FocusProvider_Update_mBF08DE91B8EEDFB370C78DD3FD0DA9D05ABA3D55,
	FocusProvider_UpdateGazeProvider_m6BD1C1703E153BDC313209FCE766BE19338A2D17,
	FocusProvider_GetFocusedObject_m34363B1D194B56795919F9381EA833FDB2157925,
	FocusProvider_TryGetFocusDetails_m2C1393C1080F79A66DF9DBA8653C41A0CFA76619,
	FocusProvider_TryOverrideFocusDetails_mDC281510A6B4F17AE8C573B842D8BB2F743BD8AC,
	FocusProvider_GenerateNewPointerId_m5DC46AE0652DF3EB0D0B1E6D2D408BB93C310CAA,
	FocusProvider_FindOrCreateUiRaycastCamera_mB8C5D0954084D4899015CDE6159D5FF7560BE6FE,
	FocusProvider_CleanUpUiRaycastCamera_mCDF009AB414796C9064D1AEFCD188DA21CBA9835,
	FocusProvider_IsPointerRegistered_m15FDADAB2BA0405C6F6CFEE2308B9C6570EB67FE,
	FocusProvider_RegisterPointer_m02CA192C760BED0E4C027F6526DA07711219119C,
	FocusProvider_RegisterPointers_m752EE4215B7AE482CB0216E572D6D919CE0EDC65,
	FocusProvider_UnregisterPointer_m747A9A0471F267FD427372B2399E9CA5BE558D71,
	NULL,
	FocusProvider_SubscribeToPrimaryPointerChanged_mE8A065E6F9983E4ED6B7ACB5E7BA1DFF25894090,
	FocusProvider_UnsubscribeFromPrimaryPointerChanged_mC84B891347B1D9C63FDE141AF6D861E7758CC8DB,
	FocusProvider_TryGetPointerData_m84B662BFAC034D8A969BBC8A21E9C04E129EBA0E,
	FocusProvider_UpdatePointers_m5A9E9E0AE3F5BDF5030097904F1A425D1C9A7C2C,
	FocusProvider_UpdatePointer_m0917842AEE2470F2EA51746333E8B64754EDAF7B,
	FocusProvider_TruncatePointerRayToHit_m8EDA73CAFC4F986408A18943046ED9A485697EC1,
	FocusProvider_GetPrioritizedHitResult_mB1F0218429B02A4D22F8EE2E9DB8653DF8C8DA8C,
	FocusProvider_ReconcilePointers_mD40D1C6375E5732BA810959C9794CE09DB680C9D,
	FocusProvider_QueryScene_m0BA963B07BEBF925BC31C1599E4DE831AE0B065A,
	FocusProvider_RaycastGraphics_mD6EA004478F55D085BE84BAF431B9D95591512B4,
	FocusProvider_RaycastGraphicsStep_mB6D1F1E3AF1A7D275579C6F72B07AEE09FE585CC,
	FocusProvider_UpdateFocusedObjects_m2FABD19BDF0775BAEFC163E2A9BAD6ADC32A2E43,
	FocusProvider_OnSourceDetected_mE6B127303131DD549E60D59088FDE645155BB4F8,
	FocusProvider_OnSourceLost_m87A12A91144EEFB30F9E9B51BC386334B025856B,
	FocusProvider_OnSpeechKeywordRecognized_m0F1914D8FBD7D1B62F28F98A28D1C7CF97567516,
	FocusProvider_GetPointerBehavior_m3BDEE6F27053B53C231E5C72170D5F82AACAC71E,
	NULL,
	FocusProvider_GetPointerBehavior_m42A082B9B97A48823BB76B76DB8FB6C32517BE65,
	FocusProvider_get_GazePointerBehavior_mDF1B6115D96E6277DF7CAC5B1C388C0405521E3D,
	FocusProvider_set_GazePointerBehavior_m0E22B435FBCB479CF07FA4A56648B217640878D4,
	NULL,
	FocusProvider__cctor_m90281887F81FD5E807A69AB0B3F2D338458688A5,
	PointerHitResult_Clear_m37A1D48BB64830FF92CFF2BF321BB231AFA7BCAB,
	PointerHitResult_Set_m5CCE1F892E8D535427EB09BE35899925007331FD,
	PointerHitResult_Set_m9390771108296A6E273D641433FE4C35E3C66D97,
	PointerHitResult_Set_m813BC86177D934696A741FAF6F684D3A3DAD9105,
	PointerHitResult__ctor_mBEC90BEDD5F2FFF6C9AB32443F0E71D5D7F08461,
	PointerData_get_StartPoint_m422E8F053289CEC020F5C70D77B659F27C293704,
	PointerData_set_StartPoint_mE7A9832F845512E22F776B7771A6FE382D19F650,
	PointerData_get_Details_m449621AE07F092995FC8D1D7779FC4A929BC25E8,
	PointerData_set_Details_m5608FB0B6C7812A601B624D10F08CAD477968972,
	PointerData_get_CurrentPointerTarget_m696E8BAF4EAC0B660C3B898B91DC7C25155DAF32,
	PointerData_get_PreviousPointerTarget_mE4A4F7D8FF4B703D0CA41233C46681F0700C4AD6,
	PointerData_set_PreviousPointerTarget_mAB6B24961FF961038C0BA412674131767E2AE158,
	PointerData_get_RayStepIndex_mBEFE82827761AD963E494D0AD4E4AF6B910244BB,
	PointerData_set_RayStepIndex_m1F1B0B9D2359CCD008CD57135A7E643D533DFDB0,
	PointerData_get_GraphicEventData_mD3DBC4A4F2CCEC60815AAE1D93CB8D7A70853E52,
	PointerData_get_IsCurrentPointerTargetInvalid_m641BF8EDEC1187908E2689D320628B6026C98FC8,
	PointerData__ctor_m77FC628173F230336DCEFB1F78EB91FE0D888F97,
	PointerData_UpdateHit_mE9B2F582288E913E769B8B075B64DA6269D05362,
	PointerData_UpdateFocusLockedHit_mEB1F4C03A34EE69E2180AE355E5E2D5318C5226E,
	PointerData_ResetFocusedObjects_m63EAC7BEAE7B82AA5A22C25F2B288BEB852C4DFC,
	PointerData_Equals_mA12226B69AF91E0379719F5055947F9F9068E0A8,
	PointerData_Equals_m45D094AC03D5303BB9628B946976B28883D84562,
	PointerData_GetHashCode_m856A80E826A7475D3EDDD9762DF44E8308DF3855,
	PointerData__cctor_m39CE37A04540D315935104AFFCE1FD41052D3379,
	PointerPreferences_Matches_m2CC26081615B3A98AF390150B7EE6E930C684968,
	PointerPreferences_Matches_mD9B2D82C9CB272C9DD307574CD31F4602C790497,
	PointerPreferences_GetBehaviorForHandedness_m86E34BC76D17378233458D330C554969F4F5DE11,
	PointerPreferences_SetBehaviorForHandedness_m75D751BBD751DC20FE5E36A409AA28DABCF5215B,
	PointerPreferences__ctor_m159AF82C116638A657FFA5DA997C1496DF89F8F9,
	GazePointerVisibilityStateMachine_get_IsGazePointerActive_m86CE3233AB1763B852185A0B288200020C3000B5,
	GazePointerVisibilityStateMachine_UpdateState_m69AACD2DA02B7EE0AC56248D7ABB821070E1139E,
	GazePointerVisibilityStateMachine_OnSpeechKeywordRecognized_m4D5233497AD43570A301E4EC30C97A8CB66686DD,
	GazePointerVisibilityStateMachine__ctor_mCAE946FE1BB775115E1CAE30873574444D956092,
	GazeProvider_get_Enabled_m368C901012FDE826E6FE567F1073EBF335F2CD41,
	GazeProvider_set_Enabled_mF1033CC00741D9CFF1DF41055C8F4774345C5F1E,
	GazeProvider_get_GazeInputSource_m77C5F3D23E07ED72FAE042EEDCA8237DCE40BA42,
	GazeProvider_get_GazePointer_mA23A36BBF88BD62DAFA0B01BDB755A3558184368,
	GazeProvider_get_GazeCursorPrefab_m40CCB21A3FFC04008C68CFF6F968A7DB50A7A4C1,
	GazeProvider_set_GazeCursorPrefab_m607EAA00B5292F16A9A8AE99089386A7D31AA285,
	GazeProvider_get_GazeCursor_m989AC4F8F64E3075BFFABBA52DD1EF390E67B5C1,
	GazeProvider_get_GazeTarget_m5D370EFB4AF8118F049EDCCC6808CA9E0AA38C33,
	GazeProvider_set_GazeTarget_m50110F0812B93A399CEA008C211161A55D751671,
	GazeProvider_get_HitInfo_mC6C64774B811FEAC54FA3CD8F373224611DC5997,
	GazeProvider_set_HitInfo_m0DB7F9C9E348535033363E78240E78F5ED9C700B,
	GazeProvider_get_HitPosition_m0CF3DB2AF31C873E2EEFE0BEE43925E2C3530343,
	GazeProvider_set_HitPosition_m66A66BE5409C4802135CE6895E0328A06E5C57AE,
	GazeProvider_get_HitNormal_m7886F528908F93E27934F25198FDAD458FC07BE8,
	GazeProvider_set_HitNormal_m309231C3EEEADBBE0604FFB3135E49172DE8D8AB,
	GazeProvider_get_GazeOrigin_m21B179E75578442F26A28448815F401E50056C39,
	GazeProvider_get_GazeDirection_mAF8C5F55A0F4F364BF9696E3B083D8E053E0A7A0,
	GazeProvider_get_HeadVelocity_mEB0602B2E2A76D9D56FB1347990EC2686B06111B,
	GazeProvider_set_HeadVelocity_m702CC2A261F592379393E8364DA12A629B16700A,
	GazeProvider_get_HeadMovementDirection_m31DFC53340F1BDF5D981D2D6D561FE9585AA4C26,
	GazeProvider_set_HeadMovementDirection_m259DD40DD5B118FF39AB473691124281F44AB982,
	GazeProvider_get_GameObjectReference_m46ECE327BE1EE346BDDC72CEE246386F0D299181,
	GazeProvider_OnValidate_m56821F9DF00A1EFAE75DEBAA6FECE428CD1AECE2,
	GazeProvider_OnEnable_m9D300CB8A0B977C9CB9E26DBAA22FB1F9CD05083,
	GazeProvider_Start_m6EAAFA7051415C3DFF106BCAB7906E0CD22A5A55,
	GazeProvider_Update_m474C9F183E25ECF7A61BCEF80B75CED169132012,
	GazeProvider_LateUpdate_m80D4AC4CAFACE4C3ADE651C644CE97F86F713ED4,
	GazeProvider_OnDisable_mF61B8B274C85BB9E150BE6AFF67D4F939270BFAB,
	GazeProvider_OnDestroy_m451C99952E39EA3EC995052F6B0F9A2B9D44854B,
	GazeProvider_RegisterHandlers_m41853DBA329FAA7DD19846DBF81E58A38A9352B9,
	GazeProvider_UnregisterHandlers_mA84EDE3F8C861856CBF3D4F187AFAAA6584F4000,
	GazeProvider_OnInputUp_m1DF81468D5F9D9D9D1348EA95ABC07B4ED5B3A6E,
	GazeProvider_OnInputDown_m1B6A7CA01295B6DEA76F8C6F50B9AF11DC564D5E,
	GazeProvider_InitializeGazePointer_m818F5D9505AA98B1416FDDC626B005B5A66A172F,
	GazeProvider_RaiseSourceDetected_mA1848997E168EC3DF06ED7ADBB21DA3577E099D8,
	GazeProvider_UpdateGazeInfoFromHit_m4C079B3BAC36B206BAA10525FEFAF553639BB8E3,
	GazeProvider_SetGazeCursor_m01E4DCAA39698054AFFAAE1EE02778146E79F81F,
	GazeProvider_get_IsEyeTrackingEnabledAndValid_m00430FEF418064AC463019D2CCF07F820EC1EE8A,
	GazeProvider_get_IsEyeTrackingDataValid_m67B147F43A4BA6083A0A97875F6D5B53561B47E5,
	GazeProvider_get_IsEyeCalibrationValid_m3C0C60B8B60763627E4084F0F629ECD0BA4AC646,
	GazeProvider_set_IsEyeCalibrationValid_m47D339440A612BCBFEF444DF9C902E94CF4E1437,
	GazeProvider_get_LatestEyeGaze_m8C2F9236CC1EFF9DE37BDD092188E0341DAC2BB7,
	GazeProvider_set_LatestEyeGaze_mC2D1E9684AEEA1D31255F4A2B6DE238379850B0E,
	GazeProvider_get_IsEyeTrackingEnabled_mCC73A14376428F67C50D3C5BC2EFB87E37F64258,
	GazeProvider_set_IsEyeTrackingEnabled_mDD8948C0BAE92D06415061E4692DA004877AE53A,
	GazeProvider_get_Timestamp_m860D6B2FDA518AB94D9C7EB2A2F6DEEACB3485AB,
	GazeProvider_set_Timestamp_m653309C70F41768C302BADBDC28F268D9201C358,
	GazeProvider_UpdateEyeGaze_m55A35E7069354DB56C836FD6815FE351F37B4939,
	GazeProvider_UpdateEyeTrackingStatus_m935F690DA106715ED089930A7D2F287D3600D8F7,
	GazeProvider_get_UseHeadGazeOverride_mDD01AC1180B147624A905121A4D1E86D1A87C6BB,
	GazeProvider_set_UseHeadGazeOverride_m2AEF31DA36CEE59029EA6C52BB1B95F4D97CB9EA,
	GazeProvider_OverrideHeadGaze_mB431591F8176DA4586E5EE1888E1410FE4CFC56F,
	GazeProvider__ctor_m79E43BF32B094720136E42A1AB34F57000A5AB11,
	GazeProvider__cctor_m8D9D0DCDDCCD5D347BB21D2A7396E995F614B38C,
	GazeProvider_U3CU3En__0_m81634D8D8E8FE66D0AE962D62DEFBF63BC93B1C3,
	InternalGazePointer__ctor_m807892FD633E185C8BA93459E411F558B1B7C063,
	InternalGazePointer_get_Controller_m23B01EFF6925D47A30E55F16ADB694469A44591B,
	InternalGazePointer_set_Controller_m75F90DF47C5A592700751CF5C81EBC321F7D4F49,
	InternalGazePointer_get_InputSourceParent_m1E2AF81A1856861FA0AE5A073AF0D12764D76A30,
	InternalGazePointer_set_InputSourceParent_m48D88841B17601D3E949CCB3C8ECCEDE8765FF85,
	InternalGazePointer_get_PointerExtent_m514958ECDB821AF2A953027C29C0C2A310C32203,
	InternalGazePointer_set_PointerExtent_m3021B1E0DFF6112C44DC9D239BAE89100A5592B5,
	InternalGazePointer_SetGazeInputSourceParent_mD9A387CE28F0916D61E0D246820FC8D5AA8D91E9,
	InternalGazePointer_OnPreSceneQuery_mC5757C5C53EE0FDB57D92CE58FC4D1026E61764F,
	InternalGazePointer_OnPostSceneQuery_m50E9702E704503FCA59503F934B824F2B00A6DE2,
	InternalGazePointer_OnPreCurrentPointerTargetChange_m3B1D5B047C205B5A54BE784C4363098739B388EF,
	InternalGazePointer_get_Position_mE73B899C274A19CDE119AAB8AE3C9F805053C4B7,
	InternalGazePointer_get_Rotation_m93AFC889420D1C8F76F55C0B469C7686D8F8A15C,
	InternalGazePointer_Reset_m37DB374B49DCDEDBC57C6674287F6486120A8ED6,
	InternalGazePointer_RaisePointerDown_mA46F01E9CF07B7134962FEA67AC4C80FC729501C,
	InternalGazePointer_RaisePointerUp_m14760F977169DA125B9393E3E06F0030B4AA68DF,
	InternalGazePointer__cctor_m8E03AD788B610A5370585EEC3463F7C75AD05D10,
	U3CStartU3Ed__63_MoveNext_mE7A9ED52606E6BAED7043E305EC7F70044C149F0,
	U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92,
	U3CRaiseSourceDetectedU3Ed__77_MoveNext_mCD2B2EAAEC3539A9CFD61476E4A1394BA6BDEF76,
	U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6,
	InputSystemGlobalHandlerListener_OnEnable_mE51B9E57A822DF166BA80C7C1F3EF0E27E9A6334,
	InputSystemGlobalHandlerListener_Start_m646082223199D214B966CDA947646D59B74E7A21,
	InputSystemGlobalHandlerListener_OnDisable_m4E7232E02F2B9AC058DF26E5B17DDDCD8B7E5A00,
	InputSystemGlobalHandlerListener_EnsureInputSystemValid_m515C9327DB925F3BA2811BA579F20852F506C450,
	NULL,
	NULL,
	InputSystemGlobalHandlerListener__ctor_m86BCCE3D91A93E6A3B28D998831596D975D79B5A,
	U3CStartU3Ed__2_MoveNext_mFC68DF4C9CA99DD620FEFA5D6D24F9D2895BB564,
	U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B,
	U3CU3Ec__cctor_mC8A90C96E1D88CE2F6A778D21FBB0E77E9466C57,
	U3CU3Ec__ctor_mD66F4EA2895C5D26EB655BC5261BD65B4996E653,
	U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m89844A843D3D8E4C9D4B3F87BB82236884B0E5B1,
	U3CEnsureInputSystemValidU3Ed__4_MoveNext_mAB5FACD80C03C56E8C4A200A03A98225C7E77AAE,
	U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A,
	InputSystemGlobalListener_OnEnable_m7A9F927B1CB1BE009B3DD85CC6FDFEAA3DBC65BB,
	InputSystemGlobalListener_Start_mD5DBB464B691AFCDE571E7F843CE88F39431E245,
	InputSystemGlobalListener_OnDisable_m53F9F208C27CBF12AEC21EFD7ECFB3D2B1256499,
	InputSystemGlobalListener_EnsureInputSystemValid_m847891045193EBB4FEC9F815149AF2A0AA9A4232,
	InputSystemGlobalListener__ctor_mD610BF668F3CA0691C5A83E4C3D3B18BB01377C7,
	U3CStartU3Ed__2_MoveNext_m4C323A0DD7F833C4C47D0256CCE1CE07BD473CF2,
	U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D,
	U3CU3Ec__cctor_mC32486E47F6CCFE4DDDB3C32656C4C825AC90A97,
	U3CU3Ec__ctor_m745EE9B0C5157FF3E75852036F2320DBD85FAF02,
	U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m0929EA834E63ED45C72F7E646CFCB56E0F8ED5B9,
	U3CEnsureInputSystemValidU3Ed__4_MoveNext_m78B851E87D4DA8335500019F0FA8824730AA31AA,
	U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750,
	MixedRealityInputModule_get_RaycastCamera_m5EBBAC2918E34E483E3E4E0A4E0E03A358A75CB6,
	MixedRealityInputModule_set_RaycastCamera_mC37A01ED874AEB9B3A305D5718A6F0E35EF0B29B,
	MixedRealityInputModule_get_ActiveMixedRealityPointers_mD404D46EFCE9678AC4FEB250D1E14D44A7D80A12,
	MixedRealityInputModule_ActivateModule_mC403E32504AE6251C56A4CC5DCE887A696E710D2,
	MixedRealityInputModule_DeactivateModule_mC87411CC8D1AFBE82E61D1EAEBEFAC4CC15917DC,
	MixedRealityInputModule_Process_m455B4DD3F3664E1EA6A9A5A07C4229AE276F9306,
	MixedRealityInputModule_ProcessMrtkPointerLost_mEB37964C8FCE6002FDEF7C3A19FC2B40EDA57706,
	MixedRealityInputModule_GetMousePointerEventData_m4DFAC7C578DDF14F8AF72979AB7728F7B5360851,
	MixedRealityInputModule_UpdateMousePointerEventData_mD256364C9C2B06D270FA21A3A7727E10E156BA17,
	MixedRealityInputModule_ResetMousePointerEventData_mC3EB2C14D43E5B9E92A0B9990A0E89E9869B3A78,
	MixedRealityInputModule_StateForPointer_mAB0D597A85B65620B373DC8C0FDD48AA53B0A3E9,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mA537E625C1156979476A7ABB1043D76EC8EA3E68,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m47DFF009BB7E154CD639B79720D4182D1D483CDF,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m628A777D1269E31730726E1FA12D0E098A5E76FF,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mC7FE04F80A7B9F2136B2748570C6143D547885DB,
	MixedRealityInputModule_IsPointerIdInRemovedList_m051D545523F3C2E9AC4152E1CA4A8C8B5757D9BF,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m5DB2995E215213532257F5F884E02CEF04D9F89A,
	MixedRealityInputModule_OnSourceDetected_m33241EA549BDE569491A5F18B3C330EE483A3E92,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m44DDB4269D52B630E885F22BCD6341D1637E1277,
	MixedRealityInputModule__ctor_mE59CA71278433B3B67E2DE1C929A65924E264E6A,
	MixedRealityInputModule__cctor_mB68480138E7A85BBC2C77576C59F689701A465CF,
	PointerData__ctor_m2F3DCA621335A4FE0F41C346AFC25BD05E877CE4,
	U3Cget_ActiveMixedRealityPointersU3Ed__8__ctor_mF3A98BD0D0D2C7C8C37FF46BDD27381E108BA7D6,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_IDisposable_Dispose_m81F86CD2815658EFE836955595D3850A73135051,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_MoveNext_mD8E1A16F6D711D801549FC470AEF4E949A80D6AC,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_U3CU3Em__Finally1_m9B0FCB95A174A826CC1F9773236D3EA24A1085F3,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m9568F1068F08E2264198020333FF2F6767945E4E,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_Reset_mEE3D9D014BC562911BB721DCDCA35E34844E192D,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_get_Current_mB1FF6B129B0CD975E4FCC900F7FCE02D92B8ED90,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m96D61CBBA557F0BE30D5907F60C15828000F9CE8,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerable_GetEnumerator_m8FAD01E0882328EB6A6490CFDF93F19424A8BC64,
	MixedRealityInputSystem__ctor_mCBFAAC863AC579F9FF4BC901CA89C88AD2306C4F,
	MixedRealityInputSystem__ctor_m49CBA9C3F6527102B8F34968831319DD4196A3BC,
	MixedRealityInputSystem_get_Name_mC32F1847F9B7F24AEF47BF87DCA70CD57EE4B5C2,
	MixedRealityInputSystem_set_Name_mB2D49E0C4C68FAA1CF4DC10EAF03BABD10A92640,
	MixedRealityInputSystem_add_InputEnabled_m1CD6DD9E8CB62896E260E26A0DEFD3C5C7C669BF,
	MixedRealityInputSystem_remove_InputEnabled_m6D3FAF296444A1C3CBAE2657E9FA58FEBC4614DB,
	MixedRealityInputSystem_add_InputDisabled_mA592CDD3F94DBE095D8B1345A24BD5296961F4FC,
	MixedRealityInputSystem_remove_InputDisabled_mA1FA59DAFA1A68AEF5F5D1C398C093043623E967,
	MixedRealityInputSystem_get_DetectedInputSources_m134E5FCEF6DD92AC33A99E81314E060BC08A723D,
	MixedRealityInputSystem_get_DetectedControllers_mA3036DA07CF5C3A6784CEE28B672B0077DD36792,
	MixedRealityInputSystem_get_InputSystemProfile_m602782FCBE0D5ECD45C04CD5196968B827C244AB,
	MixedRealityInputSystem_get_FocusProvider_mF60DEAE82915EE180314A8EF05FCFE6CF560FC3E,
	MixedRealityInputSystem_get_RaycastProvider_m6042DCEFD2951DDE3F2E7469499714D51D02BA92,
	MixedRealityInputSystem_get_GazeProvider_mDBC37FA9CF1D0BBD3CF11F3D80FE8F511AC0F5CD,
	MixedRealityInputSystem_set_GazeProvider_mD0124EAFA6FA864EDA6A7B78152E79F301E521CC,
	MixedRealityInputSystem_get_EyeGazeProvider_mEFB38A18AF9BD544D816002C7A6804C410E5F981,
	MixedRealityInputSystem_set_EyeGazeProvider_m339E998955220B401FBED8BCFFA6145C811818EE,
	MixedRealityInputSystem_get_IsInputEnabled_m05585FA46FDAC92E2CC6585F0524756B9AAA4927,
	MixedRealityInputSystem_get_CurrentInputActionRulesProfile_mBC867FA63A1B9CBF5DE129EEEC79D8F1061CB8A2,
	MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mFAB94A5E10A743D706D962E0444E04B1FB7665D4,
	MixedRealityInputSystem_CheckCapability_mE31FB54CA00A4CD5F5F4220B0A582F2149D00335,
	MixedRealityInputSystem_get_Priority_m86DE4B6F57AE4A87CD0F8B9610466F7AD56C36CD,
	MixedRealityInputSystem_Initialize_mECAD8C11A5D5B76A06FF29312B4C762D56C40D86,
	MixedRealityInputSystem_Enable_mF5FF8DCA76AF52862844B83CB7D89F62C8898D66,
	MixedRealityInputSystem_CreateDataProviders_m9DE879B9B3ECFB99115467F0D0293E0D8E5B9BCF,
	MixedRealityInputSystem_InstantiateGazeProvider_m464FC79A26459EFF33D994860C4D1E7297FA36E9,
	MixedRealityInputSystem_Reset_m77CE467C1BF6268AD3660A359E6C126C3AF5EA02,
	MixedRealityInputSystem_Disable_mC5E356D7DA2F9EF1549219A9766E7E0DF1FE12F7,
	MixedRealityInputSystem_Destroy_m27262A390DA05A5FC2475F94EE150CE78BDF384F,
	NULL,
	NULL,
	NULL,
	MixedRealityInputSystem_HandleFocusChangedEvents_m962F0FE91411C4872018954A5A1C81799BAE19E9,
	MixedRealityInputSystem_HandleFocusEvent_mAE4D22E916AEBE7F7698DDEBDACC1077DE2E068C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MixedRealityInputSystem_Register_m4519B1B2550DC1F3929AE0B3260EB5AEC6D42FF3,
	MixedRealityInputSystem_Unregister_mB90776F1DD00B1E8BB1DD1D87DC198B8D0AE02F7,
	MixedRealityInputSystem_PushInputDisable_m5FF7A5CE7CBCCFF1B8F820D619F183BE17950499,
	MixedRealityInputSystem_PopInputDisable_m121939D21BC45185DD97B59B6F11311E8D65F816,
	MixedRealityInputSystem_ClearInputDisableStack_m3E24CABEA9F7A2275BD4B0C677F9F286B4AEFAAF,
	MixedRealityInputSystem_PushModalInputHandler_m87CC890E182ACAB61F9E4681AAE0C5EDBE11F2ED,
	MixedRealityInputSystem_PopModalInputHandler_mB60637C72F670A2B69B46F8F095EB1A65A28674D,
	MixedRealityInputSystem_ClearModalInputStack_mFEB18A7B20832D73E62DA438705E812AAA1FF64D,
	MixedRealityInputSystem_PushFallbackInputHandler_m539812EE9E6EAB3BC44B1FD50CE72DCC304F5209,
	MixedRealityInputSystem_PopFallbackInputHandler_m413FAA64BDACFB976465F58C07741808F282CABE,
	MixedRealityInputSystem_ClearFallbackInputStack_m2A306DC4FD6966F9EE7A9EB79D5C40CFD598F9DF,
	MixedRealityInputSystem_GenerateNewSourceId_mC59C6F312F70F9FA4CEC2FDE916792C5CA7A7414,
	MixedRealityInputSystem_RequestNewGenericInputSource_m475CF373AE055DEBD313B94343226A52E53B35A6,
	MixedRealityInputSystem_RaiseSourceDetected_m5830B595259BF976516B64C18BA0202215708E0B,
	MixedRealityInputSystem_RaiseSourceLost_mB4C5595A95C96E4F73BCE4EE65D4D0648F75526F,
	MixedRealityInputSystem_RaiseSourceTrackingStateChanged_m6B99FAFBCAC1430AC98797B28415B2111D0FE2B6,
	MixedRealityInputSystem_RaiseSourcePositionChanged_mF6585E9AE3F97F6B6A1401C27E52E87567D9A193,
	MixedRealityInputSystem_RaiseSourcePositionChanged_m546DF514895131D86A7BCF568CD7359E66A76921,
	MixedRealityInputSystem_RaiseSourceRotationChanged_mAA9196198D6FB43C86AF62B90128263C91E11187,
	MixedRealityInputSystem_RaiseSourcePoseChanged_m6FE3FBE061655CAAE206614361E288DB74E7CA99,
	MixedRealityInputSystem_RaisePreFocusChanged_m0284B94A742D7BF44ED60AA10DC3FD196AF9D5C8,
	MixedRealityInputSystem_RaiseFocusChanged_m93AF6AB04EF1291225C67A1D52F8C39C366B6832,
	MixedRealityInputSystem_RaiseFocusEnter_mBFD2B7D78BF1B160253DF93329A49805052121A5,
	MixedRealityInputSystem_RaiseFocusExit_mF022A2A8E8D35A71269AE70F61216073C4DE693F,
	MixedRealityInputSystem_RaisePointerDown_m71F07B376C01C99B23D70AFBC458886C218AC562,
	MixedRealityInputSystem_RaisePointerDragged_m9F6A1B8030C23820F2795C99C5F0B383DFD99438,
	MixedRealityInputSystem_RaisePointerClicked_m7E0408EC3123BE2CE869A50A4DB0176F35F0FFF3,
	MixedRealityInputSystem_HandleClick_m30481C516558937AC76A9658B9024964BCA07AEB,
	MixedRealityInputSystem_RaisePointerUp_mCFEE40027BF38D2073403417DF147C99AE577B44,
	MixedRealityInputSystem_RaiseOnInputDown_mA08160D1A553B61A34AEF2688106C36913285B2B,
	MixedRealityInputSystem_RaiseOnInputUp_mF2D73D1D0EFD430E2D7423AAA20EFE48DC5EC7E2,
	MixedRealityInputSystem_RaiseFloatInputChanged_m416B75B47BBEA6386CBC3E3189870BF07C5FB3D3,
	MixedRealityInputSystem_RaisePositionInputChanged_mE87013DAEAF6EAFD42B6E31EE28E575C13EF2E12,
	MixedRealityInputSystem_RaisePositionInputChanged_m18CBF4C3A2932CC78499F37E42A7B94ECB428072,
	MixedRealityInputSystem_RaiseRotationInputChanged_m02A1D1F20495C56789D9A35E73C523F5540D241A,
	MixedRealityInputSystem_RaisePoseInputChanged_m7328018B541BED20B6F205B2E2BBC59DDCB1764D,
	MixedRealityInputSystem_RaiseGestureStarted_mDB77FF55FA1E1B4A79F6B8C834436305A7FDBEED,
	MixedRealityInputSystem_RaiseGestureUpdated_m3636951588FE28749B81334AE099F57725CB5FB7,
	MixedRealityInputSystem_RaiseGestureUpdated_mDF8C4A1DF8B2AF73B80CF6114FF0C74EF1B20E0C,
	MixedRealityInputSystem_RaiseGestureUpdated_mD3161870F9738275179EF41382C41F0FE2D6A834,
	MixedRealityInputSystem_RaiseGestureUpdated_m8D9A3F7C2D2CB74134EF3394C4DB27BF98CDBFF1,
	MixedRealityInputSystem_RaiseGestureUpdated_m4D59E6D520D56E3773DCC86E0FB1ADA5BC3A3BE2,
	MixedRealityInputSystem_RaiseGestureCompleted_m747A07338F6A0509D389BC95952A3AC791F7889D,
	MixedRealityInputSystem_RaiseGestureCompleted_m4E797F9C4FE6390F6A24941B9A4E055EE0C39E97,
	MixedRealityInputSystem_RaiseGestureCompleted_m069CAEC88EF60FD1EDA98CE45FEE30206A0D0B26,
	MixedRealityInputSystem_RaiseGestureCompleted_m1BAA275BD345A04119232A5CB451D24CEB687F35,
	MixedRealityInputSystem_RaiseGestureCompleted_m6DFFB3C58D3D735BA2919710104C071D9DD100D1,
	MixedRealityInputSystem_RaiseGestureCanceled_m42AA55427125B489E2CAFF7848456318A4CB1306,
	MixedRealityInputSystem_RaiseSpeechCommandRecognized_mD4CBE4E016FE6B3BA19CEA5170765324927BCB79,
	MixedRealityInputSystem_RaiseDictationHypothesis_m40E7B3103F4FBB159E427C913F3DB51FDF1EB9C4,
	MixedRealityInputSystem_RaiseDictationResult_mEA704DDADEB3B0E306FD82036A33BB0DCD14A673,
	MixedRealityInputSystem_RaiseDictationComplete_mE6E3C38764DF405C0EA7C732D5F986ECA090D57D,
	MixedRealityInputSystem_RaiseDictationError_m4342EAEDFD44249D510934306F66430E5491000D,
	MixedRealityInputSystem_RaiseHandJointsUpdated_mC96ABEEFA1E46BF29AAA5628139D39EF43732D53,
	MixedRealityInputSystem_RaiseHandMeshUpdated_mFC72CEE2AA6EE231AE2904091B4E8EC9A8EEA339,
	MixedRealityInputSystem_RaiseOnTouchStarted_m5BAE8EC21AEF923696B22F88DF3C2013AAACBCA8,
	MixedRealityInputSystem_RaiseOnTouchCompleted_m0DE444233AF7F66F08A16DFDEE371AD2D3B77B14,
	MixedRealityInputSystem_RaiseOnTouchUpdated_mB399F63DC294EF2D801020316E72FB707CBD3233,
	NULL,
	MixedRealityInputSystem_ProcessRules_m1C8B4AF9E3120D2C339D7AD89AC1F1C2E3A8A598,
	MixedRealityInputSystem_ProcessRules_mA96EE551DB90A6AE71D582A3E1FC7EA78E61A92A,
	MixedRealityInputSystem_ProcessRules_m8778EA7C8AF81CBA034126F7EC0641BB54DD6981,
	MixedRealityInputSystem_ProcessRules_m1BAFE9CFFE57E01C4DE6FC9ADC1F4FA1B560A92B,
	MixedRealityInputSystem_ProcessRules_m3D1B0AA50F91F222421BCB9A4244BC193B5DC461,
	MixedRealityInputSystem_ProcessRules_m42794788CE56EB3DD2720C40DDAF457860A98A0E,
	MixedRealityInputSystem__cctor_m71B547E800DA41F59CC78E17800A0921507B02A0,
	U3CU3Ec__cctor_mEB624057F0A51A303F0D70C8ACE8C6B58EE5F1A4,
	U3CU3Ec__ctor_m9D389BA8DAD63B3748236B62EAB68C7891EB8549,
	U3CU3Ec_U3C_cctorU3Eb__240_0_m525234F28C3C3C8EB7403A6A6BC6C43EBA3D93D9,
	U3CU3Ec_U3C_cctorU3Eb__240_1_m876FBFEF02A8D22AFA879B7DF10741E046883A79,
	U3CU3Ec_U3C_cctorU3Eb__240_2_mC86D31C235E922ED1C3D8742C90081B3752DC7A9,
	U3CU3Ec_U3C_cctorU3Eb__240_3_m21689171154D1D1DE85E413C58B633F809018A9F,
	U3CU3Ec_U3C_cctorU3Eb__240_4_mF865B3FA559AFF909DE84026E3A1B3303FB56481,
	U3CU3Ec_U3C_cctorU3Eb__240_5_mB756317FC4EB7BEC91EE447C7D22CB3BAF78A52D,
	U3CU3Ec_U3C_cctorU3Eb__240_6_m9C5AC2C24E7AB6C91AF08E9EF8B663438EB0AA22,
	U3CU3Ec_U3C_cctorU3Eb__240_7_m28856F0840DF3D8D5DDBCDFC5C6F13713015E496,
	U3CU3Ec_U3C_cctorU3Eb__240_8_mC4B8E2D3438D7D5C5C37B41E127E6B9A2D510DD8,
	U3CU3Ec_U3C_cctorU3Eb__240_9_m90753BC90BDBB2294CB80AD55B922BFD7A7E99BA,
	U3CU3Ec_U3C_cctorU3Eb__240_10_mA33C5B092E43F6319B946EFAE8B540CC9A4642A7,
	U3CU3Ec_U3C_cctorU3Eb__240_11_m04A424D53BA479C70DEB8DBE50F1ED72D9F62023,
	U3CU3Ec_U3C_cctorU3Eb__240_12_m57D4D9F69F782251435E06589442E8B4C4196C4A,
	U3CU3Ec_U3C_cctorU3Eb__240_13_mEB72B2EF158F9552D5C502CD1DA34AE76AFFAD53,
	U3CU3Ec_U3C_cctorU3Eb__240_14_m8AA79C3DB1C38D844774BE23AB3D8C83ADDBA922,
	U3CU3Ec_U3C_cctorU3Eb__240_15_mF0A3A28E7503A3161598B734F8BBB88AE1A6FB84,
	U3CU3Ec_U3C_cctorU3Eb__240_16_m2705537F14E66ABF1B2F73F2BD52622A75C65880,
	U3CU3Ec_U3C_cctorU3Eb__240_17_m4E8342574CE106396911C0D2FF5EBEBC81694DD0,
	U3CU3Ec_U3C_cctorU3Eb__240_18_m45431E95849ADB6CBA124A9ACBE80AE507E361CB,
	U3CU3Ec_U3C_cctorU3Eb__240_19_m3FAC2ADBF04FD4F6F4D6ADE10112A4EB53DFAE44,
	U3CU3Ec_U3C_cctorU3Eb__240_20_m68030209C501F202D588A88A9BDAC92D1AF06272,
	U3CU3Ec_U3C_cctorU3Eb__240_21_mEC04976F0D889C964D8EDA2CA596FA5D169F3518,
	U3CU3Ec_U3C_cctorU3Eb__240_22_m8F8919BD4B0D9A838E5EECB67CCB3CBA91120905,
	U3CU3Ec_U3C_cctorU3Eb__240_23_mE711847519123254395174DD39F1041409DB4F89,
	U3CU3Ec_U3C_cctorU3Eb__240_24_m41D0469969E6DEE9935DEC00BFF0C90043B6CE54,
	U3CU3Ec_U3C_cctorU3Eb__240_25_m2F8F283A0B89D18C9051B943E94706AE0F07E1A3,
	U3CU3Ec_U3C_cctorU3Eb__240_26_m644C8DE660F872590333CCA9264437AA37DBF7E2,
	U3CU3Ec_U3C_cctorU3Eb__240_27_mF2CC7FEBE3AFC5AC601884A22DDE69A58C22F357,
	U3CU3Ec_U3C_cctorU3Eb__240_28_mE6864DC22E4CD59BFEE3AC0D6C0703A6B1DAB2C2,
	U3CU3Ec_U3C_cctorU3Eb__240_29_m60D432C49EAE743751639D7EDE1109739D70C02A,
	U3CU3Ec_U3C_cctorU3Eb__240_30_mF938DD3B8724E0D0F73996353581A79A0F88A895,
	U3CU3Ec_U3C_cctorU3Eb__240_31_mE88E9A6AF80200CFE01145C9BE9B0703A1DA6712,
	U3CU3Ec_U3C_cctorU3Eb__240_32_m5DDA0AD1462A430AECCE96B5DB689CF957791CBA,
	U3CU3Ec_U3C_cctorU3Eb__240_33_m877A6A5CD849A5A026B12D9F52C9C80FD9B1D9C4,
	U3CU3Ec_U3C_cctorU3Eb__240_34_m37E35576C51863E9ABD17CEABF6C5A2790FF09F6,
	U3CU3Ec_U3C_cctorU3Eb__240_35_m076DBB3D503C733AA8E96B9F0639C55CD6BFDB20,
	U3CU3Ec_U3C_cctorU3Eb__240_36_mBD0C16B0993BCC57518755B4E3C45034122EECD1,
	U3CU3Ec_U3C_cctorU3Eb__240_37_mAC759A8D9E8FD38211408BC9C0FEFD2E6E881BD4,
	U3CU3Ec_U3C_cctorU3Eb__240_38_m32E06893EA30D8FEFED2748D5C658FC02BA2B627,
	U3CU3Ec_U3C_cctorU3Eb__240_39_mC070F0473B1CBB520E2A68B8059C5E3683C474F8,
	U3CU3Ec_U3C_cctorU3Eb__240_40_mF80EA22470EB25C36006541902A640CC27DE1B42,
	U3CU3Ec_U3C_cctorU3Eb__240_41_mE363DCF662C5B406DB95AC9F3025CE16A695AA6E,
	U3CU3Ec_U3C_cctorU3Eb__240_42_mF642F722136A72FAAF87875BBC320FBAFE771393,
	U3CU3Ec_U3C_cctorU3Eb__240_43_mB75F095F226C6E11DB43C1970E51FF06D94E8F88,
	U3CU3Ec_U3C_cctorU3Eb__240_44_mD6D0D27098C69E3BF89DACED9587BAB09BC4BC0C,
	U3CU3Ec_U3C_cctorU3Eb__240_45_m092D12124EFAD480E7475D9DA7C679F9724C2511,
	U3CU3Ec_U3C_cctorU3Eb__240_46_m39A85C3EB1C552A0EF54989FB94DAD8A2B800526,
	U3CU3Ec_U3C_cctorU3Eb__240_47_mFFD605711F52E238FFB0B751CF6E878016EE1D29,
	U3CU3Ec_U3C_cctorU3Eb__240_48_mB539E7F9519B8A142E87E84B338ABD09806C33AA,
	NearInteractionGrabbable_OnEnable_m7DEBC2467E5C6061A5A85ADC0BC494BE2BBCECBA,
	NearInteractionGrabbable__ctor_m241DE7D67105307D3B49B95D310602C5BC5F5E34,
	NearInteractionTouchable_get_LocalForward_mF25D6950ED0AD34E52E7064F5D92646BB7379062,
	NearInteractionTouchable_get_LocalUp_m32F1523D8D7190B34DECB5A51F963406327A076A,
	NearInteractionTouchable_get_AreLocalVectorsOrthogonal_mA01A7AC5185521468558395A19BC7A2B7F0F2658,
	NearInteractionTouchable_get_LocalCenter_mE97E72A423E98217E09C8CE621665835CE012533,
	NearInteractionTouchable_get_LocalRight_m2E9197705A3B4E3B07940D6B565C93F336361004,
	NearInteractionTouchable_get_Forward_mF986BCD574CFC2309B57E80533D2070587A157A5,
	NearInteractionTouchable_get_LocalPressDirection_m77CFF9CE630F8BA7004700FB7266A7201ACB0770,
	NearInteractionTouchable_get_Bounds_mD81866F6ECADCA44EAE054543D824C826B140FB5,
	NearInteractionTouchable_get_ColliderEnabled_m3E873E8CC43C4D22124909E8A24BD8DB5C555F39,
	NearInteractionTouchable_get_TouchableCollider_m610B62724339F0F8C9571F026A3176E2852FF8D9,
	NearInteractionTouchable_OnValidate_mCB421C48D4F62A93D182DAF012D8A1C2471E9A0B,
	NearInteractionTouchable_OnEnable_m047011120D7FED1B25D460F1311EFBB1D32E5F0D,
	NearInteractionTouchable_SetLocalForward_m65968DD0EBE51683F1DA87BF1411D2826F4CC25C,
	NearInteractionTouchable_SetLocalUp_mDB5C11FC8A422FC76709CA8EC094067152D57FA4,
	NearInteractionTouchable_SetLocalCenter_mDCC21B38F1D5045B59ECB340BB89A795C4D945D2,
	NearInteractionTouchable_SetBounds_mDAF144A75C3BF3517D8F77744CCF4D84894EDDC6,
	NearInteractionTouchable_SetTouchableCollider_m47C6561F66AD76B6FA99D04B694CB85FA4533273,
	NearInteractionTouchable_DistanceToTouchable_mF089274125BEFA2E6B3A67044520E0D28E93147F,
	NearInteractionTouchable__ctor_mA211A6C387CCEEE332C32DE5D6224F8BEF66CD17,
	U3CU3Ec__cctor_mD1B1E18C29182562FC99414E1442BA02F9D44D92,
	U3CU3Ec__ctor_m4844B4656AC70CA4F32DA94E483C1B15968D284A,
	U3CU3Ec_U3COnValidateU3Eb__25_0_m772CE3B48534FD5D71BD04057A2361180548FFD9,
	NULL,
	NULL,
	NULL,
	NearInteractionTouchableSurface__ctor_m6DBA5832EB7A7FBBCE489EC0E2854C2DA6CEDCA5,
	NearInteractionTouchableUnityUI_get_Instances_mDCA0E86351579A84DAF70659F7D0F4A3789E8B9D,
	NearInteractionTouchableUnityUI_get_LocalCenter_m0954A95FA0D61F634B6E14026FCAE9BA6589F53B,
	NearInteractionTouchableUnityUI_get_LocalPressDirection_m471D0AFA72DACEEACF04FEFF28E6C2F7B5F042B9,
	NearInteractionTouchableUnityUI_get_Bounds_m1BBEC715835F2ED44418B2B0198FD5056B2FB2FE,
	NearInteractionTouchableUnityUI__ctor_mA3B66DEF6CEED77F379689CC3B9595D5E2006111,
	NearInteractionTouchableUnityUI_DistanceToTouchable_m851652FACA38220BE297C1B296B7D9EF619825F2,
	NearInteractionTouchableUnityUI_OnEnable_m619029642940BD5EFA6E9ABB19365ABAE35F8422,
	NearInteractionTouchableUnityUI_OnDisable_m020E63F1BDB03FA29A2D85FB92B72ED664EE52F4,
	NearInteractionTouchableUnityUI__cctor_m7A1CC65E3B1E873CEDE6ACA5BDD92CD998C4A136,
	NearInteractionTouchableVolume_get_ColliderEnabled_m5FDAFA7735CD6DA3306ADACEE12591A4FB411E04,
	NearInteractionTouchableVolume_get_TouchableCollider_m4B05AF553637BA8B7B7E0328BF48C4C12623C10A,
	NearInteractionTouchableVolume_OnValidate_m4951B3E4E020FC5F0446EB338AE1DB02D6DD6349,
	NearInteractionTouchableVolume_DistanceToTouchable_mC4B7F8A11DB40419E1DE595C08334BAB2D09B24D,
	NearInteractionTouchableVolume__ctor_mB4B2D40ACE279F38AC0E664BAF827FBB8C16117E,
	CanvasUtility_OnPointerClicked_mF18E07625E7EB838D4B293A328002949CBBA187E,
	CanvasUtility_OnPointerDown_m273A518A59E5662F7FEB6373B0C29E483A4C78D3,
	CanvasUtility_OnPointerDragged_m2FB19C5600F07AACFA96977D8498CF03415AB2A8,
	CanvasUtility_OnPointerUp_m058194B41BDA2B38C3CFA83E7931BFA59CE7F714,
	CanvasUtility_Start_m5FD2A7B77D4CAD8FA7D017E0447388D1A7F2D074,
	CanvasUtility__ctor_mA44F4DC903F30E94859A2EE453AE3AB2C3D1953D,
	ScaleMeshEffect_Awake_mD3DECB9CC81A3832D5196401404D3BAF54921AB8,
	ScaleMeshEffect_ModifyMesh_m3AA4BF3C24E81269AF1765BA1CCA7C91F00CBA3C,
	ScaleMeshEffect__ctor_mE96C5C9DA997BF9438B902AD1E2A2F39C0CD0FBA,
};
extern void U3CStartU3Ed__63_MoveNext_mE7A9ED52606E6BAED7043E305EC7F70044C149F0_AdjustorThunk (void);
extern void U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92_AdjustorThunk (void);
extern void U3CRaiseSourceDetectedU3Ed__77_MoveNext_mCD2B2EAAEC3539A9CFD61476E4A1394BA6BDEF76_AdjustorThunk (void);
extern void U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6_AdjustorThunk (void);
extern void U3CStartU3Ed__2_MoveNext_mFC68DF4C9CA99DD620FEFA5D6D24F9D2895BB564_AdjustorThunk (void);
extern void U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_mAB5FACD80C03C56E8C4A200A03A98225C7E77AAE_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A_AdjustorThunk (void);
extern void U3CStartU3Ed__2_MoveNext_m4C323A0DD7F833C4C47D0256CCE1CE07BD473CF2_AdjustorThunk (void);
extern void U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m78B851E87D4DA8335500019F0FA8824730AA31AA_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x060000B4, U3CStartU3Ed__63_MoveNext_mE7A9ED52606E6BAED7043E305EC7F70044C149F0_AdjustorThunk },
	{ 0x060000B5, U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92_AdjustorThunk },
	{ 0x060000B6, U3CRaiseSourceDetectedU3Ed__77_MoveNext_mCD2B2EAAEC3539A9CFD61476E4A1394BA6BDEF76_AdjustorThunk },
	{ 0x060000B7, U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6_AdjustorThunk },
	{ 0x060000BF, U3CStartU3Ed__2_MoveNext_mFC68DF4C9CA99DD620FEFA5D6D24F9D2895BB564_AdjustorThunk },
	{ 0x060000C0, U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B_AdjustorThunk },
	{ 0x060000C4, U3CEnsureInputSystemValidU3Ed__4_MoveNext_mAB5FACD80C03C56E8C4A200A03A98225C7E77AAE_AdjustorThunk },
	{ 0x060000C5, U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A_AdjustorThunk },
	{ 0x060000CB, U3CStartU3Ed__2_MoveNext_m4C323A0DD7F833C4C47D0256CCE1CE07BD473CF2_AdjustorThunk },
	{ 0x060000CC, U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D_AdjustorThunk },
	{ 0x060000D0, U3CEnsureInputSystemValidU3Ed__4_MoveNext_m78B851E87D4DA8335500019F0FA8824730AA31AA_AdjustorThunk },
	{ 0x060000D1, U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750_AdjustorThunk },
};
static const int32_t s_InvokerIndices[447] = 
{
	4295,
	3604,
	4371,
	3676,
	4406,
	1802,
	4406,
	4364,
	4326,
	4406,
	4406,
	2172,
	3635,
	4326,
	3635,
	751,
	278,
	1063,
	6303,
	2172,
	3635,
	4326,
	4295,
	3604,
	4295,
	3604,
	4326,
	3635,
	4326,
	3635,
	4295,
	4371,
	4326,
	4326,
	4364,
	3635,
	3635,
	4406,
	4406,
	4406,
	4406,
	2846,
	1698,
	1700,
	4295,
	4406,
	4406,
	3176,
	3176,
	3635,
	3176,
	-1,
	2174,
	3635,
	1698,
	4406,
	3635,
	2172,
	1003,
	4406,
	4653,
	840,
	749,
	4406,
	3635,
	3635,
	3635,
	2658,
	-1,
	934,
	4295,
	3604,
	-1,
	6303,
	4406,
	214,
	305,
	215,
	4406,
	4401,
	3706,
	4264,
	3575,
	4326,
	4326,
	3635,
	4295,
	3604,
	4326,
	4364,
	3635,
	3635,
	4406,
	3670,
	3176,
	3176,
	4295,
	6303,
	1702,
	3176,
	2682,
	2192,
	2167,
	4364,
	788,
	3635,
	4406,
	4364,
	3670,
	4326,
	4326,
	4326,
	3635,
	4326,
	4326,
	3635,
	4322,
	3630,
	4401,
	3706,
	4401,
	3706,
	4401,
	4401,
	4401,
	3706,
	4401,
	3706,
	4326,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	3635,
	3635,
	4326,
	4406,
	3630,
	3635,
	4364,
	4364,
	4198,
	3501,
	4344,
	3654,
	4364,
	3670,
	4249,
	3559,
	1247,
	2174,
	4364,
	3670,
	2217,
	4406,
	6303,
	4406,
	124,
	4326,
	3635,
	4326,
	3635,
	4371,
	3676,
	3635,
	4406,
	4406,
	4406,
	4401,
	4339,
	4406,
	1210,
	1210,
	6303,
	4406,
	3635,
	4406,
	3635,
	4406,
	4406,
	4406,
	4326,
	4406,
	4406,
	4406,
	4406,
	3635,
	6303,
	4406,
	4364,
	4406,
	3635,
	4406,
	4406,
	4406,
	4326,
	4406,
	4406,
	3635,
	6303,
	4406,
	4364,
	4406,
	3635,
	4326,
	3635,
	4326,
	4406,
	4406,
	4406,
	3635,
	2843,
	3635,
	3635,
	2658,
	3635,
	3635,
	3635,
	3635,
	3144,
	3635,
	3635,
	3635,
	4406,
	6303,
	2172,
	3604,
	4406,
	4364,
	4406,
	4326,
	4406,
	4326,
	4326,
	4326,
	2172,
	3635,
	4326,
	3635,
	3635,
	3635,
	3635,
	3635,
	4326,
	4326,
	4326,
	4326,
	4326,
	4326,
	3635,
	4326,
	3635,
	4364,
	4326,
	3635,
	3144,
	4295,
	4406,
	4406,
	4406,
	3635,
	4406,
	4406,
	4406,
	-1,
	-1,
	-1,
	2172,
	1238,
	-1,
	-1,
	-1,
	-1,
	-1,
	3635,
	3635,
	4406,
	4406,
	4406,
	3635,
	4406,
	4406,
	3635,
	4406,
	4406,
	4295,
	1001,
	2172,
	2172,
	1235,
	1245,
	1246,
	1239,
	1237,
	1238,
	1238,
	2172,
	2172,
	828,
	828,
	324,
	4406,
	828,
	1253,
	1253,
	853,
	854,
	855,
	852,
	850,
	2171,
	2171,
	1232,
	1233,
	1231,
	1230,
	2171,
	1232,
	1233,
	1231,
	1230,
	2171,
	321,
	1238,
	1238,
	1238,
	1238,
	1254,
	1254,
	847,
	847,
	847,
	-1,
	1499,
	1500,
	1501,
	1502,
	1498,
	1497,
	6303,
	6303,
	4406,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	2172,
	4406,
	4406,
	4401,
	4401,
	4364,
	4401,
	4401,
	4401,
	4401,
	4398,
	4364,
	4326,
	4406,
	4406,
	3706,
	3706,
	3706,
	3703,
	3635,
	1802,
	4406,
	6303,
	4406,
	1524,
	4401,
	4401,
	4398,
	4406,
	6279,
	4401,
	4401,
	4398,
	4406,
	1802,
	4406,
	4406,
	6303,
	4364,
	4326,
	4406,
	1802,
	4406,
	3635,
	3635,
	3635,
	3635,
	4406,
	4406,
	4406,
	3635,
	4406,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x06000034, { 0, 4 } },
	{ 0x06000045, { 4, 1 } },
	{ 0x06000049, { 5, 1 } },
	{ 0x0600010E, { 6, 2 } },
	{ 0x0600010F, { 8, 2 } },
	{ 0x06000110, { 10, 3 } },
	{ 0x06000113, { 13, 3 } },
	{ 0x06000114, { 16, 1 } },
	{ 0x06000115, { 17, 1 } },
	{ 0x06000116, { 18, 1 } },
	{ 0x06000117, { 19, 1 } },
	{ 0x06000152, { 20, 6 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)2, 9078 },
	{ (Il2CppRGCTXDataType)3, 28752 },
	{ (Il2CppRGCTXDataType)2, 222 },
	{ (Il2CppRGCTXDataType)3, 28753 },
	{ (Il2CppRGCTXDataType)1, 221 },
	{ (Il2CppRGCTXDataType)1, 223 },
	{ (Il2CppRGCTXDataType)1, 318 },
	{ (Il2CppRGCTXDataType)3, 48868 },
	{ (Il2CppRGCTXDataType)1, 324 },
	{ (Il2CppRGCTXDataType)3, 48863 },
	{ (Il2CppRGCTXDataType)3, 50236 },
	{ (Il2CppRGCTXDataType)3, 50242 },
	{ (Il2CppRGCTXDataType)3, 50233 },
	{ (Il2CppRGCTXDataType)3, 50237 },
	{ (Il2CppRGCTXDataType)3, 50243 },
	{ (Il2CppRGCTXDataType)3, 50234 },
	{ (Il2CppRGCTXDataType)3, 48893 },
	{ (Il2CppRGCTXDataType)3, 48894 },
	{ (Il2CppRGCTXDataType)3, 49616 },
	{ (Il2CppRGCTXDataType)3, 49615 },
	{ (Il2CppRGCTXDataType)2, 521 },
	{ (Il2CppRGCTXDataType)2, 6437 },
	{ (Il2CppRGCTXDataType)3, 18596 },
	{ (Il2CppRGCTXDataType)3, 18597 },
	{ (Il2CppRGCTXDataType)2, 997 },
	{ (Il2CppRGCTXDataType)3, 18598 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_InputSystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSystem_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSystem_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.InputSystem.dll",
	447,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Services_InputSystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
