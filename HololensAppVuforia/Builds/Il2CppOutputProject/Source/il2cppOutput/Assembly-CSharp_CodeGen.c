﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityThread::initUnityThread(System.Boolean)
extern void UnityThread_initUnityThread_m7E1BB70A89DB1C3B71BA23F5486E2DB4ED78FFC6 (void);
// 0x00000002 System.Void UnityThread::Awake()
extern void UnityThread_Awake_m929F7BC8C4524B19FD779D6CA21D8EF8E06772E0 (void);
// 0x00000003 System.Void UnityThread::executeCoroutine(System.Collections.IEnumerator)
extern void UnityThread_executeCoroutine_mC5F0503C5441FC8618E3F702BCB5FA9C01BBE5C8 (void);
// 0x00000004 System.Void UnityThread::executeInUpdate(System.Action)
extern void UnityThread_executeInUpdate_mA5C09375FFF47F6B68D2FEB5DB550FF0D1780E22 (void);
// 0x00000005 System.Void UnityThread::Update()
extern void UnityThread_Update_m4B2985C59168E230793AC0390462B694A949213D (void);
// 0x00000006 System.Void UnityThread::executeInLateUpdate(System.Action)
extern void UnityThread_executeInLateUpdate_mB2EC436C13983185E80920E7E8F00355B0987450 (void);
// 0x00000007 System.Void UnityThread::LateUpdate()
extern void UnityThread_LateUpdate_m3D56F4EC0E45FC83EE2472B74A4C2C2E4FCD643E (void);
// 0x00000008 System.Void UnityThread::executeInFixedUpdate(System.Action)
extern void UnityThread_executeInFixedUpdate_mFACE16996AAA19E7ADDAB8FA8CA18B56684E1449 (void);
// 0x00000009 System.Void UnityThread::FixedUpdate()
extern void UnityThread_FixedUpdate_m899EB37D52E22DFC8D7A25517005EA47CA75F789 (void);
// 0x0000000A System.Void UnityThread::OnDisable()
extern void UnityThread_OnDisable_m6D06210991C8967233FEC58E1B4C6A1E1F5506B2 (void);
// 0x0000000B System.Void UnityThread::.ctor()
extern void UnityThread__ctor_mB026097B94171032E79AC8414172E7D573BC7B34 (void);
// 0x0000000C System.Void UnityThread::.cctor()
extern void UnityThread__cctor_m5062DAB784BFA1BC30459D64C0BD0D473F96E6DC (void);
// 0x0000000D System.Void UnityThread/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mCA38F8709513598A4839C7108CA09166ECAF0EEF (void);
// 0x0000000E System.Void UnityThread/<>c__DisplayClass12_0::<executeCoroutine>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CexecuteCoroutineU3Eb__0_mC54ED731AE66D35AD5252B2B7B71DE0EBCE66FEE (void);
// 0x0000000F System.Void FixPositionTarget::Update()
extern void FixPositionTarget_Update_mEA401A53F3D988802A8FBA7C322411CAE70E3BC4 (void);
// 0x00000010 System.Void FixPositionTarget::.ctor()
extern void FixPositionTarget__ctor_m5EEEDC47A4DCB88F5DD03FA9546B572952B6D960 (void);
// 0x00000011 System.Void HandleControls::Start()
extern void HandleControls_Start_mD17E2ACCFD9C8CD3B5F2E183FC4D41752E62B7E0 (void);
// 0x00000012 System.Void HandleControls::RemoveControlsComponent(UnityEngine.Color)
extern void HandleControls_RemoveControlsComponent_m2582EAFF05010344B6C63D42C0F30B2ED3443B1F (void);
// 0x00000013 System.Void HandleControls::RestoreControlsComponent()
extern void HandleControls_RestoreControlsComponent_mCAABEDF05204AE6CCD220983DE25A4A44B512B83 (void);
// 0x00000014 System.Void HandleControls::.ctor()
extern void HandleControls__ctor_mFB9F27C767688D4C3209FCE65734160CEBDF8ABA (void);
// 0x00000015 System.Void NetworkManager::Awake()
extern void NetworkManager_Awake_m264F69270EE58EA94BD639B7CFC5DBB6CEAD7E2F (void);
// 0x00000016 System.Void NetworkManager::Start()
extern void NetworkManager_Start_mF62F7D5D531194AFEF4C3FFAC8BFE07E10F72365 (void);
// 0x00000017 System.Void NetworkManager::OnApplicationQuit()
extern void NetworkManager_OnApplicationQuit_m2DB9568B0EA3F3129AF50C5E56400CC4B4CF1C37 (void);
// 0x00000018 System.Void NetworkManager::ConnectClient(System.String)
extern void NetworkManager_ConnectClient_m8765E65D21C6C4A7852EF60BECAA2F632489322A (void);
// 0x00000019 System.Void NetworkManager::.ctor()
extern void NetworkManager__ctor_m1C3DBB165C04540C090C4D74FEDBD83DEFC5C62F (void);
// 0x0000001A System.Void ReadInput::Readtext()
extern void ReadInput_Readtext_m0F3DFBA189C187F1A9C09FCBFD628A50E1FF6B25 (void);
// 0x0000001B System.Void ReadInput::.ctor()
extern void ReadInput__ctor_m7F5E5A65F56506205D2044487E9BB7E69FB8947E (void);
// 0x0000001C Manipulation UpdateObject::get_State()
extern void UpdateObject_get_State_m9E51E530AE3A97069DA74E8254F06CA2AEC8A185 (void);
// 0x0000001D System.Void UpdateObject::set_State(Manipulation)
extern void UpdateObject_set_State_m859D0FDD24D20DAEE41F6F987F23A9DD366B3E57 (void);
// 0x0000001E UnityEngine.Color UpdateObject::get_MyColor()
extern void UpdateObject_get_MyColor_mA2D987AF363580B88ED776CD16D9E045DA0D93AD (void);
// 0x0000001F System.Void UpdateObject::set_MyColor(UnityEngine.Color)
extern void UpdateObject_set_MyColor_m51FD70A35B5A665D9F807E1DB4469225564AB4B2 (void);
// 0x00000020 System.Void UpdateObject::RequestControl(System.Int32)
extern void UpdateObject_RequestControl_m7062D67D3FAD517F0293AEDED06B53202E58364A (void);
// 0x00000021 System.Void UpdateObject::ReleseControl()
extern void UpdateObject_ReleseControl_m5E36D3E935544E473F906B0E4E7F22DA1304E1C0 (void);
// 0x00000022 System.Void UpdateObject::Start()
extern void UpdateObject_Start_m8DCC5CCEE199EBFDB14FB08B301715C0B897A54F (void);
// 0x00000023 System.Void UpdateObject::Update()
extern void UpdateObject_Update_mB5AA7D9EF03C148F94F97412C5782D09C8F71F09 (void);
// 0x00000024 System.Collections.IEnumerable UpdateObject::DelayTime(System.Single)
extern void UpdateObject_DelayTime_mC09895EC20B1B4716E4631F2584CC91BE343C29D (void);
// 0x00000025 System.Void UpdateObject::SendRotation()
extern void UpdateObject_SendRotation_mF6C4959D15A0FDEBDEA48C975CF798F64C487210 (void);
// 0x00000026 System.Void UpdateObject::SendScale()
extern void UpdateObject_SendScale_mE45BEB7372336DC4E36D2D6CDE5BDA44A45C63DD (void);
// 0x00000027 System.Void UpdateObject::SendPosition()
extern void UpdateObject_SendPosition_m9E0558ABAF1B7BEFA92C87BAF735AFE650011621 (void);
// 0x00000028 System.Void UpdateObject::UpdatePosition(UnityEngine.Vector3)
extern void UpdateObject_UpdatePosition_m9CF760E40796CE98F38103DCFAB1D246D765A588 (void);
// 0x00000029 System.Void UpdateObject::UpdateRotation(UnityEngine.Quaternion)
extern void UpdateObject_UpdateRotation_m18BE3270C6BDDBA7660BC3B48078372389CC84C6 (void);
// 0x0000002A System.Void UpdateObject::UpdateScale(UnityEngine.Vector3)
extern void UpdateObject_UpdateScale_mFC4C97A0500193EA5C1C633818DDF619EAA40750 (void);
// 0x0000002B System.Void UpdateObject::UpdateEulerAngle(UnityEngine.Vector3)
extern void UpdateObject_UpdateEulerAngle_mD49E5EACD2CD65B67FC25BF36BF9624FC5E0D2FD (void);
// 0x0000002C System.Void UpdateObject::.ctor()
extern void UpdateObject__ctor_m4DAA3DF1D7076362B414297675D3056346661847 (void);
// 0x0000002D System.Void UpdateObject/<DelayTime>d__14::.ctor(System.Int32)
extern void U3CDelayTimeU3Ed__14__ctor_m6EE898869F89AF142FA27B9F00BEDEE8756829C3 (void);
// 0x0000002E System.Void UpdateObject/<DelayTime>d__14::System.IDisposable.Dispose()
extern void U3CDelayTimeU3Ed__14_System_IDisposable_Dispose_mC27B5775D488708A9796CB96912372B2962C14CD (void);
// 0x0000002F System.Boolean UpdateObject/<DelayTime>d__14::MoveNext()
extern void U3CDelayTimeU3Ed__14_MoveNext_m09AC91B981B2EF73CC2F7C29C2CAC727621ECA13 (void);
// 0x00000030 System.Object UpdateObject/<DelayTime>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayTimeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m579ABD90A41F73673715242D3B4CF16AE0F02E20 (void);
// 0x00000031 System.Void UpdateObject/<DelayTime>d__14::System.Collections.IEnumerator.Reset()
extern void U3CDelayTimeU3Ed__14_System_Collections_IEnumerator_Reset_mA917D5877B3E2F40D649757641CCB56AC5E86D85 (void);
// 0x00000032 System.Object UpdateObject/<DelayTime>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CDelayTimeU3Ed__14_System_Collections_IEnumerator_get_Current_mFDACDEDD7600C9D80FF8ED0E6D6108B12D7C4AFE (void);
// 0x00000033 System.Collections.Generic.IEnumerator`1<System.Object> UpdateObject/<DelayTime>d__14::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void U3CDelayTimeU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m8B4CF67FC53EC9A4359FBA6FE62D0F68D05E63E2 (void);
// 0x00000034 System.Collections.IEnumerator UpdateObject/<DelayTime>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3CDelayTimeU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mBE023A7E1C6FE13F4B68FB946E49801199622E75 (void);
// 0x00000035 System.Void DimBoxes.BoundBox::Reset()
extern void BoundBox_Reset_m59620396D5B366579DB69DB412253510D9FFE064 (void);
// 0x00000036 System.Void DimBoxes.BoundBox::Start()
extern void BoundBox_Start_m262BD0F03D60CACDA3F72F256FC6B5D3B6DC063D (void);
// 0x00000037 System.Void DimBoxes.BoundBox::init()
extern void BoundBox_init_m798D364FC16E94832858F65FF704F202CACC3F4C (void);
// 0x00000038 System.Void DimBoxes.BoundBox::LateUpdate()
extern void BoundBox_LateUpdate_m5FC7F74CD60A10D7179394987CC90C2CB5713820 (void);
// 0x00000039 System.Void DimBoxes.BoundBox::calculateBounds()
extern void BoundBox_calculateBounds_m7A23D83CB7837B3EEAF4C797200E96906EC84A31 (void);
// 0x0000003A System.Void DimBoxes.BoundBox::SetPoints()
extern void BoundBox_SetPoints_m351DF77B6A2819402BAECD55EFB31F398A934A23 (void);
// 0x0000003B System.Void DimBoxes.BoundBox::SetLines()
extern void BoundBox_SetLines_m0802731CCA33C4E5D61B0D5EF26B87B88338B170 (void);
// 0x0000003C System.Void DimBoxes.BoundBox::SetLineRenderers()
extern void BoundBox_SetLineRenderers_m7B4AE7ACFD716DDB4CE4DDC727CDECA6D9D325E2 (void);
// 0x0000003D System.Void DimBoxes.BoundBox::OnEnable()
extern void BoundBox_OnEnable_m1149BDBA317B257993E0C3AECB5040C60FCEAA09 (void);
// 0x0000003E System.Void DimBoxes.BoundBox::OnDestroy()
extern void BoundBox_OnDestroy_mB9A0DF21534607D190E80A45D13D2D6F9711CAE8 (void);
// 0x0000003F System.Void DimBoxes.BoundBox::OnDisable()
extern void BoundBox_OnDisable_mA29F947BB9AF78670E9A9BD2C820290270B306FC (void);
// 0x00000040 System.Void DimBoxes.BoundBox::OnDrawGizmos()
extern void BoundBox_OnDrawGizmos_mB0CA1303CF0F98BEDB514772ADBFD2AC52A90E5D (void);
// 0x00000041 System.Void DimBoxes.BoundBox::OnRenderObject()
extern void BoundBox_OnRenderObject_mD7CE4E00151496250A8E0684A07EE1B5CC468A5A (void);
// 0x00000042 System.Void DimBoxes.BoundBox::.ctor()
extern void BoundBox__ctor_m640CFADC57BCE58550D0CB034B3F93110134B987 (void);
// 0x00000043 System.Void DimBoxes.BoundBoxExample::Start()
extern void BoundBoxExample_Start_m51800006EEDD4657B74C9569CAE3D7E31240FE6F (void);
// 0x00000044 System.Void DimBoxes.BoundBoxExample::EnableLines(System.Boolean)
extern void BoundBoxExample_EnableLines_m1D726FF8C1C29BD4D5A416FF8A8AF7EA785F769B (void);
// 0x00000045 System.Void DimBoxes.BoundBoxExample::EnableWires(System.Boolean)
extern void BoundBoxExample_EnableWires_m2A5EE3102F99C86675C5E56AC55512D69FAB5091 (void);
// 0x00000046 System.Void DimBoxes.BoundBoxExample::SetLineWidth(UnityEngine.UI.Slider)
extern void BoundBoxExample_SetLineWidth_m37490F94463FD600D4777CDB8252B9F8494E9E62 (void);
// 0x00000047 System.Void DimBoxes.BoundBoxExample::SetNumCapVertices(UnityEngine.UI.Slider)
extern void BoundBoxExample_SetNumCapVertices_m68B2CEA3D624FFC8F4F6064AC5F064C4CF4C9DDA (void);
// 0x00000048 System.Void DimBoxes.BoundBoxExample::.ctor()
extern void BoundBoxExample__ctor_mC12A3F18C2C9A098FB5C9FB0DED68E3B4FF7892D (void);
// 0x00000049 System.Void DimBoxes.DrawLines::OnEnable()
extern void DrawLines_OnEnable_m0B44FD76EA205DFB25BD2CB8DD72323CC44C0BB8 (void);
// 0x0000004A System.Void DimBoxes.DrawLines::Start()
extern void DrawLines_Start_m95AEEF8FB01DFE3D7CAD0842593918B85E5376C6 (void);
// 0x0000004B System.Void DimBoxes.DrawLines::OnRenderObject()
extern void DrawLines_OnRenderObject_mA3B5D37740D2BA4FB3ED2D944FA1E659A2883F7D (void);
// 0x0000004C System.Void DimBoxes.DrawLines::setOutlines(UnityEngine.Vector3[0...,0...],UnityEngine.Color)
extern void DrawLines_setOutlines_m0BC4634935A4C554F708D9415AD918C54A1D74E6 (void);
// 0x0000004D System.Void DimBoxes.DrawLines::setScreenOutlines(UnityEngine.Vector3[0...,0...],UnityEngine.Color)
extern void DrawLines_setScreenOutlines_m6A091BB51C8F17852035AC1F02198799BF012EF1 (void);
// 0x0000004E System.Void DimBoxes.DrawLines::setOutlines(UnityEngine.Vector3[0...,0...],UnityEngine.Color,UnityEngine.Vector3[][])
extern void DrawLines_setOutlines_mB76E33D697E057FD8407720B31E7310E4632791F (void);
// 0x0000004F System.Void DimBoxes.DrawLines::Update()
extern void DrawLines_Update_m09852A44FC777E54F5B5A8AE5E158AA10E919F64 (void);
// 0x00000050 System.Void DimBoxes.DrawLines::.ctor()
extern void DrawLines__ctor_mF14A63A634356A725E932D0C6E8F463F8BCC5388 (void);
// 0x00000051 System.Void DimBoxes.TestDynamicBehaviour::Start()
extern void TestDynamicBehaviour_Start_m9B8EA71B705689F8A2405893F7B1C4858FB35161 (void);
// 0x00000052 System.Void DimBoxes.TestDynamicBehaviour::Update()
extern void TestDynamicBehaviour_Update_m295D2DC22D5AEC5CCA7547BA9B35FD291ABA9047 (void);
// 0x00000053 System.Void DimBoxes.TestDynamicBehaviour::Test(System.Boolean)
extern void TestDynamicBehaviour_Test_mB8EFB4AE022466876C693427F2171E89367741BA (void);
// 0x00000054 System.Void DimBoxes.TestDynamicBehaviour::.ctor()
extern void TestDynamicBehaviour__ctor_m93F8AE7A088F923ED29DA257748123D64B828829 (void);
// 0x00000055 System.Void DimBoxes.maxCamera::Start()
extern void maxCamera_Start_mB12ECE1DE6A1E942E2A9E91315405E5951A8C988 (void);
// 0x00000056 System.Void DimBoxes.maxCamera::OnEnable()
extern void maxCamera_OnEnable_m5778008067723C05AA2DA74092B076094302BA36 (void);
// 0x00000057 System.Void DimBoxes.maxCamera::Init()
extern void maxCamera_Init_mF8E96F567C89D554273382B8C08DC30ECA5C7F77 (void);
// 0x00000058 System.Void DimBoxes.maxCamera::LateUpdate()
extern void maxCamera_LateUpdate_mDA3F2CE587BC04153AA1671933FB730BD61A83AE (void);
// 0x00000059 System.Single DimBoxes.maxCamera::ClampAngle(System.Single,System.Single,System.Single)
extern void maxCamera_ClampAngle_m3F2158E3F4CEA37191FACF602CCDEF3BC9482A93 (void);
// 0x0000005A System.Void DimBoxes.maxCamera::Update()
extern void maxCamera_Update_m1526E853678AB25BD1FFABF33AA4ABCE3DF015F2 (void);
// 0x0000005B System.Collections.IEnumerator DimBoxes.maxCamera::DragObject(UnityEngine.Vector3)
extern void maxCamera_DragObject_mAD1346C04E5516343C509A9E10FC7EA7D43F3843 (void);
// 0x0000005C System.Void DimBoxes.maxCamera::SetRadius(System.Single)
extern void maxCamera_SetRadius_m66ABFE466ACB0274E934CD7DC14EF946C390596D (void);
// 0x0000005D System.Void DimBoxes.maxCamera::.ctor()
extern void maxCamera__ctor_m23D16D546D96F3F270A765F070E91CEAEF76A958 (void);
// 0x0000005E System.Void DimBoxes.maxCamera/<DragObject>d__34::.ctor(System.Int32)
extern void U3CDragObjectU3Ed__34__ctor_mD6CE69B1B7E77F218A9B9CA5933CA4508739CABB (void);
// 0x0000005F System.Void DimBoxes.maxCamera/<DragObject>d__34::System.IDisposable.Dispose()
extern void U3CDragObjectU3Ed__34_System_IDisposable_Dispose_mCBF41367AE39B0051050F1CD9D623591604377FE (void);
// 0x00000060 System.Boolean DimBoxes.maxCamera/<DragObject>d__34::MoveNext()
extern void U3CDragObjectU3Ed__34_MoveNext_mD90FA51BD5E05C90C8257E9102AF2000B6CCF4B1 (void);
// 0x00000061 System.Object DimBoxes.maxCamera/<DragObject>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDragObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE977501C9712C138E0BDD0EB3A972F2923ECDBB2 (void);
// 0x00000062 System.Void DimBoxes.maxCamera/<DragObject>d__34::System.Collections.IEnumerator.Reset()
extern void U3CDragObjectU3Ed__34_System_Collections_IEnumerator_Reset_mBA9759EC27FEF954670F924101C567393D0774B3 (void);
// 0x00000063 System.Object DimBoxes.maxCamera/<DragObject>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CDragObjectU3Ed__34_System_Collections_IEnumerator_get_Current_mAD36677BBE6ABC6940F1028D3D7A4DEF0C0944E1 (void);
// 0x00000064 UnityEngine.Camera HoloToolkit.Unity.CameraCache::get_Main()
extern void CameraCache_get_Main_m620E8E7552448CD482C4ED03B0F6AC6B154FD9B4 (void);
// 0x00000065 UnityEngine.Camera HoloToolkit.Unity.CameraCache::Refresh(UnityEngine.Camera)
extern void CameraCache_Refresh_m48F440049E292F53B87295A7B7C6D804B23B1ADC (void);
// 0x00000066 System.Boolean HoloToolkit.Unity.Interpolator::get_AnimatingPosition()
extern void Interpolator_get_AnimatingPosition_m57E7ABC9036E6EDCB9F762CFE1E3EF617A1A793B (void);
// 0x00000067 System.Void HoloToolkit.Unity.Interpolator::set_AnimatingPosition(System.Boolean)
extern void Interpolator_set_AnimatingPosition_m4A723496EB756442FD5E6E410BC2B5AD0FA36DFB (void);
// 0x00000068 System.Boolean HoloToolkit.Unity.Interpolator::get_AnimatingRotation()
extern void Interpolator_get_AnimatingRotation_m7201936CBDD7BD6ACB01678115235B193C28BF92 (void);
// 0x00000069 System.Void HoloToolkit.Unity.Interpolator::set_AnimatingRotation(System.Boolean)
extern void Interpolator_set_AnimatingRotation_m6EDE5A4B35E1C3551D12F39B5857FFAEB5521687 (void);
// 0x0000006A System.Boolean HoloToolkit.Unity.Interpolator::get_AnimatingLocalRotation()
extern void Interpolator_get_AnimatingLocalRotation_m339D88A11EF303C2FEF3FC7D57392C2247CF5C70 (void);
// 0x0000006B System.Void HoloToolkit.Unity.Interpolator::set_AnimatingLocalRotation(System.Boolean)
extern void Interpolator_set_AnimatingLocalRotation_m6DC1C0CF5B9CC197CC1D0823EC9590C1A70B155C (void);
// 0x0000006C System.Boolean HoloToolkit.Unity.Interpolator::get_AnimatingLocalScale()
extern void Interpolator_get_AnimatingLocalScale_mCC228EEE443EA8A49FD326CF166BA62466824690 (void);
// 0x0000006D System.Void HoloToolkit.Unity.Interpolator::set_AnimatingLocalScale(System.Boolean)
extern void Interpolator_set_AnimatingLocalScale_m70BF55824BE1B623A46989FB792786B9A09AA9C4 (void);
// 0x0000006E System.Void HoloToolkit.Unity.Interpolator::add_InterpolationStarted(System.Action)
extern void Interpolator_add_InterpolationStarted_mBC37270CD5F2FB61B6999A46B012939BA64703C6 (void);
// 0x0000006F System.Void HoloToolkit.Unity.Interpolator::remove_InterpolationStarted(System.Action)
extern void Interpolator_remove_InterpolationStarted_m6DF682B30A92A129162A3E32EF3F27C9F78F6035 (void);
// 0x00000070 System.Void HoloToolkit.Unity.Interpolator::add_InterpolationDone(System.Action)
extern void Interpolator_add_InterpolationDone_m62475E1E1DD151BEFA8C3E8C57099E3E15DDAB3B (void);
// 0x00000071 System.Void HoloToolkit.Unity.Interpolator::remove_InterpolationDone(System.Action)
extern void Interpolator_remove_InterpolationDone_m0FE2F49EFACC7657BF14D8A74E724E648BB8E547 (void);
// 0x00000072 UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::get_PositionVelocity()
extern void Interpolator_get_PositionVelocity_mCAE50CC2B5436F38EF71239C56F7BD75E29F7BEA (void);
// 0x00000073 System.Void HoloToolkit.Unity.Interpolator::set_PositionVelocity(UnityEngine.Vector3)
extern void Interpolator_set_PositionVelocity_m64822951DFFC2200006FF5B33F051EDE4B593B04 (void);
// 0x00000074 System.Boolean HoloToolkit.Unity.Interpolator::get_Running()
extern void Interpolator_get_Running_m66F6F94D33D4BBA1AA6D19CA2BC784732A72CC61 (void);
// 0x00000075 System.Void HoloToolkit.Unity.Interpolator::Awake()
extern void Interpolator_Awake_mB2FDB7B4E28B653C1541356A7675E9D2B1416E6E (void);
// 0x00000076 System.Void HoloToolkit.Unity.Interpolator::SetTargetPosition(UnityEngine.Vector3)
extern void Interpolator_SetTargetPosition_m0B337E11828E5772B1F94C4B95E4A51840A67EA6 (void);
// 0x00000077 System.Void HoloToolkit.Unity.Interpolator::SetTargetRotation(UnityEngine.Quaternion)
extern void Interpolator_SetTargetRotation_m2FBE94B30CC463C2405E9A04EADA2A2C16C10FE4 (void);
// 0x00000078 System.Void HoloToolkit.Unity.Interpolator::SetTargetLocalRotation(UnityEngine.Quaternion)
extern void Interpolator_SetTargetLocalRotation_m4A1B55D72DE21716F52E939358909F4FA431BA36 (void);
// 0x00000079 System.Void HoloToolkit.Unity.Interpolator::SetTargetLocalScale(UnityEngine.Vector3)
extern void Interpolator_SetTargetLocalScale_m7033FDE05BD75726A7CB4510D288E26680DA4BFB (void);
// 0x0000007A UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::NonLinearInterpolateTo(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Interpolator_NonLinearInterpolateTo_mEE47DD2EF658372780E851A7BFF1118D1F030F13 (void);
// 0x0000007B System.Void HoloToolkit.Unity.Interpolator::Update()
extern void Interpolator_Update_m04C79AC166B3106802DF5B6A057D00DB9629B598 (void);
// 0x0000007C System.Void HoloToolkit.Unity.Interpolator::SnapToTarget()
extern void Interpolator_SnapToTarget_m1ABC78F74B6C1D591BD7ADB4381C1EDD5AE7DDF3 (void);
// 0x0000007D System.Void HoloToolkit.Unity.Interpolator::StopInterpolating()
extern void Interpolator_StopInterpolating_m59FD1BC368CCFB6E6479B63471352D04B38D0642 (void);
// 0x0000007E System.Void HoloToolkit.Unity.Interpolator::Reset()
extern void Interpolator_Reset_m9D5B6BBEE328F507675D160D2CC65089CCF2D261 (void);
// 0x0000007F UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::get_TargetPosition()
extern void Interpolator_get_TargetPosition_mE639B9EFF6E0379A1E172EE48050A277C27490F4 (void);
// 0x00000080 UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::get_TargetRotation()
extern void Interpolator_get_TargetRotation_mF75D9EAE3BEEB6BDDD8085473B22C8A98F50FD68 (void);
// 0x00000081 UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::get_TargetLocalRotation()
extern void Interpolator_get_TargetLocalRotation_m47E6C7D899FC52E30A5609551CC83BCF738E5EE3 (void);
// 0x00000082 UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::get_TargetLocalScale()
extern void Interpolator_get_TargetLocalScale_mA435EAB66DA550E87BF7468383BF9451E58B05B7 (void);
// 0x00000083 System.Void HoloToolkit.Unity.Interpolator::.ctor()
extern void Interpolator__ctor_m388BF47AF9F4C0A42978B483F58E4EF8F81662B1 (void);
// 0x00000084 System.Void HoloToolkit.Unity.Billboard::OnEnable()
extern void Billboard_OnEnable_m7B6C746E1ABD853BF71C684366646011643368D8 (void);
// 0x00000085 System.Void HoloToolkit.Unity.Billboard::Update()
extern void Billboard_Update_m266806D96B009FC8D2B890C15105C385D17CE14F (void);
// 0x00000086 System.Void HoloToolkit.Unity.Billboard::.ctor()
extern void Billboard__ctor_mB94741F3C3E18B5C4A70CA1087374C00C882D2B8 (void);
// 0x00000087 System.Void HoloToolkit.Unity.FixedAngularSize::Start()
extern void FixedAngularSize_Start_mE293E8B99411BCD16091F1809D38EA0C8BA49309 (void);
// 0x00000088 System.Void HoloToolkit.Unity.FixedAngularSize::SetSizeRatio(System.Single)
extern void FixedAngularSize_SetSizeRatio_m2F18BEBAB64630AED4291EC1A65BB74AFD4DE590 (void);
// 0x00000089 System.Void HoloToolkit.Unity.FixedAngularSize::Update()
extern void FixedAngularSize_Update_mEF299CCF4E1D7C27971217CE409CBBF3E6EEB9E3 (void);
// 0x0000008A System.Void HoloToolkit.Unity.FixedAngularSize::.ctor()
extern void FixedAngularSize__ctor_m5DEDCBF834FEB6660D06B733384F23C030698E3D (void);
// 0x0000008B System.Void HoloToolkit.Unity.SimpleTagalong::Start()
extern void SimpleTagalong_Start_mD110391CF168157D7B9A596E6662C0A9692DA069 (void);
// 0x0000008C System.Void HoloToolkit.Unity.SimpleTagalong::Update()
extern void SimpleTagalong_Update_m1AFEE64B5F908011173AF460E466AD9F278869F9 (void);
// 0x0000008D System.Boolean HoloToolkit.Unity.SimpleTagalong::CalculateTagalongTargetPosition(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void SimpleTagalong_CalculateTagalongTargetPosition_m221BE5079FC9D662126479D5CB2E5B44A81F77EE (void);
// 0x0000008E System.Void HoloToolkit.Unity.SimpleTagalong::.ctor()
extern void SimpleTagalong__ctor_mC344395CA376A23C69E2548A0AD5EBC1C34AD421 (void);
// 0x0000008F System.Void HoloToolkit.Unity.Tagalong::Start()
extern void Tagalong_Start_mBEC0FC64FAC9C26FC13977A38D4F0F0732F7C1DF (void);
// 0x00000090 System.Void HoloToolkit.Unity.Tagalong::Update()
extern void Tagalong_Update_m883BB1DFDB965A7D5833F332AFB5DD8524C10D1B (void);
// 0x00000091 System.Boolean HoloToolkit.Unity.Tagalong::CalculateTagalongTargetPosition(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void Tagalong_CalculateTagalongTargetPosition_mD9A2A6F6221EAF8134BF083A64812B103A06F489 (void);
// 0x00000092 UnityEngine.Vector3 HoloToolkit.Unity.Tagalong::CalculateTargetPosition(System.Boolean,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.Plane,System.Boolean)
extern void Tagalong_CalculateTargetPosition_mCBC20CD385370474672B54CED9FE60ABBF70485C (void);
// 0x00000093 System.Boolean HoloToolkit.Unity.Tagalong::AdjustTagalongDistance(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void Tagalong_AdjustTagalongDistance_m25DE534C629FC6DA5B4DB483551F2BB77E6F70C3 (void);
// 0x00000094 System.Void HoloToolkit.Unity.Tagalong::.ctor()
extern void Tagalong__ctor_m245759F2DFDA3AA84EE9610AF4166E534F7288A7 (void);
// 0x00000095 System.Void Assets.Scripts.ClientHandleData::InitializePackets()
extern void ClientHandleData_InitializePackets_m28FC5FDEB3C5E36845741741B9E2F075FB478EAE (void);
// 0x00000096 System.Void Assets.Scripts.ClientHandleData::HandleData(System.Byte[])
extern void ClientHandleData_HandleData_m868832BA9260602AAA0AC8744E3B6B980D1A70A7 (void);
// 0x00000097 System.Void Assets.Scripts.ClientHandleData::HandleDataPackets(System.Byte[])
extern void ClientHandleData_HandleDataPackets_mB6A8CC336A7E8C1650C74A0347E8BA788654AF52 (void);
// 0x00000098 System.Void Assets.Scripts.ClientHandleData::.ctor()
extern void ClientHandleData__ctor_m8D74E80620A9C52220B8E0C6EB606A654B13994B (void);
// 0x00000099 System.Void Assets.Scripts.ClientHandleData::.cctor()
extern void ClientHandleData__cctor_mD5F1EF5FDDB45404301E69522F19C946E8CB4FB6 (void);
// 0x0000009A System.Void Assets.Scripts.ClientHandleData/Packet::.ctor(System.Object,System.IntPtr)
extern void Packet__ctor_mBF7634DA169E58964FB3A95B1AD9C8F9B7D8997E (void);
// 0x0000009B System.Void Assets.Scripts.ClientHandleData/Packet::Invoke(System.Byte[])
extern void Packet_Invoke_mED57B86349F0C45C5E44B44E6680734CF5CE2608 (void);
// 0x0000009C System.IAsyncResult Assets.Scripts.ClientHandleData/Packet::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern void Packet_BeginInvoke_m5D8C0A6FDFED8E999B5DB1CFAD37925B218466E6 (void);
// 0x0000009D System.Void Assets.Scripts.ClientHandleData/Packet::EndInvoke(System.IAsyncResult)
extern void Packet_EndInvoke_mB2145753879BE0FDFFCFFBA9100F32D542623F7E (void);
// 0x0000009E System.String Assets.Scripts.ClientTCP::get_ServerAddress()
extern void ClientTCP_get_ServerAddress_m0A08D35116AAB4B55B9B311076BF028C0A5BDB24 (void);
// 0x0000009F System.Void Assets.Scripts.ClientTCP::set_ServerAddress(System.String)
extern void ClientTCP_set_ServerAddress_m03BF542E000365B50334312EB43169DDC88998FF (void);
// 0x000000A0 System.Void Assets.Scripts.ClientTCP::InitializingNetworking()
extern void ClientTCP_InitializingNetworking_m47A51567FCDC3545C79A8851962E399180474447 (void);
// 0x000000A1 System.Void Assets.Scripts.ClientTCP::ClientConnectCallback(System.IAsyncResult)
extern void ClientTCP_ClientConnectCallback_m0DB03784A1A471BC9CEA69B0E624C79D2E6F1BF9 (void);
// 0x000000A2 System.Void Assets.Scripts.ClientTCP::ReciveCallBack(System.IAsyncResult)
extern void ClientTCP_ReciveCallBack_m2C03C241C7D82516490B3E258120A4781A5955BB (void);
// 0x000000A3 System.Void Assets.Scripts.ClientTCP::SendData(System.Byte[])
extern void ClientTCP_SendData_m26FCD379C3855A2202C2571DFB2D3BA93F40CE42 (void);
// 0x000000A4 System.Void Assets.Scripts.ClientTCP::Disconnect()
extern void ClientTCP_Disconnect_m8959BD3114AD40E5D917D708ECE07276ED183C27 (void);
// 0x000000A5 System.Void Assets.Scripts.ClientTCP/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m8CE1B2274692E61F2D9B5765446DB7C05A9D61EA (void);
// 0x000000A6 System.Void Assets.Scripts.ClientTCP/<>c__DisplayClass9_0::<ReciveCallBack>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CReciveCallBackU3Eb__0_m41A34ACD7A2BC99625BCD06162148E3E482B5A2C (void);
// 0x000000A7 HandleControls Assets.Scripts.DataReceiver::get_HandleControls()
extern void DataReceiver_get_HandleControls_m595346F521CC5FCC4344F533236C71B7A62779AF (void);
// 0x000000A8 System.Void Assets.Scripts.DataReceiver::set_HandleControls(HandleControls)
extern void DataReceiver_set_HandleControls_m27CAAE88A37497E7548A932195A17E16BAB378E3 (void);
// 0x000000A9 UpdateObject Assets.Scripts.DataReceiver::get_UpdateObject()
extern void DataReceiver_get_UpdateObject_mF84254800A0BB07B22B463253691681036A4453E (void);
// 0x000000AA System.Void Assets.Scripts.DataReceiver::set_UpdateObject(UpdateObject)
extern void DataReceiver_set_UpdateObject_m7680135EE0CD267B72B834D8E4D33EF1C0666299 (void);
// 0x000000AB System.Void Assets.Scripts.DataReceiver::HandleWelcomeMsg(System.Byte[])
extern void DataReceiver_HandleWelcomeMsg_m4A95B90A3D51EAD38B38229ABA3565BE25A57675 (void);
// 0x000000AC System.Void Assets.Scripts.DataReceiver::HandleRemoveControls(System.Byte[])
extern void DataReceiver_HandleRemoveControls_mF307DDD4FA48DC859979E4500CB5FB1CB8527AE3 (void);
// 0x000000AD System.Void Assets.Scripts.DataReceiver::HandleRestoreControls(System.Byte[])
extern void DataReceiver_HandleRestoreControls_m3611AC1825A330FA0C4BF9FC63F1267BC84060B8 (void);
// 0x000000AE System.Void Assets.Scripts.DataReceiver::HandleUpdatePosition(System.Byte[])
extern void DataReceiver_HandleUpdatePosition_m717DEF8D7FC742A194DDB972D1490231D6DF9CAE (void);
// 0x000000AF System.Void Assets.Scripts.DataReceiver::HandleUpdateRotation(System.Byte[])
extern void DataReceiver_HandleUpdateRotation_m955FDAE07A0EC55A8C0053963B85368FAB043CEF (void);
// 0x000000B0 System.Void Assets.Scripts.DataReceiver::HandleUpdateScale(System.Byte[])
extern void DataReceiver_HandleUpdateScale_m58842B4C3F18683FD447E8F8896BF000794C5FFC (void);
// 0x000000B1 System.Void Assets.Scripts.DataReceiver::HandleUpdateEulerAngle(System.Byte[])
extern void DataReceiver_HandleUpdateEulerAngle_m97882A134BDEFF20CB5C2D7C607C03976BE8CF2C (void);
// 0x000000B2 UnityEngine.Vector3 Assets.Scripts.DataReceiver/Transform::get_position()
extern void Transform_get_position_m40493E2832EDF3E0F5547B0F7F7CC9405B9B6FAE (void);
// 0x000000B3 System.Void Assets.Scripts.DataReceiver/Transform::set_position(UnityEngine.Vector3)
extern void Transform_set_position_m07E6CE9DEB2487A514DF2AD066B0EDD9DD42B4D7 (void);
// 0x000000B4 UnityEngine.Quaternion Assets.Scripts.DataReceiver/Transform::get_rotation()
extern void Transform_get_rotation_m8DF4F966A0A24A06365A04DDC09A8BCE30861A65 (void);
// 0x000000B5 System.Void Assets.Scripts.DataReceiver/Transform::set_rotation(UnityEngine.Quaternion)
extern void Transform_set_rotation_m27324098C175CF997D9742275BCFFD37FFBA3721 (void);
// 0x000000B6 UnityEngine.Vector3 Assets.Scripts.DataReceiver/Transform::get_scale()
extern void Transform_get_scale_mCF603B5B95DF42B1BE15C5BE9DDBEC366C2F6F5B (void);
// 0x000000B7 System.Void Assets.Scripts.DataReceiver/Transform::set_scale(UnityEngine.Vector3)
extern void Transform_set_scale_mDB9EFF14A21301D90E2DCB2369529046704E4FB2 (void);
// 0x000000B8 System.Void Assets.Scripts.DataReceiver/Transform::.ctor()
extern void Transform__ctor_m06BA9E38690E2875A4F20582C71C3D358891198B (void);
// 0x000000B9 System.Void Assets.Scripts.DataSender::SendHelloServer()
extern void DataSender_SendHelloServer_m6D38FB3A9F8673AF8ADF26B63CCD2EC7D908B7D5 (void);
// 0x000000BA System.Void Assets.Scripts.DataSender::SendPressButton()
extern void DataSender_SendPressButton_mE6C46F026EC70CB4C552F156E3AA1DF311B535FF (void);
// 0x000000BB System.Void Assets.Scripts.DataSender::SendRotation(System.String)
extern void DataSender_SendRotation_m4AC4CD6D05B142F4487147F761B3FEE41379EC4D (void);
// 0x000000BC System.Void Assets.Scripts.DataSender::SendScale(System.String)
extern void DataSender_SendScale_mD4329BC60E261BE3AF9B62AB59B2CE453603C787 (void);
// 0x000000BD System.Void Assets.Scripts.DataSender::SendPosition(System.String)
extern void DataSender_SendPosition_m0664D4AD31088592E6A30AEAE6461F20518081C9 (void);
// 0x000000BE System.Void Assets.Scripts.DataSender::SendEulerAngle(System.String)
extern void DataSender_SendEulerAngle_m31732818B5B51B09911D58269160D49A515DEE39 (void);
// 0x000000BF System.Void Assets.Scripts.DataSender::RequestControl(System.String)
extern void DataSender_RequestControl_mF73E6596A198F1848577350F257BB72FC725DC9E (void);
// 0x000000C0 System.Void Assets.Scripts.DataSender::ReleseControl()
extern void DataSender_ReleseControl_mE5F8C7DFB3F6DE92030ECA8A15D7B8BF05C58DCB (void);
// 0x000000C1 System.Void Assets.Externals.ByteBuffer::.ctor()
extern void ByteBuffer__ctor_mEA0AC4F1C6E5E19ECAF6665379978C238A85AFAF (void);
// 0x000000C2 System.Int32 Assets.Externals.ByteBuffer::GetReadPosition()
extern void ByteBuffer_GetReadPosition_m966F42A2617F5FAA919B759E95FC328BF04C9FFE (void);
// 0x000000C3 System.Byte[] Assets.Externals.ByteBuffer::ToArray()
extern void ByteBuffer_ToArray_m74D771A8270D9567AA28C156EB74D088CCC5062A (void);
// 0x000000C4 System.Int32 Assets.Externals.ByteBuffer::Count()
extern void ByteBuffer_Count_m4547A36FD8DE966F20FFF941EBF2ABE881503C89 (void);
// 0x000000C5 System.Int32 Assets.Externals.ByteBuffer::Lenght()
extern void ByteBuffer_Lenght_m7DA18368F392B7DB8CB30ED37663B83CB7B35C2B (void);
// 0x000000C6 System.Void Assets.Externals.ByteBuffer::Clear()
extern void ByteBuffer_Clear_m0ACE6A874541EFCA14EA5FE14D203FCB8BF5AC11 (void);
// 0x000000C7 System.Void Assets.Externals.ByteBuffer::WriteByte(System.Byte)
extern void ByteBuffer_WriteByte_m82546EFFE3CA0DF19111FA4264B53B08A9767FBF (void);
// 0x000000C8 System.Void Assets.Externals.ByteBuffer::WriteBytes(System.Byte[])
extern void ByteBuffer_WriteBytes_m584F0CCE011E85D7200C04CFF8B4038F2B002D2D (void);
// 0x000000C9 System.Void Assets.Externals.ByteBuffer::WriteShort(System.Int16)
extern void ByteBuffer_WriteShort_m77D6F343E46F40CA9AA19D0011CA21C9AA46C336 (void);
// 0x000000CA System.Void Assets.Externals.ByteBuffer::WriteInt(System.Int32)
extern void ByteBuffer_WriteInt_mCEA3E2256412BDE8FD2C92CCAE3769588B20A60A (void);
// 0x000000CB System.Void Assets.Externals.ByteBuffer::WriteLong(System.Int64)
extern void ByteBuffer_WriteLong_mC6E2C288F4C670E372756D3A085FC2BCCDDE1AF7 (void);
// 0x000000CC System.Void Assets.Externals.ByteBuffer::WriteFloat(System.Single)
extern void ByteBuffer_WriteFloat_m7485C04B642C2C560DB8B3722B81EA6FF1E7DC10 (void);
// 0x000000CD System.Void Assets.Externals.ByteBuffer::WriteBool(System.Boolean)
extern void ByteBuffer_WriteBool_mEF97341B15738960AE261FAED74F043E7C2FEDA3 (void);
// 0x000000CE System.Void Assets.Externals.ByteBuffer::WriteString(System.String)
extern void ByteBuffer_WriteString_m08DBF3914213E9593E7B57E0A6AEE2FC3FE36E40 (void);
// 0x000000CF System.Byte Assets.Externals.ByteBuffer::ReadByte(System.Boolean)
extern void ByteBuffer_ReadByte_mA428978AEC6C303834D986C4081B0F7CF759796C (void);
// 0x000000D0 System.Byte[] Assets.Externals.ByteBuffer::ReadBytes(System.Int32,System.Boolean)
extern void ByteBuffer_ReadBytes_m73298B91F94EF5CBF1104106A3C8379733B61730 (void);
// 0x000000D1 System.Int16 Assets.Externals.ByteBuffer::ReadShort(System.Boolean)
extern void ByteBuffer_ReadShort_m1E1B1D965226869B57275BF6CD734B59A31A8768 (void);
// 0x000000D2 System.Int32 Assets.Externals.ByteBuffer::ReadInt(System.Boolean)
extern void ByteBuffer_ReadInt_m1B94932BE3C0264C6FDBDBEA2712A8A133CE550D (void);
// 0x000000D3 System.Int64 Assets.Externals.ByteBuffer::ReadLong(System.Boolean)
extern void ByteBuffer_ReadLong_m747EAEAE6D1BE5478FCA8895753B404D8314B648 (void);
// 0x000000D4 System.Single Assets.Externals.ByteBuffer::ReadFloat(System.Boolean)
extern void ByteBuffer_ReadFloat_mC9A6FD6FDE0785F86C4823976AB978043D500A48 (void);
// 0x000000D5 System.Boolean Assets.Externals.ByteBuffer::ReadBool(System.Boolean)
extern void ByteBuffer_ReadBool_m7DC1F34FAE2F8DCC0394F4A844C47FDB5D10A45D (void);
// 0x000000D6 System.String Assets.Externals.ByteBuffer::ReadString(System.Boolean)
extern void ByteBuffer_ReadString_m18110F4BC4FC2180790F353C37C5FEA873460F8D (void);
// 0x000000D7 System.Void Assets.Externals.ByteBuffer::Dispose(System.Boolean)
extern void ByteBuffer_Dispose_m5AD4C9077E4A7AA25452B3158494875A80D9C705 (void);
// 0x000000D8 System.Void Assets.Externals.ByteBuffer::Dispose()
extern void ByteBuffer_Dispose_m08128BC6E4CDB7B29293CB16C1157287F5E2B5BB (void);
// 0x000000D9 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[217] = 
{
	UnityThread_initUnityThread_m7E1BB70A89DB1C3B71BA23F5486E2DB4ED78FFC6,
	UnityThread_Awake_m929F7BC8C4524B19FD779D6CA21D8EF8E06772E0,
	UnityThread_executeCoroutine_mC5F0503C5441FC8618E3F702BCB5FA9C01BBE5C8,
	UnityThread_executeInUpdate_mA5C09375FFF47F6B68D2FEB5DB550FF0D1780E22,
	UnityThread_Update_m4B2985C59168E230793AC0390462B694A949213D,
	UnityThread_executeInLateUpdate_mB2EC436C13983185E80920E7E8F00355B0987450,
	UnityThread_LateUpdate_m3D56F4EC0E45FC83EE2472B74A4C2C2E4FCD643E,
	UnityThread_executeInFixedUpdate_mFACE16996AAA19E7ADDAB8FA8CA18B56684E1449,
	UnityThread_FixedUpdate_m899EB37D52E22DFC8D7A25517005EA47CA75F789,
	UnityThread_OnDisable_m6D06210991C8967233FEC58E1B4C6A1E1F5506B2,
	UnityThread__ctor_mB026097B94171032E79AC8414172E7D573BC7B34,
	UnityThread__cctor_m5062DAB784BFA1BC30459D64C0BD0D473F96E6DC,
	U3CU3Ec__DisplayClass12_0__ctor_mCA38F8709513598A4839C7108CA09166ECAF0EEF,
	U3CU3Ec__DisplayClass12_0_U3CexecuteCoroutineU3Eb__0_mC54ED731AE66D35AD5252B2B7B71DE0EBCE66FEE,
	FixPositionTarget_Update_mEA401A53F3D988802A8FBA7C322411CAE70E3BC4,
	FixPositionTarget__ctor_m5EEEDC47A4DCB88F5DD03FA9546B572952B6D960,
	HandleControls_Start_mD17E2ACCFD9C8CD3B5F2E183FC4D41752E62B7E0,
	HandleControls_RemoveControlsComponent_m2582EAFF05010344B6C63D42C0F30B2ED3443B1F,
	HandleControls_RestoreControlsComponent_mCAABEDF05204AE6CCD220983DE25A4A44B512B83,
	HandleControls__ctor_mFB9F27C767688D4C3209FCE65734160CEBDF8ABA,
	NetworkManager_Awake_m264F69270EE58EA94BD639B7CFC5DBB6CEAD7E2F,
	NetworkManager_Start_mF62F7D5D531194AFEF4C3FFAC8BFE07E10F72365,
	NetworkManager_OnApplicationQuit_m2DB9568B0EA3F3129AF50C5E56400CC4B4CF1C37,
	NetworkManager_ConnectClient_m8765E65D21C6C4A7852EF60BECAA2F632489322A,
	NetworkManager__ctor_m1C3DBB165C04540C090C4D74FEDBD83DEFC5C62F,
	ReadInput_Readtext_m0F3DFBA189C187F1A9C09FCBFD628A50E1FF6B25,
	ReadInput__ctor_m7F5E5A65F56506205D2044487E9BB7E69FB8947E,
	UpdateObject_get_State_m9E51E530AE3A97069DA74E8254F06CA2AEC8A185,
	UpdateObject_set_State_m859D0FDD24D20DAEE41F6F987F23A9DD366B3E57,
	UpdateObject_get_MyColor_mA2D987AF363580B88ED776CD16D9E045DA0D93AD,
	UpdateObject_set_MyColor_m51FD70A35B5A665D9F807E1DB4469225564AB4B2,
	UpdateObject_RequestControl_m7062D67D3FAD517F0293AEDED06B53202E58364A,
	UpdateObject_ReleseControl_m5E36D3E935544E473F906B0E4E7F22DA1304E1C0,
	UpdateObject_Start_m8DCC5CCEE199EBFDB14FB08B301715C0B897A54F,
	UpdateObject_Update_mB5AA7D9EF03C148F94F97412C5782D09C8F71F09,
	UpdateObject_DelayTime_mC09895EC20B1B4716E4631F2584CC91BE343C29D,
	UpdateObject_SendRotation_mF6C4959D15A0FDEBDEA48C975CF798F64C487210,
	UpdateObject_SendScale_mE45BEB7372336DC4E36D2D6CDE5BDA44A45C63DD,
	UpdateObject_SendPosition_m9E0558ABAF1B7BEFA92C87BAF735AFE650011621,
	UpdateObject_UpdatePosition_m9CF760E40796CE98F38103DCFAB1D246D765A588,
	UpdateObject_UpdateRotation_m18BE3270C6BDDBA7660BC3B48078372389CC84C6,
	UpdateObject_UpdateScale_mFC4C97A0500193EA5C1C633818DDF619EAA40750,
	UpdateObject_UpdateEulerAngle_mD49E5EACD2CD65B67FC25BF36BF9624FC5E0D2FD,
	UpdateObject__ctor_m4DAA3DF1D7076362B414297675D3056346661847,
	U3CDelayTimeU3Ed__14__ctor_m6EE898869F89AF142FA27B9F00BEDEE8756829C3,
	U3CDelayTimeU3Ed__14_System_IDisposable_Dispose_mC27B5775D488708A9796CB96912372B2962C14CD,
	U3CDelayTimeU3Ed__14_MoveNext_m09AC91B981B2EF73CC2F7C29C2CAC727621ECA13,
	U3CDelayTimeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m579ABD90A41F73673715242D3B4CF16AE0F02E20,
	U3CDelayTimeU3Ed__14_System_Collections_IEnumerator_Reset_mA917D5877B3E2F40D649757641CCB56AC5E86D85,
	U3CDelayTimeU3Ed__14_System_Collections_IEnumerator_get_Current_mFDACDEDD7600C9D80FF8ED0E6D6108B12D7C4AFE,
	U3CDelayTimeU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m8B4CF67FC53EC9A4359FBA6FE62D0F68D05E63E2,
	U3CDelayTimeU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mBE023A7E1C6FE13F4B68FB946E49801199622E75,
	BoundBox_Reset_m59620396D5B366579DB69DB412253510D9FFE064,
	BoundBox_Start_m262BD0F03D60CACDA3F72F256FC6B5D3B6DC063D,
	BoundBox_init_m798D364FC16E94832858F65FF704F202CACC3F4C,
	BoundBox_LateUpdate_m5FC7F74CD60A10D7179394987CC90C2CB5713820,
	BoundBox_calculateBounds_m7A23D83CB7837B3EEAF4C797200E96906EC84A31,
	BoundBox_SetPoints_m351DF77B6A2819402BAECD55EFB31F398A934A23,
	BoundBox_SetLines_m0802731CCA33C4E5D61B0D5EF26B87B88338B170,
	BoundBox_SetLineRenderers_m7B4AE7ACFD716DDB4CE4DDC727CDECA6D9D325E2,
	BoundBox_OnEnable_m1149BDBA317B257993E0C3AECB5040C60FCEAA09,
	BoundBox_OnDestroy_mB9A0DF21534607D190E80A45D13D2D6F9711CAE8,
	BoundBox_OnDisable_mA29F947BB9AF78670E9A9BD2C820290270B306FC,
	BoundBox_OnDrawGizmos_mB0CA1303CF0F98BEDB514772ADBFD2AC52A90E5D,
	BoundBox_OnRenderObject_mD7CE4E00151496250A8E0684A07EE1B5CC468A5A,
	BoundBox__ctor_m640CFADC57BCE58550D0CB034B3F93110134B987,
	BoundBoxExample_Start_m51800006EEDD4657B74C9569CAE3D7E31240FE6F,
	BoundBoxExample_EnableLines_m1D726FF8C1C29BD4D5A416FF8A8AF7EA785F769B,
	BoundBoxExample_EnableWires_m2A5EE3102F99C86675C5E56AC55512D69FAB5091,
	BoundBoxExample_SetLineWidth_m37490F94463FD600D4777CDB8252B9F8494E9E62,
	BoundBoxExample_SetNumCapVertices_m68B2CEA3D624FFC8F4F6064AC5F064C4CF4C9DDA,
	BoundBoxExample__ctor_mC12A3F18C2C9A098FB5C9FB0DED68E3B4FF7892D,
	DrawLines_OnEnable_m0B44FD76EA205DFB25BD2CB8DD72323CC44C0BB8,
	DrawLines_Start_m95AEEF8FB01DFE3D7CAD0842593918B85E5376C6,
	DrawLines_OnRenderObject_mA3B5D37740D2BA4FB3ED2D944FA1E659A2883F7D,
	DrawLines_setOutlines_m0BC4634935A4C554F708D9415AD918C54A1D74E6,
	DrawLines_setScreenOutlines_m6A091BB51C8F17852035AC1F02198799BF012EF1,
	DrawLines_setOutlines_mB76E33D697E057FD8407720B31E7310E4632791F,
	DrawLines_Update_m09852A44FC777E54F5B5A8AE5E158AA10E919F64,
	DrawLines__ctor_mF14A63A634356A725E932D0C6E8F463F8BCC5388,
	TestDynamicBehaviour_Start_m9B8EA71B705689F8A2405893F7B1C4858FB35161,
	TestDynamicBehaviour_Update_m295D2DC22D5AEC5CCA7547BA9B35FD291ABA9047,
	TestDynamicBehaviour_Test_mB8EFB4AE022466876C693427F2171E89367741BA,
	TestDynamicBehaviour__ctor_m93F8AE7A088F923ED29DA257748123D64B828829,
	maxCamera_Start_mB12ECE1DE6A1E942E2A9E91315405E5951A8C988,
	maxCamera_OnEnable_m5778008067723C05AA2DA74092B076094302BA36,
	maxCamera_Init_mF8E96F567C89D554273382B8C08DC30ECA5C7F77,
	maxCamera_LateUpdate_mDA3F2CE587BC04153AA1671933FB730BD61A83AE,
	maxCamera_ClampAngle_m3F2158E3F4CEA37191FACF602CCDEF3BC9482A93,
	maxCamera_Update_m1526E853678AB25BD1FFABF33AA4ABCE3DF015F2,
	maxCamera_DragObject_mAD1346C04E5516343C509A9E10FC7EA7D43F3843,
	maxCamera_SetRadius_m66ABFE466ACB0274E934CD7DC14EF946C390596D,
	maxCamera__ctor_m23D16D546D96F3F270A765F070E91CEAEF76A958,
	U3CDragObjectU3Ed__34__ctor_mD6CE69B1B7E77F218A9B9CA5933CA4508739CABB,
	U3CDragObjectU3Ed__34_System_IDisposable_Dispose_mCBF41367AE39B0051050F1CD9D623591604377FE,
	U3CDragObjectU3Ed__34_MoveNext_mD90FA51BD5E05C90C8257E9102AF2000B6CCF4B1,
	U3CDragObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE977501C9712C138E0BDD0EB3A972F2923ECDBB2,
	U3CDragObjectU3Ed__34_System_Collections_IEnumerator_Reset_mBA9759EC27FEF954670F924101C567393D0774B3,
	U3CDragObjectU3Ed__34_System_Collections_IEnumerator_get_Current_mAD36677BBE6ABC6940F1028D3D7A4DEF0C0944E1,
	CameraCache_get_Main_m620E8E7552448CD482C4ED03B0F6AC6B154FD9B4,
	CameraCache_Refresh_m48F440049E292F53B87295A7B7C6D804B23B1ADC,
	Interpolator_get_AnimatingPosition_m57E7ABC9036E6EDCB9F762CFE1E3EF617A1A793B,
	Interpolator_set_AnimatingPosition_m4A723496EB756442FD5E6E410BC2B5AD0FA36DFB,
	Interpolator_get_AnimatingRotation_m7201936CBDD7BD6ACB01678115235B193C28BF92,
	Interpolator_set_AnimatingRotation_m6EDE5A4B35E1C3551D12F39B5857FFAEB5521687,
	Interpolator_get_AnimatingLocalRotation_m339D88A11EF303C2FEF3FC7D57392C2247CF5C70,
	Interpolator_set_AnimatingLocalRotation_m6DC1C0CF5B9CC197CC1D0823EC9590C1A70B155C,
	Interpolator_get_AnimatingLocalScale_mCC228EEE443EA8A49FD326CF166BA62466824690,
	Interpolator_set_AnimatingLocalScale_m70BF55824BE1B623A46989FB792786B9A09AA9C4,
	Interpolator_add_InterpolationStarted_mBC37270CD5F2FB61B6999A46B012939BA64703C6,
	Interpolator_remove_InterpolationStarted_m6DF682B30A92A129162A3E32EF3F27C9F78F6035,
	Interpolator_add_InterpolationDone_m62475E1E1DD151BEFA8C3E8C57099E3E15DDAB3B,
	Interpolator_remove_InterpolationDone_m0FE2F49EFACC7657BF14D8A74E724E648BB8E547,
	Interpolator_get_PositionVelocity_mCAE50CC2B5436F38EF71239C56F7BD75E29F7BEA,
	Interpolator_set_PositionVelocity_m64822951DFFC2200006FF5B33F051EDE4B593B04,
	Interpolator_get_Running_m66F6F94D33D4BBA1AA6D19CA2BC784732A72CC61,
	Interpolator_Awake_mB2FDB7B4E28B653C1541356A7675E9D2B1416E6E,
	Interpolator_SetTargetPosition_m0B337E11828E5772B1F94C4B95E4A51840A67EA6,
	Interpolator_SetTargetRotation_m2FBE94B30CC463C2405E9A04EADA2A2C16C10FE4,
	Interpolator_SetTargetLocalRotation_m4A1B55D72DE21716F52E939358909F4FA431BA36,
	Interpolator_SetTargetLocalScale_m7033FDE05BD75726A7CB4510D288E26680DA4BFB,
	Interpolator_NonLinearInterpolateTo_mEE47DD2EF658372780E851A7BFF1118D1F030F13,
	Interpolator_Update_m04C79AC166B3106802DF5B6A057D00DB9629B598,
	Interpolator_SnapToTarget_m1ABC78F74B6C1D591BD7ADB4381C1EDD5AE7DDF3,
	Interpolator_StopInterpolating_m59FD1BC368CCFB6E6479B63471352D04B38D0642,
	Interpolator_Reset_m9D5B6BBEE328F507675D160D2CC65089CCF2D261,
	Interpolator_get_TargetPosition_mE639B9EFF6E0379A1E172EE48050A277C27490F4,
	Interpolator_get_TargetRotation_mF75D9EAE3BEEB6BDDD8085473B22C8A98F50FD68,
	Interpolator_get_TargetLocalRotation_m47E6C7D899FC52E30A5609551CC83BCF738E5EE3,
	Interpolator_get_TargetLocalScale_mA435EAB66DA550E87BF7468383BF9451E58B05B7,
	Interpolator__ctor_m388BF47AF9F4C0A42978B483F58E4EF8F81662B1,
	Billboard_OnEnable_m7B6C746E1ABD853BF71C684366646011643368D8,
	Billboard_Update_m266806D96B009FC8D2B890C15105C385D17CE14F,
	Billboard__ctor_mB94741F3C3E18B5C4A70CA1087374C00C882D2B8,
	FixedAngularSize_Start_mE293E8B99411BCD16091F1809D38EA0C8BA49309,
	FixedAngularSize_SetSizeRatio_m2F18BEBAB64630AED4291EC1A65BB74AFD4DE590,
	FixedAngularSize_Update_mEF299CCF4E1D7C27971217CE409CBBF3E6EEB9E3,
	FixedAngularSize__ctor_m5DEDCBF834FEB6660D06B733384F23C030698E3D,
	SimpleTagalong_Start_mD110391CF168157D7B9A596E6662C0A9692DA069,
	SimpleTagalong_Update_m1AFEE64B5F908011173AF460E466AD9F278869F9,
	SimpleTagalong_CalculateTagalongTargetPosition_m221BE5079FC9D662126479D5CB2E5B44A81F77EE,
	SimpleTagalong__ctor_mC344395CA376A23C69E2548A0AD5EBC1C34AD421,
	Tagalong_Start_mBEC0FC64FAC9C26FC13977A38D4F0F0732F7C1DF,
	Tagalong_Update_m883BB1DFDB965A7D5833F332AFB5DD8524C10D1B,
	Tagalong_CalculateTagalongTargetPosition_mD9A2A6F6221EAF8134BF083A64812B103A06F489,
	Tagalong_CalculateTargetPosition_mCBC20CD385370474672B54CED9FE60ABBF70485C,
	Tagalong_AdjustTagalongDistance_m25DE534C629FC6DA5B4DB483551F2BB77E6F70C3,
	Tagalong__ctor_m245759F2DFDA3AA84EE9610AF4166E534F7288A7,
	ClientHandleData_InitializePackets_m28FC5FDEB3C5E36845741741B9E2F075FB478EAE,
	ClientHandleData_HandleData_m868832BA9260602AAA0AC8744E3B6B980D1A70A7,
	ClientHandleData_HandleDataPackets_mB6A8CC336A7E8C1650C74A0347E8BA788654AF52,
	ClientHandleData__ctor_m8D74E80620A9C52220B8E0C6EB606A654B13994B,
	ClientHandleData__cctor_mD5F1EF5FDDB45404301E69522F19C946E8CB4FB6,
	Packet__ctor_mBF7634DA169E58964FB3A95B1AD9C8F9B7D8997E,
	Packet_Invoke_mED57B86349F0C45C5E44B44E6680734CF5CE2608,
	Packet_BeginInvoke_m5D8C0A6FDFED8E999B5DB1CFAD37925B218466E6,
	Packet_EndInvoke_mB2145753879BE0FDFFCFFBA9100F32D542623F7E,
	ClientTCP_get_ServerAddress_m0A08D35116AAB4B55B9B311076BF028C0A5BDB24,
	ClientTCP_set_ServerAddress_m03BF542E000365B50334312EB43169DDC88998FF,
	ClientTCP_InitializingNetworking_m47A51567FCDC3545C79A8851962E399180474447,
	ClientTCP_ClientConnectCallback_m0DB03784A1A471BC9CEA69B0E624C79D2E6F1BF9,
	ClientTCP_ReciveCallBack_m2C03C241C7D82516490B3E258120A4781A5955BB,
	ClientTCP_SendData_m26FCD379C3855A2202C2571DFB2D3BA93F40CE42,
	ClientTCP_Disconnect_m8959BD3114AD40E5D917D708ECE07276ED183C27,
	U3CU3Ec__DisplayClass9_0__ctor_m8CE1B2274692E61F2D9B5765446DB7C05A9D61EA,
	U3CU3Ec__DisplayClass9_0_U3CReciveCallBackU3Eb__0_m41A34ACD7A2BC99625BCD06162148E3E482B5A2C,
	DataReceiver_get_HandleControls_m595346F521CC5FCC4344F533236C71B7A62779AF,
	DataReceiver_set_HandleControls_m27CAAE88A37497E7548A932195A17E16BAB378E3,
	DataReceiver_get_UpdateObject_mF84254800A0BB07B22B463253691681036A4453E,
	DataReceiver_set_UpdateObject_m7680135EE0CD267B72B834D8E4D33EF1C0666299,
	DataReceiver_HandleWelcomeMsg_m4A95B90A3D51EAD38B38229ABA3565BE25A57675,
	DataReceiver_HandleRemoveControls_mF307DDD4FA48DC859979E4500CB5FB1CB8527AE3,
	DataReceiver_HandleRestoreControls_m3611AC1825A330FA0C4BF9FC63F1267BC84060B8,
	DataReceiver_HandleUpdatePosition_m717DEF8D7FC742A194DDB972D1490231D6DF9CAE,
	DataReceiver_HandleUpdateRotation_m955FDAE07A0EC55A8C0053963B85368FAB043CEF,
	DataReceiver_HandleUpdateScale_m58842B4C3F18683FD447E8F8896BF000794C5FFC,
	DataReceiver_HandleUpdateEulerAngle_m97882A134BDEFF20CB5C2D7C607C03976BE8CF2C,
	Transform_get_position_m40493E2832EDF3E0F5547B0F7F7CC9405B9B6FAE,
	Transform_set_position_m07E6CE9DEB2487A514DF2AD066B0EDD9DD42B4D7,
	Transform_get_rotation_m8DF4F966A0A24A06365A04DDC09A8BCE30861A65,
	Transform_set_rotation_m27324098C175CF997D9742275BCFFD37FFBA3721,
	Transform_get_scale_mCF603B5B95DF42B1BE15C5BE9DDBEC366C2F6F5B,
	Transform_set_scale_mDB9EFF14A21301D90E2DCB2369529046704E4FB2,
	Transform__ctor_m06BA9E38690E2875A4F20582C71C3D358891198B,
	DataSender_SendHelloServer_m6D38FB3A9F8673AF8ADF26B63CCD2EC7D908B7D5,
	DataSender_SendPressButton_mE6C46F026EC70CB4C552F156E3AA1DF311B535FF,
	DataSender_SendRotation_m4AC4CD6D05B142F4487147F761B3FEE41379EC4D,
	DataSender_SendScale_mD4329BC60E261BE3AF9B62AB59B2CE453603C787,
	DataSender_SendPosition_m0664D4AD31088592E6A30AEAE6461F20518081C9,
	DataSender_SendEulerAngle_m31732818B5B51B09911D58269160D49A515DEE39,
	DataSender_RequestControl_mF73E6596A198F1848577350F257BB72FC725DC9E,
	DataSender_ReleseControl_mE5F8C7DFB3F6DE92030ECA8A15D7B8BF05C58DCB,
	ByteBuffer__ctor_mEA0AC4F1C6E5E19ECAF6665379978C238A85AFAF,
	ByteBuffer_GetReadPosition_m966F42A2617F5FAA919B759E95FC328BF04C9FFE,
	ByteBuffer_ToArray_m74D771A8270D9567AA28C156EB74D088CCC5062A,
	ByteBuffer_Count_m4547A36FD8DE966F20FFF941EBF2ABE881503C89,
	ByteBuffer_Lenght_m7DA18368F392B7DB8CB30ED37663B83CB7B35C2B,
	ByteBuffer_Clear_m0ACE6A874541EFCA14EA5FE14D203FCB8BF5AC11,
	ByteBuffer_WriteByte_m82546EFFE3CA0DF19111FA4264B53B08A9767FBF,
	ByteBuffer_WriteBytes_m584F0CCE011E85D7200C04CFF8B4038F2B002D2D,
	ByteBuffer_WriteShort_m77D6F343E46F40CA9AA19D0011CA21C9AA46C336,
	ByteBuffer_WriteInt_mCEA3E2256412BDE8FD2C92CCAE3769588B20A60A,
	ByteBuffer_WriteLong_mC6E2C288F4C670E372756D3A085FC2BCCDDE1AF7,
	ByteBuffer_WriteFloat_m7485C04B642C2C560DB8B3722B81EA6FF1E7DC10,
	ByteBuffer_WriteBool_mEF97341B15738960AE261FAED74F043E7C2FEDA3,
	ByteBuffer_WriteString_m08DBF3914213E9593E7B57E0A6AEE2FC3FE36E40,
	ByteBuffer_ReadByte_mA428978AEC6C303834D986C4081B0F7CF759796C,
	ByteBuffer_ReadBytes_m73298B91F94EF5CBF1104106A3C8379733B61730,
	ByteBuffer_ReadShort_m1E1B1D965226869B57275BF6CD734B59A31A8768,
	ByteBuffer_ReadInt_m1B94932BE3C0264C6FDBDBEA2712A8A133CE550D,
	ByteBuffer_ReadLong_m747EAEAE6D1BE5478FCA8895753B404D8314B648,
	ByteBuffer_ReadFloat_mC9A6FD6FDE0785F86C4823976AB978043D500A48,
	ByteBuffer_ReadBool_m7DC1F34FAE2F8DCC0394F4A844C47FDB5D10A45D,
	ByteBuffer_ReadString_m18110F4BC4FC2180790F353C37C5FEA873460F8D,
	ByteBuffer_Dispose_m5AD4C9077E4A7AA25452B3158494875A80D9C705,
	ByteBuffer_Dispose_m08128BC6E4CDB7B29293CB16C1157287F5E2B5BB,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
static const int32_t s_InvokerIndices[217] = 
{
	6221,
	4406,
	6218,
	6218,
	4406,
	6218,
	4406,
	6218,
	4406,
	4406,
	4406,
	6303,
	4406,
	4406,
	4406,
	4406,
	4406,
	3546,
	4406,
	4406,
	4406,
	4406,
	4406,
	3635,
	4406,
	4406,
	4406,
	4295,
	3604,
	4239,
	3546,
	3604,
	4406,
	4406,
	4406,
	2851,
	4406,
	4406,
	4406,
	3706,
	3650,
	3706,
	3706,
	4406,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	4326,
	4326,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	3670,
	3670,
	3635,
	3635,
	4406,
	4406,
	4406,
	4406,
	2160,
	2160,
	1214,
	4406,
	4406,
	4406,
	4406,
	3670,
	4406,
	4406,
	4406,
	4406,
	4406,
	5401,
	4406,
	2854,
	3676,
	4406,
	3604,
	4406,
	4364,
	4326,
	4406,
	4326,
	6279,
	6106,
	4364,
	3670,
	4364,
	3670,
	4364,
	3670,
	4364,
	3670,
	3635,
	3635,
	3635,
	3635,
	4401,
	3706,
	4364,
	4406,
	3706,
	3650,
	3650,
	3706,
	5132,
	4406,
	4406,
	4406,
	4406,
	4401,
	4339,
	4339,
	4401,
	4406,
	4406,
	4406,
	4406,
	4406,
	3676,
	4406,
	4406,
	4406,
	4406,
	1750,
	4406,
	4406,
	4406,
	1750,
	95,
	1750,
	4406,
	6303,
	6218,
	6218,
	4406,
	6303,
	2169,
	3635,
	1003,
	3635,
	6279,
	6218,
	6303,
	6218,
	6218,
	6218,
	6303,
	4406,
	4406,
	6279,
	6218,
	6279,
	6218,
	6218,
	6218,
	6218,
	6218,
	6218,
	6218,
	6218,
	4401,
	3706,
	4339,
	3650,
	4401,
	3706,
	4406,
	6303,
	6303,
	6218,
	6218,
	6218,
	6218,
	6218,
	6303,
	4406,
	4295,
	4326,
	4295,
	4295,
	4406,
	3670,
	3635,
	3603,
	3604,
	3605,
	3676,
	3670,
	3635,
	3211,
	1516,
	2467,
	2682,
	2782,
	3326,
	3211,
	2850,
	3670,
	4406,
	6017,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	217,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
