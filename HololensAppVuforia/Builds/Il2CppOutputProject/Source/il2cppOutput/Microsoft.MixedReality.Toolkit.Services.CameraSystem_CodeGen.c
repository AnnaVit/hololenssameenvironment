﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void MixedRealityCameraSystem__ctor_m38911A4E6D216CBEE883148FF107C0BE5628FA9F (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.ctor(Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void MixedRealityCameraSystem__ctor_m738D26A0C117709E9D949ADB65A120EF896AE958 (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_Name()
extern void MixedRealityCameraSystem_get_Name_mC2954562ACB4A76F8705B242D421AE44DA5C360F (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::set_Name(System.String)
extern void MixedRealityCameraSystem_set_Name_mC26BE764FB71BB73948948D1E1DAE20A30D2E048 (void);
// 0x00000005 System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_IsOpaque()
extern void MixedRealityCameraSystem_get_IsOpaque_mB5EC943246C46C6919E5EADAF2AC621113D01EC7 (void);
// 0x00000006 System.UInt32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceId()
extern void MixedRealityCameraSystem_get_SourceId_m0C6BBD1991142D53FD7CCB7A8EACA528AE436E67 (void);
// 0x00000007 System.String Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceName()
extern void MixedRealityCameraSystem_get_SourceName_m7072E6335FB43C3284F353393AD0DDBD2FE9D93D (void);
// 0x00000008 Microsoft.MixedReality.Toolkit.MixedRealityCameraProfile Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_CameraProfile()
extern void MixedRealityCameraSystem_get_CameraProfile_mAC2EAB143440EE56B0E3F4D036643C68D1004D8E (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Initialize()
extern void MixedRealityCameraSystem_Initialize_m2C3BBFD152EA6337AF4AA2BA2478FF4D851D9E5D (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Enable()
extern void MixedRealityCameraSystem_Enable_m0FFD02C445B78D45AD037B4D0C9002A460D9272C (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Disable()
extern void MixedRealityCameraSystem_Disable_mB57506E4D8013ECBCE7563F1BEF80C63F3BB8C3F (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Destroy()
extern void MixedRealityCameraSystem_Destroy_m839A10E59F28BD3D39E6B27727F262533D0FF487 (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Update()
extern void MixedRealityCameraSystem_Update_mE8FA51D217EE6AAA139A5B71920B79129AC11704 (void);
// 0x0000000E System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForOpaqueDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_m2A7FAAC3D25ECD5C79B5245150F31D7E886F9562 (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForTransparentDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_m7DDF4CC4E0B516216D31CB185F50279743701D1E (void);
// 0x00000010 System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_mEC603B8C0049CAA21312383348577D1814C53B05 (void);
// 0x00000011 System.Int32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mF8D5C2CAC9434C9AA215B1335B0BA5E815AF2F46 (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.cctor()
extern void MixedRealityCameraSystem__cctor_m516533D87E724A280C8516161C0DF5A40455036A (void);
static Il2CppMethodPointer s_methodPointers[18] = 
{
	MixedRealityCameraSystem__ctor_m38911A4E6D216CBEE883148FF107C0BE5628FA9F,
	MixedRealityCameraSystem__ctor_m738D26A0C117709E9D949ADB65A120EF896AE958,
	MixedRealityCameraSystem_get_Name_mC2954562ACB4A76F8705B242D421AE44DA5C360F,
	MixedRealityCameraSystem_set_Name_mC26BE764FB71BB73948948D1E1DAE20A30D2E048,
	MixedRealityCameraSystem_get_IsOpaque_mB5EC943246C46C6919E5EADAF2AC621113D01EC7,
	MixedRealityCameraSystem_get_SourceId_m0C6BBD1991142D53FD7CCB7A8EACA528AE436E67,
	MixedRealityCameraSystem_get_SourceName_m7072E6335FB43C3284F353393AD0DDBD2FE9D93D,
	MixedRealityCameraSystem_get_CameraProfile_mAC2EAB143440EE56B0E3F4D036643C68D1004D8E,
	MixedRealityCameraSystem_Initialize_m2C3BBFD152EA6337AF4AA2BA2478FF4D851D9E5D,
	MixedRealityCameraSystem_Enable_m0FFD02C445B78D45AD037B4D0C9002A460D9272C,
	MixedRealityCameraSystem_Disable_mB57506E4D8013ECBCE7563F1BEF80C63F3BB8C3F,
	MixedRealityCameraSystem_Destroy_m839A10E59F28BD3D39E6B27727F262533D0FF487,
	MixedRealityCameraSystem_Update_mE8FA51D217EE6AAA139A5B71920B79129AC11704,
	MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_m2A7FAAC3D25ECD5C79B5245150F31D7E886F9562,
	MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_m7DDF4CC4E0B516216D31CB185F50279743701D1E,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_mEC603B8C0049CAA21312383348577D1814C53B05,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mF8D5C2CAC9434C9AA215B1335B0BA5E815AF2F46,
	MixedRealityCameraSystem__cctor_m516533D87E724A280C8516161C0DF5A40455036A,
};
static const int32_t s_InvokerIndices[18] = 
{
	2172,
	3635,
	4326,
	3635,
	4364,
	4295,
	4326,
	4326,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	4406,
	1704,
	2658,
	6303,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_CameraSystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_CameraSystem_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_CameraSystem_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.CameraSystem.dll",
	18,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Services_CameraSystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
