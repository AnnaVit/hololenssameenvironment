﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void Microsoft_MixedReality_Toolkit_Async_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[1];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[3];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[4];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x2E\x54\x6F\x6F\x6C\x6B\x69\x74\x2E\x54\x65\x73\x74\x73\x2E\x50\x6C\x61\x79\x4D\x6F\x64\x65\x54\x65\x73\x74\x73"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[6];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x35\x2E\x33\x2E\x30"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[7];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x20\x61\x69\x70\x6D\x72\x61\x67\x65\x6E\x74\x5F\x77\x6F\x72\x6B"), NULL);
	}
}
static void U3CU3Ec_tCBC87012B90B3B6F6BF405A04C803878478D343A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m512F9096DDFF479C59DAC8A1511D36264905D0AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m0142AF0B04F8A74FD491B425CD09CB9212968234(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m8C5F4FB79A7484D3BA83243D7FC481B3F02B7DE9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9DDB4B63B036094F2D75508AB4A009EE23B9357F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m33994726D0B802DAC589974E38BBECF6D0EDD60F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m4406C7241628C4BE0138CC9A4919876AAC7378E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_mF7265561C482417D743CA6E279F8EED4C907C1C2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9A2BBDD2550F6EB54E20DA74275DBF7F97F87B47(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_mC18898EAA13A71D8BFDC992B59B58EB8648FB56D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m265357C8F5D41F54F5BF90E700F8450BE91DA3DA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m1B8FF1ECF199945C1FDBF420D49BAEE97996F305(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m63C69E2812B0407CDCF814974116D534FD2B48C4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9F8A57146B37CBEED99DDB67918DEC32373342A8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_1_get_IsCompleted_m51F3709E548923A8D946B8CEBD1ECF71D661E65C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_1_set_IsCompleted_m9E93A5CA4C23A5DCAAEE6E87CE51A89FC0D5E455(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_get_IsCompleted_m23F52130D905F5F3C9B35E7E454C90C278E248F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_set_IsCompleted_m9C4C0FC64913B415CB4DBD1921FBB941D94CA29D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CoroutineWrapper_1_t900C242D9F2D970C72EEB07314D87727C224DD76_CustomAttributesCacheGenerator_CoroutineWrapper_1_Run_m77A0EBA1E668B61F834CE52FE7C677015B4E73A4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_0_0_0_var), NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3__ctor_m9713775266FBF27D2955107D0156F7BCCAE19D64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_IDisposable_Dispose_m5A1AFC79EA118869D79FD6D6579A38468E031C7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5C40E32044A5B975944D9A163BFD17318E838C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_IEnumerator_Reset_m7B1876DC6DC7FA3A25B5A2C4D0078D88FB4230F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_IEnumerator_get_Current_m6749AAD9DDFD37331E057E677BACE5241BF858C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ReturnVoid_mD022779C07C51FDC74018F5B660873966AC47480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_0_0_0_var), NULL);
	}
}
static void InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_AssetBundleCreateRequest_mA0E069DFF0C300ACEBAC6EA3232F3CA55679EF0A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_0_0_0_var), NULL);
	}
}
static void InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ReturnSelf_m406D1552681F4203C5205516E3E6C7EB3A733DF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_0_0_0_var), NULL);
	}
}
static void InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_AssetBundleRequest_mA18579E5D6C4DCC87C7F07708E66A8CFAD8D99DA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_0_0_0_var), NULL);
	}
}
static void InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ResourceRequest_mCE171462253C4E540E4402CBB1823D5AA72CDFA4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_0_0_0_var), NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0__ctor_m6FEAAA49A381416674CA9B85F4E55EF115E7BF77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m27CAB859E8D06AAEC599A7202319B64148665A45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1B69654B4C1CA7D1B2D9ABFF487CFBD7F849DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mA2F833C1707C9E403E13986B7D0A8A7DDE55CB9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m724B5FD67B9479605BFF0BFCF343B4B643841C2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1__ctor_m8DBD580C5433983031595374937DE8576FF08D6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m94E54716364CEBAFE63E70C9EA25613D6D99FC93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8127002C1248A845A231700D2B8478E8B7BB2D57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mCF94D1D05CA888D1A87E756341AC2C3730894F1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m28D0284032AB784807EEFD0A385005667BA185E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1__ctor_m6E23C04D54195CDF5CF42BA3F3A2F81BD065AF00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_IDisposable_Dispose_mE6895B3DBC34A07E24197DB6888C56AFBA58A626(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ED0D3338B6D0D2E767C48FD838B66B235494EF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_IEnumerator_Reset_mE10712C7C6BE01874BA5190591B3D71DB2A210D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_IEnumerator_get_Current_m751FB74925079887C896DEC6E8BB1EF39A5C0248(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3__ctor_m5BF5419E18224E3BB05C8822D69C9DEE9F604637(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_mFF55C5A68DEADCD98AB648D732B1924E9E72F621(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D2CDEE5A6F7DC7AF28623546CA6E40B03C82DD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_mDB8C815A5A1A4A3C398522C110FE3BB83A261DD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m44C9F7B1CA14F65178A60EE1FF9C992F1BB12877(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4__ctor_m8AB4E0721573E50A5E0A4209C8D4A70C5E9A577D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m04064ABC86F37919780BB4F73BF1F591C26FD441(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF18EBCDA5C34ABC9425A00AC2064EC6745978A01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mCE1DC9C2BCAC62574898B1F75F0C1FA9BFA552FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m7593290530819EB9E05A09E991AB99D57F123F30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t8C5533C1BE405E3ABA2CDC9729183234C7F0823D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t8090DFE625B218DA6E64DBBA1BC16BBA9E216339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t2E2F5C513FDC0E7275C9CF9D1934498FDDA95B1B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_1_tD7CBA1D7CE5D8D7B312AED5EDBEBF7C3E2D75EFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_tC9A5A618A7EFEEDD53025CA0A9F14D98866C04AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tFF26EAA0DD7FCD35FA2370DC8ED116A37A06F855_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_1_t706CD5D9B42658A298817B14900845179B614737_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncCoroutineRunner_tCCFED9F0B0740135C852B7464D50B5A57170EBDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x43\x6F\x72\x65\x2F\x41\x73\x79\x6E\x63\x43\x6F\x72\x6F\x75\x74\x69\x6E\x65\x52\x75\x6E\x6E\x65\x72"), NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_U3CUnityThreadIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_U3CUnitySynchronizationContextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_Initialize_mC185FB5956026D2EC84F569046DFB577CBE56AE1(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_get_UnityThreadId_m18C75B3EB16752A8D4ECC98D7330F1191FE2D805(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_set_UnityThreadId_mB4DC95D9BAA29112A27E221617E9902A4F3DF22A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_get_UnitySynchronizationContext_mE335E621F442423DEB6978469B66135D979C5599(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_set_UnitySynchronizationContext_mACECEE32A8BB9054DE2228E798A22ED2BDAFFA9B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Async_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Async_AttributeGenerators[79] = 
{
	U3CU3Ec_tCBC87012B90B3B6F6BF405A04C803878478D343A_CustomAttributesCacheGenerator,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t8C5533C1BE405E3ABA2CDC9729183234C7F0823D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t8090DFE625B218DA6E64DBBA1BC16BBA9E216339_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t2E2F5C513FDC0E7275C9CF9D1934498FDDA95B1B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_1_tD7CBA1D7CE5D8D7B312AED5EDBEBF7C3E2D75EFA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_tC9A5A618A7EFEEDD53025CA0A9F14D98866C04AC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tFF26EAA0DD7FCD35FA2370DC8ED116A37A06F855_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_1_t706CD5D9B42658A298817B14900845179B614737_CustomAttributesCacheGenerator,
	AsyncCoroutineRunner_tCCFED9F0B0740135C852B7464D50B5A57170EBDF_CustomAttributesCacheGenerator,
	SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField,
	SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_U3CUnityThreadIdU3Ek__BackingField,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_U3CUnitySynchronizationContextU3Ek__BackingField,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m512F9096DDFF479C59DAC8A1511D36264905D0AF,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m0142AF0B04F8A74FD491B425CD09CB9212968234,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m8C5F4FB79A7484D3BA83243D7FC481B3F02B7DE9,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9DDB4B63B036094F2D75508AB4A009EE23B9357F,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m33994726D0B802DAC589974E38BBECF6D0EDD60F,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m4406C7241628C4BE0138CC9A4919876AAC7378E0,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_mF7265561C482417D743CA6E279F8EED4C907C1C2,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9A2BBDD2550F6EB54E20DA74275DBF7F97F87B47,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_mC18898EAA13A71D8BFDC992B59B58EB8648FB56D,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m265357C8F5D41F54F5BF90E700F8450BE91DA3DA,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m1B8FF1ECF199945C1FDBF420D49BAEE97996F305,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m63C69E2812B0407CDCF814974116D534FD2B48C4,
	AwaiterExtensions_t07004BD298E1459E9A96BC037886E033D59C3CC2_CustomAttributesCacheGenerator_AwaiterExtensions_GetAwaiter_m9F8A57146B37CBEED99DDB67918DEC32373342A8,
	SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_1_get_IsCompleted_m51F3709E548923A8D946B8CEBD1ECF71D661E65C,
	SimpleCoroutineAwaiter_1_t6B0A6B0528A6228A3C96E2A5101110410A9D3947_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_1_set_IsCompleted_m9E93A5CA4C23A5DCAAEE6E87CE51A89FC0D5E455,
	SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_get_IsCompleted_m23F52130D905F5F3C9B35E7E454C90C278E248F0,
	SimpleCoroutineAwaiter_t49DBC04962E85CD43B215FBB4513948997E9808E_CustomAttributesCacheGenerator_SimpleCoroutineAwaiter_set_IsCompleted_m9C4C0FC64913B415CB4DBD1921FBB941D94CA29D,
	CoroutineWrapper_1_t900C242D9F2D970C72EEB07314D87727C224DD76_CustomAttributesCacheGenerator_CoroutineWrapper_1_Run_m77A0EBA1E668B61F834CE52FE7C677015B4E73A4,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3__ctor_m9713775266FBF27D2955107D0156F7BCCAE19D64,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_IDisposable_Dispose_m5A1AFC79EA118869D79FD6D6579A38468E031C7A,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5C40E32044A5B975944D9A163BFD17318E838C1,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_IEnumerator_Reset_m7B1876DC6DC7FA3A25B5A2C4D0078D88FB4230F9,
	U3CRunU3Ed__3_t1D9D4C8EF8BE2EA3C6B98AB3CEC696B7EB0078F7_CustomAttributesCacheGenerator_U3CRunU3Ed__3_System_Collections_IEnumerator_get_Current_m6749AAD9DDFD37331E057E677BACE5241BF858C5,
	InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ReturnVoid_mD022779C07C51FDC74018F5B660873966AC47480,
	InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_AssetBundleCreateRequest_mA0E069DFF0C300ACEBAC6EA3232F3CA55679EF0A,
	InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ReturnSelf_m406D1552681F4203C5205516E3E6C7EB3A733DF2,
	InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_AssetBundleRequest_mA18579E5D6C4DCC87C7F07708E66A8CFAD8D99DA,
	InstructionWrappers_t95738760FA7BF9D4779CDD1676FAA7CFC0A92C60_CustomAttributesCacheGenerator_InstructionWrappers_ResourceRequest_mCE171462253C4E540E4402CBB1823D5AA72CDFA4,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0__ctor_m6FEAAA49A381416674CA9B85F4E55EF115E7BF77,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m27CAB859E8D06AAEC599A7202319B64148665A45,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1B69654B4C1CA7D1B2D9ABFF487CFBD7F849DB,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mA2F833C1707C9E403E13986B7D0A8A7DDE55CB9E,
	U3CReturnVoidU3Ed__0_t8F21BE4213BEF8BE0AFFC226F627E6D72BDAC3D6_CustomAttributesCacheGenerator_U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m724B5FD67B9479605BFF0BFCF343B4B643841C2A,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1__ctor_m8DBD580C5433983031595374937DE8576FF08D6E,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m94E54716364CEBAFE63E70C9EA25613D6D99FC93,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8127002C1248A845A231700D2B8478E8B7BB2D57,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mCF94D1D05CA888D1A87E756341AC2C3730894F1E,
	U3CAssetBundleCreateRequestU3Ed__1_t27218991C666F8F6BFD9BE6F0D837ABBB026E16A_CustomAttributesCacheGenerator_U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m28D0284032AB784807EEFD0A385005667BA185E0,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1__ctor_m6E23C04D54195CDF5CF42BA3F3A2F81BD065AF00,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_IDisposable_Dispose_mE6895B3DBC34A07E24197DB6888C56AFBA58A626,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ED0D3338B6D0D2E767C48FD838B66B235494EF8,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_IEnumerator_Reset_mE10712C7C6BE01874BA5190591B3D71DB2A210D6,
	U3CReturnSelfU3Ed__2_1_t49F75E81DBAD736BEC9B759C87F296207ABED00C_CustomAttributesCacheGenerator_U3CReturnSelfU3Ed__2_1_System_Collections_IEnumerator_get_Current_m751FB74925079887C896DEC6E8BB1EF39A5C0248,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3__ctor_m5BF5419E18224E3BB05C8822D69C9DEE9F604637,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_mFF55C5A68DEADCD98AB648D732B1924E9E72F621,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D2CDEE5A6F7DC7AF28623546CA6E40B03C82DD8,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_mDB8C815A5A1A4A3C398522C110FE3BB83A261DD0,
	U3CAssetBundleRequestU3Ed__3_t3E7F49FD900A9BD340B267F0119116F76F57B834_CustomAttributesCacheGenerator_U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m44C9F7B1CA14F65178A60EE1FF9C992F1BB12877,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4__ctor_m8AB4E0721573E50A5E0A4209C8D4A70C5E9A577D,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m04064ABC86F37919780BB4F73BF1F591C26FD441,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF18EBCDA5C34ABC9425A00AC2064EC6745978A01,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mCE1DC9C2BCAC62574898B1F75F0C1FA9BFA552FD,
	U3CResourceRequestU3Ed__4_t856E8EB7F6B30C772AB76A754F5FA56EE86D10E8_CustomAttributesCacheGenerator_U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m7593290530819EB9E05A09E991AB99D57F123F30,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_Initialize_mC185FB5956026D2EC84F569046DFB577CBE56AE1,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_get_UnityThreadId_m18C75B3EB16752A8D4ECC98D7330F1191FE2D805,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_set_UnityThreadId_mB4DC95D9BAA29112A27E221617E9902A4F3DF22A,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_get_UnitySynchronizationContext_mE335E621F442423DEB6978469B66135D979C5599,
	SyncContextUtility_t32F175903CC53496FE37939038574D5DA368B8EC_CustomAttributesCacheGenerator_SyncContextUtility_set_UnitySynchronizationContext_mACECEE32A8BB9054DE2228E798A22ED2BDAFFA9B,
	Microsoft_MixedReality_Toolkit_Async_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
