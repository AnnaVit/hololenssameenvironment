﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DisallowMultipleComponent
struct  DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct  HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct  RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct  AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct  RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const RuntimeMethod* method);
static void Microsoft_MixedReality_Toolkit_Services_InputSystem_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[2];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[3];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x20\x61\x69\x70\x6D\x72\x61\x67\x65\x6E\x74\x5F\x77\x6F\x72\x6B"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x35\x2E\x33\x2E\x30"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[5];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void BaseNearInteractionTouchable_t8428632C00A86AB7751812857B3C658000F3ABCE_CustomAttributesCacheGenerator_eventsToReceive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseNearInteractionTouchable_t8428632C00A86AB7751812857B3C658000F3ABCE_CustomAttributesCacheGenerator_debounceThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x6E\x20\x66\x72\x6F\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x75\x72\x66\x61\x63\x65\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x79\x6F\x75\x20\x77\x69\x6C\x6C\x20\x72\x65\x63\x65\x69\x76\x65\x20\x61\x20\x74\x6F\x75\x63\x68\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x64\x20\x65\x76\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColliderNearInteractionTouchable_t45BA3BECB3EEC9CC33F96B8BCE0A73953FCB6F0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x42\x61\x73\x65\x4E\x65\x61\x72\x49\x6E\x74\x65\x61\x72\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65"), true, NULL);
	}
}
static void ColliderNearInteractionTouchable_t45BA3BECB3EEC9CC33F96B8BCE0A73953FCB6F0D_CustomAttributesCacheGenerator_touchableCollider(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider__ctor_m6C9BFDF95ED7D2A3637CFADB18E944021FAD5502(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x28\x72\x65\x67\x69\x73\x74\x72\x61\x72\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x72\x65\x71\x75\x69\x72\x65\x64\x29\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x61\x20\x66\x75\x74\x75\x72\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x2E"), NULL);
	}
}
static void DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider_get_Name_mB538072A2AA39C1F0D4E94CAD47753B13883B819(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider_set_Name_mC07E922AC5547DC7329A72AC0D40E3B22D2D3368(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x6D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x54\x6F\x6F\x6C\x6B\x69\x74\x2D\x55\x6E\x69\x74\x79\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x49\x6E\x70\x75\x74\x2F\x4F\x76\x65\x72\x76\x69\x65\x77\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNumNearPointersActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNumFarPointersActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_PrimaryPointerChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CGazePointerBehaviorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider__ctor_m8D0E8A29D807C00C1D81BB5A466615370FC4A8DF(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x28\x72\x65\x67\x69\x73\x74\x72\x61\x72\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x72\x65\x71\x75\x69\x72\x65\x64\x29\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x61\x20\x66\x75\x74\x75\x72\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x2E"), NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_NumNearPointersActive_mD274E9C0A892E817F66A743C56DEF37E67ABF072(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_NumNearPointersActive_m9613789E92FCBB8522F58F2DA6A078661EE12387(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_NumFarPointersActive_mC0F81CB40E3972A1117970B2DD5B0D726855EC4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_NumFarPointersActive_m13DC9A8A3E637896C6ACA296C6990AF2CA2ACCD4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_Name_m720C4506B2E808BF28B2B3023E7CE2EC657C493A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_Name_m503BE058D12180B3711D681E4EED3A2279D4E90A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_add_PrimaryPointerChanged_m0B2C3345940B23C2293660EEBAA5496D4A050E07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_remove_PrimaryPointerChanged_mF9C04B5518DD91BBB33D102BBF7E59ECB3EA935B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_GazePointerBehavior_mDF1B6115D96E6277DF7CAC5B1C388C0405521E3D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_GazePointerBehavior_m0E22B435FBCB479CF07FA4A56648B217640878D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CStartPointU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CPreviousPointerTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CRayStepIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_StartPoint_m422E8F053289CEC020F5C70D77B659F27C293704(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_StartPoint_mE7A9832F845512E22F776B7771A6FE382D19F650(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_PreviousPointerTarget_mE4A4F7D8FF4B703D0CA41233C46681F0700C4AD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_PreviousPointerTarget_mAB6B24961FF961038C0BA412674131767E2AE158(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_RayStepIndex_mBEFE82827761AD963E494D0AD4E4AF6B910244BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_RayStepIndex_m1F1B0B9D2359CCD008CD57135A7E643D533DFDB0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x47\x61\x7A\x65\x50\x72\x6F\x76\x69\x64\x65\x72"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_lockCursorWhenFocusLocked(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x65\x73\x20\x74\x68\x65\x20\x67\x61\x7A\x65\x20\x63\x75\x72\x73\x6F\x72\x20\x74\x6F\x20\x73\x74\x61\x79\x20\x6C\x6F\x63\x6B\x65\x64\x20\x6F\x6E\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x63\x75\x72\x73\x6F\x72\x27\x73\x20\x66\x6F\x63\x75\x73\x20\x69\x73\x20\x6C\x6F\x63\x6B\x65\x64\x2C\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x63\x6F\x6E\x74\x69\x6E\x75\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x74\x68\x65\x20\x68\x65\x61\x64\x27\x73\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_setCursorInvisibleWhenFocusLocked(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x67\x61\x7A\x65\x20\x63\x75\x72\x73\x6F\x72\x20\x77\x69\x6C\x6C\x20\x64\x69\x73\x61\x70\x70\x65\x61\x72\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x70\x6F\x69\x6E\x74\x65\x72\x27\x73\x20\x66\x6F\x63\x75\x73\x20\x69\x73\x20\x6C\x6F\x63\x6B\x65\x64\x2C\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x74\x68\x65\x20\x63\x75\x72\x73\x6F\x72\x20\x66\x72\x6F\x6D\x20\x66\x6C\x6F\x61\x74\x69\x6E\x67\x20\x69\x64\x6C\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x77\x6F\x72\x6C\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_maxGazeCollisionDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x67\x61\x7A\x65\x20\x63\x61\x6E\x20\x68\x69\x74\x20\x61\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_raycastLayerMasks(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x4C\x61\x79\x65\x72\x4D\x61\x73\x6B\x73\x2C\x20\x69\x6E\x20\x70\x72\x69\x6F\x72\x69\x74\x69\x7A\x65\x64\x20\x6F\x72\x64\x65\x72\x2C\x20\x74\x68\x61\x74\x20\x61\x72\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x74\x68\x65\x20\x47\x61\x7A\x65\x54\x61\x72\x67\x65\x74\x20\x77\x68\x65\x6E\x20\x72\x61\x79\x63\x61\x73\x74\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_stabilizer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x62\x69\x6C\x69\x7A\x65\x72\x2C\x20\x69\x66\x20\x61\x6E\x79\x2C\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x73\x6D\x6F\x6F\x74\x68\x20\x6F\x75\x74\x20\x74\x68\x65\x20\x67\x61\x7A\x65\x20\x72\x61\x79\x20\x64\x61\x74\x61\x2E"), NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_gazeTransform(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x74\x68\x61\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x72\x65\x70\x72\x65\x73\x65\x6E\x74\x20\x74\x68\x65\x20\x67\x61\x7A\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E\x20\x44\x65\x66\x61\x75\x6C\x74\x73\x20\x74\x6F\x20\x43\x61\x6D\x65\x72\x61\x43\x61\x63\x68\x65\x2E\x4D\x61\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_minHeadVelocityThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x68\x65\x61\x64\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_maxHeadVelocityThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x68\x65\x61\x64\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 5.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CGazeCursorPrefabU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CGazeTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHeadVelocityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHeadMovementDirectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CIsEyeCalibrationValidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CLatestEyeGazeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CIsEyeTrackingEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CUseHeadGazeOverrideU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_GazeCursorPrefab_m40CCB21A3FFC04008C68CFF6F968A7DB50A7A4C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_GazeCursorPrefab_m607EAA00B5292F16A9A8AE99089386A7D31AA285(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_GazeTarget_m5D370EFB4AF8118F049EDCCC6808CA9E0AA38C33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_GazeTarget_m50110F0812B93A399CEA008C211161A55D751671(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitInfo_mC6C64774B811FEAC54FA3CD8F373224611DC5997(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitInfo_m0DB7F9C9E348535033363E78240E78F5ED9C700B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitPosition_m0CF3DB2AF31C873E2EEFE0BEE43925E2C3530343(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitPosition_m66A66BE5409C4802135CE6895E0328A06E5C57AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitNormal_m7886F528908F93E27934F25198FDAD458FC07BE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitNormal_m309231C3EEEADBBE0604FFB3135E49172DE8D8AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HeadVelocity_mEB0602B2E2A76D9D56FB1347990EC2686B06111B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HeadVelocity_m702CC2A261F592379393E8364DA12A629B16700A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HeadMovementDirection_m31DFC53340F1BDF5D981D2D6D561FE9585AA4C26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HeadMovementDirection_m259DD40DD5B118FF39AB473691124281F44AB982(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_Start_m6EAAFA7051415C3DFF106BCAB7906E0CD22A5A55(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_0_0_0_var), NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_RaiseSourceDetected_mA1848997E168EC3DF06ED7ADBB21DA3577E099D8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_0_0_0_var), NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_IsEyeCalibrationValid_m3C0C60B8B60763627E4084F0F629ECD0BA4AC646(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_IsEyeCalibrationValid_m47D339440A612BCBFEF444DF9C902E94CF4E1437(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_LatestEyeGaze_m8C2F9236CC1EFF9DE37BDD092188E0341DAC2BB7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_LatestEyeGaze_mC2D1E9684AEEA1D31255F4A2B6DE238379850B0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_IsEyeTrackingEnabled_mCC73A14376428F67C50D3C5BC2EFB87E37F64258(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_IsEyeTrackingEnabled_mDD8948C0BAE92D06415061E4692DA004877AE53A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_Timestamp_m860D6B2FDA518AB94D9C7EB2A2F6DEEACB3485AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_Timestamp_m653309C70F41768C302BADBDC28F268D9201C358(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_UseHeadGazeOverride_mDD01AC1180B147624A905121A4D1E86D1A87C6BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_UseHeadGazeOverride_m2AEF31DA36CEE59029EA6C52BB1B95F4D97CB9EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_U3CU3En__0_m81634D8D8E8FE66D0AE962D62DEFBF63BC93B1C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_U3CControllerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_U3CInputSourceParentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_get_Controller_m23B01EFF6925D47A30E55F16ADB694469A44591B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_set_Controller_m75F90DF47C5A592700751CF5C81EBC321F7D4F49(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_get_InputSourceParent_m1E2AF81A1856861FA0AE5A073AF0D12764D76A30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_set_InputSourceParent_m48D88841B17601D3E949CCB3C8ECCEDE8765FF85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_CustomAttributesCacheGenerator_U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_CustomAttributesCacheGenerator_U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InputSystemGlobalHandlerListener_t09087E6B34459C5FB9AC0BB470B46EB5BFE207AF_CustomAttributesCacheGenerator_InputSystemGlobalHandlerListener_Start_m646082223199D214B966CDA947646D59B74E7A21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_0_0_0_var), NULL);
	}
}
static void InputSystemGlobalHandlerListener_t09087E6B34459C5FB9AC0BB470B46EB5BFE207AF_CustomAttributesCacheGenerator_InputSystemGlobalHandlerListener_EnsureInputSystemValid_m515C9327DB925F3BA2811BA579F20852F506C450(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_CustomAttributesCacheGenerator_U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tAC9846EA72C35425C3E34DAFDEC9F2A5C859CB95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_CustomAttributesCacheGenerator_U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x49\x6E\x70\x75\x74\x53\x79\x73\x74\x65\x6D\x47\x6C\x6F\x62\x61\x6C\x4C\x69\x73\x74\x65\x6E\x65\x72"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x53\x79\x73\x74\x65\x6D\x47\x6C\x6F\x62\x61\x6C\x4C\x69\x73\x74\x65\x6E\x65\x72\x20\x75\x73\x65\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x67\x6C\x6F\x62\x61\x6C\x20\x69\x6E\x70\x75\x74\x20\x65\x76\x65\x6E\x74\x20\x72\x65\x67\x69\x73\x74\x72\x61\x74\x69\x6F\x6E\x20\x41\x50\x49\x2E\x20\x55\x73\x65\x20\x52\x65\x67\x69\x73\x74\x65\x72\x48\x61\x6E\x64\x6C\x65\x72\x2F\x55\x6E\x72\x65\x67\x69\x73\x74\x65\x72\x48\x61\x6E\x64\x6C\x65\x72\x20\x41\x50\x49\x20\x64\x69\x72\x65\x63\x74\x6C\x79\x20\x28\x70\x72\x65\x66\x65\x72\x72\x65\x64\x29\x20\x6F\x72\x20\x49\x6E\x70\x75\x74\x53\x79\x73\x74\x65\x6D\x47\x6C\x6F\x62\x61\x6C\x48\x61\x6E\x64\x6C\x65\x72\x4C\x69\x73\x74\x65\x6E\x65\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator_InputSystemGlobalListener_Start_mD5DBB464B691AFCDE571E7F843CE88F39431E245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_0_0_0_var), NULL);
	}
}
static void InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator_InputSystemGlobalListener_EnsureInputSystemValid_m847891045193EBB4FEC9F815149AF2A0AA9A4232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_CustomAttributesCacheGenerator_U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t9A0375B64A146377A49F701563A9C5D170401D63_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_CustomAttributesCacheGenerator_U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x49\x6E\x70\x75\x74\x4D\x6F\x64\x75\x6C\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var), NULL);
	}
}
static void MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_U3CRaycastCameraU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_get_RaycastCamera_m5EBBAC2918E34E483E3E4E0A4E0E03A358A75CB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_set_RaycastCamera_mC37A01ED874AEB9B3A305D5718A6F0E35EF0B29B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_get_ActiveMixedRealityPointers_mD404D46EFCE9678AC4FEB250D1E14D44A7D80A12(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_0_0_0_var), NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8__ctor_mF3A98BD0D0D2C7C8C37FF46BDD27381E108BA7D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_IDisposable_Dispose_m81F86CD2815658EFE836955595D3850A73135051(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m9568F1068F08E2264198020333FF2F6767945E4E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_Reset_mEE3D9D014BC562911BB721DCDCA35E34844E192D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_get_Current_mB1FF6B129B0CD975E4FCC900F7FCE02D92B8ED90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m96D61CBBA557F0BE30D5907F60C15828000F9CE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerable_GetEnumerator_m8FAD01E0882328EB6A6490CFDF93F19424A8BC64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x6D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x54\x6F\x6F\x6C\x6B\x69\x74\x2D\x55\x6E\x69\x74\x79\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x49\x6E\x70\x75\x74\x2F\x4F\x76\x65\x72\x76\x69\x65\x77\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_InputEnabled(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_InputDisabled(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CDetectedInputSourcesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CDetectedControllersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CGazeProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CEyeGazeProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CCurrentInputActionRulesProfileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem__ctor_mCBFAAC863AC579F9FF4BC901CA89C88AD2306C4F(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x28\x72\x65\x67\x69\x73\x74\x72\x61\x72\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x72\x65\x71\x75\x69\x72\x65\x64\x29\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x61\x20\x66\x75\x74\x75\x72\x65\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x2E"), NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_Name_mC32F1847F9B7F24AEF47BF87DCA70CD57EE4B5C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_Name_mB2D49E0C4C68FAA1CF4DC10EAF03BABD10A92640(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_add_InputEnabled_m1CD6DD9E8CB62896E260E26A0DEFD3C5C7C669BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_remove_InputEnabled_m6D3FAF296444A1C3CBAE2657E9FA58FEBC4614DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_add_InputDisabled_mA592CDD3F94DBE095D8B1345A24BD5296961F4FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_remove_InputDisabled_mA1FA59DAFA1A68AEF5F5D1C398C093043623E967(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_DetectedInputSources_m134E5FCEF6DD92AC33A99E81314E060BC08A723D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_DetectedControllers_mA3036DA07CF5C3A6784CEE28B672B0077DD36792(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_GazeProvider_mDBC37FA9CF1D0BBD3CF11F3D80FE8F511AC0F5CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_GazeProvider_mD0124EAFA6FA864EDA6A7B78152E79F301E521CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_EyeGazeProvider_mEFB38A18AF9BD544D816002C7A6804C410E5F981(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_EyeGazeProvider_m339E998955220B401FBED8BCFFA6145C811818EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_CurrentInputActionRulesProfile_mBC867FA63A1B9CBF5DE129EEEC79D8F1061CB8A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mFAB94A5E10A743D706D962E0444E04B1FB7665D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t7216F63FC8F5BAE16F9FBAD226C0C2BAED045402_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x47\x72\x61\x62\x62\x61\x62\x6C\x65"), NULL);
	}
}
static void NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator_ShowTetherWhenManipulating(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x61\x20\x74\x65\x74\x68\x65\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x77\x68\x65\x72\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x61\x73\x20\x67\x72\x61\x62\x62\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x68\x61\x6E\x64\x20\x77\x68\x65\x6E\x20\x6D\x61\x6E\x69\x70\x75\x6C\x61\x74\x69\x6E\x67\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x74\x68\x69\x6E\x67\x73\x20\x6C\x69\x6B\x65\x20\x62\x6F\x75\x6E\x64\x69\x6E\x67\x20\x62\x6F\x78\x65\x73\x20\x77\x68\x65\x72\x65\x20\x72\x65\x73\x69\x7A\x69\x6E\x67\x2F\x72\x6F\x74\x61\x74\x69\x6E\x67\x20\x6D\x69\x67\x68\x74\x20\x62\x65\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x65\x64\x2E"), NULL);
	}
}
static void NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator_IsBoundsHandles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x64\x20\x74\x6F\x20\x64\x65\x73\x69\x67\x6E\x61\x74\x65\x20\x74\x68\x69\x73\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x20\x67\x72\x61\x62\x62\x61\x62\x6C\x65\x20\x61\x73\x20\x61\x20\x62\x6F\x75\x6E\x64\x73\x20\x68\x61\x6E\x64\x6C\x65"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localForward(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x6C\x20\x73\x70\x61\x63\x65\x20\x66\x6F\x72\x77\x61\x72\x64\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x6C\x20\x73\x70\x61\x63\x65\x20\x75\x70\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localCenter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x6C\x20\x73\x70\x61\x63\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x63\x65\x6E\x74\x65\x72"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x75\x6E\x64\x73\x20\x6F\x72\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x32\x44\x20\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65\x50\x6C\x61\x6E\x65"), NULL);
	}
}
static void NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_touchableCollider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x78\x43\x6F\x6C\x6C\x69\x64\x65\x72\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x20\x62\x6F\x75\x6E\x64\x73\x20\x61\x6E\x64\x20\x6C\x6F\x63\x61\x6C\x20\x63\x65\x6E\x74\x65\x72\x2C\x20\x69\x66\x20\x6E\x6F\x74\x20\x73\x65\x74\x20\x62\x65\x66\x6F\x72\x65\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x73\x27\x73\x20\x42\x6F\x78\x43\x6F\x6C\x6C\x69\x64\x65\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x62\x79\x20\x64\x65\x66\x61\x75\x6C\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t2D8EA7D1E133F68412799DEA3D57618DC14A7DFD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NearInteractionTouchableUnityUI_t75CCE2C6D7F1567DC5E52B5FCBC68A5F2650B881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65\x55\x6E\x69\x74\x79\x55\x49"), NULL);
	}
}
static void NearInteractionTouchableVolume_tE92D9B4C9BF7341FA199F07C2FFC312C8ED847AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x4E\x65\x61\x72\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x61\x62\x6C\x65\x56\x6F\x6C\x75\x6D\x65"), NULL);
	}
}
static void NearInteractionTouchableVolume_tE92D9B4C9BF7341FA199F07C2FFC312C8ED847AE_CustomAttributesCacheGenerator_touchableCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void CanvasUtility_t4A0D685B90FCA564B96DE8970310D6E2E32EA60C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x43\x61\x6E\x76\x61\x73\x55\x74\x69\x6C\x69\x74\x79"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ScaleMeshEffect_t48252C2AA57E9A2676093E3B9388DB8DDF60769C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x6D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x54\x6F\x6F\x6C\x6B\x69\x74\x2D\x55\x6E\x69\x74\x79\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x52\x45\x41\x44\x4D\x45\x5F\x4D\x52\x54\x4B\x53\x74\x61\x6E\x64\x61\x72\x64\x53\x68\x61\x64\x65\x72\x2E\x68\x74\x6D\x6C\x23\x75\x67\x75\x69\x2D\x73\x75\x70\x70\x6F\x72\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x53\x65\x72\x76\x69\x63\x65\x73\x2F\x53\x63\x61\x6C\x65\x4D\x65\x73\x68\x45\x66\x66\x65\x63\x74"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), il2cpp_codegen_type_get_object(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_InputSystem_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Services_InputSystem_AttributeGenerators[161] = 
{
	ColliderNearInteractionTouchable_t45BA3BECB3EEC9CC33F96B8BCE0A73953FCB6F0D_CustomAttributesCacheGenerator,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator,
	U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_CustomAttributesCacheGenerator,
	U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_CustomAttributesCacheGenerator,
	U3CU3Ec_tAC9846EA72C35425C3E34DAFDEC9F2A5C859CB95_CustomAttributesCacheGenerator,
	U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_CustomAttributesCacheGenerator,
	InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_CustomAttributesCacheGenerator,
	U3CU3Ec_t9A0375B64A146377A49F701563A9C5D170401D63_CustomAttributesCacheGenerator,
	U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_CustomAttributesCacheGenerator,
	MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator,
	U3CU3Ec_t7216F63FC8F5BAE16F9FBAD226C0C2BAED045402_CustomAttributesCacheGenerator,
	NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator,
	U3CU3Ec_t2D8EA7D1E133F68412799DEA3D57618DC14A7DFD_CustomAttributesCacheGenerator,
	NearInteractionTouchableUnityUI_t75CCE2C6D7F1567DC5E52B5FCBC68A5F2650B881_CustomAttributesCacheGenerator,
	NearInteractionTouchableVolume_tE92D9B4C9BF7341FA199F07C2FFC312C8ED847AE_CustomAttributesCacheGenerator,
	CanvasUtility_t4A0D685B90FCA564B96DE8970310D6E2E32EA60C_CustomAttributesCacheGenerator,
	ScaleMeshEffect_t48252C2AA57E9A2676093E3B9388DB8DDF60769C_CustomAttributesCacheGenerator,
	BaseNearInteractionTouchable_t8428632C00A86AB7751812857B3C658000F3ABCE_CustomAttributesCacheGenerator_eventsToReceive,
	BaseNearInteractionTouchable_t8428632C00A86AB7751812857B3C658000F3ABCE_CustomAttributesCacheGenerator_debounceThreshold,
	ColliderNearInteractionTouchable_t45BA3BECB3EEC9CC33F96B8BCE0A73953FCB6F0D_CustomAttributesCacheGenerator_touchableCollider,
	DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNumNearPointersActiveU3Ek__BackingField,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNumFarPointersActiveU3Ek__BackingField,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_PrimaryPointerChanged,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_U3CGazePointerBehaviorU3Ek__BackingField,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CStartPointU3Ek__BackingField,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CPreviousPointerTargetU3Ek__BackingField,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_U3CRayStepIndexU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_lockCursorWhenFocusLocked,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_setCursorInvisibleWhenFocusLocked,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_maxGazeCollisionDistance,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_raycastLayerMasks,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_stabilizer,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_gazeTransform,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_minHeadVelocityThreshold,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_maxHeadVelocityThreshold,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CGazeCursorPrefabU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CGazeTargetU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitInfoU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitPositionU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHitNormalU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHeadVelocityU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CHeadMovementDirectionU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CIsEyeCalibrationValidU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CLatestEyeGazeU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CIsEyeTrackingEnabledU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CTimestampU3Ek__BackingField,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_U3CUseHeadGazeOverrideU3Ek__BackingField,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_U3CControllerU3Ek__BackingField,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_U3CInputSourceParentU3Ek__BackingField,
	MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_U3CRaycastCameraU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_InputEnabled,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_InputDisabled,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CDetectedInputSourcesU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CDetectedControllersU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CGazeProviderU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CEyeGazeProviderU3Ek__BackingField,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_U3CCurrentInputActionRulesProfileU3Ek__BackingField,
	NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator_ShowTetherWhenManipulating,
	NearInteractionGrabbable_tAC7B11432FDEAFF71B15CC59CBF9121A846A383E_CustomAttributesCacheGenerator_IsBoundsHandles,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localForward,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localUp,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_localCenter,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_bounds,
	NearInteractionTouchable_tE18F235D780462DA69CCEFD473DA0599B242DAA5_CustomAttributesCacheGenerator_touchableCollider,
	NearInteractionTouchableVolume_tE92D9B4C9BF7341FA199F07C2FFC312C8ED847AE_CustomAttributesCacheGenerator_touchableCollider,
	DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider__ctor_m6C9BFDF95ED7D2A3637CFADB18E944021FAD5502,
	DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider_get_Name_mB538072A2AA39C1F0D4E94CAD47753B13883B819,
	DefaultRaycastProvider_tDED744A0C3A8AF204C451E882A2BEC09C7169DCC_CustomAttributesCacheGenerator_DefaultRaycastProvider_set_Name_mC07E922AC5547DC7329A72AC0D40E3B22D2D3368,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider__ctor_m8D0E8A29D807C00C1D81BB5A466615370FC4A8DF,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_NumNearPointersActive_mD274E9C0A892E817F66A743C56DEF37E67ABF072,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_NumNearPointersActive_m9613789E92FCBB8522F58F2DA6A078661EE12387,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_NumFarPointersActive_mC0F81CB40E3972A1117970B2DD5B0D726855EC4F,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_NumFarPointersActive_m13DC9A8A3E637896C6ACA296C6990AF2CA2ACCD4,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_Name_m720C4506B2E808BF28B2B3023E7CE2EC657C493A,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_Name_m503BE058D12180B3711D681E4EED3A2279D4E90A,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_add_PrimaryPointerChanged_m0B2C3345940B23C2293660EEBAA5496D4A050E07,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_remove_PrimaryPointerChanged_mF9C04B5518DD91BBB33D102BBF7E59ECB3EA935B,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_get_GazePointerBehavior_mDF1B6115D96E6277DF7CAC5B1C388C0405521E3D,
	FocusProvider_t186899F0F9F0FA3EB8536733A760DF7DF6CE8278_CustomAttributesCacheGenerator_FocusProvider_set_GazePointerBehavior_m0E22B435FBCB479CF07FA4A56648B217640878D4,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_StartPoint_m422E8F053289CEC020F5C70D77B659F27C293704,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_StartPoint_mE7A9832F845512E22F776B7771A6FE382D19F650,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_PreviousPointerTarget_mE4A4F7D8FF4B703D0CA41233C46681F0700C4AD6,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_PreviousPointerTarget_mAB6B24961FF961038C0BA412674131767E2AE158,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_get_RayStepIndex_mBEFE82827761AD963E494D0AD4E4AF6B910244BB,
	PointerData_t46042858A30EAB90DD8B2C6D2D1E9D8E3F75DC95_CustomAttributesCacheGenerator_PointerData_set_RayStepIndex_m1F1B0B9D2359CCD008CD57135A7E643D533DFDB0,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_GazeCursorPrefab_m40CCB21A3FFC04008C68CFF6F968A7DB50A7A4C1,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_GazeCursorPrefab_m607EAA00B5292F16A9A8AE99089386A7D31AA285,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_GazeTarget_m5D370EFB4AF8118F049EDCCC6808CA9E0AA38C33,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_GazeTarget_m50110F0812B93A399CEA008C211161A55D751671,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitInfo_mC6C64774B811FEAC54FA3CD8F373224611DC5997,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitInfo_m0DB7F9C9E348535033363E78240E78F5ED9C700B,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitPosition_m0CF3DB2AF31C873E2EEFE0BEE43925E2C3530343,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitPosition_m66A66BE5409C4802135CE6895E0328A06E5C57AE,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HitNormal_m7886F528908F93E27934F25198FDAD458FC07BE8,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HitNormal_m309231C3EEEADBBE0604FFB3135E49172DE8D8AB,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HeadVelocity_mEB0602B2E2A76D9D56FB1347990EC2686B06111B,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HeadVelocity_m702CC2A261F592379393E8364DA12A629B16700A,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_HeadMovementDirection_m31DFC53340F1BDF5D981D2D6D561FE9585AA4C26,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_HeadMovementDirection_m259DD40DD5B118FF39AB473691124281F44AB982,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_Start_m6EAAFA7051415C3DFF106BCAB7906E0CD22A5A55,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_RaiseSourceDetected_mA1848997E168EC3DF06ED7ADBB21DA3577E099D8,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_IsEyeCalibrationValid_m3C0C60B8B60763627E4084F0F629ECD0BA4AC646,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_IsEyeCalibrationValid_m47D339440A612BCBFEF444DF9C902E94CF4E1437,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_LatestEyeGaze_m8C2F9236CC1EFF9DE37BDD092188E0341DAC2BB7,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_LatestEyeGaze_mC2D1E9684AEEA1D31255F4A2B6DE238379850B0E,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_IsEyeTrackingEnabled_mCC73A14376428F67C50D3C5BC2EFB87E37F64258,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_IsEyeTrackingEnabled_mDD8948C0BAE92D06415061E4692DA004877AE53A,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_Timestamp_m860D6B2FDA518AB94D9C7EB2A2F6DEEACB3485AB,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_Timestamp_m653309C70F41768C302BADBDC28F268D9201C358,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_get_UseHeadGazeOverride_mDD01AC1180B147624A905121A4D1E86D1A87C6BB,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_set_UseHeadGazeOverride_m2AEF31DA36CEE59029EA6C52BB1B95F4D97CB9EA,
	GazeProvider_t8960220F20947A051B1CFC7DC469F178BDF522EA_CustomAttributesCacheGenerator_GazeProvider_U3CU3En__0_m81634D8D8E8FE66D0AE962D62DEFBF63BC93B1C3,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_get_Controller_m23B01EFF6925D47A30E55F16ADB694469A44591B,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_set_Controller_m75F90DF47C5A592700751CF5C81EBC321F7D4F49,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_get_InputSourceParent_m1E2AF81A1856861FA0AE5A073AF0D12764D76A30,
	InternalGazePointer_tDF9C1AA4891A69BD01CE4FEE8BB8C184ECDB8D4E_CustomAttributesCacheGenerator_InternalGazePointer_set_InputSourceParent_m48D88841B17601D3E949CCB3C8ECCEDE8765FF85,
	U3CStartU3Ed__63_tB75C0E393414359D5052F08DB98244142C1651B0_CustomAttributesCacheGenerator_U3CStartU3Ed__63_SetStateMachine_m1362C4C3993E9596DB2351318015407B87933B92,
	U3CRaiseSourceDetectedU3Ed__77_t378FDBE633B06E8A1EC0703B14DC3ACD1EF1961E_CustomAttributesCacheGenerator_U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_m5598A02048A1888C348FEC7F3BBF90C12C8838D6,
	InputSystemGlobalHandlerListener_t09087E6B34459C5FB9AC0BB470B46EB5BFE207AF_CustomAttributesCacheGenerator_InputSystemGlobalHandlerListener_Start_m646082223199D214B966CDA947646D59B74E7A21,
	InputSystemGlobalHandlerListener_t09087E6B34459C5FB9AC0BB470B46EB5BFE207AF_CustomAttributesCacheGenerator_InputSystemGlobalHandlerListener_EnsureInputSystemValid_m515C9327DB925F3BA2811BA579F20852F506C450,
	U3CStartU3Ed__2_t74E599C5E90E739DF7A6DBAF17EE162AC6A6E8E8_CustomAttributesCacheGenerator_U3CStartU3Ed__2_SetStateMachine_mC64598532E531C272738DCFB0F48B04428AAD25B,
	U3CEnsureInputSystemValidU3Ed__4_tAB278F926D1F7A7739AF0DECBC274AEC5C185B1F_CustomAttributesCacheGenerator_U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m4ACF348BC07030377C4CCDB29994C9E6B6D5430A,
	InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator_InputSystemGlobalListener_Start_mD5DBB464B691AFCDE571E7F843CE88F39431E245,
	InputSystemGlobalListener_t507925004D31FACE106C465B948B2C3C126431CD_CustomAttributesCacheGenerator_InputSystemGlobalListener_EnsureInputSystemValid_m847891045193EBB4FEC9F815149AF2A0AA9A4232,
	U3CStartU3Ed__2_tDD1411570BF4E7E97FF3DCB8C657BB3E4AF8BCA8_CustomAttributesCacheGenerator_U3CStartU3Ed__2_SetStateMachine_m478B8CEC0FE31D92AB912553AF3163F434AA186D,
	U3CEnsureInputSystemValidU3Ed__4_tE8F0D0B875AD793737278DAFEEB2101F556A6739_CustomAttributesCacheGenerator_U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m806FAD60C35894F65DAC59B59C5B4817DAAAF750,
	MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_get_RaycastCamera_m5EBBAC2918E34E483E3E4E0A4E0E03A358A75CB6,
	MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_set_RaycastCamera_mC37A01ED874AEB9B3A305D5718A6F0E35EF0B29B,
	MixedRealityInputModule_tB3757BD5F1643C8C5CB28095423D0CFFF3820469_CustomAttributesCacheGenerator_MixedRealityInputModule_get_ActiveMixedRealityPointers_mD404D46EFCE9678AC4FEB250D1E14D44A7D80A12,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8__ctor_mF3A98BD0D0D2C7C8C37FF46BDD27381E108BA7D6,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_IDisposable_Dispose_m81F86CD2815658EFE836955595D3850A73135051,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m9568F1068F08E2264198020333FF2F6767945E4E,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_Reset_mEE3D9D014BC562911BB721DCDCA35E34844E192D,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerator_get_Current_mB1FF6B129B0CD975E4FCC900F7FCE02D92B8ED90,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m96D61CBBA557F0BE30D5907F60C15828000F9CE8,
	U3Cget_ActiveMixedRealityPointersU3Ed__8_tD5D35EBB85807C8342CEF8A7F1E6E74CBCB3CCBA_CustomAttributesCacheGenerator_U3Cget_ActiveMixedRealityPointersU3Ed__8_System_Collections_IEnumerable_GetEnumerator_m8FAD01E0882328EB6A6490CFDF93F19424A8BC64,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem__ctor_mCBFAAC863AC579F9FF4BC901CA89C88AD2306C4F,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_Name_mC32F1847F9B7F24AEF47BF87DCA70CD57EE4B5C2,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_Name_mB2D49E0C4C68FAA1CF4DC10EAF03BABD10A92640,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_add_InputEnabled_m1CD6DD9E8CB62896E260E26A0DEFD3C5C7C669BF,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_remove_InputEnabled_m6D3FAF296444A1C3CBAE2657E9FA58FEBC4614DB,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_add_InputDisabled_mA592CDD3F94DBE095D8B1345A24BD5296961F4FC,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_remove_InputDisabled_mA1FA59DAFA1A68AEF5F5D1C398C093043623E967,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_DetectedInputSources_m134E5FCEF6DD92AC33A99E81314E060BC08A723D,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_DetectedControllers_mA3036DA07CF5C3A6784CEE28B672B0077DD36792,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_GazeProvider_mDBC37FA9CF1D0BBD3CF11F3D80FE8F511AC0F5CD,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_GazeProvider_mD0124EAFA6FA864EDA6A7B78152E79F301E521CC,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_EyeGazeProvider_mEFB38A18AF9BD544D816002C7A6804C410E5F981,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_EyeGazeProvider_m339E998955220B401FBED8BCFFA6145C811818EE,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_get_CurrentInputActionRulesProfile_mBC867FA63A1B9CBF5DE129EEEC79D8F1061CB8A2,
	MixedRealityInputSystem_t04A61305F900B914CB2747FC597F132D94FF2B97_CustomAttributesCacheGenerator_MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mFAB94A5E10A743D706D962E0444E04B1FB7665D4,
	Microsoft_MixedReality_Toolkit_Services_InputSystem_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
